%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%close all
clf
clear all

a=1; c=1; d=5; s=4; x0= -1.6; eps=0.01;
I=4; 
b=2.7; %square-wave burst
b=2.52; %plateau burst

b=2.5; I=4.4;

flor = @(t,x) [ x(2)-a*x(1).^3+b*x(1).^2-x(3)+I; c-d*x(1).^2-x(2); eps*(s*(x(1)-x0)-x(3)) ];
[t,x] = ode15s(flor,[0:0.1:5400], [0,0,0]);

figure(1),

subplot(2,2,2),plot(x(:,3),x(:,1),'b'),hold on, xlabel('z'),ylabel('x') 
subplot(2,2,4),plot3(x(:,3),x(:,2),x(:,1),'b'),hold on,xlabel('z'),ylabel('y'),zlabel('x')

[t1,x1] = ode15s(flor,[0:0.1:1000], [x(end,1),x(end,2),x(end,3)]);

figure(1),subplot(2,2,1),plot(t1+t(end,1),x1(:,1),'r'),hold on,xlabel('t'),ylabel('x')
          subplot(2,2,2),plot(x1(:,3),x1(:,1),'r'),hold on, xlabel('z'),ylabel('x') 
          subplot(2,2,3),plot(x1(:,3), x1(:,1),'r'),hold on,xlabel('z'),ylabel('y')
          subplot(2,2,4),plot3(x1(:,3),x1(:,2),x1(:,1),'r'),hold on,xlabel('z'),ylabel('y'),zlabel('x')
hold on
