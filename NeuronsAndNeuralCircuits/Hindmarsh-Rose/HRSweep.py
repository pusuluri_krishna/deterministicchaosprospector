import matplotlib

#matplotlib.use('Agg')
matplotlib.use('Qt4Agg')
import ctypes as ct
from pylab import *
import numpy as np
from matplotlib import colors
from numpy.ma import masked_array

N = 540000*20

# Adjust these bins to change precision depending on the system; Bins do not have to be equally sized; Have more bins in the region where more precision is required and less bins where its not; Using 10 bins for a start (9 bin boundaries); Best represented by char to compute PC, may also be represented by the digits 0 to 9 if number of bins < 10

#voltageBinBoundaries = np.array([-2., -1.5, -1., -0.5, 0., 0.5, 1., 1.5, 2], dtype='float64')
#voltageBinBoundaries = np.array([-2., -1., 0., 1., 2], dtype='float64')
#voltageBinBoundaries = np.array([-1.,0., 1.], dtype='float64')
#voltageBinBoundaries = np.array([-1.2], dtype='float64')
voltageBinBoundaries = np.array([], dtype='float64')

#intervalBinBoundaries = np.array([0.5, 1, 2, 3, 5, 10, 20, 40, 80], dtype='float64')
#intervalBinBoundaries = np.array([1, 10, 40], dtype='float64')
#intervalBinBoundaries = np.array([], dtype='float64')
intervalBinBoundaries = np.array([25], dtype='float64')

# arrays must be created with double values not integers (default) since that's how it is passed to C function later;
#  Otherwise giving errors and memory copied incorrectly

lib = ct.cdll.LoadLibrary('./HR_PCLZ.so')
lib.sweepHR.argtypes = [ct.POINTER(ct.c_double),
                        ct.POINTER(ct.c_double), ct.POINTER(ct.c_double),
                        ct.c_double, ct.c_double, ct.c_uint,
                        ct.c_double, ct.c_double, ct.c_uint,
                        ct.c_double, ct.c_uint, ct.c_uint,
                        ct.c_uint, ct.c_uint,
                        ct.c_uint, ct.c_uint]

def sweepHR(dt=0.1, N=N, stride=1,
            bmin=2.5, bmax=3.3,
            Imin=2.2, Imax=4.4,
            kneadingsStart=1000,
            kneadingsEnd=2000,
            sweepSize=100
            ):
    kneadingsWeightedSumSet = np.zeros(sweepSize * sweepSize, float)
    lib.sweepHR(kneadingsWeightedSumSet.ctypes.data_as(ct.POINTER(ct.c_double)),
                voltageBinBoundaries.ctypes.data_as(ct.POINTER(ct.c_double)),
                intervalBinBoundaries.ctypes.data_as(ct.POINTER(ct.c_double)),
                ct.c_double(bmin), ct.c_double(bmax), ct.c_uint(sweepSize),
                ct.c_double(Imin), ct.c_double(Imax), ct.c_uint(sweepSize),
                ct.c_double(dt), ct.c_uint(N), ct.c_uint(stride),
                ct.c_uint(len(voltageBinBoundaries)), ct.c_uint(len(intervalBinBoundaries)),
                ct.c_uint(kneadingsStart), ct.c_uint(kneadingsEnd))
    # imshow takes fortran style array input
    return np.reshape(kneadingsWeightedSumSet, (sweepSize, sweepSize), 'F')

#For LZ, use the returned value between 0 to 1 with a grey colormap
#For PC, return the circular hash of the period string, and use it with a custom colormap; If needed, we can also return, say, 6digits from the hash and use 2 each for R,G,B

print 'Running sweepHR'

colorMapLevels=2**8

blue = np.linspace(0.01, 1, colorMapLevels)
red = 1 - blue
min = 0.;
max = .8
green = min + np.random.random(colorMapLevels) * (max - min)
RGB = np.column_stack((red, green, blue))
customColorMap = colors.ListedColormap(RGB)

blue = np.linspace(0.8, 0, colorMapLevels)
red = blue
green = blue
RGB = np.column_stack((red, green, blue))
customGrayColorMap = colors.ListedColormap(RGB)


figSize = 5
fig = figure(figsize=(figSize, figSize))
outputdir = 'Output/HRSweep_PCLZ_'
matplotlib.rcParams['savefig.dpi'] = 600


fig.clear()
bmin=2.5; bmax=3.3; Imin=2.2; Imax=4.5; sweepSize = 100;
#bmin=2.5; bmax=2.50000000000001; Imin=4.4; Imax=4.40000000000001; sweepSize = 1;

sweepData = sweepHR(dt=0.1, N=N, stride=1, bmin=bmin, bmax=bmax, Imin=Imin, Imax=Imax,
           kneadingsStart=5000, kneadingsEnd=10000, sweepSize=sweepSize)

sweepData[sweepData == -1.1] = 0.

#PC hash can be any integer value (positive/negative), LZ lies between 0 and 1, -0.5 for insufficient N
sweepDataInsufficientN = masked_array(sweepData, sweepData != -0.5)
sweepDataComputed = masked_array(sweepData, sweepData == -0.5 )
sweepDataChaotic = masked_array(sweepDataComputed, (sweepData <= 0) | (sweepData >=1))
sweepDataPeriodic = masked_array(sweepDataComputed, (sweepData > 0) & (sweepData <1))


# to force common color scheme without scaling across images, use the vmin and vmax arguments of imshow(), and make sure they are the same for all your images
imshow(sweepDataPeriodic,
       extent=[bmin, bmax, Imin, Imax], aspect=(bmax - bmin) / (Imax - Imin),
       cmap=customColorMap,
       origin='lower',#vmin=-1., vmax=0.1
       ) #making vmax 0.1 instead of 0. to match the colormaps for short and long kneadings

imshow(sweepDataChaotic,
       extent=[bmin, bmax, Imin, Imax], aspect=(bmax - bmin) / (Imax - Imin),
       cmap=customGrayColorMap,
       origin='lower')

imshow(sweepDataInsufficientN,
       extent=[bmin, bmax, Imin, Imax], aspect=(bmax - bmin) / (Imax - Imin),
       cmap=colors.ListedColormap(['white']),
       origin='lower')

fig.savefig(outputdir + 'voltageBins_'+str(voltageBinBoundaries)+'_intervalBins_'+str(intervalBinBoundaries) + '_sweepSize_' + str(sweepSize) + ".jpg")

show()