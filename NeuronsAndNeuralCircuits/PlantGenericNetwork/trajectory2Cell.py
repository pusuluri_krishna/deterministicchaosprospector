from __future__ import division
#to get default literal division to be floating point arithmetic

import trajectoryGeneric
import numpy as np

networkSize = 2

#VInh = -80, VExc = 0, Iapp = 0., gh = 0.0004, Vhh = -54, gleak = 0.003, CaShift = -75., xShift =2.
# For cells Si3s
intrinsicParameterSet = np.array([-55, 0, 0., 0.0, -55, 0.003, -45., -2.,
                            -55, 0, 0., 0.0, -55, 0.003, -45., -2.], dtype='float64')

csElec = np.asarray(np.matrix('0 0; 0 0'))
#one-sided electrical connections

##Corresponding to each synapse there would be an S variable to be computed at each step of the integration!
csGInh = 0.1* np.asarray(np.matrix('0 1; 1 0 '));
csAlphaInh = 0.01*np.asarray(np.matrix('0 1; 1 0'));
csBetaInh = 0.01*np.asarray(np.matrix('0 1; 1 0'));

csGExc = np.asarray(np.matrix("0 0; 0 0"))
csAlphaExc = np.asarray(np.matrix("0 0 ; 0 0"))
csBetaExc = np.asarray(np.matrix("0 0; 0 0 "))

parameterSet = np.concatenate((intrinsicParameterSet,csElec,csGInh,csAlphaInh,csBetaInh,csGExc,csAlphaExc,csBetaExc), axis=None)
#intrinsicParameterSet.flatten()

#print repr(parameterSet) #repr() prints the numpy array with commas (to use in C code)
#V2 = y[0], Ca2 = y[1], h2 = y[2], n2 = y[3], x2 = y[4], y2 = y[5], s2 = y[6];
initialConditions = np.asarray([0, 1, 0., 0., 0.05, 0.,
                                       20, 1.4, 0., 0., 0.6, 0.], dtype='float64')
    #Initial condition should be set appropriately to separate tonic spiking and quiescent; For these values for Si3, cell 4 is either tonic/quiescent (which is what we are plotting), while cell 3 is always tonic spiking - therefore, plotting it cann't distinguish either case

N=2*int(600000/2.5); dt=2.5*.1;

#trajectoryGeneric.plotTrajectory(networkSize, parameterSet, initialConditions, N, dt)



#Images for symbolicNetwork paper

# g=0.1; xShift = -2.; for all as in HCO sweeps grid

dataSaved = True;

def plotTraj(g=0.1, xShift=-2., CaShift=-40, alpha=0.01, beta=0.01):
    parameterSet[7] = xShift; parameterSet[15] = xShift;
    parameterSet[6] = CaShift; parameterSet[14] = CaShift;
    parameterSet[21] = g; parameterSet[22] = g;
    parameterSet[25] = alpha; parameterSet[26] = alpha;
    parameterSet[29] = beta; parameterSet[30] = beta;
    return trajectoryGeneric.plotTrajectory(networkSize, parameterSet, initialConditions, N, dt, plotIt=False)

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import pickle
import gzip
from pylab import *

matplotlib.rcParams['savefig.dpi'] = 1200 #2*sweepSize/figSize #300
defaultFontSize = 11; largeFontSize = 14; indexFontSize = 16;
matplotlib.rcParams.update({'font.size': largeFontSize,
              'axes.labelsize': largeFontSize,
              'axes.titlesize': largeFontSize,
              'font.size': defaultFontSize,
              'legend.fontsize': largeFontSize,
              'xtick.labelsize': defaultFontSize,
              'ytick.labelsize': defaultFontSize})

#comment these two for rainbow text function to run properly, along with setting fig.dpi = 1200 described near rainbow text
matplotlib.rc('text', usetex=True)
matplotlib.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
matplotlib.rcParams['savefig.dpi'] = 600
bbox_props = dict(boxstyle="square,pad=0.2", fc="white", ec="k", lw=.5, alpha=0.)
bbox_props_label = dict(boxstyle="square,pad=0.01", fc="white", ec="k", lw=0	)
bbox_props_index = dict(boxstyle="square,pad=0.2", fc="white", ec="k", lw=1., alpha=0.)
bbox_props_fig = dict(boxstyle="square,pad=0.001", fc="white", ec="k", lw=0)

outputdir = "Output/"

#"""
# Image HCO symbolic

# CaShift=-45; alpha=0.014, beta=0.011 for 5 spike burst, symbols can be added;
# This can be separate image to explain symbols;

figSizeX=3; figSizeY=2; gridX = 1 ; gridY = 1;
fig = plt.figure(figsize=(figSizeX, figSizeY))
gs1 = gridspec.GridSpec(gridX, gridY)
gs1.update(wspace=0.03, hspace=0.03)  # set the spacing between axes.
ax = plt.subplot(gs1[0,0])

networkSize=2

if not dataSaved :
    pickleDataFile1 = gzip.open(outputdir+'PlantHCO_trajectorySymbols.gz','wb')
else :
    pickleDataFile1 = gzip.open(outputdir+'PlantHCO_trajectorySymbols.gz','rb')


if not dataSaved:
    plotData1 = plotTraj(CaShift=-45., alpha=0.014, beta=0.011)
    pickle.dump(plotData1, pickleDataFile1)
else:
    plotData1 = pickle.load(pickleDataFile1)

xlimMin=52500; xlimMax=72500;
tSize=int((xlimMax-xlimMin)/10)
ax.set_xlim(xlimMin, xlimMax)
ax.set_ylim(30-networkSize*100, 30+20)

plt.axis('on')
ax.set_xticks([])
ax.set_yticks([])

for i in range(networkSize):
    line, = ax.plot(dt * np.arange(0, N, 1.), plotData1[0:N, i] - i * 100, color='b', linestyle='-', linewidth=0.75)

ax.plot(arange(xlimMin+int(tSize),xlimMin+2*tSize,int(tSize/5)), -15.*np.ones(5,float),'k-')

xbarSize=tSize/1000
ax.annotate(r'$\boldsymbol{'+str('%d'%xbarSize if xbarSize == int(xbarSize) else '%s'%xbarSize)+'s}$', xy=(xlimMin+int(tSize)+int(tSize/4), -18), xytext=(xlimMin+int(tSize)+int(tSize/4), -18), bbox=bbox_props, color='k', va='top', fontSize=defaultFontSize)
ax.plot((xlimMin+int(tSize))*np.ones(4,float), arange(-12,28,10), 'k-')
ax.annotate(r'$\boldsymbol{40mV}$', xy=(xlimMin+int(tSize), -12), xytext=(xlimMin+int(tSize), -12), bbox=bbox_props, color='k', va='bottom', ha='right', rotation=90, fontsize=defaultFontSize)

ax.axhline(y=10.,color='purple', linestyle='dashed', linewidth=0.75)
ax.axhline(y=10.-100.,color='purple', linestyle='dashed', linewidth=0.75)
ax.axhline(y=-55,color='gray', linestyle='dashed', linewidth=0.75)

#ax.axvline(x=201222, color='gray', linestyle='dashed', linewidth=0.75)
#axRight.plot(201222, 27.976, 'o', color='purple', ms=4)

#cell1List = [53373, 60484, 61362, 62395, 63504, 64803, 71815, 72692]
cell1List = [53050, 60032, 60915, 61882, 62975, 64237, 71155, 72059]
for xval in cell1List:
    ax.plot(xval*np.ones(2,float), [27,50], color='gray', linestyle='dashed', linewidth=0.75)
    ax.plot(xval, 27, 'o', color='purple', ms=4)
    #ax.axvline(x=xval, color='gray', linestyle='dashed', linewidth=0.75)

ax.annotate(r'$\boldsymbol{b}$', xy=(59000, 35), xytext=(59000, 35), bbox=bbox_props, color='gray')
ax.annotate(r'$\boldsymbol{a}$', xy=(60200, 35), xytext=(60200, 35), bbox=bbox_props, color='gray')
ax.annotate(r'$\boldsymbol{b}$', xy=(70400, 35), xytext=(70400, 35), bbox=bbox_props, color='gray')
ax.annotate(r'$\boldsymbol{a}$', xy=(71300, 35), xytext=(71300, 35), bbox=bbox_props, color='gray')

#cell2List = [54677,  55583.5, 56662, 57770, 59090, 66178, 67056, 68091, 69199, 70461, 77367, 78302]
cell2List = [54417,  55300.5, 56289, 57403, 58665, 65625, 66487, 67475, 68569, 69788]
for xval in cell2List:
    ax.plot(xval*np.ones(2,float), [-72,-55], color='gray', linestyle='dashed', linewidth=0.75)
    ax.plot(xval, -72.3668, 'o', color='purple', ms=4)

ax.annotate(r'$\boldsymbol{d}$', xy=(53200, -69), xytext=(53200, -69), bbox=bbox_props, color='gray')
ax.annotate(r'$\boldsymbol{c}$', xy=(54650, -69), xytext=(54650, -69), bbox=bbox_props, color='gray')
ax.annotate(r'$\boldsymbol{d}$', xy=(64700, -69), xytext=(64700, -69), bbox=bbox_props, color='gray')
ax.annotate(r'$\boldsymbol{c}$', xy=(65848, -69), xytext=(65848, -69), bbox=bbox_props, color='gray')

plt.subplots_adjust(left=0.1, right=0.999, top=0.999, bottom=0.1)

fig.savefig('PlantHCO_trajectorySymbols.png', bbox_inches='tight')

plt.show()
#"""

"""
#Image HCO trajectory grid
# CaShift=-50; alpha =0.04; beta =0.01 for 21 spike burst
# CaShift=-45; alpha =0.01; beta =0.04 quiescence
# CaShift=-55; beta =0.04; alpha =0.01 for red tonic*2, alpha=0.02 for chaotic spiking


figSizeX=6; figSizeY=4; gridX = 2 ; gridY = 3;
fig = plt.figure(figsize=(figSizeX, figSizeY))
gs1 = gridspec.GridSpec(gridX, gridY)
gs1.update(wspace=0.03, hspace=0.03)  # set the spacing between axes.

networkSize=2

if not dataSaved :
    pickleDataFile1 = gzip.open(outputdir+'PlantHCO_AllTrajectories.gz','wb')
else :
    pickleDataFile1 = gzip.open(outputdir+'PlantHCO_AllTrajectories.gz','rb')



axTonic = plt.subplot(gs1[0,0])
axChaotic = plt.subplot(gs1[0,1])
axSuppress = plt.subplot(gs1[0,2])
axBurst = plt.subplot(gs1[1,0])
axBurst2 = plt.subplot(gs1[1,1])
axQuiet = plt.subplot(gs1[1,2])

#Bursting
if not dataSaved:
    plotData1 = plotTraj(CaShift=-50., alpha=0.04, beta=0.01)
    pickle.dump(plotData1, pickleDataFile1)
else:
    plotData1 = pickle.load(pickleDataFile1)

xlimMin=46000; xlimMax=106000;
tSize=int((xlimMax-xlimMin)/10)
axBurst.set_xlim(xlimMin, xlimMax)
axBurst.set_ylim(30-networkSize*100, 30+20)

plt.axis('on')
axBurst.set_xticks([])
axBurst.set_yticks([])

for i in range(networkSize):
    line, = axBurst.plot(dt * np.arange(0, N, 1.), plotData1[0:N, i] - i * 100, color='b', linestyle='-', linewidth=0.75)

axBurst.plot(arange(xlimMin+int(tSize),xlimMin+int(tSize)+int(tSize),int(tSize/5)), -15.*np.ones(5,float),'k-')
xbarSize=tSize/1000
axBurst.annotate(r'$\boldsymbol{'+str('%d'%xbarSize if xbarSize == int(xbarSize) else '%s'%xbarSize)+'s}$', xy=(xlimMin+int(tSize), -18), xytext=(xlimMin+int(tSize), -18), bbox=bbox_props, color='k', va='top', fontSize=defaultFontSize)
axBurst.plot((xlimMin+int(tSize))*np.ones(4,float), arange(-12,28,10), 'k-')
axBurst.annotate(r'$\boldsymbol{40mV}$', xy=(xlimMin+int(tSize), -12), xytext=(xlimMin+int(tSize), -12), bbox=bbox_props, color='k', va='bottom', ha='right', rotation=90, fontsize=defaultFontSize)

axBurst.text(0.98, 0.98, r'$\boldsymbol{D}$', ha='right', va='top', bbox=bbox_props_index,
                   transform=axBurst.transAxes, fontsize=defaultFontSize)

#Bursting2
if not dataSaved:
    plotData1 = plotTraj(CaShift=-50., alpha=0.04, beta=0.03)
    pickle.dump(plotData1, pickleDataFile1)
else:
    plotData1 = pickle.load(pickleDataFile1)

xlimMin=50000; xlimMax=110000;
tSize=int((xlimMax-xlimMin)/10)
axBurst2.set_xlim(xlimMin, xlimMax)
axBurst2.set_ylim(30-networkSize*100, 30+20)

plt.axis('on')
axBurst2.set_xticks([])
axBurst2.set_yticks([])

for i in range(networkSize):
    line, = axBurst2.plot(dt * np.arange(0, N, 1.), plotData1[0:N, i] - i * 100, color='b', linestyle='-', linewidth=0.75)

#axBurst2.plot(arange(xlimMin+int(tSize/2)+int(tSize/8),xlimMin+int(tSize/2)+int(tSize/8)+int(tSize),int(tSize/5)), -15.*np.ones(5,float),'k-')
#xbarSize=tSize/1000
#axBurst2.annotate(r'$\boldsymbol{'+str('%d'%xbarSize if xbarSize == int(xbarSize) else '%s'%xbarSize)+'s}$', xy=(xlimMin+int(tSize/2)+int(tSize/8)+int(tSize/8), -18), xytext=(xlimMin+int(tSize/2)+int(tSize/8)+int(tSize/8), -18), bbox=bbox_props, color='k', va='top', fontSize=defaultFontSize)
#axBurst2.plot((xlimMin+int(tSize/2)+int(tSize/8))*np.ones(4,float), arange(-12,28,10), 'k-')
#axBurst2.annotate(r'$\boldsymbol{40mV}$', xy=(xlimMin+int(tSize/2)+int(tSize/8), -12), xytext=(xlimMin+int(tSize/2)+int(tSize/8), -12), bbox=bbox_props, color='k', va='bottom', ha='right', rotation=90, fontsize=defaultFontSize)

axBurst2.text(0.98, 0.98, r'$\boldsymbol{E}$', ha='right', va='top', bbox=bbox_props_index,
                   transform=axBurst2.transAxes, fontsize=defaultFontSize)

#Quiescent
if not dataSaved:
    plotData1 = plotTraj(CaShift=-45., alpha=0.01, beta=0.04) # for quiescent
    pickle.dump(plotData1, pickleDataFile1)
else:
    plotData1 = pickle.load(pickleDataFile1)

xlimMin=00000; xlimMax=60000; # for quiescent
tSize=int((xlimMax-xlimMin)/10)
axQuiet.set_xlim(xlimMin, xlimMax)
axQuiet.set_ylim(30-networkSize*100, 30+20)

plt.axis('on')
axQuiet.set_xticks([])
axQuiet.set_yticks([])

for i in range(networkSize):
    line, = axQuiet.plot(dt * np.arange(0, N, 1.), plotData1[0:N, i] - i * 100, color='b', linestyle='-', linewidth=0.75)

#axQuiet.plot(arange(xlimMin+int(tSize/2)+int(tSize/8),xlimMin+int(tSize/2)+int(tSize/8)+int(tSize),int(tSize/5)), -15.*np.ones(5,float),'k-')
#xbarSize=tSize/1000
#axQuiet.annotate(r'$\boldsymbol{'+str('%d'%xbarSize if xbarSize == int(xbarSize) else '%s'%xbarSize)+'s}$', xy=(xlimMin+int(tSize/2)+int(tSize/8)+int(tSize/8), -18), xytext=(xlimMin+int(tSize/2)+int(tSize/8)+int(tSize/8), -18), bbox=bbox_props, color='k', va='top', fontSize=defaultFontSize)
#axQuiet.plot((xlimMin+int(tSize/2)+int(tSize/8))*np.ones(4,float), arange(-12,28,10), 'k-')
#axQuiet.annotate(r'$\boldsymbol{40mV}$', xy=(xlimMin+int(tSize/2)+int(tSize/8), -12), xytext=(xlimMin+int(tSize/2)+int(tSize/8), -12), bbox=bbox_props, color='k', va='bottom', ha='right', rotation=90, fontsize=defaultFontSize)

axQuiet.text(0.98, 0.98, r'$\boldsymbol{F}$', ha='right', va='top', bbox=bbox_props_index,
                   transform=axQuiet.transAxes, fontsize=defaultFontSize)

#Suppression
if not dataSaved:
    plotData1 = plotTraj(CaShift=-55., alpha=0.02, beta=0.01)
    pickle.dump(plotData1, pickleDataFile1)
else:
    plotData1 = pickle.load(pickleDataFile1)

xlimMin=30000; xlimMax=90000;
tSize=int((xlimMax-xlimMin)/10)
axSuppress.set_xlim(xlimMin, xlimMax)
axSuppress.set_ylim(30-networkSize*100, 30+20)

plt.axis('on')
axSuppress.set_xticks([])
axSuppress.set_yticks([])

for i in range(networkSize):
    line, = axSuppress.plot(dt * np.arange(0, N, 1.), plotData1[0:N, i] - i * 100, color='b', linestyle='-', linewidth=0.75)

#axSuppress.plot(arange(xlimMin+int(tSize/2)+int(tSize/8),xlimMin+int(tSize/2)+int(tSize/8)+int(tSize),int(tSize/5)), -15.*np.ones(5,float),'k-')
#xbarSize=tSize/1000
#axSuppress.annotate(r'$\boldsymbol{'+str('%d'%xbarSize if xbarSize == int(xbarSize) else '%s'%xbarSize)+'s}$', xy=(xlimMin+int(tSize/2)+int(tSize/8)+int(tSize/8), -18), xytext=(xlimMin+int(tSize/2)+int(tSize/8)+int(tSize/8), -18), bbox=bbox_props, color='k', va='top', fontSize=defaultFontSize)
#axSuppress.plot((xlimMin+int(tSize/2)+int(tSize/8))*np.ones(4,float), arange(-12,28,10), 'k-')
#axSuppress.annotate(r'$\boldsymbol{40mV}$', xy=(xlimMin+int(tSize/2)+int(tSize/8), -12), xytext=(xlimMin+int(tSize/2)+int(tSize/8), -12), bbox=bbox_props, color='k', va='bottom', ha='right', rotation=90, fontsize=defaultFontSize)

axSuppress.text(0.98, 0.98, r'$\boldsymbol{C}$', ha='right', va='top', bbox=bbox_props_index,
                   transform=axSuppress.transAxes, fontsize=defaultFontSize)

#Tonic
if not dataSaved:
    plotData1 = plotTraj(CaShift=-55., alpha=0.01, beta=0.04)
    pickle.dump(plotData1, pickleDataFile1)
else:
    plotData1 = pickle.load(pickleDataFile1)

xlimMin=30000; xlimMax=90000;
tSize=int((xlimMax-xlimMin)/10)
axTonic.set_xlim(xlimMin, xlimMax)
axTonic.set_ylim(30-networkSize*100, 30+20)

plt.axis('on')
axTonic.set_xticks([])
axTonic.set_yticks([])

for i in range(networkSize):
    line, = axTonic.plot(dt * np.arange(0, N, 1.), plotData1[0:N, i] - i * 100, color='b', linestyle='-', linewidth=0.75)

#axTonic.plot(arange(xlimMin+int(tSize/2)+int(tSize/8),xlimMin+int(tSize/2)+int(tSize/8)+int(tSize),int(tSize/5)), -15.*np.ones(5,float),'k-')
#xbarSize=tSize/1000
#axTonic.annotate(r'$\boldsymbol{'+str('%d'%xbarSize if xbarSize == int(xbarSize) else '%s'%xbarSize)+'s}$', xy=(xlimMin+int(tSize/2)+int(tSize/8)+int(tSize/8), -18), xytext=(xlimMin+int(tSize/2)+int(tSize/8)+int(tSize/8), -18), bbox=bbox_props, color='k', va='top', fontSize=defaultFontSize)
#axTonic.plot((xlimMin+int(tSize/2)+int(tSize/8))*np.ones(4,float), arange(-12,28,10), 'k-')
#axTonic.annotate(r'$\boldsymbol{40mV}$', xy=(xlimMin+int(tSize/2)+int(tSize/8), -12), xytext=(xlimMin+int(tSize/2)+int(tSize/8), -12), bbox=bbox_props, color='k', va='bottom', ha='right', rotation=90, fontsize=defaultFontSize)

axTonic.text(0.98, 0.98, r'$\boldsymbol{A}$', ha='right', va='top', bbox=bbox_props_index,
                   transform=axTonic.transAxes, fontsize=defaultFontSize)

#Chaotic
if not dataSaved:
    plotData1 = plotTraj(CaShift=-55., alpha=0.035, beta=0.04)
    pickle.dump(plotData1, pickleDataFile1)
else:
    plotData1 = pickle.load(pickleDataFile1)

xlimMin=30000; xlimMax=90000;
tSize=int((xlimMax-xlimMin)/10)
axChaotic.set_xlim(xlimMin, xlimMax)
axChaotic.set_ylim(30-networkSize*100, 30+20)

plt.axis('on')
axChaotic.set_xticks([])
axChaotic.set_yticks([])

for i in range(networkSize):
    line, = axChaotic.plot(dt * np.arange(0, N, 1.), plotData1[0:N, i] - i * 100, color='b', linestyle='-', linewidth=0.75)

#axChaotic.plot(arange(xlimMin+int(tSize/2)+int(tSize/8),xlimMin+int(tSize/2)+int(tSize/8)+int(tSize),int(tSize/5)), -15.*np.ones(5,float),'k-')
#xbarSize=tSize/1000
#axChaotic.annotate(r'$\boldsymbol{'+str('%d'%xbarSize if xbarSize == int(xbarSize) else '%s'%xbarSize)+'s}$', xy=(xlimMin+int(tSize/2)+int(tSize/8)+int(tSize/8), -18), xytext=(xlimMin+int(tSize/2)+int(tSize/8)+int(tSize/8), -18), bbox=bbox_props, color='k', va='top', fontSize=defaultFontSize)
#axChaotic.plot((xlimMin+int(tSize/2)+int(tSize/8))*np.ones(4,float), arange(-12,28,10), 'k-')
#axChaotic.annotate(r'$\boldsymbol{40mV}$', xy=(xlimMin+int(tSize/2)+int(tSize/8), -12), xytext=(xlimMin+int(tSize/2)+int(tSize/8), -12), bbox=bbox_props, color='k', va='bottom', ha='right', rotation=90, fontsize=defaultFontSize)

axChaotic.text(0.98, 0.98, r'$\boldsymbol{B}$', ha='right', va='top', bbox=bbox_props_index,
                   transform=axChaotic.transAxes, fontsize=defaultFontSize)

### whole image
plt.subplots_adjust(left=0.1, right=0.999, top=0.999, bottom=0.1)

fig.savefig('PlantHCO_AllTrajectories.png', bbox_inches='tight')

"""