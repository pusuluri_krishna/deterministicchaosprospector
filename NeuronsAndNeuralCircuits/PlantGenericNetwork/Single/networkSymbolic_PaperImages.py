#All labels adjusted for Smaug workstation at lab

from __future__ import division

import matplotlib
matplotlib.use('TkAgg')
#matplotlib.use('Qt4Agg')
import ctypes as ct
from pylab import *
import numpy as np
from matplotlib import colors
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from mpl_toolkits.axes_grid.inset_locator import inset_axes
from mpl_toolkits.axes_grid.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid.inset_locator import mark_inset
import matplotlib.patches as patches
import pickle
import gzip
from scipy.integrate import ode
from matplotlib import transforms
import matplotlib.image as mpimg
from numpy.ma import masked_array

colorMapLevels = 2**8; dataSaved = True; sweepSize = 1000; outputdir = 'Output/'

blue = np.linspace(0.01, 1, colorMapLevels)
red = 1 - blue
min = 0.;
max = .8
green = min + np.random.random(colorMapLevels) * (max - min)
RGB = np.column_stack((red, green, blue))
customColorMap = colors.ListedColormap(RGB)

blue = np.linspace(0.8, 0, colorMapLevels)
red = blue
green = blue
RGB = np.column_stack((red, green, blue))
customGrayColorMap = colors.ListedColormap(RGB)

figSize = 5

matplotlib.rcParams['savefig.dpi'] = 1200 #2*sweepSize/figSize #300
defaultFontSize = 11; largeFontSize = 14; indexFontSize = 16;
matplotlib.rcParams.update({'font.size': largeFontSize,
              'axes.labelsize': largeFontSize,
              'axes.titlesize': largeFontSize,
              'font.size': defaultFontSize,
              'legend.fontsize': largeFontSize,
              'xtick.labelsize': defaultFontSize,
              'ytick.labelsize': defaultFontSize})

#comment these two for rainbow text function to run properly, along with setting fig.dpi = 1200 described near rainbow text
matplotlib.rc('text', usetex=True)
matplotlib.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]

sweepData = [[0.1,0.2],[0.3,0.4]]                           #### For testing purposes

bbox_props = dict(boxstyle="square,pad=0.2", fc="white", ec="k", lw=.5, alpha=0.)
arrowprops = dict(arrowstyle="->", connectionstyle="arc3", facecolor='white', lw=1.,
                  ec="white")  # ,shrink=0.05, width=defaultFontSize/10.)
arrowpropsblack = dict(arrowstyle="->", connectionstyle="arc3", facecolor='k', lw=1.,
                  ec="k")  # ,shrink=0.05, width=defaultFontSize/10.)
bbox_props_fig = dict(boxstyle="square,pad=0.001", fc="white", ec="k", lw=0)
bbox_props_index = dict(boxstyle="square,pad=0.2", fc="white", ec="k", lw=1., alpha=0.)
bbox_props_label = dict(boxstyle="square,pad=0.01", fc="white", ec="k", lw=0	)


def processSweepData(axis, sweepData, aMin, aMax, bMin, bMax):

    # PC hash can be any integer value (positive/negative), LZ lies between 0 and 1, -0.5 for insufficient N
    sweepDataInsufficientN = masked_array(sweepData, sweepData != -0.5)
    sweepDataComputed = masked_array(sweepData, sweepData == -0.5)
    sweepDataChaotic = masked_array(sweepDataComputed, (sweepData <= 0) | (sweepData >= 1))
    sweepDataPeriodic = masked_array(sweepDataComputed, (sweepData > 0) & (sweepData < 1))

    # to force common color scheme without scaling across images, use the vmin and vmax arguments of imshow(), and make sure they are the same for all your images
    axis.imshow(sweepDataPeriodic,
           extent=[aMin, aMax, bMin, bMax],
           aspect=(aMax - aMin) / (bMax - bMin),
           cmap=customColorMap,
           origin='lower',  # vmin=-1., vmax=0.1
           )  # making vmax 0.1 instead of 0. to match the colormaps for short and long kneadings

    axis.imshow(sweepDataChaotic,
           extent=[aMin, aMax, bMin, bMax],
           aspect=(aMax - aMin) / (bMax - bMin),
           cmap=customGrayColorMap,
           origin='lower')

    axis.imshow(sweepDataInsufficientN,
           extent=[aMin, aMax, bMin, bMax],
           aspect=(aMax - aMin) / (bMax - bMin),
           cmap=colors.ListedColormap(['white']),
           origin='lower')

"""

################################ Image1: Plant sweeps ########################################################################


####### Whole Image
lib = ct.cdll.LoadLibrary('./sweep_PCLZ.so')
lib.sweep.argtypes = [ct.POINTER(ct.c_double),
                        ct.POINTER(ct.c_double), ct.POINTER(ct.c_double),
                        ct.c_double, ct.c_double, ct.c_uint,
                        ct.c_double, ct.c_double, ct.c_uint,
                        ct.c_double, ct.c_double, ct.c_double, ct.c_double,
                        ct.c_double, ct.c_uint, ct.c_uint,
                        ct.c_uint, ct.c_uint,
                        ct.c_uint, ct.c_uint]

def sweep(dt=1, N=300000, stride=1,
            CaShiftMin=-10., CaShiftMax=5.,
            xShiftMin=-2.5, xShiftMax=-1.5,
            voltageBinBoundaries=np.array([], dtype='float64'),
            intervalBinBoundaries=np.array([], dtype='float64'),
            Iapp = 0., gh = 0., Vhh = -55, gleak = 0.003,
            kneadingsStart=100,
            kneadingsEnd=200,
            sweepSize=100
            ):
    kneadingsWeightedSumSet = np.zeros(sweepSize * sweepSize, float)
    lib.sweep(kneadingsWeightedSumSet.ctypes.data_as(ct.POINTER(ct.c_double)),
              voltageBinBoundaries.ctypes.data_as(ct.POINTER(ct.c_double)),
              intervalBinBoundaries.ctypes.data_as(ct.POINTER(ct.c_double)),
              ct.c_double(CaShiftMin), ct.c_double(CaShiftMax), ct.c_uint(sweepSize),
              ct.c_double(xShiftMin), ct.c_double(xShiftMax), ct.c_uint(sweepSize),
              ct.c_double(Iapp), ct.c_double(gh), ct.c_double(Vhh), ct.c_double(gleak),
              ct.c_double(dt), ct.c_uint(N), ct.c_uint(stride),
              ct.c_uint(len(voltageBinBoundaries)), ct.c_uint(len(intervalBinBoundaries)),
              ct.c_uint(kneadingsStart), ct.c_uint(kneadingsEnd))
    # imshow takes fortran style array input
    return np.reshape(kneadingsWeightedSumSet, (sweepSize, sweepSize), 'F')

print('Generating Image1')

if not dataSaved :
    pickleDataFile1 = gzip.open(outputdir+'plantSweeps.gz','wb')
else :
    pickleDataFile1 = gzip.open(outputdir+'plantSweeps.gz','rb')

matplotlib.rcParams['savefig.dpi'] = 600

figSizeX=6; figSizeY=4; gridX = 2 ; gridY = 3;

fig = plt.figure(figsize=(figSizeX, figSizeY))
gs1 = gridspec.GridSpec(gridX, gridY)
gs1.update(wspace=0.03, hspace=0.03)  # set the spacing between axes.

axLeft = plt.subplot(gs1[0:2,0:2])
axRightTop = plt.subplot(gs1[0,2])
axRightBottom = plt.subplot(gs1[1,2])

######### Full sweep
CaShiftMin = -140; CaShiftMax = 100; xShiftMin = -10; xShiftMax = 70;
N = 300000*10; dt=1;
voltageBinBoundaries = np.array([-60., -40., 10.], dtype='float64');
intervalBinBoundaries = np.array([], dtype='float64');

plt.axis('on')
axLeft.set_xticks([])
axLeft.set_yticks([])
axLeft.text( 0., -0.01, CaShiftMin, ha='left', va='top', bbox=bbox_props_label, transform=axLeft.transAxes,
             fontsize=defaultFontSize)
axLeft.text( 1., -0.01, CaShiftMax, ha='right', va='top', bbox=bbox_props_label, transform=axLeft.transAxes,
             fontsize=defaultFontSize)
axLeft.text(-0.01, 0, xShiftMin, ha='right', va='bottom', rotation=90, bbox=bbox_props_label,
            transform=axLeft.transAxes, fontsize=defaultFontSize)
axLeft.text(-0.01, 1., xShiftMax, ha='right', va='top', rotation=90, bbox=bbox_props_label,
            transform=axLeft.transAxes, fontsize=defaultFontSize)

print('Plant full sweep')
if not dataSaved:
    sweepData = sweep(dt=dt, N=N, stride=1, CaShiftMin=CaShiftMin, CaShiftMax=CaShiftMax, xShiftMin=xShiftMin,
                      xShiftMax=xShiftMax, kneadingsStart=1000, kneadingsEnd=2000,
                      sweepSize=sweepSize, voltageBinBoundaries=voltageBinBoundaries, intervalBinBoundaries=intervalBinBoundaries)
    pickle.dump(sweepData, pickleDataFile1)
else:
    sweepData = pickle.load(pickleDataFile1)

processSweepData(axLeft, sweepData, CaShiftMin, CaShiftMax, xShiftMin, xShiftMax);

axLeft.autoscale(False)
axLeft.set_adjustable('box')

axLeft.text(0.01, .99, r'$\boldsymbol{A}$', ha='left', va='top', transform=axLeft.transAxes, color='w', bbox=bbox_props_index)

axLeft.annotate(r'$\boldsymbol{bursting~with}$''\n'r'$\boldsymbol{spike~adding}$', xy=(-35., 44.), xytext=(14., 51.), arrowprops=arrowprops,
             bbox=bbox_props, color='white' )
axLeft.annotate(r'$\boldsymbol{quiescent}$', xy=(15.,20.), xytext=(15.,20.), bbox=bbox_props,
                color='k' )
axLeft.annotate(r'$\boldsymbol{tonic~spiking}$', xy=(-130.,-5.), xytext=(-130.,-5.), bbox=bbox_props,
                color='w' )
axLeft.annotate(r'$\boldsymbol{B}$', xy=(-119.,5.), xytext=(-119.,50.), bbox=bbox_props,
                color='w' )
axLeft.annotate(r'$\boldsymbol{C}$', xy=(-22.,-5), xytext=(-22.,-5), bbox=bbox_props,
                color='k' )

#Parameters sampled for HCO sweeps
axLeft.plot(-40., -2., 'o', color='k', ms=2)
axLeft.plot(-45., -2., 'o', color='k', ms=2)
axLeft.plot(-50., -2., 'o', color='k', ms=2)
axLeft.plot(-55., -2., 'o', color='k', ms=2)

######### Top chaos zoom
CaShiftMin = -120; CaShiftMax = -70; xShiftMin = 44; xShiftMax = 53;
N = 300000*10; dt=1;
voltageBinBoundaries = np.array([], dtype='float64');
intervalBinBoundaries = np.array([100], dtype='float64');

plt.axis('on')
axRightTop.set_xticks([])
axRightTop.set_yticks([])
axRightTop.text( 0., .02, CaShiftMin, ha='left', va='bottom', transform=axRightTop.transAxes,
                 fontsize=defaultFontSize, color='w')
axRightTop.text( .99, .02, CaShiftMax, ha='right', va='bottom', transform=axRightTop.transAxes,
                 fontsize=defaultFontSize, color='w')
axRightTop.text(1.02, 0, xShiftMin, ha='left', va='bottom', rotation=90, bbox=bbox_props_label,
                transform=axRightTop.transAxes, fontsize=defaultFontSize)
axRightTop.text(1.02, 1., xShiftMax, ha='left', va='top', rotation=90, bbox=bbox_props_label,
                transform=axRightTop.transAxes, fontsize=defaultFontSize)

print('Plant top chaos sweep')
if not dataSaved:
    sweepData = sweep(dt=dt, N=N, stride=1, CaShiftMin=CaShiftMin, CaShiftMax=CaShiftMax, xShiftMin=xShiftMin,
                      xShiftMax=xShiftMax, kneadingsStart=1000, kneadingsEnd=2000,
                      sweepSize=sweepSize, voltageBinBoundaries=voltageBinBoundaries, intervalBinBoundaries=intervalBinBoundaries)
    pickle.dump(sweepData, pickleDataFile1)
else:
    sweepData = pickle.load(pickleDataFile1)

processSweepData(axRightTop, sweepData, CaShiftMin, CaShiftMax, xShiftMin, xShiftMax);

axRightTop.autoscale(False)
axRightTop.set_adjustable('box')

axLeft.add_patch(patches.Rectangle((CaShiftMin, xShiftMin), CaShiftMax-CaShiftMin, xShiftMax-xShiftMin, fill=False,
                                   linewidth=1., color='w'))
axRightTop.text(.98, .98, r'$\boldsymbol{B}$', ha='right', va='top', transform=axRightTop.transAxes, color='w', bbox=bbox_props_index)

axRightTop.annotate(r'$\boldsymbol{chaos}$', xy=(-96., 51.3), xytext=(-105., 52.2), arrowprops=arrowprops,
             bbox=bbox_props, color='white', ha='right' )


######### Bottom chaos zoom
CaShiftMin = -22; CaShiftMax = -10; xShiftMin = -1.87; xShiftMax = -1.5;
N = 300000*10; dt=1;
voltageBinBoundaries = np.array([], dtype='float64');
intervalBinBoundaries = np.array([100], dtype='float64');

plt.axis('on')
axRightBottom.set_xticks([])
axRightBottom.set_yticks([])
axRightBottom.text( 0., -0.02, CaShiftMin, ha='left', va='top', bbox=bbox_props_label, transform=axRightBottom.transAxes, fontsize=defaultFontSize)
axRightBottom.text( .99, -0.02, CaShiftMax, ha='right', va='top', bbox=bbox_props_label,
                    transform=axRightBottom.transAxes, fontsize=defaultFontSize)
axRightBottom.text(1.02, 0, xShiftMin, ha='left', va='bottom', rotation=90, bbox=bbox_props_label,
                   transform=axRightBottom.transAxes, fontsize=defaultFontSize)
axRightBottom.text(1.02, 1., xShiftMax, ha='left', va='top', rotation=90, bbox=bbox_props_label,
                   transform=axRightBottom.transAxes, fontsize=defaultFontSize)

print('Plant bottom chaos sweep')
if not dataSaved:
    sweepData = sweep(dt=dt, N=N, stride=1, CaShiftMin=CaShiftMin, CaShiftMax=CaShiftMax, xShiftMin=xShiftMin,
                      xShiftMax=xShiftMax, kneadingsStart=1000, kneadingsEnd=2000,
                      sweepSize=sweepSize, voltageBinBoundaries=voltageBinBoundaries, intervalBinBoundaries=intervalBinBoundaries)
    pickle.dump(sweepData, pickleDataFile1)
else:
    sweepData = pickle.load(pickleDataFile1)

processSweepData(axRightBottom, sweepData, CaShiftMin, CaShiftMax, xShiftMin, xShiftMax);

axRightBottom.autoscale(False)
axRightBottom.set_adjustable('box')

axLeft.add_patch(patches.Rectangle((CaShiftMin, xShiftMin), CaShiftMax-CaShiftMin, xShiftMax-xShiftMin, fill=False,
                                   linewidth=3., color='k'))
axRightBottom.text(.98, .98, r'$\boldsymbol{C}$', ha='right', va='top', transform=axRightBottom.transAxes, color='w', bbox=bbox_props_index)
axRightBottom.annotate(r'$\boldsymbol{chaos}$', xy=(-16., -1.72), xytext=(-18., -1.8), arrowprops=arrowpropsblack,
             bbox=bbox_props, color='black', ha='right' )

######## Whole image
xlabelX=0.38; xlabel1X = 0.84; xlabelY=0.055; ylabelX=0.065; ylabelY=0.5;
xlabel = r'$\mathrm{Ca_{shift}}$';
ylabel = r'$\mathrm{x_{shift}}$'
fig.text(xlabelX, xlabelY, xlabel, ha='center', bbox=bbox_props_fig, color='darkslategray', fontsize=largeFontSize,
         weight='roman')
fig.text(ylabelX, ylabelY, ylabel, va='center', rotation='vertical', bbox=bbox_props_fig, color='darkslategray',
         fontsize=largeFontSize, weight='roman')

plt.subplots_adjust(left=0.1, right=0.999, top=0.999, bottom=0.1)
fig.savefig(outputdir+'Image1.png', bbox_inches='tight')

pickleDataFile1.close()

################################ End Image1: Plant sweeps ########################################################################

"""

"""
############################# Image0: Plant trajectory: symbols ####################################################################

libPlantTrajectory = ct.cdll.LoadLibrary('./trajectory.so')

N_EQ=6
libPlantTrajectory.integrator_rk4.argtypes = [ct.POINTER(ct.c_double), ct.POINTER(ct.c_double),ct.POINTER(ct.c_double),
                          ct.c_double,ct.c_uint,ct.c_uint]
def integrator_rk4 (y_initial = np.asarray([-39.99, 0.9, 0.5, 0.2, 0.7, 0.2]),
                    dt=0.001,
                    N=30000,
                    stride = 1
                ):
    output = np.zeros(N*N_EQ, float)
    params = np.asarray([-8.61873, -1.94959, 0., 0., -55., 0.003], float)  # shift_Ca2, shift_x2, Iapp, gh, Vhh, gleak; #chaotic second loop
    libPlantTrajectory.integrator_rk4(y_initial.ctypes.data_as(ct.POINTER(ct.c_double)), output.ctypes.data_as(ct.POINTER(ct.c_double)), params.ctypes.data_as(ct.POINTER(ct.c_double)),
                       ct.c_double(dt), ct.c_uint(N), ct.c_uint(stride))
    #imshow takes fortran style array input
    return np.reshape(output, (N,N_EQ), 'C')

print('Generating Image0')

if not dataSaved :
    pickleDataFile1 = gzip.open(outputdir+'plantTrajectorySymbols.gz','wb')
else :
    pickleDataFile1 = gzip.open(outputdir+'plantTrajectorySymbols.gz','rb')

matplotlib.rcParams['savefig.dpi'] = 600

figSizeX=6; figSizeY=2; gridX = 1 ; gridY = 2;

fig = plt.figure(figsize=(figSizeX, figSizeY))
gs1 = gridspec.GridSpec(gridX, gridY)
gs1.update(wspace=0.03, hspace=0.03)  # set the spacing between axes.

axLeft = plt.subplot(gs1[0,0])
axRight = plt.subplot(gs1[0,1])

N=430000; dt=1


### Chaotic trajectory
plt.axis('on')
axLeft.set_xticks([])
axLeft.set_yticks([])

if not dataSaved:
    plotData = integrator_rk4(N=N, dt=dt)
    pickle.dump(plotData, pickleDataFile1)
else:
    plotData = pickle.load(pickleDataFile1)

axLeft.set_xlim(310000, 430000)

axLeft.set_ylim(-75, 50)

axLeft.axhline(y=-57.,color='purple', linestyle='dashed', linewidth=0.75)
axLeft.axhline(y=-40.,color='purple', linestyle='dashed', linewidth=0.75)
axLeft.axhline(y=10.,color='purple', linestyle='dashed', linewidth=0.75)

line, = axLeft.plot(dt*arange(0,N,1.), plotData[0:N,0],'b-', linewidth=0.75)

axLeft.autoscale(False)
axLeft.set_adjustable('box')

axLeft.text(0.02, .98, r'$\boldsymbol{A}$', ha='left', va='top', bbox=bbox_props_index,
                   transform=axLeft.transAxes, fontsize=defaultFontSize)

axLeft.text( -0.02, 0.14, -60, ha='right', va='center', bbox=bbox_props_label, transform=axLeft.transAxes,
                    fontsize=defaultFontSize)
axLeft.text( -0.02, 0.28, -40, ha='right', va='center', bbox=bbox_props_label, transform=axLeft.transAxes,
                    fontsize=defaultFontSize)
axLeft.text( -0.02, 0.69, 10, ha='right', va='center', bbox=bbox_props_label, transform=axLeft.transAxes,
                    fontsize=defaultFontSize)

line, = axLeft.plot(dt*arange(418000,428000,2000), -63*np.ones(5,float),'b-')
axLeft.annotate(r'$\boldsymbol{20s}$', xy=(418000, -73.), xytext=(418000, -73.), bbox=bbox_props,
                color='b')

axLeft.annotate(r'$\boldsymbol{a}$', xy=(340000,-67.), xytext=(340000,-67.), bbox=bbox_props,
                color='purple')
axLeft.annotate(r'$\boldsymbol{b}$', xy=(340000,-48.), xytext=(340000,-48.), bbox=bbox_props,
                color='purple')
axLeft.annotate(r'$\boldsymbol{c}$', xy=(340000,-14.), xytext=(340000,-14.), bbox=bbox_props,
                color='purple')
axLeft.annotate(r'$\boldsymbol{d}$', xy=(340000, 24.), xytext=(340000, 24.), bbox=bbox_props,
                color='purple')


############ Symbols
plt.axis('on')
axRight.set_xticks([])
axRight.set_yticks([])
axRight.set_xlim(201000, 201900)
axRight.set_ylim(-75, 50)
#axRight.set_xlabel('time', fontsize=20)
#axRight.set_ylabel('V', fontsize=20)

axRight.autoscale(False)
axRight.set_adjustable('box')

line, = axRight.plot(dt*arange(201750,201810,12), -63.*np.ones(5,float),'b-')
axRight.annotate(r'$\boldsymbol{60ms}$', xy=(201750, -73.), xytext=(201750, -73.), bbox=bbox_props,
                color='b')

axRight.axvline(x=201222, color='gray', linestyle='dashed', linewidth=0.75)
axRight.axvline(x=201291, color='gray', linestyle='dashed', linewidth=0.75)
axRight.axvline(x=201647, color='gray', linestyle='dashed', linewidth=0.75)
axRight.axvline(x=201716, color='gray', linestyle='dashed', linewidth=0.75)

axRight.axhline(y=-40.,color='purple', linestyle='dashed', linewidth=0.75)

line, = axRight.plot(dt*arange(0,N,1.), plotData[0:N,0],'b-', linewidth=0.75)

axRight.plot(201222, 27.976, 'o', color='purple', ms=4)
axRight.plot(201291, -51.0472, 'o', color='purple', ms=4)
axRight.plot(201647, 27.976, 'o', color='purple', ms=4)
axRight.plot(201716, -51.0472, 'o', color='purple', ms=4)

axRight.text(0.02, 0.98, r'$\boldsymbol{B}$', ha='left', va='top', bbox=bbox_props_index,
                   transform=axRight.transAxes, fontsize=defaultFontSize)

axRight.annotate(r'$\boldsymbol{d}$', xy=(201170, 27.976), xytext=(201170, 27.976), bbox=bbox_props,
                color='purple')
axRight.annotate(r'$\boldsymbol{b}$', xy=(201321, -57), xytext=(201321, -57), bbox=bbox_props,
                color='purple')
axRight.annotate(r'$\boldsymbol{d}$', xy=(201598, 27.976), xytext=(201598, 27.976), bbox=bbox_props,
                color='purple')
axRight.annotate(r'$\boldsymbol{b}$', xy=(201746, -57), xytext=(201746, -57), bbox=bbox_props,
                color='purple')

axRight.annotate(r'$\boldsymbol{q}$', xy=(201110, 38), xytext=(201110, 38), bbox=bbox_props,
                color='gray')
axRight.annotate(r'$\boldsymbol{p}$', xy=(201240, 38), xytext=(201240, 38), bbox=bbox_props,
                color='gray')
axRight.annotate(r'$\boldsymbol{q}$', xy=(201460, 38), xytext=(201460, 38), bbox=bbox_props,
                color='gray')
axRight.annotate(r'$\boldsymbol{p}$', xy=(201660, 38), xytext=(201660, 38), bbox=bbox_props,
                color='gray')
axRight.annotate(r'$\boldsymbol{q}$', xy=(201780, 38), xytext=(201780, 38), bbox=bbox_props,
                color='gray')

#Whole image
xlabelX=0.3; xlabel1X = 0.76; xlabelY=0.002; ylabelX=0.06; ylabelY=0.54;
xlabel = r'$\mathrm{time}$';
ylabel = r'$\mathrm{V (mV)}$'
fig.text(xlabelX, xlabelY, xlabel, ha='center', bbox=bbox_props_fig, color='darkslategray', fontsize=defaultFontSize,
         weight='roman')
fig.text(xlabel1X, xlabelY, xlabel, ha='center', bbox=bbox_props_fig, color='darkslategray', fontsize=defaultFontSize,
         weight='roman')
fig.text(ylabelX, ylabelY, ylabel, va='center', rotation='vertical', bbox=bbox_props_fig, color='darkslategray',
         fontsize=defaultFontSize, weight='roman')

plt.subplots_adjust(left=0.1, right=0.999, top=0.999, bottom=0.1)

fig.savefig(outputdir+'plantTrajectorySymbols.png', bbox_inches='tight')

pickleDataFile1.close()

############################# End Image0: Plant trajectory symbols  ####################################################################
"""

#"""
############################# Image1: All Plant trajectories ####################################################################

libPlantTrajectory = ct.cdll.LoadLibrary('./trajectory.so')

N_EQ=6
libPlantTrajectory.integrator_rk4.argtypes = [ct.POINTER(ct.c_double), ct.POINTER(ct.c_double),ct.POINTER(ct.c_double),
                          ct.c_double,ct.c_uint,ct.c_uint]
def integrator_rk4 (params = np.asarray([-8.61873, -1.94959, 0., 0., -55., 0.003], float),  # shift_Ca2, shift_x2, Iapp, gh, Vhh, gleak; #chaotic second loop
                    y_initial = np.asarray([-39.99, 0.9, 0.5, 0.2, 0.7, 0.2]),
                    dt=0.001,
                    N=30000,
                    stride = 1
                ):
    output = np.zeros(N*N_EQ, float)
    libPlantTrajectory.integrator_rk4(y_initial.ctypes.data_as(ct.POINTER(ct.c_double)), output.ctypes.data_as(ct.POINTER(ct.c_double)), params.ctypes.data_as(ct.POINTER(ct.c_double)),
                       ct.c_double(dt), ct.c_uint(N), ct.c_uint(stride))
    #imshow takes fortran style array input
    return np.reshape(output, (N,N_EQ), 'C')

print('Generating Plant All Trajectories')

if not dataSaved :
    pickleDataFile1 = gzip.open(outputdir+'PlantAllTrajectories.gz','wb')
else :
    pickleDataFile1 = gzip.open(outputdir+'PlantAllTrajectories.gz','rb')

matplotlib.rcParams['savefig.dpi'] = 600

figSizeX=6; figSizeY=2; rows = 2 ; columns = 3;

fig = plt.figure(figsize=(figSizeX, figSizeY))
gs1 = gridspec.GridSpec(rows, columns)
gs1.update(wspace=0.1, hspace=0.2)  # set the spacing between axes.

axTonic = plt.subplot(gs1[1,0])
axChaos = plt.subplot(gs1[1,1])
axBurst = plt.subplot(gs1[0,0])

axMMO = plt.subplot(gs1[0,1])

from mpl_toolkits.mplot3d import Axes3D
ax3D = plt.subplot(gs1[:,2], projection='3d')

N=430000; dt=1


### Burst trajectory
plt.axis('on')
axBurst.set_xticks([])
axBurst.set_yticks([])

if not dataSaved:
    params = np.asarray([-40., 7., 0., 0., -55., 0.003], float)
    plotData1 = integrator_rk4(params=params, N=N, dt=dt)
    pickle.dump(plotData1, pickleDataFile1)
    params = np.asarray([-40., -2., 0., 0., -55., 0.003], float)
    plotData2 = integrator_rk4(params=params, N=N, dt=dt)
    pickle.dump(plotData2, pickleDataFile1)

else:
    plotData1 = pickle.load(pickleDataFile1)
    plotData2 = pickle.load(pickleDataFile1)

axBurst.set_xlim(405000, 425000)

axBurst.set_ylim(-80, 40)

line, = axBurst.plot(dt*arange(0,N,1.), plotData1[0:N,0], color='b', linestyle='-', linewidth=0.75)
line, = axBurst.plot(dt*(arange(0,N,1.)+300000), plotData2[0:N,0]-25,'r', linestyle='-', linewidth=0.75)

axBurst.autoscale(False)
axBurst.set_adjustable('box')

axBurst.text( -0.02, 0., -80, ha='right', va='bottom', bbox=bbox_props_label, transform=axBurst.transAxes,
                    fontsize=defaultFontSize, rotation=90)
axBurst.text( -0.02, 1., 40, ha='right', va='top', bbox=bbox_props_label, transform=axBurst.transAxes,
                    fontsize=defaultFontSize, rotation=90)
axBurst.spines['right'].set_visible(False)
axBurst.spines['top'].set_visible(False)
axBurst.spines['bottom'].set_visible(False)

line, = axBurst.plot(arange(410000,412000,400), 10*np.ones(5,float),'k-')
axBurst.annotate(r'$\boldsymbol{2s}$', xy=(410000, 5.), xytext=(410000, 5.), bbox=bbox_props,
                color='k', va='top')

axBurst.text(0.02, 0.9, r'$\boldsymbol{A}$', ha='left', va='bottom', bbox=bbox_props_index,
                   transform=axBurst.transAxes, fontsize=defaultFontSize)

### Chaotic trajectory (MMOs)
plt.axis('on')
axMMO.set_xticks([])
axMMO.set_yticks([])

if not dataSaved:
    plotData1 = integrator_rk4(N=N, dt=dt)
    pickle.dump(plotData1, pickleDataFile1)
    plotData2 = integrator_rk4(y_initial=np.asarray([-63.0481, 0.972129, 0.5, 0.05, 0.809502, 0.]), N=N, dt=dt)
    pickle.dump(plotData2, pickleDataFile1)

else:
    plotData1 = pickle.load(pickleDataFile1)
    plotData2 = pickle.load(pickleDataFile1)

axMMO.set_xlim(240000, 360000)

axMMO.set_ylim(-80, 40)

line, = axMMO.plot(dt*arange(0,N,1.), plotData1[0:N,0], color='b', linestyle='-', linewidth=0.75)
line, = axMMO.plot(dt*(arange(0,N,1.)+240000), plotData2[0:N,0]-25,'r', linestyle='-', linewidth=0.75)

axMMO.autoscale(False)
axMMO.set_adjustable('box')

axMMO.spines['right'].set_visible(False)
axMMO.spines['top'].set_visible(False)
axMMO.spines['bottom'].set_visible(False)

axMMO.text(0.02, 0.9, r'$\boldsymbol{C}$', ha='left', va='bottom', bbox=bbox_props_index,
                   transform=axMMO.transAxes, fontsize=defaultFontSize)

line, = axMMO.plot(arange(260000,270000,2000), 10*np.ones(5,float),'k-')
axMMO.annotate(r'$\boldsymbol{10s}$', xy=(260000, 5.), xytext=(260000, 5.), bbox=bbox_props,
                color='k', va='top')

############ 3D phase space
plt.axis('on')

ax3D.autoscale(False)
ax3D.set_adjustable('box')
ax3D.set_xticks([])
ax3D.set_yticks([])
ax3D.set_zticks([])
ax3D.set_xlim(0.6,1.)
ax3D.set_ylim(0.1,1.)
ax3D.set_zlim(-60,20.)
ax3D.plot3D(plotData1[1000:,1], plotData1[1000:,4], plotData1[1000:,0], color='b', linestyle='-', linewidth=0.2) #Ca, x, V
ax3D.plot3D(plotData2[int(N/4):N,1], plotData2[int(N/4):N,4], plotData2[int(N/4):N,0], 'r', linestyle='-', linewidth=0.2) #Ca, x, V

### Tonic trajectory

plt.axis('on')
axTonic.set_xticks([])
axTonic.set_yticks([])

if not dataSaved:
    params = np.asarray([-55., -2., 0., 0., -55., 0.003], float)
    plotData0 = integrator_rk4(params=params, N=N, dt=dt)
    pickle.dump(plotData0, pickleDataFile1)

else:
    plotData0 = pickle.load(pickleDataFile1)

axTonic.set_xlim(160000, 200000)
axTonic.set_ylim(-60, 40)

line, = axTonic.plot(dt*arange(0,N,1.), plotData0[0:N,0], color='b', linestyle='-', linewidth=0.75)

axTonic.autoscale(False)
axTonic.set_adjustable('box')

axTonic.text( -0.0133, 0., -60, ha='right', va='bottom', bbox=bbox_props_label, transform=axTonic.transAxes,
                    fontsize=defaultFontSize, rotation=90)
axTonic.text( -0.0133, 1., 40, ha='right', va='top', bbox=bbox_props_label, transform=axTonic.transAxes,
                    fontsize=defaultFontSize, rotation=90)
axTonic.spines['right'].set_visible(False)
axTonic.spines['top'].set_visible(False)
axTonic.spines['bottom'].set_visible(False)

line, = axTonic.plot(arange(170000,174000,800), -58*np.ones(5,float),'k-')
axTonic.annotate(r'$\boldsymbol{4s}$', xy=(170000, -60), xytext=(170000, -60), bbox=bbox_props,
                color='k', va='top')

axTonic.text(0.02, 0.9, r'$\boldsymbol{B}$', ha='left', va='bottom', bbox=bbox_props_index,
                   transform=axTonic.transAxes, fontsize=defaultFontSize)

### Chaotic trajectory (top)

plt.axis('on')
axChaos.set_xticks([])
axChaos.set_yticks([])

if not dataSaved:
    params = np.asarray([-104.72, 48.2885, 0., 0., -55., 0.003], float)
    plotData0 = integrator_rk4(params=params, N=N, dt=dt)
    pickle.dump(plotData0, pickleDataFile1)

else:
    plotData0 = pickle.load(pickleDataFile1)

axChaos.set_xlim(160000, 200000)
axChaos.set_ylim(-60, 40)

line, = axChaos.plot(dt*arange(0,N,1.), plotData0[0:N,0], color='b', linestyle='-', linewidth=0.75)

axChaos.autoscale(False)
axChaos.set_adjustable('box')

axChaos.spines['right'].set_visible(False)
axChaos.spines['top'].set_visible(False)
axChaos.spines['bottom'].set_visible(False)

line, = axChaos.plot(arange(170000,174000,800), -58*np.ones(5,float),'k-')
axChaos.annotate(r'$\boldsymbol{4s}$', xy=(170000, -60), xytext=(170000, -60), bbox=bbox_props,
                color='k', va='top')

axChaos.text(0.02, 0.9, r'$\boldsymbol{D}$', ha='left', va='bottom', bbox=bbox_props_index,
                   transform=axChaos.transAxes, fontsize=defaultFontSize)



#axChaos.annotate(r'$\boldsymbol{3s}$', xy=(230000, -80.), xytext=(230000, -80.), bbox=bbox_props,
#                color='b')
#fig.text(.912, .73, r'$\boldsymbol{3s}$', ha='right', va='top', bbox=bbox_props_index, fontsize=defaultFontSize, color='k')

#Whole image
xlabelX=0.35; xlabelY=0.01; ylabelX=0.07; ylabelY=0.32;
xlabel = r'$\mathrm{time}$';
ylabel = r'$\mathrm{V}$'
#fig.text(xlabelX, xlabelY, xlabel, ha='center', bbox=bbox_props_fig, color='darkslategray', fontsize=defaultFontSize,
         #weight='roman')
fig.text(ylabelX, ylabelY, ylabel, va='center', rotation='vertical', bbox=bbox_props_fig, color='darkslategray',
         fontsize=defaultFontSize, weight='roman')

#fig.text(xlabelX, 0.715, xlabel, ha='center', bbox=bbox_props_fig, color='darkslategray', fontsize=defaultFontSize,
         #weight='roman')
fig.text(ylabelX, 0.83, r'$\mathrm{V}$', va='center', rotation='vertical', bbox=bbox_props_fig, color='darkslategray',
         fontsize=defaultFontSize, weight='roman')

fig.text(0.3, 0.62, r'$\mathrm{V-25}$', va='top', bbox=bbox_props_fig, color='r',
         fontsize=defaultFontSize, weight='roman')

#fig.text(0.05, 1., r'$\boldsymbol{A}$', ha='right', va='top', bbox=bbox_props_index, fontsize=defaultFontSize)
#fig.text(0.05, .5, r'$\boldsymbol{B}$', ha='right', va='top', bbox=bbox_props_index, fontsize=defaultFontSize)
#fig.text(.7, .7, r'$\boldsymbol{C}$', ha='right', va='top', bbox=bbox_props_index, fontsize=defaultFontSize)

fig.text(0.77, 1.02, r'$\boldsymbol{E}$', ha='right', va='top', bbox=bbox_props_index, fontsize=defaultFontSize)

fig.text(0.84, 0.15, 'Ca', ha='center', color='darkslategray', fontsize=defaultFontSize, weight='roman')
fig.text(0.94, 0.25, 'x', ha='center', color='darkslategray', fontsize=defaultFontSize, weight='roman')
fig.text(0.943, 0.6, 'V', va='center', color='darkslategray', fontsize=defaultFontSize, weight='roman')

plt.subplots_adjust(left=0.1, right=0.999, top=0.999, bottom=0.1)

fig.savefig(outputdir+'PlantAllTrajectories.png', bbox_inches='tight')

pickleDataFile1.close()

############################# End Image1: All Plant trajectories ####################################################################

#"""


show()
