#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include <sys/time.h>
#include<string.h>
#include<stdbool.h>

#define N_EQ1	6
#define MAX_KNEADING_LENGTH 10000
#define NUM_THREADS_PER_BLOCK 512


__device__ double fe(double v1, double b, double k){
    return exp((b- 1.21*v1-78.7)/k);
}

__device__ double i1(double v1, double h1){
    return 4*pow((1/(exp((-22-v1)/8)+1)),3)*h1*(30-v1);
}

__device__ double i2(double v1, double n1){
    return 0.3*pow(n1,4)*(-75-v1);
}

__device__ double i3(double v1, double x1){
    return 0.01*x1*(30-v1);
}

__device__ double i4(double v1, double ca1){
    return 0.03*ca1/(0.5 + ca1)*(-75 - v1);
}

__device__ double ileak(double v1){
    return (-40-v1);
}

__device__ void stepper(const double* y, double* dydt, const double* params)
{
	double shift_Ca2=params[0], shift_x2=params[1], Iapp = params[2], gh = params[3], Vhh = params[4], gleak = params[5];

    double V2=y[0], Ca2=y[1], h2=y[2], n2=y[3], x2=y[4], y2=y[5];

    dydt[0] = 4*pow(((0.1*(50 - (127*V2/105 + 8265/105))/(exp((50 - (127*V2/105 + 8265/105))/10) - 1))/((0.1*(50 - (127*V2/105 + 8265/105))/(exp((50 - (127*V2/105 + 8265/105))/10) - 1))+(4*exp((25 - (127*V2/105 + 8265/105))/18)))),3)*h2*(30 - V2)
                + i2(V2,n2)
                + i3(V2,x2)
                + i4(V2, Ca2)
                + gleak*ileak(V2)
                + gh*pow((1/(1+exp(-(V2+63)/7.8))),3)*y2*(-V2+120)
                + Iapp;
    dydt[1] = 0.00015*(0.0085*x2*(140 - V2+shift_Ca2)-Ca2);
    dydt[2] = ((1-h2)*(0.07*exp((25 - (127*V2/105 + 8265/105))/20))-h2*(1.0/(1 + exp((55 - (127*V2/105 + 8265/105))/10))))/12.5;
    dydt[3] = ((1-n2)*(0.01*(55 - (127*V2/105 + 8265/105))/(exp((55 - (127*V2/105 + 8265/105))/10) - 1))-n2*(0.125*exp((45 - (127*V2/105 + 8265/105))/80)))/12.5;
    dydt[4] =  ((1/(exp(0.15*(-V2-50+shift_x2)) + 1))-x2)/100;
    dydt[5] = .5*((1/(1+exp(10*(V2-Vhh))))-y2)/(7.1+10.4/(1+exp((V2+68)/2.2)));

}

__device__ void computeFirstDerivative(const double *y, double *dydt, const double* params){
	stepper(y, dydt, params);
	return;
}

//from: https://github.com/benjaminfrot/LZ76/blob/master/C/LZ76.c
__device__ double LZ76(const char * s, const int n) {
  int c=1,l=1,i=0,k=1,kmax = 1,stop=0;
  while(stop ==0) {
    if (s[i+k-1] != s[l+k-1]) {
      if (k > kmax) {
        kmax=k;
      }
      i++;

      if (i==l) {
        c++;
        l += kmax;
        if (l+1>n)
          stop = 1;
        else {
          i=0;
          k=1;
          kmax=1;
        }
      } else {
        k=1;
      }
    } else {
      k++;
      if (l+k > n) {
        c++;
        stop =1;
      }
    }
  }
  return double(c)/n;
}



__device__ unsigned NextCoprime(unsigned* coPrimes,unsigned x){
    coPrimes[x] = (coPrimes[x] - x) * coPrimes[x] + x;
    return coPrimes[x];
}

//https://stackoverflow.com/questions/40303333/how-to-replicate-java-hashcode-in-c-language
__device__ unsigned javaHashCode(const unsigned x) {
    int hash = 0, length = floor(log10(fabs(x+0.))) + 1;
    for(int i=0; i<length; i++){
        hash = 31*hash+(x%int(pow(10.,i+0.)));
    }
    return hash;
}

__device__ unsigned NextPair(unsigned* coPrimes, const unsigned a, const unsigned b, const unsigned x, const unsigned y){
    return a * NextCoprime(coPrimes, x) * javaHashCode(x) + b * javaHashCode(y);
}

// Need circular hash function : https://stackoverflow.com/questions/2585092/is-there-a-circular-hash-function
// https://mathoverflow.net/questions/135297/general-euclid-fermat-sequences
//This function can return either positive or negative values
__device__ double circularHash(const char *kneadings, const unsigned periodLength)
{
    int H = 0;

    if (periodLength > 0)
    {
        //any arbitrary coprime numbers
        unsigned a = periodLength, b = periodLength + 1;

        //an array of Euclid-Fermat sequences to generate additional coprimes for each duplicate character occurrence
        unsigned noOfCoprimes = 255; //see if this is enough or more are needed
        unsigned coPrimes[255];

        for (int i = 1; i < noOfCoprimes; i++)
        {
            coPrimes[i] = i + 1;
        }

        //for i=0 we need to wrap around to the last character
        H = NextPair(coPrimes, a, b, kneadings[periodLength - 1], kneadings[0]);

        //for i=1...n we use the previous character
        for (int i = 1; i < periodLength; i++)
        {
            H ^= NextPair(coPrimes, a, b, kneadings[i - 1], kneadings[i]);
        }
    }
    //printf("\nCircular hash : %d\n",H);

    //return double(H);
    //H can be any integer; In order to not conflict with LZ, we make sure we return a value greater than 1 or less than -1, so LZ can be between 0 and 1, and -0.5 can be used as marker for insufficient N.
    if(H>=0) return double(H+1);
    else return double(H-1);
}

/*
__device__ double  computePeriodNormalizedKneadingSum(const char *kneadings, const unsigned kneadingsLength, const unsigned
periodLength ){
    double kneadingSum=0, minPeriodSum=0, currPeriodSum=0;
    unsigned i=0, normalizedPeriodIndex=0;

    char s[200];unsigned si=0;
    //Also normalizing symmetric orbits -- so 00000000.. and 1111111111.. are treated identically
    double minPeriodSumSymmetric=0, currPeriodSumSymmetric=0;
    unsigned normalizedPeriodIndexSymmetric=0;

    if(periodLength<kneadingsLength){
        for(i=0; i<periodLength; i++) {
            currPeriodSum=0;
            for(unsigned j=0; j<periodLength; j++) {
                currPeriodSum+= 2*currPeriodSum + kneadings[i+j];
            }
            if(minPeriodSum==0 || currPeriodSum < minPeriodSum) {
                minPeriodSum = currPeriodSum;
                normalizedPeriodIndex=i;
            }
        }
        for(i=0; i<periodLength; i++) {
            currPeriodSumSymmetric=0;
            for(unsigned j=0; j<periodLength; j++) {
                currPeriodSumSymmetric+= 2*currPeriodSumSymmetric + 1-kneadings[i+j];
            }
            if(minPeriodSumSymmetric==0 || currPeriodSumSymmetric < minPeriodSumSymmetric) {
                minPeriodSumSymmetric = currPeriodSumSymmetric;
                normalizedPeriodIndexSymmetric=i;
            }
        }
    }

    //filling kneading sequence with normalized period
    for(i=0; i<kneadingsLength; i++) {
        if(minPeriodSum < minPeriodSumSymmetric){
            kneadingSum = kneadingSum + kneadings[normalizedPeriodIndex+periodLength-1-i%periodLength]/pow(2.,double(-i+kneadingsLength));
    	    s[si++]='0'+int(kneadings[normalizedPeriodIndex+periodLength-1-i%periodLength]);
        } else {
            kneadingSum = kneadingSum + (1-kneadings[normalizedPeriodIndexSymmetric+periodLength-1-i%periodLength])/pow(2.,double(-i+kneadingsLength));
    	    s[si++]='0'+int(kneadings[normalizedPeriodIndexSymmetric+periodLength-1-i%periodLength]);
        }
    }
    if(periodLength<kneadingsLength){
        //printf("\nPeriod length: %d, symbolic sequence: %s ",periodLength,s);
    }

    //not filling kneading sequence with normalized period, just using a single period
    //for(i=0; i<periodLength; i++) {
      //  if(minPeriodSum < minPeriodSumSymmetric){
        //    kneadingSum = kneadingSum + kneadings[normalizedPeriodIndex+periodLength-1-i%periodLength]/pow(2.,double
        //(-i+periodLength));
        //} else {
        //    kneadingSum = kneadingSum + (1-kneadings[normalizedPeriodIndexSymmetric+periodLength-1-i%periodLength])
        ///pow(2.,double(-i+periodLength));
        //}
    //}

    return kneadingSum;

}
*/

__device__ double periodicPCorChaoticLZ(const char* kneadings,const  unsigned kneadingsLength){
    //After a long transient when the periodic orbits if any have already been reached, this method computes a kneading sum that is invariant between different cyclic permutations of the period

    bool periodFound=true;
    unsigned periodLength = kneadingsLength;
    for(unsigned currPeriod=1; currPeriod < kneadingsLength/2; currPeriod++) {
        periodFound=true;
        //Check if the kneading sequence has a period with periodicity of currPeriod
        for(unsigned i=currPeriod; i < kneadingsLength-currPeriod; i+=currPeriod) {
            for ( unsigned j=0; j<currPeriod; j++) {
                if(kneadings[j] != kneadings[i+j]) {
                    periodFound=false;
                    break;
                }
            }
            if(!periodFound) {
                break;
            }
        }
        //compute kneadingSum based on period found, if any
        if(periodFound) {
            //currPeriod is the period of the kneading sequence. So this will be normalized to a sequence with the sorted period(0's followed by 1's)
            periodLength = currPeriod;
            break;
        }
    }
    //return periodLength==kneadingsLength? LZ76(kneadings, kneadingsLength) : -computePeriodNormalizedKneadingSum(kneadings, kneadingsLength, periodLength);
    //return periodLength==kneadingsLength? LZ76(kneadings, kneadingsLength) : -periodLength;

    //printf("\nPeriod Length: %d\n", periodLength);
    return periodLength==kneadingsLength? LZ76(kneadings, kneadingsLength) : -circularHash(kneadings, periodLength);

}


__device__ char getBinSymbol(const double value, const double* binBoundariesGpu, const double numberOfBinBoundaries){
    unsigned i;

    if(numberOfBinBoundaries==0) return char('a');
    for(i=0;i<numberOfBinBoundaries;i++){
        if(value < binBoundariesGpu[i])
            return char('a'+i);
    }
    return char('a'+i);
}

__device__ double integrator_rk4(double* y_current, const double* voltageBinBoundariesGpu, const double* intervalBinBoundariesGpu, const double* params, const double dt, const unsigned N, const unsigned stride, const unsigned nV, const unsigned nI, const unsigned kneadingsStart, const unsigned kneadingsEnd)
{
	unsigned i, j, k, kneadingIndex=0, kneadingArrayIndex=0, previousEvent=0;
	double dt2, dt6;
	double y1[N_EQ1], y2[N_EQ1], k1[N_EQ1], k2[N_EQ1], k3[N_EQ1], k4[N_EQ1];
	double firstDerivativeCurrent[N_EQ1],firstDerivativePrevious;
	double kneadingsWeightedSum=0;
    char kneadings[MAX_KNEADING_LENGTH];

	bool isPreviousEventOne=false, isDerviative2FirstTimeOnThisSidePositive=0, isPreviousDerivative2Positive=0, isCurrentDerivate2Positive=0;

	dt2 = dt/2.; dt6 = dt/6.;

	for(i=1; i<N; i++)
	{
		for(j=0; j<stride; j++)
		{
			stepper(y_current, k1, params);
			for(k=0; k<N_EQ1; k++) y1[k] = y_current[k]+k1[k]*dt2;
			stepper(y1, k2, params);
			for(k=0; k<N_EQ1; k++) y2[k] = y_current[k]+k2[k]*dt2;
			stepper(y2, k3, params);
			for(k=0; k<N_EQ1; k++) y2[k] = y_current[k]+k3[k]*dt;
			stepper(y2, k4, params);

			//Copy latest value into y_current
			for(k=0; k<N_EQ1; k++) y_current[k] += dt6*(k1[k]+2.*(k2[k]+k3[k])+k4[k]);
		}

		computeFirstDerivative(y_current, firstDerivativeCurrent, params);


        if(firstDerivativePrevious * firstDerivativeCurrent[0] < 0) {
            if(kneadingIndex<kneadingsStart){
                //skip ahead upto the kneadingsStart symbol
                kneadingIndex++;
            } else if(kneadingArrayIndex==0 and firstDerivativePrevious<0){
                //First symbol is always a maxima to maintain the order of maxima,time,minima,time... and to reuse the same symbols for all four items in the sequence; We ignore the first minima if any until we encounter the first maxima (which should immediately follow the minima)
                kneadingIndex++;
            } else {
               if(kneadingArrayIndex!=0){
                    //Except for the first maxima, every other current event will be preceded by the time interval from the last interval
                    kneadings[kneadingArrayIndex++] = getBinSymbol(dt * (i - previousEvent), intervalBinBoundariesGpu, nI)+13; //intervalBin; using a to m for voltage bins, and n to z for time bins so that when computing circular permutations, there is no confusion
                    kneadingIndex++;
               }
               kneadings[kneadingArrayIndex++] = getBinSymbol(y_current[0], voltageBinBoundariesGpu, nV); //voltageBin
               kneadingIndex++;
            }
            previousEvent = i;
        }
		firstDerivativePrevious = firstDerivativeCurrent[0];

		if(kneadingIndex>kneadingsEnd){
            //print test
            //kneadings[kneadingArrayIndex++]=char('\0'); printf("%lf %lf %s\n length: %d", params[0], params[1], kneadings, kneadingArrayIndex); //return 0;

			return periodicPCorChaoticLZ(kneadings, kneadingArrayIndex);
			//return LZ(kneadings, kneadingArrayIndex); //temporarily only using LZ
		}
	}
	//print test
	//printf("%lf %lf %s \t length: %d\n", params[0], params[1], kneadings, kneadingIndex); //return 0;
	return -0.5;
}

__global__ void sweepThreads(double* kneadingsWeightedSumSet, const double* voltageBinBoundariesGpu, const double* intervalBinBoundariesGpu,
                                        const double alphaStart, const double alphaEnd, const unsigned alphaCount,
										const double lStart, const double lEnd, const unsigned lCount,
										const double Iapp, const double gh, const double Vhh, const double gleak,
										const double dt, const unsigned N, const unsigned stride,
										const unsigned nV, const unsigned nI,
										const unsigned kneadingsStart, const unsigned kneadingsEnd){

    int tx=blockIdx.x * blockDim.x + threadIdx.x;

	double params[6],a,b;
	double alphaStep = alphaCount==1?0:(alphaEnd - alphaStart)/(alphaCount-1);
	double lStep = lCount==1?0:(lEnd-lStart)/(lCount -1);
	int i,j,k;


    if(tx<alphaCount*lCount){

        i=tx/alphaCount;
        j=tx%alphaCount;

        params[0]=alphaStart+i*alphaStep;
        params[1]=lStart+j*lStep;
        params[2]=Iapp;
        params[3]=gh;
        params[4]=Vhh;
        params[5]=gleak;

        //V2=y[0], Ca2=y[1], h2=y[2], n2=y[3], x2=y[4], y2=y[5]
      	double y_initial[N_EQ1]={-39.99, 0.9, 0.5, 0.2, 0.7, 0.2};

        kneadingsWeightedSumSet[i*lCount+j] = integrator_rk4(y_initial, voltageBinBoundariesGpu, intervalBinBoundariesGpu, params, dt, N, stride, nV, nI, kneadingsStart, kneadingsEnd);

    }
}

/**
 * error checking routine
 */
void checkAndDisplayErrors(char *label)
{
  // we need to synchronise first to catch errors due to
  // asynchronous operations that would otherwise
  // potentially go unnoticed

  cudaError_t err;

  err = cudaThreadSynchronize();
  if (err != cudaSuccess)
  {
    char *e = (char*) cudaGetErrorString(err);
    fprintf(stderr, "CUDA Error: %s (at %s)", e, label);
  }

  err = cudaGetLastError();
  if (err != cudaSuccess)
  {
    char *e = (char*) cudaGetErrorString(err);
    fprintf(stderr, "CUDA Error: %s (at %s)", e, label);
  }
}


extern "C" {

void sweep(double* kneadingsWeightedSumSet,const double* voltageBinBoundaries,const double* intervalBinBoundaries,
                                        const double alphaStart, const double alphaEnd, const unsigned alphaCount,
										const double lStart, const double lEnd, const unsigned lCount,
										const double Iapp, const double gh, const double Vhh, const double gleak,
										const double dt, const unsigned N, const unsigned stride,
										const unsigned nV, const unsigned nI,
										const unsigned kneadingsStart, const unsigned kneadingsEnd){

        int totalParameterSpaceSize = alphaCount*lCount;
        double *kneadingsWeightedSumSetGpu, *voltageBinBoundariesGpu, *intervalBinBoundariesGpu;

        printf("Before malloc gpu\n");
        /*allocate device memory. */
        (cudaMalloc( (void**) &kneadingsWeightedSumSetGpu, totalParameterSpaceSize*sizeof(double)));
        (cudaMalloc( (void**) &voltageBinBoundariesGpu, nV*sizeof(double)));
        (cudaMalloc( (void**) &intervalBinBoundariesGpu, nI*sizeof(double)));
        checkAndDisplayErrors("Memory allocation");
        printf("After malloc gpu\n");

        /*copy data from host to device */
        (cudaMemcpy( voltageBinBoundariesGpu, voltageBinBoundaries, nV*sizeof(double), cudaMemcpyHostToDevice));
        (cudaMemcpy( intervalBinBoundariesGpu, intervalBinBoundaries, nI*sizeof(double), cudaMemcpyHostToDevice));
        checkAndDisplayErrors("Error while copying bin boundaries from host to device.");

        /*timing*/
        cudaEvent_t start_event, stop_event;
        cudaEventCreate(&start_event) ;
        cudaEventCreate(&stop_event) ;
        cudaEventRecord(start_event, 0);

        /*Dimensions. */

        int gridXDimension = totalParameterSpaceSize/NUM_THREADS_PER_BLOCK;
        if(totalParameterSpaceSize%NUM_THREADS_PER_BLOCK!=0) {
                gridXDimension += 1;
        }
        dim3 dimGrid(gridXDimension,1);
        dim3 dimBlock(NUM_THREADS_PER_BLOCK,1);

        printf(" Num of blocks per grid:       %d\n", gridXDimension);
        printf(" Num of threads per block:     %d\n", NUM_THREADS_PER_BLOCK);
        printf(" Total Num of threads running: %d\n", gridXDimension*NUM_THREADS_PER_BLOCK);
        printf(" Parameters alphaCount=%d, lCount=%d\n",alphaCount,lCount);

        /*Call kernel(global function)*/
        sweepThreads<<<dimGrid, dimBlock>>>(kneadingsWeightedSumSetGpu, voltageBinBoundariesGpu, intervalBinBoundariesGpu, alphaStart, alphaEnd, alphaCount, lStart, lEnd, lCount, Iapp, gh, Vhh, gleak, dt, N, stride, nV, nI, kneadingsStart, kneadingsEnd);

        cudaThreadSynchronize();
        cudaEventRecord(stop_event, 0);
        cudaEventSynchronize(stop_event);

        float time_kernel;
        cudaEventElapsedTime(&time_kernel, start_event, stop_event);
        printf("Total time(sec) %f\n", time_kernel/1000);

        /*copy data from device memory to memory. */
        (cudaMemcpy( kneadingsWeightedSumSet, kneadingsWeightedSumSetGpu, totalParameterSpaceSize*sizeof(double), cudaMemcpyDeviceToHost));
        checkAndDisplayErrors("Error while copying kneading sum values from device to host.");

        /*Free all allocated memory. */
        cudaFree(kneadingsWeightedSumSetGpu);
        cudaFree(voltageBinBoundariesGpu);
        cudaFree(intervalBinBoundariesGpu);

}

}


int main(){
	double dt=1;
	unsigned N=30000*10;
	unsigned stride = 1;
	unsigned maxKneadings = 201;
	unsigned sweepSize = 5;
	double *kneadingsWeightedSumSet;
	int i,j;
	double voltageBinBoundaries[] = {-60., -40., 10.}, intervalBinBoundaries [] = {};

	printf("Enter sweep size :");
	scanf("%d",&sweepSize);
	kneadingsWeightedSumSet = (double *)malloc(sweepSize*sweepSize*sizeof(double));


	sweep(kneadingsWeightedSumSet, voltageBinBoundaries, intervalBinBoundaries, -2, -2, sweepSize, 4., 4., sweepSize, 0., 0., -55., 0.003, dt, N, stride, 3, 0, 100, 200);

	for(i=0;i<sweepSize;i++){
	    for(j=0;j<sweepSize;j++){
	        printf("%f \n",kneadingsWeightedSumSet[i*sweepSize+j]);
	    }
	}

}

