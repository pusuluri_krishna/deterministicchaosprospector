#!/bin/bash

nvcc --ptxas-options=-v -arch=sm_30 --compiler-options '-fPIC' -o sweepPhases.so --shared sweepPhases.cu

gcc -fPIC -shared -o trajectory.so trajectory.c
