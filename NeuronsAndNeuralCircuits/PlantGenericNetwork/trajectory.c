#include <sys/time.h>
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include <sys/time.h>
#include<string.h>
#include<stdbool.h>

#define N_EQ	6
#define NP 8
#define MAX_KNEADING_LENGTH 10000
#define NUM_THREADS_PER_BLOCK 512

double fe(double v1, double b, double k){
    return exp((b- 1.21*v1-78.7)/k);
}

double i1(double v1, double h1){
    return 4*pow((1/(exp((-22-v1)/8)+1)),3)*h1*(30-v1);
}

double i2(double v1, double n1){
    return 0.3*pow(n1,4)*(-75-v1);
}

double i3(double v1, double x1){
    return 0.01*x1*(30-v1);
}

double i4(double v1, double ca1){
    return 0.03*ca1/(0.5 + ca1)*(-75 - v1);
}

double ileak(double v1){
    return (-40-v1);
}

void stepper(const double* y, double* dydt, const double* params)
{
    unsigned i,j,k;

    double Iapp = params[2], gh = params[3], Vhh = params[4], gleak = params[5], shift_Ca=params[6], shift_x=params[7];

    double V2=y[0], Ca2=y[1], h2=y[2], n2=y[3], x2=y[4], y2=y[5];

    dydt[0] = 4*pow(((0.1*(50 - (127*V2/105 + 8265/105))/(exp((50 - (127*V2/105 + 8265/105))/10) - 1))/((0.1*(50 - (127*V2/105 + 8265/105))/(exp((50 - (127*V2/105 + 8265/105))/10) - 1))+(4*exp((25 - (127*V2/105 + 8265/105))/18)))),3)*h2*(30 - V2)
                + i2(V2,n2)
                + i3(V2,x2)
                + i4(V2, Ca2)
                + gleak*ileak(V2)
                + gh*pow((1/(1+exp(-(V2+63)/7.8))),3)*y2*(-V2+120)
                + Iapp;
                //+ g21*(-55 - V2)*s3;
    dydt[1] = 0.00015*(0.0085*x2*(140 - V2+shift_Ca)-Ca2);
    dydt[2] = ((1-h2)*(0.07*exp((25 - (127*V2/105 + 8265/105))/20))-h2*(1.0/(1 + exp((55 - (127*V2/105 + 8265/105))/10))))/12.5;
    dydt[3] = ((1-n2)*(0.01*(55 - (127*V2/105 + 8265/105))/(exp((55 - (127*V2/105 + 8265/105))/10) - 1))-n2*(0.125*exp((45 - (127*V2/105 + 8265/105))/80)))/12.5;
    dydt[4] =  ((1/(exp(0.15*(-V2-50+shift_x)) + 1))-x2)/100;
    dydt[5] = .5*((1/(1+exp(10*(V2-Vhh))))-y2)/(7.1+10.4/(1+exp((V2+68)/2.2)));

}

void stepper_coupled(const double* y, double* dydt, const double* parameterSet, const int networkSize)
{
    const double *csElec = parameterSet+networkSize*NP,
                    *csGInh = parameterSet+networkSize*NP+networkSize*networkSize,
                    *csAlphaInh =  parameterSet+networkSize*NP+2*networkSize*networkSize,
                    *csBetaInh =  parameterSet+networkSize*NP+3*networkSize*networkSize,
                    *csGExc =  parameterSet+networkSize*NP+4*networkSize*networkSize,
                    *csAlphaExc =  parameterSet+networkSize*NP+5*networkSize*networkSize,
                    *csBetaExc =  parameterSet+networkSize*NP+6*networkSize*networkSize;

	double *yInh = y+networkSize*N_EQ, *yExc = y+networkSize*N_EQ+networkSize*networkSize;
	double *dydtInh = dydt+networkSize*N_EQ, *dydtExc = dydt+networkSize*N_EQ+networkSize*networkSize;
	unsigned i, j;

    //Compute dydt
	for(i=0;i <networkSize; i++){
		stepper(y+i*N_EQ, dydt+i*N_EQ, parameterSet+i*NP);
	}

    //Compute dsdt for SInh and SExc
	for(i=0;i<networkSize;i++){
	    for(j=0;j<networkSize;j++){
            //dydtInh[i*networkSize+j] = csAlphaInh[i*networkSize+j]*(1-yInh[i*networkSize+j])/(1+exp(-10*(y[i*N_EQ]+20)))-csBetaInh[i*networkSize+j]*yInh[i*networkSize+j]/(1+exp(10*(y[i*N_EQ]+20)));
            dydtInh[i*networkSize+j] = csAlphaInh[i*networkSize+j]*(1-yInh[i*networkSize+j])/(1+exp(-10*(y[i*N_EQ]+20)))-csBetaInh[i*networkSize+j]*yInh[i*networkSize+j];

            //dydtExc[i*networkSize+j] = csAlphaExc[i*networkSize+j]*(1-yExc[i*networkSize+j])/(1+exp(-10*(y[i*N_EQ]+20)))-csBetaExc[i*networkSize+j]*yExc[i*networkSize+j]/(1+exp(10*(y[i*N_EQ]+20)));
            dydtExc[i*networkSize+j] = csAlphaExc[i*networkSize+j]*(1-yExc[i*networkSize+j])/(1+exp(-10*(y[i*N_EQ]+20)))-csBetaExc[i*networkSize+j]*yExc[i*networkSize+j];
        }
	}

    //Add synaptic inputs to dydt
    for(i=0;i<networkSize;i++){
        for(j=0;j<networkSize;j++){
       		   dydt[i*N_EQ] += (y[j*N_EQ]-y[i*N_EQ])*csElec[j*networkSize+i]        //            electric j to i
       		                    +  (parameterSet[i*NP+0] - y[i*N_EQ])* csGInh[j*networkSize+i] * yInh[j*networkSize+i] // AlphaInh j to i
       		                    +  (parameterSet[i*NP+1] - y[i*N_EQ])* csGExc[j*networkSize+i] * yExc[j*networkSize+i]; // AlphaExc j to i
        }
    }
}


double integrator_rk4(double* y_current, double *output, const double* params, const unsigned networkSize, const double dt, const unsigned N, const unsigned stride)
{
	unsigned i=0, j=0, k=0, kneadingIndex=0, N_EQ1 = N_EQ*networkSize + 2*networkSize * networkSize ; // For each cell N_EQ equations plus networkSize^2 equations each for SInh and SExc.
	double dt2, dt6;
	double y1[N_EQ1], y2[N_EQ1], k1[N_EQ1], k2[N_EQ1], k3[N_EQ1], k4[N_EQ1];

	dt2 = dt/2.; dt6 = dt/6.;

    for(k=0; k<networkSize; k++){
        //printf("%lf\t",y_current[k*N_EQ]);
        output[i*networkSize+k] = y_current[k*N_EQ];
    }
    //printf("\n");

	for(i=1; i<N; i++)
	{
		for(j=0; j<stride; j++)
		{
			stepper_coupled(y_current, k1, params, networkSize);
			for(k=0; k<N_EQ1; k++) y1[k] = y_current[k]+k1[k]*dt2;
			stepper_coupled(y1, k2, params, networkSize);
			for(k=0; k<N_EQ1; k++) y2[k] = y_current[k]+k2[k]*dt2;
			stepper_coupled(y2, k3, params, networkSize);
			for(k=0; k<N_EQ1; k++) y2[k] = y_current[k]+k3[k]*dt;
			stepper_coupled(y2, k4, params, networkSize);

			//Copy latest value into y_current
			for(k=0; k<N_EQ1; k++) y_current[k] += dt6*(k1[k]+2.*(k2[k]+k3[k])+k4[k]);
		}
        for(k=0; k<networkSize; k++){
            //printf("%lf\t",y_current[k*N_EQ]);
            output[i*networkSize+k] = y_current[k*N_EQ];
        }

	}
	return 0;
}

int main(){
    /*
	double y_initial[]={0.01,0.,0.,0.,0.,0.};
	double dt=0.01;
	unsigned N=30000;
	unsigned stride = 1;
	unsigned maxKneadings = 20;
	unsigned sweepSize = 2;
	double g = 50, sigma = 1.5;
	double *kneadingsWeightedSumSet;
	double alpha;
	int i,j;
	printf("Enter alpha :");
	scanf("%lf",&alpha);
	kneadingsWeightedSumSet = (double *)malloc(sweepSize*sweepSize*sizeof(double));
	*/
	/*
	for(i=0;i<sweepSize;i++){
	    for(j=0;j<sweepSize;j++){
	        printf("%f \n",kneadingsWeightedSumSet[i*sweepSize+j]);
	    }
	}
	*/
}
