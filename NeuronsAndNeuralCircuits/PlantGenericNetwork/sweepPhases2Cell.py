
from __future__ import division
#to get default literal division to be floating point arithmetic
import sweepPhasesGeneric
import numpy as np

networkSize = 2

#VInh = -80, VExc = 0, Iapp = 0., gh = 0.0004, Vhh = -54, gleak = 0.003, CaShift = -75., xShift =2.
# For cells Si3s
intrinsicParameterSet = np.array([-55, 0, 0., 0.0, -55, 0.003, -45., -2.,
                            -55, 0, 0., 0.0, -55, 0.003, -45., -2.], dtype='float64')

csElec = np.asarray(np.matrix('0 0; 0 0'))
#one-sided electrical connections

##Corresponding to each synapse there would be an S variable to be computed at each step of the integration!
csGInh = np.asarray(np.matrix('0 0.1; 0.1 0 '));
csAlphaInh = np.asarray(np.matrix('0 0.01; 0.01 0'));
csBetaInh = np.asarray(np.matrix('0 0.005; 0.005 0'));

csGExc = np.asarray(np.matrix("0 0; 0 0"))
csAlphaExc = np.asarray(np.matrix("0 0 ; 0 0"))
csBetaExc = np.asarray(np.matrix("0 0; 0 0 "))

parameterSet = np.concatenate((intrinsicParameterSet,csElec,csGInh,csAlphaInh,csBetaInh,csGExc,csAlphaExc,csBetaExc), axis=None)
#intrinsicParameterSet.flatten()

#print repr(parameterSet) #repr() prints the numpy array with commas (to use in C code)
#V2 = y[0], Ca2 = y[1], h2 = y[2], n2 = y[3], x2 = y[4], y2 = y[5], s2 = y[6];
initialConditions = np.asarray([0, 1, 0., 0., 0.05, 0.,
                                       20, 1.4, 0., 0., 0.6, 0.], dtype='float64')
    #Initial condition should be set appropriately to separate tonic spiking and quiescent; For these values for Si3, cell 4 is either tonic/quiescent (which is what we are plotting), while cell 3 is always tonic spiking - therefore, plotting it cann't distinguish either case

voltageBinBoundaries = np.asarray([10], dtype='float64');
intervalBinBoundaries = np.asarray([2500], dtype='float64');

spikesStart = 50; spikesEnd = 100; nDataPointsToShow=5
N = 300*1000; dt=1;
dataSaved = True; sweepSize = 50;


sweepParameter1Indices = np.asarray([25, 26], dtype='uint32'); sweepParameter1Start = 0.0; sweepParameter1End = 0.05;   #alpha
sweepParameter2Indices = np.asarray([29, 30], dtype='uint32'); sweepParameter2Start = 0.0; sweepParameter2End = 0.05;   #beta

def runSweep(sweepParameter1Indices, sweepParameter1Start, sweepParameter1End, sweepParameter2Indices, sweepParameter2Start, sweepParameter2End, g=0.1, xShift=-2., CaShift=-40, ax=None, plotDataPoints=True):
    parameterSet[21] = g; parameterSet[22] = g;
    parameterSet[7] = xShift; parameterSet[15] = xShift;
    parameterSet[6] = CaShift; parameterSet[14] = CaShift;
    fileName = '2CellSweep_g_'+str(g)+'_xShift_'+str(xShift)+'_CaShift_'+str(CaShift)+'_'+str(sweepParameter1Indices)+'_'+str(sweepParameter1Start)+'_'+str(sweepParameter1End)+'_'+str(sweepParameter2Indices)+'_'+str(sweepParameter2Start)+'_'+str(sweepParameter2End)+'_'+str(sweepSize);
    print(fileName, ax==None)
    sweepPhasesGeneric.plotSweep(parameterSet, initialConditions, networkSize, sweepParameter1Indices, sweepParameter1Start, sweepParameter1End, sweepParameter2Indices, sweepParameter2Start, sweepParameter2End, sweepSize, spikesStart, spikesEnd, dt, N, voltageBinBoundaries, intervalBinBoundaries, fileName, dataSaved, nDataPointsToShow, ax=ax, plotDataPoints=plotDataPoints);


#Images for paper
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

matplotlib.rcParams['savefig.dpi'] = 1200 #2*sweepSize/figSize #300
defaultFontSize = 11; largeFontSize = 14; indexFontSize = 16;
matplotlib.rcParams.update({'font.size': largeFontSize,
              'axes.labelsize': largeFontSize,
              'axes.titlesize': largeFontSize,
              'font.size': defaultFontSize,
              'legend.fontsize': largeFontSize,
              'xtick.labelsize': defaultFontSize,
              'ytick.labelsize': defaultFontSize})

#comment these two for rainbow text function to run properly, along with setting fig.dpi = 1200 described near rainbow text
matplotlib.rc('text', usetex=True)
matplotlib.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
matplotlib.rcParams['savefig.dpi'] = 600
bbox_props_label = dict(boxstyle="square,pad=0.01", fc="white", ec="k", lw=0	)
bbox_props_index = dict(boxstyle="square,pad=0.2", fc="white", ec="k", lw=1., alpha=0.)
bbox_props_fig = dict(boxstyle="square,pad=0.001", fc="white", ec="k", lw=0)

figSizeX=4; figSizeY=4; gridX = 2 ; gridY = 2;

fig = plt.figure(figsize=(figSizeX, figSizeY))
gs1 = gridspec.GridSpec(gridX, gridY)
gs1.update(wspace=0.03, hspace=0.03)  # set the spacing between axes.

CaShiftArray = [-55, -50, -45, -40]

for i in range(4):
    CaShift = CaShiftArray[i]
    ax = plt.subplot(gs1[int(i/2),int(i%2)])
    if i==0:
        plotDataPoints=False
    else:
        plotDataPoints=True
    runSweep(sweepParameter1Indices, sweepParameter1Start, sweepParameter1End, sweepParameter2Indices, sweepParameter2Start, sweepParameter2End, CaShift=CaShift, ax=ax, plotDataPoints=plotDataPoints)
    plt.axis('on')
    ax.set_xticks([])
    ax.set_yticks([])
    if(int(i/2)==1):
        ax.text(0., -0.01, sweepParameter1Start, ha='left', va='top', bbox=bbox_props_label, transform=ax.transAxes,
                    fontsize=defaultFontSize)
        ax.text(1., -0.01, sweepParameter1End, ha='right', va='top', bbox=bbox_props_label, transform=ax.transAxes,
                    fontsize=defaultFontSize)
        ax.text(0.5, -0.01, r'$\mathrm{\alpha_{inh}}$' , ha='center', va='top', bbox=bbox_props_label, transform=ax.transAxes,
            fontsize=largeFontSize)

    if(int(i%2)==0):
        ax.text(-0.01, 0, sweepParameter2Start, ha='right', va='bottom', rotation=90, bbox=bbox_props_label,
                    transform=ax.transAxes, fontsize=defaultFontSize)
        ax.text(-0.01, 1., sweepParameter2End, ha='right', va='top', rotation=90, bbox=bbox_props_label,
                    transform=ax.transAxes, fontsize=defaultFontSize)
        ax.text(-0.01, 0.5, r'$\mathrm{\beta_{inh}}$', ha='right', va='center', rotation=90, bbox=bbox_props_label,
                transform=ax.transAxes, fontsize=largeFontSize)

    ax.text(0.02, .98, r'$\boldsymbol{'+chr(i + ord('A'))+'}$', ha='left', va='top', transform=ax.transAxes, color='k',
                bbox=bbox_props_index, fontsize=largeFontSize)

plt.subplots_adjust(left=0.1, right=0.999, top=0.999, bottom=0.1)

#fig.text(0.35, 0.6, r'$\boldsymbol{tonick~spiker~}$''\n'r'$\boldsymbol{suppressed}$', ha='center', bbox=bbox_props_index, color='darkslategray', fontsize=defaultFontSize, weight='roman')
fig.text(0.35, 0.6, r'$\boldsymbol{tonic+}$''\n'r'$\boldsymbol{suppressed}$', ha='center', bbox=bbox_props_index, color='w', fontsize=defaultFontSize, weight='roman')
fig.text(0.13, 0.9, r'$\boldsymbol{tonic*2}$', ha='center', va='top', bbox=bbox_props_index, color='w', fontsize=defaultFontSize, weight='roman', rotation=90)
#fig.text(0.42, 0.91, r'$\boldsymbol{HCO}$''\n'r'$\boldsymbol{~~~burst}$', ha='center', bbox=bbox_props_index, color='w', fontsize=defaultFontSize, weight='roman')
fig.text(0.4, 0.22, r'$\boldsymbol{HCO~burst}$', ha='center', bbox=bbox_props_index, color='w', fontsize=defaultFontSize, weight='roman')
fig.text(0.3, 0.5, r'$\boldsymbol{quiescent}$', ha='center', bbox=bbox_props_index, color='darkslategray', fontsize=defaultFontSize, weight='roman')

fig.savefig('Plant_HCO_Grid.png', bbox_inches='tight')

