#!/bin/bash

nvcc --ptxas-options=-v -arch=sm_30 --compiler-options '-fPIC' -o sweep_PCLZ.so --shared sweep_PCLZ.cu
nvcc --ptxas-options=-v -arch=sm_30 --compiler-options '-fPIC' -o sweep_PCLZ_gBeta.so --shared sweep_PCLZ_gBeta.cu
nvcc --ptxas-options=-v -arch=sm_30 --compiler-options '-fPIC' -o sweepPhases_PCLZ_gBeta.so --shared sweepPhases_PCLZ_gBeta.cu

gcc -fPIC -shared -o trajectory.so trajectory.c
