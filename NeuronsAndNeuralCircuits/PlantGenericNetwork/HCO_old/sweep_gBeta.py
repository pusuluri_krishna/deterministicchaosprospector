import matplotlib

matplotlib.use('Qt4Agg')
#matplotlib.use('Agg')
import ctypes as ct
from pylab import *
import numpy as np
from matplotlib import colors
from numpy.ma import masked_array
import pickle
import gzip

dataSaved = True;
kneadingsStart=2000; kneadingsEnd=4000;  #If using both cells' events
#kneadingsStart=1000; kneadingsEnd=2000; #Equivalent to above if using only single cell's events. In C comment out event computation of the other cell

#"""
#Full sweep
gMin = 0.; gMax = 1.;
betaMin = 0.; betaMax = 0.01; ghList=[0.]; #ghList=[-0.0004, 0.0004, 0.004];
CaShift = -75.; xShift = 2.;
sweepSize = 100; N = 300000*10; dt=1;
Iapp = 0.;
#"""


# Adjust these bins to change precision depending on the system; Bins do not have to be equally sized; Have more bins in the region where more precision is required and less bins where its not; Using 10 bins for a start (9 bin boundaries); Best represented by char to compute PC, may also be represented by the digits 0 to 9 if number of bins < 10

#voltageBinBoundaries = np.array([-60., -40., 10.], dtype='float64')
#voltageBinBoundaries = np.array([], dtype='float64')
voltageBinBoundaries = np.array([10.], dtype='float64')

#intervalBinBoundaries = np.array([100], dtype='float64')
intervalBinBoundaries = np.array([], dtype='float64')

# arrays must be created with double values not integers (default) since that's how it is passed to C function later;
#  Otherwise giving errors and memory copied incorrectly

lib = ct.cdll.LoadLibrary('./sweep_PCLZ_gBeta.so')
lib.sweep.argtypes = [ct.POINTER(ct.c_double),
                        ct.POINTER(ct.c_double), ct.POINTER(ct.c_double),
                        ct.c_double, ct.c_double, ct.c_uint,
                        ct.c_double, ct.c_double, ct.c_uint,
                        ct.c_double, ct.c_double, ct.c_double, ct.c_double,
                        ct.c_double, ct.c_double,
                        ct.c_double, ct.c_uint, ct.c_uint,
                        ct.c_uint, ct.c_uint,
                        ct.c_uint, ct.c_uint]

def sweep(dt=1, N=300000, stride=1,
            gMin=-10., gMax=5.,
            betaMin=-2.5, betaMax=-1.5,
            Iapp = 0., gh = 0., Vhh = -55, gleak = 0.003,
            CaShift = -75., xShift =2.,
            kneadingsStart=100,
            kneadingsEnd=200,
            sweepSize=100
            ):
    kneadingsWeightedSumSet = np.zeros(sweepSize * sweepSize, float)
    lib.sweep(kneadingsWeightedSumSet.ctypes.data_as(ct.POINTER(ct.c_double)),
              voltageBinBoundaries.ctypes.data_as(ct.POINTER(ct.c_double)),
              intervalBinBoundaries.ctypes.data_as(ct.POINTER(ct.c_double)),
              ct.c_double(gMin), ct.c_double(gMax), ct.c_uint(sweepSize),
              ct.c_double(betaMin), ct.c_double(betaMax), ct.c_uint(sweepSize),
              ct.c_double(Iapp), ct.c_double(gh), ct.c_double(Vhh), ct.c_double(gleak),
              ct.c_double(CaShift), ct.c_double(xShift),
              ct.c_double(dt), ct.c_uint(N), ct.c_uint(stride),
              ct.c_uint(len(voltageBinBoundaries)), ct.c_uint(len(intervalBinBoundaries)),
              ct.c_uint(kneadingsStart), ct.c_uint(kneadingsEnd))
    # imshow takes fortran style array input
    return np.reshape(kneadingsWeightedSumSet, (sweepSize, sweepSize), 'F')

#For LZ, use the returned value between 0 to 1 with a grey colormap
#For PC, return the circular hash of the period string, and use it with a custom colormap; If needed, we can also return, say, 6digits from the hash and use 2 each for R,G,B

print 'Running Plant HCO sweep'

colorMapLevels=2**8

blue = np.linspace(0.01, 1, colorMapLevels)
red = 1 - blue
min = 0.;
max = .8
green = min + np.random.random(colorMapLevels) * (max - min)
RGB = np.column_stack((red, green, blue))
customColorMap = colors.ListedColormap(RGB)

blue = np.linspace(0.8, 0, colorMapLevels)
red = blue
green = blue
RGB = np.column_stack((red, green, blue))
customGrayColorMap = colors.ListedColormap(RGB)


figSize = 5
fig = figure(figsize=(figSize, figSize))
outputdir = 'Output/PlantHCO_DiffICVCa_BothCells_PCLZ'
matplotlib.rcParams['savefig.dpi'] = 600


fig.clear()

for gh in ghList:

    filename = outputdir + '_gMin_'+str(gMin)+ '_gMax_'+str(gMax)+ '_betaMin_'+str(betaMin)+ \
               '_betaMax_'+str(betaMax)+'_Iapp_' + str(Iapp) + '_gh_' + str(gh) + '_CaShift_' + str(CaShift) + '_xShift_' + str(xShift) + '_voltageBins_' + str(voltageBinBoundaries) + '_intervalBins_' + str(intervalBinBoundaries) + '_sweepSize_' + str(sweepSize)

    if not dataSaved:
        pickleDataFile1 = gzip.open(filename + '.gz', 'wb')
        sweepData = sweep(dt=dt, N=N, stride=1, gMin=gMin, gMax=gMax, betaMin=betaMin,
                          betaMax=betaMax, Iapp=Iapp, gh=gh, CaShift=CaShift, xShift=xShift,
                          kneadingsStart=kneadingsStart,
                          kneadingsEnd=kneadingsEnd, sweepSize=sweepSize)
        pickle.dump(sweepData, pickleDataFile1)
    else:
        pickleDataFile1 = gzip.open(filename + '.gz', 'rb')
        sweepData = pickle.load(pickleDataFile1)
    pickleDataFile1.close()

    # print sweepData
    # sweepData[sweepData == -1.1] = 0.

    # PC hash can be any integer value (positive/negative), LZ lies between 0 and 1, -0.5 for insufficient N
    sweepDataInsufficientN = masked_array(sweepData, sweepData != -0.5)
    sweepDataComputed = masked_array(sweepData, sweepData == -0.5)
    sweepDataChaotic = masked_array(sweepDataComputed, (sweepData <= 0) | (sweepData >= 1))
    sweepDataPeriodic = masked_array(sweepDataComputed, (sweepData > 0) & (sweepData < 1))

    aMin = gMin; aMax = gMax; bMin = betaMin; bMax = betaMax;
    # to force common color scheme without scaling across images, use the vmin and vmax arguments of imshow(), and make sure they are the same for all your images
    imshow(sweepDataPeriodic,
           extent=[aMin, aMax, bMin, bMax],
           aspect=(aMax - aMin) / (bMax - bMin),
           cmap=customColorMap,
           origin='lower',  # vmin=-1., vmax=0.1
           )  # making vmax 0.1 instead of 0. to match the colormaps for short and long kneadings

    imshow(sweepDataChaotic,
           extent=[aMin, aMax, bMin, bMax],
           aspect=(aMax - aMin) / (bMax - bMin),
           cmap=customGrayColorMap,
           origin='lower')

    imshow(sweepDataInsufficientN,
           extent=[aMin, aMax, bMin, bMax],
           aspect=(aMax - aMin) / (bMax - bMin),
           cmap=colors.ListedColormap(['white']),
           origin='lower')

    xlabel('g');
    ylabel('beta');
    fig.savefig(filename + ".jpg")

    show()


