#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include <sys/time.h>
#include<string.h>
#include<stdbool.h>

#define N_EQ1	14
#define MAX_KNEADING_LENGTH 10000
#define NUM_THREADS_PER_BLOCK 512

#define numberOfOutputMetrics 4
//KneadingSum, burst period, numberOfSpikesInBurst, AverageS

__device__ double fe(double v1, double b, double k){
    return exp((b- 1.21*v1-78.7)/k);
}

__device__ double i1(double v1, double h1){
    return 4*pow((1/(exp((-22-v1)/8)+1)),3)*h1*(30-v1);
}

__device__ double i2(double v1, double n1){
    return 0.3*pow(n1,4)*(-75-v1);
}

__device__ double i3(double v1, double x1){
    return 0.01*x1*(30-v1);
}

__device__ double i4(double v1, double ca1){
    return 0.03*ca1/(0.5 + ca1)*(-75 - v1);
}

__device__ double ileak(double v1){
    return (-40-v1);
}

__device__ void stepper(const double* y, double* dydt, const double* params)
{
    double alpha = params[0], beta =  params[1], g12 = params[2], g21 = params[2], Iapp = params[3], gh = params[4], Vhh = params[5], gleak = params[6], shift_Ca=params[7], shift_x=params[8];

    double V2=y[0], Ca2=y[1], h2=y[2], n2=y[3], x2=y[4], y2=y[5], s2=y[6];
    double V3=y[7], Ca3=y[8], h3=y[9], n3=y[10], x3=y[11], y3=y[12], s3=y[13];

    dydt[0] = 4*pow(((0.1*(50 - (127*V2/105 + 8265/105))/(exp((50 - (127*V2/105 + 8265/105))/10) - 1))/((0.1*(50 - (127*V2/105 + 8265/105))/(exp((50 - (127*V2/105 + 8265/105))/10) - 1))+(4*exp((25 - (127*V2/105 + 8265/105))/18)))),3)*h2*(30 - V2)
                + i2(V2,n2)
                + i3(V2,x2)
                + i4(V2, Ca2)
                + gleak*ileak(V2)
                + gh*pow((1/(1+exp(-(V2+63)/7.8))),3)*y2*(-V2+120)
                + Iapp
                + g21*(-55 - V2)*s3;
    dydt[1] = 0.00015*(0.0085*x2*(140 - V2+shift_Ca)-Ca2);
    dydt[2] = ((1-h2)*(0.07*exp((25 - (127*V2/105 + 8265/105))/20))-h2*(1.0/(1 + exp((55 - (127*V2/105 + 8265/105))/10))))/12.5;
    dydt[3] = ((1-n2)*(0.01*(55 - (127*V2/105 + 8265/105))/(exp((55 - (127*V2/105 + 8265/105))/10) - 1))-n2*(0.125*exp((45 - (127*V2/105 + 8265/105))/80)))/12.5;
    dydt[4] =  ((1/(exp(0.15*(-V2-50+shift_x)) + 1))-x2)/100;
    dydt[5] = .5*((1/(1+exp(10*(V2-Vhh))))-y2)/(7.1+10.4/(1+exp((V2+68)/2.2)));
    dydt[6] = alpha*(1-s2)/(1+exp(-10*(V2+20)))-beta*s2/(1+exp(10*(V2+20)));


    dydt[7] = 4*pow(((0.1*(50 - (127*V3/105 + 8265/105))/(exp((50 - (127*V3/105 + 8265/105))/10) - 1))/((0.1*(50 - (127*V3/105 + 8265/105))/(exp((50 - (127*V3/105 + 8265/105))/10) - 1))+(4*exp((25 - (127*V3/105 + 8265/105))/18)))),3)*h3*(30 - V3)
                + i2(V3,n3)
                + i3(V3,x3)
                + i4(V3, Ca3)
                + gleak*ileak(V3)
                + gh*pow((1/(1+exp(-(V3+63)/7.8))),3)*y3*(-V3+120)
                + Iapp
                + g12*(-55 - V3)*s2;
    dydt[8] = 0.00015*(0.0085*x3*(140 - V3+shift_Ca)-Ca3);
    dydt[9] = ((1-h3)*(0.07*exp((25 - (127*V3/105 + 8265/105))/20))-h3*(1.0/(1 + exp((55 - (127*V3/105 + 8265/105))/10))))/12.5;
    dydt[10] = ((1-n3)*(0.01*(55 - (127*V3/105 + 8265/105))/(exp((55 - (127*V3/105 + 8265/105))/10) - 1))-n3*(0.125*exp((45 - (127*V3/105 + 8265/105))/80)))/12.5;
    dydt[11] =  ((1/(exp(0.15*(-V3-50+shift_x)) + 1))-x3)/100;
    dydt[12] = .5*((1/(1+exp(10*(V3-Vhh))))-y3)/(7.1+10.4/(1+exp((V3+68)/2.2)));
    dydt[13] = alpha*(1-s3)/(1+exp(-10*(V3+20)))-beta*s3/(1+exp(10*(V3+20)));

}

__device__ void computeFirstDerivative(const double *y, double *dydt, const double* params){
	stepper(y, dydt, params);
	return;
}

//from: https://github.com/benjaminfrot/LZ76/blob/master/C/LZ76.c
__device__ double LZ76(const char * s, const int n) {
  int c=1,l=1,i=0,k=1,kmax = 1,stop=0;
  while(stop ==0) {
    if (s[i+k-1] != s[l+k-1]) {
      if (k > kmax) {
        kmax=k;
      }
      i++;

      if (i==l) {
        c++;
        l += kmax;
        if (l+1>n)
          stop = 1;
        else {
          i=0;
          k=1;
          kmax=1;
        }
      } else {
        k=1;
      }
    } else {
      k++;
      if (l+k > n) {
        c++;
        stop =1;
      }
    }
  }
  return double(c)/n;
}



__device__ unsigned NextCoprime(unsigned* coPrimes,unsigned x){
    coPrimes[x] = (coPrimes[x] - x) * coPrimes[x] + x;
    return coPrimes[x];
}

//https://stackoverflow.com/questions/40303333/how-to-replicate-java-hashcode-in-c-language
__device__ unsigned javaHashCode(const unsigned x) {
    int hash = 0, length = floor(log10(fabs(x+0.))) + 1;
    for(int i=0; i<length; i++){
        hash = 31*hash+(x%int(pow(10.,i+0.)));
    }
    return hash;
}

__device__ unsigned NextPair(unsigned* coPrimes, const unsigned a, const unsigned b, const unsigned x, const unsigned y){
    return a * NextCoprime(coPrimes, x) * javaHashCode(x) + b * javaHashCode(y);
}

// Need circular hash function : https://stackoverflow.com/questions/2585092/is-there-a-circular-hash-function
// https://mathoverflow.net/questions/135297/general-euclid-fermat-sequences
//This function can return either positive or negative values
__device__ double circularHash(const char *kneadings, const unsigned periodLength)
{
    int H = 0;

    if (periodLength > 0)
    {
        //any arbitrary coprime numbers
        unsigned a = periodLength, b = periodLength + 1;

        //an array of Euclid-Fermat sequences to generate additional coprimes for each duplicate character occurrence
        unsigned noOfCoprimes = 255; //see if this is enough or more are needed
        unsigned coPrimes[255];

        for (int i = 1; i < noOfCoprimes; i++)
        {
            coPrimes[i] = i + 1;
        }

        //for i=0 we need to wrap around to the last character
        H = NextPair(coPrimes, a, b, kneadings[periodLength - 1], kneadings[0]);

        //for i=1...n we use the previous character
        for (int i = 1; i < periodLength; i++)
        {
            H ^= NextPair(coPrimes, a, b, kneadings[i - 1], kneadings[i]);
        }
    }
    //printf("\nCircular hash : %d\n",H);

    //return double(H);
    //H can be any integer; In order to not conflict with LZ, we make sure we return a value greater than 1 or less than -1, so LZ can be between 0 and 1, and -0.5 can be used as marker for insufficient N.
    if(H>=0) return double(H+1);
    else return double(H-1);
}


__device__ double periodicPCorChaoticLZ(const char* kneadings,const  unsigned kneadingsLength){
    //After a long transient when the periodic orbits if any have already been reached, this method computes a kneading sum that is invariant between different cyclic permutations of the period

    bool periodFound=true;
    unsigned periodLength = kneadingsLength;
    for(unsigned currPeriod=1; currPeriod < kneadingsLength/2; currPeriod++) {
        periodFound=true;
        //Check if the kneading sequence has a period with periodicity of currPeriod
        for(unsigned i=currPeriod; i < kneadingsLength-currPeriod; i+=currPeriod) {
            for ( unsigned j=0; j<currPeriod; j++) {
                if(kneadings[j] != kneadings[i+j]) {
                    periodFound=false;
                    break;
                }
            }
            if(!periodFound) {
                break;
            }
        }
        //compute kneadingSum based on period found, if any
        if(periodFound) {
            //currPeriod is the period of the kneading sequence. So this will be normalized to a sequence with the sorted period(0's followed by 1's)
            periodLength = currPeriod;
            break;
        }
    }
    //return periodLength==kneadingsLength? LZ76(kneadings, kneadingsLength) : -computePeriodNormalizedKneadingSum(kneadings, kneadingsLength, periodLength);
    //return periodLength==kneadingsLength? LZ76(kneadings, kneadingsLength) : -periodLength;

    //printf("\nPeriod Length: %d\n", periodLength);
    return periodLength==kneadingsLength? LZ76(kneadings, kneadingsLength) : -circularHash(kneadings, periodLength);

}


__device__ char getBinSymbol(const double value, const double* binBoundariesGpu, const double numberOfBinBoundaries, const char startingChar){
    unsigned i;

    if(numberOfBinBoundaries==0) return startingChar;
    for(i=0;i<numberOfBinBoundaries;i++){
        if(value < binBoundariesGpu[i])
            return char(startingChar+i);
    }
    return char(startingChar+i);
}

__device__ void integrator_rk4(double* outputMetrics, double* y_current, const double* voltageBinBoundariesGpu, const double* intervalBinBoundariesGpu, const double* params, const double dt, const unsigned N, const unsigned stride, const unsigned nV, const unsigned nI, const unsigned kneadingsStart, const unsigned kneadingsEnd)
{
	unsigned i, j, k, kneadingIndex=0, kneadingArrayIndex=0, previousEventCell1=0, previousEventCell2=0;
	double dt2, dt6;
	double y1[N_EQ1], y2[N_EQ1], k1[N_EQ1], k2[N_EQ1], k3[N_EQ1], k4[N_EQ1];
	double firstDerivativeCurrent[N_EQ1],firstDerivativePreviousCell1, firstDerivativePreviousCell2;
	double kneadingsWeightedSum=0;
    char kneadings[MAX_KNEADING_LENGTH], lastSymbolCell1, lastSymbolCell2;
    int simultaneousEventsTimeGap = 50, firstBurstTime = 0, lastBurstTime=0, numberOfBursts=-1, spikesUptoLastBurst=0, totalSpikes=0;
    double SUptoLastBurst=0, totalS=0;

	dt2 = dt/2.; dt6 = dt/6.;

	for(i=1; i<N; i++)
	{
		for(j=0; j<stride; j++)
		{
			stepper(y_current, k1, params);
			for(k=0; k<N_EQ1; k++) y1[k] = y_current[k]+k1[k]*dt2;
			stepper(y1, k2, params);
			for(k=0; k<N_EQ1; k++) y2[k] = y_current[k]+k2[k]*dt2;
			stepper(y2, k3, params);
			for(k=0; k<N_EQ1; k++) y2[k] = y_current[k]+k3[k]*dt;
			stepper(y2, k4, params);

			//Copy latest value into y_current
			for(k=0; k<N_EQ1; k++) y_current[k] += dt6*(k1[k]+2.*(k2[k]+k3[k])+k4[k]);
		}

		computeFirstDerivative(y_current, firstDerivativeCurrent, params);

        if(numberOfBursts>=0){
            totalS += y_current[13];
        }

        if(firstDerivativePreviousCell1 > 0 && firstDerivativeCurrent[0] < 0 && y_current[0] > voltageBinBoundariesGpu[nV-1]) {
            if(kneadingIndex<kneadingsStart){
                //skip ahead upto the kneadingsStart symbol
                kneadingIndex++;
            } else {
                char c = getBinSymbol(dt * (i - previousEventCell1), intervalBinBoundariesGpu, nI, 'a');
                if(kneadingArrayIndex==0 || (c != lastSymbolCell1)){
                    //multiple spikes in a burst will be shown by just one symbol at its start
                    kneadings[kneadingArrayIndex++] = c;
                    lastSymbolCell1 = c;
                    if(dt * (i - previousEventCell1) > intervalBinBoundariesGpu[nI-1]){
                        numberOfBursts++;
                        if(numberOfBursts==0){
                            firstBurstTime = i;
                        }else if(numberOfBursts>0){

                        }
                        lastBurstTime = i;
                        spikesUptoLastBurst = totalSpikes;
                        SUptoLastBurst = totalS;
                    }
                }
                if(numberOfBursts>=0) totalSpikes++;
                kneadingIndex++;
            }
            previousEventCell1 = i;
            //TODO issues when events in both cells seem to be occurring very close to each other so they sometimes alternate between two repeats of the period, so symbolic sequences are also altered, hence periodic sequence can't be detected.. at the beginning of the bursts in phase .. might not matter for HCO, but might be required for 4 cell network where two burst in phase
        }
		firstDerivativePreviousCell1 = firstDerivativeCurrent[0];



        if(firstDerivativePreviousCell2 > 0 && firstDerivativeCurrent[7] < 0 && y_current[7] > voltageBinBoundariesGpu[nV-1]) {
            if(kneadingIndex<kneadingsStart){
                //skip ahead upto the kneadingsStart symbol
                kneadingIndex++;
            } else {
                char c = getBinSymbol(dt * (i - previousEventCell2), intervalBinBoundariesGpu, nI, 'A');
                if(kneadingArrayIndex==0 || c != lastSymbolCell2){
                    //multiple spikes in a burst will be shown by just one symbol at its start
                    kneadings[kneadingArrayIndex++] = c;
                    lastSymbolCell2 = c;
                }
                kneadingIndex++;
            }
            previousEventCell2 = i;
        }
		firstDerivativePreviousCell2 = firstDerivativeCurrent[7];

		if(kneadingIndex>kneadingsEnd){
            //print test
            //kneadings[kneadingArrayIndex++]=char('\0'); printf("%lf %lf \n %s \t length: %d \n", params[0], params[1], kneadings, kneadingArrayIndex); return;

            //KneadingSum, burst period of cell 1, spikes per burst of cell 1, AverageS of cell 2
			outputMetrics[0] = periodicPCorChaoticLZ(kneadings, kneadingArrayIndex);
			outputMetrics[1] = numberOfBursts==0?0:(lastBurstTime-firstBurstTime)*dt/numberOfBursts; //Average burst period
			outputMetrics[2] = numberOfBursts==0?0:spikesUptoLastBurst/numberOfBursts; // Average spikes per burst
            outputMetrics[3] = numberOfBursts==0?0:SUptoLastBurst/(lastBurstTime-firstBurstTime); // Average S per burst
			return;

    	    //return periodicPCorChaoticLZ(kneadings, kneadingArrayIndex);
			//return LZ(kneadings, kneadingArrayIndex); //temporarily only using LZ
		}
	}
	//print test
	//printf("%lf %lf %s \t length: %d\n", params[0], params[1], kneadings, kneadingIndex); //return 0;

	outputMetrics[0] = -0.5;outputMetrics[1] = -0.5;outputMetrics[2] = -0.5;outputMetrics[3] = -0.5;
	return;
	//return -0.5;
}

__global__ void sweepThreads(double* kneadingsWeightedSumSetGpu, const double* voltageBinBoundariesGpu, const double* intervalBinBoundariesGpu,
                                        const double alphaStart, const double alphaEnd, const unsigned alphaCount,
										const double betaStart, const double betaEnd, const unsigned betaCount,
										const double g,
										const double Iapp, const double gh, const double Vhh, const double gleak,
										const double CaShift, const double xShift,
										const double dt, const unsigned N, const unsigned stride,
										const unsigned nV, const unsigned nI,
										const unsigned kneadingsStart, const unsigned kneadingsEnd){

    int tx=blockIdx.x * blockDim.x + threadIdx.x;

	double params[9],a,b;
	double alphaStep = alphaCount==1?0:(alphaEnd - alphaStart)/(alphaCount-1);
	double betaStep = betaCount==1?0:(betaEnd-betaStart)/(betaCount -1);
	int i,j,k;


    if(tx<alphaCount*betaCount){

        i=tx/alphaCount;
        j=tx%alphaCount;

        params[0]=alphaStart+i*alphaStep;
        params[1]=betaStart+j*betaStep;
        params[2]=g;
        params[3]=Iapp;
        params[4]=gh;
        params[5]=Vhh;
        params[6]=gleak;
        params[7]=CaShift;
        params[8]=xShift;


        //V2=y[0], Ca2=y[1], h2=y[2], n2=y[3], x2=y[4], y2=y[5], s2=y[6]
      	double y_initial[N_EQ1]={-39.99, 0.5, 0.5, 0.2, 0.7, 0.2, 0., 19.99, 0.9, 0.5, 0.2, 0.7, 0.2, 0.};

        integrator_rk4(kneadingsWeightedSumSetGpu+(i*alphaCount+j)*numberOfOutputMetrics, y_initial, voltageBinBoundariesGpu, intervalBinBoundariesGpu, params, dt, N, stride, nV, nI, kneadingsStart, kneadingsEnd);

    }
}

/**
 * error checking routine
 */
void checkAndDisplayErrors(char *label)
{
  // we need to synchronise first to catch errors due to
  // asynchronous operations that would otherwise
  // potentially go unnoticed

  cudaError_t err;

  err = cudaThreadSynchronize();
  if (err != cudaSuccess)
  {
    char *e = (char*) cudaGetErrorString(err);
    fprintf(stderr, "CUDA Error: %s (at %s)", e, label);
  }

  err = cudaGetLastError();
  if (err != cudaSuccess)
  {
    char *e = (char*) cudaGetErrorString(err);
    fprintf(stderr, "CUDA Error: %s (at %s)", e, label);
  }
}


extern "C" {

void sweep(double* kneadingsWeightedSumSet,const double* voltageBinBoundaries,const double* intervalBinBoundaries,
                                        const double alphaStart, const double alphaEnd, const unsigned alphaCount,
										const double betaStart, const double betaEnd, const unsigned betaCount,
										const double g,
										const double Iapp, const double gh, const double Vhh, const double gleak,
										const double CaShift, const double xShift,
										const double dt, const unsigned N, const unsigned stride,
										const unsigned nV, const unsigned nI,
										const unsigned kneadingsStart, const unsigned kneadingsEnd){

        int totalParameterSpaceSize = alphaCount*betaCount;
        double *kneadingsWeightedSumSetGpu; //// using this not just for kneadingSum; for each point in the sweep we compute: KneadingSum, burst period, numberOfSpikesInBurst, AverageS values for the trajectory and put everything in this array sequentially

        double *voltageBinBoundariesGpu, *intervalBinBoundariesGpu;

        printf("Before malloc gpu\n");
        /*allocate device memory. */
        (cudaMalloc( (void**) &kneadingsWeightedSumSetGpu, totalParameterSpaceSize*sizeof(double)*numberOfOutputMetrics));
        (cudaMalloc( (void**) &voltageBinBoundariesGpu, nV*sizeof(double)));
        (cudaMalloc( (void**) &intervalBinBoundariesGpu, nI*sizeof(double)));
        checkAndDisplayErrors("Memory allocation");
        printf("After malloc gpu\n");

        /*copy data from host to device */
        (cudaMemcpy( voltageBinBoundariesGpu, voltageBinBoundaries, nV*sizeof(double), cudaMemcpyHostToDevice));
        (cudaMemcpy( intervalBinBoundariesGpu, intervalBinBoundaries, nI*sizeof(double), cudaMemcpyHostToDevice));
        checkAndDisplayErrors("Error while copying bin boundaries from host to device.");

        /*timing*/
        cudaEvent_t start_event, stop_event;
        cudaEventCreate(&start_event) ;
        cudaEventCreate(&stop_event) ;
        cudaEventRecord(start_event, 0);

        /*Dimensions. */

        int gridXDimension = totalParameterSpaceSize/NUM_THREADS_PER_BLOCK;
        if(totalParameterSpaceSize%NUM_THREADS_PER_BLOCK!=0) {
                gridXDimension += 1;
        }
        dim3 dimGrid(gridXDimension,1);
        dim3 dimBlock(NUM_THREADS_PER_BLOCK,1);

        printf(" Num of blocks per grid:       %d\n", gridXDimension);
        printf(" Num of threads per block:     %d\n", NUM_THREADS_PER_BLOCK);
        printf(" Total Num of threads running: %d\n", gridXDimension*NUM_THREADS_PER_BLOCK);
        printf(" Parameters alphaCount=%d, betaCount=%d\n",alphaCount,betaCount);

        /*Call kernel(global function)*/
        sweepThreads<<<dimGrid, dimBlock>>>(kneadingsWeightedSumSetGpu, voltageBinBoundariesGpu, intervalBinBoundariesGpu, alphaStart, alphaEnd, alphaCount, betaStart, betaEnd, betaCount, g, Iapp, gh, Vhh, gleak, CaShift, xShift, dt, N, stride, nV, nI, kneadingsStart, kneadingsEnd);

        cudaThreadSynchronize();
        cudaEventRecord(stop_event, 0);
        cudaEventSynchronize(stop_event);

        float time_kernel;
        cudaEventElapsedTime(&time_kernel, start_event, stop_event);
        printf("Total time(sec) %f\n", time_kernel/1000);

        /*copy data from device memory to memory. */
        (cudaMemcpy( kneadingsWeightedSumSet, kneadingsWeightedSumSetGpu, totalParameterSpaceSize*sizeof(double)*numberOfOutputMetrics, cudaMemcpyDeviceToHost));
        checkAndDisplayErrors("Error while copying kneading sum values from device to host.");

        /*Free all allocated memory. */
        cudaFree(kneadingsWeightedSumSetGpu);
        cudaFree(voltageBinBoundariesGpu);
        cudaFree(intervalBinBoundariesGpu);

}

}


int main(){
	double dt=1;
	unsigned N=300000*10;
	unsigned stride = 1;
	unsigned maxKneadings = 2500;
	unsigned sweepSize = 5;
	double *kneadingsWeightedSumSet;
	int i,j;
	//double voltageBinBoundaries[] = {-60., -40., 10.}, intervalBinBoundaries [] = {};
    double voltageBinBoundaries[] = {10}, intervalBinBoundaries [] = {2500};

	printf("Enter sweep size :");
	scanf("%d",&sweepSize);
	kneadingsWeightedSumSet = (double *)malloc(sweepSize*sweepSize*sizeof(double));

	sweep(kneadingsWeightedSumSet, voltageBinBoundaries, intervalBinBoundaries, 0.06, 0.06, sweepSize, 0.06, 0.06, sweepSize, 0.1, 0., 0., -55., 0.003, -45, -3., dt, N, stride, 1, 1, 500, 1000);

	for(i=0;i<sweepSize;i++){
	    for(j=0;j<sweepSize;j++){
	        printf("%f \t",kneadingsWeightedSumSet[i*sweepSize+j]);
	    }
	    printf("\n");
	}

}

