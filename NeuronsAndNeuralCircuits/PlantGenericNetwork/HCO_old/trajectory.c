#include <sys/time.h>
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include <sys/time.h>
#include<string.h>
#include<stdbool.h>

#define N_EQ1	14
#define MAX_KNEADING_LENGTH 10000
#define NUM_THREADS_PER_BLOCK 512

double fe(double v1, double b, double k){
    return exp((b- 1.21*v1-78.7)/k);
}

double i1(double v1, double h1){
    return 4*pow((1/(exp((-22-v1)/8)+1)),3)*h1*(30-v1);
}

double i2(double v1, double n1){
    return 0.3*pow(n1,4)*(-75-v1);
}

double i3(double v1, double x1){
    return 0.01*x1*(30-v1);
}

double i4(double v1, double ca1){
    return 0.03*ca1/(0.5 + ca1)*(-75 - v1);
}

double ileak(double v1){
    return (-40-v1);
}

void stepper(const double* y, double* dydt, const double* params)
{
	double shift_Ca=params[0], shift_x=params[1], Iapp = params[2], gh = params[3], Vhh = params[4], gleak = params[5],
	            g12 = params[6], g21 = params[6];

    double V2=y[0], Ca2=y[1], h2=y[2], n2=y[3], x2=y[4], y2=y[5], s2=y[6];
    double V3=y[7], Ca3=y[8], h3=y[9], n3=y[10], x3=y[11], y3=y[12], s3=y[13];
    double alpha = params[7], beta =  params[8];

    dydt[0] = 4*pow(((0.1*(50 - (127*V2/105 + 8265/105))/(exp((50 - (127*V2/105 + 8265/105))/10) - 1))/((0.1*(50 - (127*V2/105 + 8265/105))/(exp((50 - (127*V2/105 + 8265/105))/10) - 1))+(4*exp((25 - (127*V2/105 + 8265/105))/18)))),3)*h2*(30 - V2)
                + i2(V2,n2)
                + i3(V2,x2)
                + i4(V2, Ca2)
                + gleak*ileak(V2)
                + gh*pow((1/(1+exp(-(V2+63)/7.8))),3)*y2*(-V2+120)
                + Iapp
                + g21*(-55 - V2)*s3;
    dydt[1] = 0.00015*(0.0085*x2*(140 - V2+shift_Ca)-Ca2);
    dydt[2] = ((1-h2)*(0.07*exp((25 - (127*V2/105 + 8265/105))/20))-h2*(1.0/(1 + exp((55 - (127*V2/105 + 8265/105))/10))))/12.5;
    dydt[3] = ((1-n2)*(0.01*(55 - (127*V2/105 + 8265/105))/(exp((55 - (127*V2/105 + 8265/105))/10) - 1))-n2*(0.125*exp((45 - (127*V2/105 + 8265/105))/80)))/12.5;
    dydt[4] =  ((1/(exp(0.15*(-V2-50+shift_x)) + 1))-x2)/100;
    dydt[5] = .5*((1/(1+exp(10*(V2-Vhh))))-y2)/(7.1+10.4/(1+exp((V2+68)/2.2)));
    dydt[6] = alpha*(1-s2)/(1+exp(-10*(V2+20)))-beta*s2/(1+exp(10*(V2+20)));


    dydt[7] = 4*pow(((0.1*(50 - (127*V3/105 + 8265/105))/(exp((50 - (127*V3/105 + 8265/105))/10) - 1))/((0.1*(50 - (127*V3/105 + 8265/105))/(exp((50 - (127*V3/105 + 8265/105))/10) - 1))+(4*exp((25 - (127*V3/105 + 8265/105))/18)))),3)*h3*(30 - V3)
                + i2(V3,n3)
                + i3(V3,x3)
                + i4(V3, Ca3)
                + gleak*ileak(V3)
                + gh*pow((1/(1+exp(-(V3+63)/7.8))),3)*y3*(-V3+120)
                + Iapp
                + g12*(-55 - V3)*s2;
    dydt[8] = 0.00015*(0.0085*x3*(140 - V3+shift_Ca)-Ca3);
    dydt[9] = ((1-h3)*(0.07*exp((25 - (127*V3/105 + 8265/105))/20))-h3*(1.0/(1 + exp((55 - (127*V3/105 + 8265/105))/10))))/12.5;
    dydt[10] = ((1-n3)*(0.01*(55 - (127*V3/105 + 8265/105))/(exp((55 - (127*V3/105 + 8265/105))/10) - 1))-n3*(0.125*exp((45 - (127*V3/105 + 8265/105))/80)))/12.5;
    dydt[11] =  ((1/(exp(0.15*(-V3-50+shift_x)) + 1))-x3)/100;
    dydt[12] = .5*((1/(1+exp(10*(V3-Vhh))))-y3)/(7.1+10.4/(1+exp((V3+68)/2.2)));
    dydt[13] = alpha*(1-s3)/(1+exp(-10*(V3+20)))-beta*s3/(1+exp(10*(V3+20)));

}

void computeFirstDerivative(const double *y, double *dydt, const double* params){
	stepper(y, dydt, params);
	return;
}

double integrator_rk4(double* y_current, double *output, const double* params, const double dt, const unsigned N, const unsigned stride)
{
	unsigned i, j, k, kneadingIndex=0;
	double dt2, dt6;
	double y1[N_EQ1], y2[N_EQ1], k1[N_EQ1], k2[N_EQ1], k3[N_EQ1], k4[N_EQ1];
	double firstDerivativeCurrent[N_EQ1],firstDerivativePrevious;

	dt2 = dt/2.; dt6 = dt/6.;

	for(i=1; i<N; i++)
	{
		for(j=0; j<stride; j++)
		{
			stepper(y_current, k1, params);
			for(k=0; k<N_EQ1; k++) y1[k] = y_current[k]+k1[k]*dt2;
			stepper(y1, k2, params);
			for(k=0; k<N_EQ1; k++) y2[k] = y_current[k]+k2[k]*dt2;
			stepper(y2, k3, params);
			for(k=0; k<N_EQ1; k++) y2[k] = y_current[k]+k3[k]*dt;
			stepper(y2, k4, params);

			//Copy latest value into y_current
			for(k=0; k<N_EQ1; k++) y_current[k] += dt6*(k1[k]+2.*(k2[k]+k3[k])+k4[k]);
		}
        for(k=0; k<N_EQ1; k++) output[i*N_EQ1+k] = y_current[k];
	}
	return 0;
}

int main(){
    /*
	double y_initial[]={0.01,0.,0.,0.,0.,0.};
	double dt=0.01;
	unsigned N=30000;
	unsigned stride = 1;
	unsigned maxKneadings = 20;
	unsigned sweepSize = 2;
	double g = 50, sigma = 1.5;
	double *kneadingsWeightedSumSet;
	double alpha;
	int i,j;
	printf("Enter alpha :");
	scanf("%lf",&alpha);
	kneadingsWeightedSumSet = (double *)malloc(sweepSize*sweepSize*sizeof(double));
	*/
	/*
	for(i=0;i<sweepSize;i++){
	    for(j=0;j<sweepSize;j++){
	        printf("%f \n",kneadingsWeightedSumSet[i*sweepSize+j]);
	    }
	}
	*/
}
