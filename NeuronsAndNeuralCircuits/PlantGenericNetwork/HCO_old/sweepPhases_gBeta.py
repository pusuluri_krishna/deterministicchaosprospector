import matplotlib

matplotlib.use('Qt4Agg')
#matplotlib.use('Agg')
import ctypes as ct
from pylab import *
import numpy as np
from matplotlib import colors
from numpy.ma import masked_array
import pickle
import gzip

dataSaved = True;
kneadingsStart=500; kneadingsEnd=1000;  #If using both cells' events
#kneadingsStart=1000; kneadingsEnd=2000; #Equivalent to above if using only single cell's events. In C comment out event computation of the other cell

#Full sweep
alphaMin = 0.; alphaMax = 0.05;
betaMin = 0.; betaMax = 0.05;
gh=0.;
N = 300000*10; dt=1;
Iapp = 0.;
nDataPointsToShow = 5;

#sweeps do not show any bursting in regions that are far away from the border between tonic spiking and quiescent.. so even though they don't have to be inherent bursters, looks like the inherently tonic spiking cells need to be close to the boundary either to quiescent or to bursting in order to produce bursting emergent network bursting..
sweepSize = 100;
CaShift_xShift_g_array = [[-45., -3., 0.1],[-40., -3., 0.1],[-35., -3., 0.1],[-30., -3., 0.1],
                          #[-45., -3., 0.2], [-40., -3., 0.2],[-30., -3., 0.2], [-30., -3., 0.2]
                         ]

# Adjust these bins to change precision depending on the system; Bins do not have to be equally sized; Have more bins in the region where more precision is required and less bins where its not; Using 10 bins for a start (9 bin boundaries); Best represented by char to compute PC, may also be represented by the digits 0 to 9 if number of bins < 10

#voltageBinBoundaries = np.array([-60., -40., 10.], dtype='float64')
#voltageBinBoundaries = np.array([], dtype='float64')
voltageBinBoundaries = np.array([10.], dtype='float64')

#intervalBinBoundaries = np.array([100], dtype='float64')
intervalBinBoundaries = np.array([2500], dtype='float64')

# arrays must be created with double values not integers (default) since that's how it is passed to C function later;
#  Otherwise giving errors and memory copied incorrectly

lib = ct.cdll.LoadLibrary('./sweepPhases_PCLZ_gBeta.so')
lib.sweep.argtypes = [ct.POINTER(ct.c_double),
                        ct.POINTER(ct.c_double), ct.POINTER(ct.c_double),
                        ct.c_double, ct.c_double, ct.c_uint,
                        ct.c_double, ct.c_double, ct.c_uint,
                        ct.c_double,
                        ct.c_double, ct.c_double, ct.c_double, ct.c_double,
                        ct.c_double, ct.c_double,
                        ct.c_double, ct.c_uint, ct.c_uint,
                        ct.c_uint, ct.c_uint,
                        ct.c_uint, ct.c_uint]

def sweep(dt=1, N=300000, stride=1,
            alphaMin=-10., alphaMax=5.,
            betaMin=-2.5, betaMax=-1.5,
            g = 0.1,
            Iapp = 0., gh = 0., Vhh = -55, gleak = 0.003,
            CaShift = -75., xShift =2.,
            kneadingsStart=100,
            kneadingsEnd=200,
            sweepSize=100
            ):
    kneadingsWeightedSumSet = np.zeros(sweepSize * sweepSize * 4, float)
    # using this not just for kneadingSum; for each point in the sweep we KneadingSum, burst period of cell 1, spikes per burst of cell 1, AverageS of cell 2 for the trajectory and put everything in this array sequentially
    lib.sweep(kneadingsWeightedSumSet.ctypes.data_as(ct.POINTER(ct.c_double)),
              voltageBinBoundaries.ctypes.data_as(ct.POINTER(ct.c_double)),
              intervalBinBoundaries.ctypes.data_as(ct.POINTER(ct.c_double)),
              ct.c_double(alphaMin), ct.c_double(alphaMax), ct.c_uint(sweepSize),
              ct.c_double(betaMin), ct.c_double(betaMax), ct.c_uint(sweepSize),
              ct.c_double(g),
              ct.c_double(Iapp), ct.c_double(gh), ct.c_double(Vhh), ct.c_double(gleak),
              ct.c_double(CaShift), ct.c_double(xShift),
              ct.c_double(dt), ct.c_uint(N), ct.c_uint(stride),
              ct.c_uint(len(voltageBinBoundaries)), ct.c_uint(len(intervalBinBoundaries)),
              ct.c_uint(kneadingsStart), ct.c_uint(kneadingsEnd))
    # imshow takes fortran style array input
    return np.reshape(kneadingsWeightedSumSet, (4, sweepSize, sweepSize), 'F')

#For LZ, use the returned value between 0 to 1 with a grey colormap
#For PC, return the circular hash of the period string, and use it with a custom colormap; If needed, we can also return, say, 6digits from the hash and use 2 each for R,G,B

print 'Running Plant HCO sweep'

colorMapLevels=2**8

blue = np.linspace(0.01, 1, colorMapLevels)
red = 1 - blue
min = 0.;
max = .8
green = min + np.random.random(colorMapLevels) * (max - min)
RGB = np.column_stack((red, green, blue))
customColorMap = colors.ListedColormap(RGB)

blue = np.linspace(0., 1., colorMapLevels)
red = blue
green = blue
RGB = np.column_stack((red, green, blue))
customGrayColorMap = colors.ListedColormap(RGB)


figSize = 5
fig = figure(figsize=(figSize, figSize))
outputdir = 'Output/PlantHCO_DiffICVCa_BothCells_PCLZ_phase_'
matplotlib.rcParams['savefig.dpi'] = 600


for CaShift, xShift, g in CaShift_xShift_g_array:
    fig.clear()

    filename = outputdir + '_alphaMin_' + str(alphaMin) + '_alphaMax_' + str(alphaMax) + '_betaMin_' + str(betaMin) + \
               '_betaMax_' + str(betaMax) + '_g_'+ str(g) +'_Iapp_' + str(Iapp) + '_gh_' + str(gh) + '_CaShift_' + str(
        CaShift) + '_xShift_' + str(xShift) + '_voltageBins_' + str(voltageBinBoundaries) + '_intervalBins_' + str(
        intervalBinBoundaries) + '_sweepSize_' + str(sweepSize)

    if not dataSaved:
        pickleDataFile1 = gzip.open(filename + '.gz', 'wb')
        sweepData = sweep(dt=dt, N=N, stride=1, alphaMin=alphaMin, alphaMax=alphaMax, betaMin=betaMin,
                          betaMax=betaMax, g=g, Iapp=Iapp, gh=gh, CaShift=CaShift, xShift=xShift,
                          kneadingsStart=kneadingsStart,
                          kneadingsEnd=kneadingsEnd, sweepSize=sweepSize)
        pickle.dump(sweepData, pickleDataFile1)
    else:
        pickleDataFile1 = gzip.open(filename + '.gz', 'rb')
        sweepData = pickle.load(pickleDataFile1)
    # print sweepData
    pickleDataFile1.close()

    # print sweepData
    # sweepData[sweepData == -1.1] = 0.

    # PC hash can be any integer value (positive/negative), LZ lies between 0 and 1, -0.5 for insufficient N
    sweepDataInsufficientN = masked_array(sweepData[0], sweepData[0] != -0.5)
    sweepDataComputed = masked_array(sweepData[0], sweepData[0] == -0.5)

    sweepDataChaotic = masked_array(sweepDataComputed, (sweepData[0] <= 0) | (sweepData[0] >= 1))
    # for tonic spiking, LZ complexity could be 1 or 2 depending on whether both or just one fire, so %1.
    # sweepDataChaotic = masked_array(sweepDataComputed, sweepData <= 0)  ## for tonic spiking,

    sweepDataPeriodic = masked_array(sweepDataComputed, (sweepData[0] > 0) & (sweepData[0] < 1))
    # sweepDataPeriodic = masked_array(sweepDataComputed, sweepData > 0)

    aMin = alphaMin;
    aMax = alphaMax;
    bMin = betaMin;
    bMax = betaMax;
    # to force common color scheme without scaling across images, use the vmin and vmax arguments of imshow(), and make sure they are the same for all your images


    # imshow(sweepDataPeriodic, ##hash seems to have a large range causing unclear figures. Due to this, tonic spiking
    # and bursting are shown in same color for 1500 and above, although running a single trajectory sweep prints them
    # differently.. for tonic spiking there is either 1 or 2 symbols depending on whether one or both spike tonically..
    # so LZ complexity not very meaningful.. for one symbol might just show it as periodic
    imshow(sweepDataPeriodic.astype(np.int64) % colorMapLevels,
           extent=[aMin, aMax, bMin, bMax],
           aspect=(aMax - aMin) / (bMax - bMin),
           cmap=customColorMap,
           origin='lower',  # vmin=-1., vmax=0.1
           )  # making vmax 0.1 instead of 0. to match the colormaps for short and long kneadings
    # colorbar()

    imshow(sweepDataChaotic % 1.,
           # for tonic spiking, LZ complexity could be 1 or 2 depending on whether both or just one fire, so %1.
           extent=[aMin, aMax, bMin, bMax],
           aspect=(aMax - aMin) / (bMax - bMin),
           cmap=customGrayColorMap,
           origin='lower')

    imshow(sweepDataInsufficientN,
           extent=[aMin, aMax, bMin, bMax],
           aspect=(aMax - aMin) / (bMax - bMin),
           cmap=colors.ListedColormap(['white']),
           origin='lower')

    xlabel('alpha');
    ylabel('beta');
    fig.suptitle("DCP"+": CaShift ="+str(CaShift)+", xShift = "+str(xShift)+", g = "+str(g));
    fig.savefig(filename + ".jpg")

    sweepStep = sweepSize / (nDataPointsToShow)
    aStep = (aMax - aMin) / (nDataPointsToShow)
    bStep = (bMax - bMin) / (nDataPointsToShow)

    # Avg. burst period
    figSize = 5
    fig = figure(figsize=(figSize, figSize))
    ax = fig.add_subplot(111)
    ax.imshow(sweepData[1],
              extent=[aMin, aMax, bMin, bMax],
              aspect=(aMax - aMin) / (bMax - bMin),
              cmap='Reds',
              origin='lower', vmin=5000, vmax=50000
              )  # making vmax 0.1 instead of 0. to match the colormaps for short and long kneadings
    for i in np.arange(1, nDataPointsToShow):
        for j in np.arange(1, nDataPointsToShow):
            ax.plot(aMin + i * aStep, bMin + j * bStep, 'o', color='k', ms=1)
            ax.text(aMin + i * aStep, bMin + j * bStep - bStep / 10.,
                    round(sweepData[1][0 + j * sweepStep, 0 + i * sweepStep], 2), color='black', ha='center',
                    va='center', size=4)
    xlabel('alpha');
    ylabel('beta');
    fig.suptitle("Avg. burst period"+": CaShift ="+str(CaShift)+", xShift = "+str(xShift)+", g = "+str(g));
    fig.savefig(filename + "_burstPeriod.jpg")

    # Avg. spikes per burst
    figSize = 5
    fig = figure(figsize=(figSize, figSize))
    ax = fig.add_subplot(111)
    ax.imshow(sweepData[2],
              extent=[aMin, aMax, bMin, bMax],
              aspect=(aMax - aMin) / (bMax - bMin),
              cmap=customColorMap,
              origin='lower', vmin=5., vmax=50
              )  # making vmax 0.1 instead of 0. to match the colormaps for short and long kneadings
    for i in np.arange(1, nDataPointsToShow):
        for j in np.arange(1, nDataPointsToShow):
            ax.plot(aMin + i * aStep, bMin + j * bStep, 'o', color='k', ms=1)
            ax.text(aMin + i * aStep, bMin + j * bStep - bStep / 10.,
                    round(sweepData[2][0 + j * sweepStep, 0 + i * sweepStep], 2), color='black', ha='center',
                    va='center', size=4)
    xlabel('alpha');
    ylabel('beta');
    fig.suptitle("Avg. spikes per burst"+": CaShift ="+str(CaShift)+", xShift = "+str(xShift)+", g = "+str(g));
    fig.savefig(filename + "_spikesPerBurst.jpg")

    # Avg. S
    figSize = 5
    fig = figure(figsize=(figSize, figSize))
    ax = fig.add_subplot(111)
    ax.imshow(sweepData[3],
              extent=[aMin, aMax, bMin, bMax],
              aspect=(aMax - aMin) / (bMax - bMin),
              cmap=customColorMap,
              origin='lower'  # , vmin=.002, vmax=0.02
              )  # making vmax 0.1 instead of 0. to match the colormaps for short and long kneadings
    for i in np.arange(1, nDataPointsToShow):
        for j in np.arange(1, nDataPointsToShow):
            ax.plot(aMin + i * aStep, bMin + j * bStep, 'o', color='k', ms=1)
            ax.text(aMin + i * aStep, bMin + j * bStep - bStep / 10.,
                    round(sweepData[3][0 + j * sweepStep, 0 + i * sweepStep], 4), color='black', ha='center',
                    va='center', size=4)
    xlabel('alpha');
    ylabel('beta');
    fig.suptitle("Avg. S"+": CaShift ="+str(CaShift)+", xShift = "+str(xShift)+", g = "+str(g));
    fig.savefig(filename + "_avgS.jpg")

    # Avg. spike frequency within a burst
    figSize = 5
    fig = figure(figsize=(figSize, figSize))
    ax = fig.add_subplot(111)
    ax.imshow(sweepData[2]/sweepData[1],
              extent=[aMin, aMax, bMin, bMax],
              aspect=(aMax - aMin) / (bMax - bMin),
              cmap=customColorMap,
              origin='lower'  # , vmin=.002, vmax=0.02
              )  # making vmax 0.1 instead of 0. to match the colormaps for short and long kneadings
    for i in np.arange(1, nDataPointsToShow):
        for j in np.arange(1, nDataPointsToShow):
            ax.plot(aMin + i * aStep, bMin + j * bStep, 'o', color='k', ms=1)
            ax.text(aMin + i * aStep, bMin + j * bStep - bStep / 10.,
                    round(sweepData[2][0 + j * sweepStep, 0 + i * sweepStep]/sweepData[1][0 + j * sweepStep, 0 + i * sweepStep], 4), color='black', ha='center',
                    va='center', size=4)
    xlabel('alpha');
    ylabel('beta');
    fig.suptitle("Avg. burst spike frequency "+": CaShift ="+str(CaShift)+", xShift = "+str(xShift)+", g = "+str(g));
    fig.savefig(filename + "_avgBurstSpikeFrequency.jpg")

#show()