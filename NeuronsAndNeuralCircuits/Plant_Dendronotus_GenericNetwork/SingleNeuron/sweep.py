import matplotlib

matplotlib.use('Qt4Agg')
#matplotlib.use('Agg')
import ctypes as ct
from pylab import *
import numpy as np
from matplotlib import colors
from numpy.ma import masked_array
import pickle
import gzip

import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

matplotlib.rcParams['savefig.dpi'] = 1200 #2*sweepSize/figSize #300
defaultFontSize = 11; largeFontSize = 14; indexFontSize = 16;
matplotlib.rcParams.update({'font.size': largeFontSize,
              'axes.labelsize': largeFontSize,
              'axes.titlesize': largeFontSize,
              'font.size': defaultFontSize,
              'legend.fontsize': largeFontSize,
              'xtick.labelsize': defaultFontSize,
              'ytick.labelsize': defaultFontSize})

#comment these two for rainbow text function to run properly, along with setting fig.dpi = 1200 described near rainbow text
matplotlib.rc('text', usetex=True)
matplotlib.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
matplotlib.rcParams['savefig.dpi'] = 600
bbox_props_label = dict(boxstyle="square,pad=0.01", fc="white", ec="k", lw=0	)
bbox_props_index = dict(boxstyle="square,pad=0.2", fc="white", ec="k", lw=1., alpha=0.)
bbox_props_fig = dict(boxstyle="square,pad=0.001", fc="white", ec="k", lw=0)

dataSaved = True;
#"""
#Full sweep
CaShiftMin = -150.; CaShiftMax = -50.;
xShiftMin = -25; xShiftMax = 80; ghList=[0.]; #ghList=[-0.0004, 0.0004, 0.004];
sweepSize = 500; N = 300000*10*4; dt=.25;
Iapp = 0.;kneadingsStart=100*2; kneadingsEnd=200*2;
#"""

# Adjust these bins to change precision depending on the system; Bins do not have to be equally sized; Have more bins in the region where more precision is required and less bins where its not; Using 10 bins for a start (9 bin boundaries); Best represented by char to compute PC, may also be represented by the digits 0 to 9 if number of bins < 10

voltageBinBoundaries = np.array([-60., -40., 10.], dtype='float64')
#voltageBinBoundaries = np.array([], dtype='float64')

#intervalBinBoundaries = np.array([100], dtype='float64')
intervalBinBoundaries = np.array([], dtype='float64')

# arrays must be created with double values not integers (default) since that's how it is passed to C function later;
#  Otherwise giving errors and memory copied incorrectly

lib = ct.cdll.LoadLibrary('./sweep_PCLZ.so')
lib.sweep.argtypes = [ct.POINTER(ct.c_double),
                        ct.POINTER(ct.c_double), ct.POINTER(ct.c_double),
                        ct.c_double, ct.c_double, ct.c_uint,
                        ct.c_double, ct.c_double, ct.c_uint,
                        ct.c_double, ct.c_double, ct.c_double, ct.c_double,
                        ct.c_double, ct.c_uint, ct.c_uint,
                        ct.c_uint, ct.c_uint,
                        ct.c_uint, ct.c_uint]

#shift_Ca2, shift_x2, Iapp, gh, Vhh, gleak;
#params = np.asarray([-110, -3.5, 0., 0.0001, -85., 0.005], float)
def sweep(dt=1, N=300000, stride=1,
            CaShiftMin=-10., CaShiftMax=5.,
            xShiftMin=-2.5, xShiftMax=-1.5,
            Iapp = 0., gh = 0.0001, Vhh = -85, gleak = 0.005,
            kneadingsStart=100,
            kneadingsEnd=200,
            sweepSize=100
            ):
    kneadingsWeightedSumSet = np.zeros(sweepSize * sweepSize, float)
    lib.sweep(kneadingsWeightedSumSet.ctypes.data_as(ct.POINTER(ct.c_double)),
              voltageBinBoundaries.ctypes.data_as(ct.POINTER(ct.c_double)),
              intervalBinBoundaries.ctypes.data_as(ct.POINTER(ct.c_double)),
              ct.c_double(CaShiftMin), ct.c_double(CaShiftMax), ct.c_uint(sweepSize),
              ct.c_double(xShiftMin), ct.c_double(xShiftMax), ct.c_uint(sweepSize),
              ct.c_double(Iapp), ct.c_double(gh), ct.c_double(Vhh), ct.c_double(gleak),
              ct.c_double(dt), ct.c_uint(N), ct.c_uint(stride),
              ct.c_uint(len(voltageBinBoundaries)), ct.c_uint(len(intervalBinBoundaries)),
              ct.c_uint(kneadingsStart), ct.c_uint(kneadingsEnd))
    # imshow takes fortran style array input
    return np.reshape(kneadingsWeightedSumSet, (sweepSize, sweepSize), 'F')

#For LZ, use the returned value between 0 to 1 with a grey colormap
#For PC, return the circular hash of the period string, and use it with a custom colormap; If needed, we can also return, say, 6digits from the hash and use 2 each for R,G,B

print('Running sweepHR')

colorMapLevels=2**8

blue = np.linspace(0.01, 1, colorMapLevels)
red = 1 - blue
min = 0.;
max = .8
green = min + np.random.random(colorMapLevels) * (max - min)
RGB = np.column_stack((red, green, blue))
customColorMap = colors.ListedColormap(RGB)

blue = np.linspace(0.8, 0, colorMapLevels)
red = blue
green = blue
RGB = np.column_stack((red, green, blue))
customGrayColorMap = colors.ListedColormap(RGB)


figSize = 3
fig = figure(figsize=(figSize, figSize))
gs1 = gridspec.GridSpec(1,1)
gs1.update(wspace=0.02, hspace=0.02)  # set the spacing between axes.
ax = plt.subplot(gs1[0, 0])

outputdir = 'Output/Plant_PCLZ'
matplotlib.rcParams['savefig.dpi'] = 600


fig.clear()

for gh in ghList:

    filename = outputdir + '_CaShiftMin_'+str(CaShiftMin)+ '_CaShiftMax_'+str(CaShiftMax)+ '_xShiftMin_'+str(xShiftMin)+ '_xShiftMax_'+str(xShiftMax)+'_Iapp_' + str(Iapp) + '_gh_' + str(gh) + '_voltageBins_' + str(voltageBinBoundaries) + '_intervalBins_' + str(intervalBinBoundaries) + '_sweepSize_' + str(sweepSize)

    if not dataSaved:
        pickleDataFile1 = gzip.open(filename + '.gz', 'wb')
        sweepData = sweep(dt=dt, N=N, stride=1, CaShiftMin=CaShiftMin, CaShiftMax=CaShiftMax, xShiftMin=xShiftMin,
                          xShiftMax=xShiftMax, Iapp=Iapp, gh=gh, kneadingsStart=kneadingsStart, kneadingsEnd=kneadingsEnd, sweepSize=sweepSize)
        pickle.dump(sweepData, pickleDataFile1)
    else:
        pickleDataFile1 = gzip.open(filename + '.gz', 'rb')
        sweepData = pickle.load(pickleDataFile1)
    pickleDataFile1.close()

    # print sweepData
    # sweepData[sweepData == -1.1] = 0.

    # PC hash can be any integer value (positive/negative), LZ lies between 0 and 1, -0.5 for insufficient N
    sweepDataInsufficientN = masked_array(sweepData, sweepData != -0.5)
    sweepDataComputed = masked_array(sweepData, sweepData == -0.5)
    sweepDataChaotic = masked_array(sweepDataComputed, (sweepData <= 0) | (sweepData >= 1))
    sweepDataPeriodic = masked_array(sweepDataComputed, (sweepData > 0) & (sweepData < 1))

    aMin = CaShiftMin; aMax = CaShiftMax; bMin = xShiftMin; bMax = xShiftMax;
    # to force common color scheme without scaling across images, use the vmin and vmax arguments of imshow(), and make sure they are the same for all your images
    imshow(sweepDataPeriodic,
           extent=[aMin, aMax, bMin, bMax],
           aspect=(aMax - aMin) / (bMax - bMin),
           cmap=customColorMap,
           origin='lower',  # vmin=-1., vmax=0.1
           )  # making vmax 0.1 instead of 0. to match the colormaps for short and long kneadings

    imshow(sweepDataChaotic,
           extent=[aMin, aMax, bMin, bMax],
           aspect=(aMax - aMin) / (bMax - bMin),
           cmap=customGrayColorMap,
           origin='lower')

    imshow(sweepDataInsufficientN,
           extent=[aMin, aMax, bMin, bMax],
           aspect=(aMax - aMin) / (bMax - bMin),
           cmap=colors.ListedColormap(['white']),
           origin='lower')
    plt.axis('on')
    xticks([])
    yticks([])

    xlabel = r'$\mathrm{Ca_{shift}}$';
    ylabel = r'$\mathrm{x_{shift}}$';
    fig.text(-0.05, -0.02, CaShiftMin, ha='left', va='top', bbox=bbox_props_label, transform=ax.transAxes,
            fontsize=defaultFontSize)
    fig.text(1.14, -0.02, CaShiftMax, ha='right', va='top', bbox=bbox_props_label, transform=ax.transAxes,
            fontsize=defaultFontSize)
    fig.text(0.5, -0.03, xlabel, ha='center', va='top', bbox=bbox_props_label, transform=ax.transAxes,
            fontsize=largeFontSize)

    fig.text(-0.04, -0, xShiftMin, ha='right', va='bottom', rotation=90, bbox=bbox_props_label,
            transform=ax.transAxes, fontsize=defaultFontSize)
    fig.text(-0.04, 1.16, xShiftMax, ha='right', va='top', rotation=90, bbox=bbox_props_label,
            transform=ax.transAxes, fontsize=defaultFontSize)
    fig.text(-0.05, 0.55, ylabel, ha='right', va='center', rotation=90, bbox=bbox_props_label, transform=ax.transAxes,
            fontsize=largeFontSize)
    plt.subplots_adjust(left=0.1, right=0.999, top=0.999, bottom=0.1)
    plot(-110., -3.5, 'o', color='w', ms=2)

    fig.text(0.3, 0.2, r'$\boldsymbol{tonic}$', ha='center', va='top', bbox=bbox_props_index, color='w',
             fontsize=defaultFontSize, weight='roman')
    fig.text(0.7, 0.8, r'$\boldsymbol{quiescent}$', ha='center', bbox=bbox_props_index, color='darkslategray',
             fontsize=defaultFontSize, weight='roman')
    fig.savefig('Dendronotus_Si3_CaShift_vs_xShift.png', bbox_inches='tight')

    show()
