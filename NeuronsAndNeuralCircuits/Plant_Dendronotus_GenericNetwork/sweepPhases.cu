#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include <sys/time.h>
#include<string.h>
#include<stdbool.h>

#define N_EQ	6
#define NP 8
#define MAX_KNEADING_LENGTH 10000
#define NUM_THREADS_PER_BLOCK 512

#define numberOfOutputMetrics 3
//KneadingSum, burst period, numberOfSpikesInBurst, AverageS


__device__ double i2(double v1, double n1){
    return 1.3*pow(n1,4)*(-75-v1);
}

__device__ double i3(double v1, double x1){
    return 0.01*x1*(30-v1);
}

__device__ double i4(double v1, double ca1){
    return 0.03*ca1/(0.5 + ca1)*(-75 - v1);
}

__device__ double ileak(double v1){
    return (-50-v1);
}

__device__ void stepper(const double* y, double* dydt, const double* params)
{
    unsigned i,j,k;

    double Iapp = params[2], gh = params[3], Vhh = params[4], gleak = params[5], shift_Ca=params[6], shift_x=params[7];

    double V2=y[0], Ca2=y[1], h2=y[2], n2=y[3], x2=y[4], y2=y[5];

    dydt[0] = 8*pow(((0.1*(50 - (127*V2/105 + 8265/105))/(exp((50 - (127*V2/105 + 8265/105))/10) - 1))/((0.1*(50 - (127*V2/105 + 8265/105))/(exp((50 - (127*V2/105 + 8265/105))/10) - 1))+(4*exp((25 - (127*V2/105 + 8265/105))/18)))),3)*h2*(30 - V2)
                + i2(V2,n2)
                + i3(V2,x2)
                + i4(V2, Ca2)
                + gleak*ileak(V2)
                + gh*pow((1/(1+exp((V2+75)/7.8))),3)*y2*(-V2+120)
                + Iapp;
                //+ g21*(-55 - V2)*s3;
    dydt[1] = 0.0003*(0.0085*x2*(140 - V2+shift_Ca)-Ca2);
    dydt[2] = ((1-h2)*(0.07*exp((25 - (127*V2/105 + 8265/105))/20))-h2*(1.0/(1 + exp((55 - (127*V2/105 + 8265/105))/10))))/12.5;
    dydt[3] = ((1-n2)*(0.01*(55 - (127*V2/105 + 8265/105))/(exp((55 - (127*V2/105 + 8265/105))/10) - 1))-n2*(0.125*exp((45 - (127*V2/105 + 8265/105))/80)))/12.5;
    dydt[4] =  ((1/(exp(0.1*(-V2-50+shift_x)) + 1))-x2)/235;
    dydt[5] = .5*((1/(1+exp(1*(V2-Vhh))))-y2)/(7.1+10.4/(1+exp((V2+75)/2.2)));

}

__device__ void stepper_coupled(double* y, double* dydt, const double* parameterSet, const int networkSize)
{
    const double *csElec = parameterSet+networkSize*NP,
                    *csGInh = parameterSet+networkSize*NP+networkSize*networkSize,
                    *csAlphaInh =  parameterSet+networkSize*NP+2*networkSize*networkSize,
                    *csBetaInh =  parameterSet+networkSize*NP+3*networkSize*networkSize,
                    *csGExc =  parameterSet+networkSize*NP+4*networkSize*networkSize,
                    *csAlphaExc =  parameterSet+networkSize*NP+5*networkSize*networkSize,
                    *csBetaExc =  parameterSet+networkSize*NP+6*networkSize*networkSize;

	double *yInh = y+networkSize*N_EQ, *yExc = y+networkSize*N_EQ+networkSize*networkSize;
	double *dydtInh = dydt+networkSize*N_EQ, *dydtExc = dydt+networkSize*N_EQ+networkSize*networkSize;
	unsigned i, j;

    //Compute dydt
	for(i=0;i <networkSize; i++){
		stepper(y+i*N_EQ, dydt+i*N_EQ, parameterSet+i*NP);
	}

    //Compute dsdt for SInh and SExc
	for(i=0;i<networkSize;i++){
	    for(j=0;j<networkSize;j++){
            if(i==2 || i==3){
                dydtInh[i*networkSize+j] = csAlphaInh[i*networkSize+j]*(1-yInh[i*networkSize+j])/(1+exp(-10*(y[i*N_EQ]+30)))-csBetaInh[i*networkSize+j]*yInh[i*networkSize+j]; // /(1+exp(10*(y[i*N_EQ]+20)));
            }else{
                dydtInh[i*networkSize+j] = csAlphaInh[i*networkSize+j]*yInh[i*networkSize+j]*(1-yInh[i*networkSize+j])/(1+exp(-10*(y[i*N_EQ]+30)))-csBetaInh[i*networkSize+j]*(yInh[i*networkSize+j]-0.01); // /(1+exp(10*(y[i*N_EQ]+20)));
            }
            dydtExc[i*networkSize+j] = csAlphaExc[i*networkSize+j]*(1-yExc[i*networkSize+j])/(1+exp(-10*(y[i*N_EQ]+30)))-csBetaExc[i*networkSize+j]*(yExc[i*networkSize+j]-0.01); // /(1+exp(10*(y[i*N_EQ]+20)));
        }
	}

    //Add synaptic inputs to dydt
    for(i=0;i<networkSize;i++){
        for(j=0;j<networkSize;j++){
       		   dydt[i*N_EQ] += (y[j*N_EQ]-y[i*N_EQ])*csElec[j*networkSize+i]        //            electric j to i
       		                    +  (parameterSet[i*NP+1] - y[i*N_EQ])* csGExc[j*networkSize+i] * yExc[j*networkSize+i] // AlphaExc j to i
       		                    +  (((i==2 && j==1) || (i==3 && j==0))?(-75 - y[i*N_EQ]):(parameterSet[i*NP+0] - y[i*N_EQ]))* csGInh[j*networkSize+i] * yInh[j*networkSize+i]; // AlphaInh j to i // Hack for Si3 cells with two different V_inh values for different synapses - ideally should be a separate array for all cells with diff V_inh for say  potassium vs chloride
        }
    }
}

//from: https://github.com/benjaminfrot/LZ76/blob/master/C/LZ76.c
__device__ double LZ76(const char * s, const int n) {
  int c=1,l=1,i=0,k=1,kmax = 1,stop=0;
  while(stop ==0) {
    if (s[i+k-1] != s[l+k-1]) {
      if (k > kmax) {
        kmax=k;
      }
      i++;

      if (i==l) {
        c++;
        l += kmax;
        if (l+1>n)
          stop = 1;
        else {
          i=0;
          k=1;
          kmax=1;
        }
      } else {
        k=1;
      }
    } else {
      k++;
      if (l+k > n) {
        c++;
        stop =1;
      }
    }
  }
  return double(c)/n;
}



__device__ unsigned NextCoprime(unsigned* coPrimes,unsigned x){
    coPrimes[x] = (coPrimes[x] - x) * coPrimes[x] + x;
    return coPrimes[x];
}

//https://stackoverflow.com/questions/40303333/how-to-replicate-java-hashcode-in-c-language
__device__ unsigned javaHashCode(const unsigned x) {
    int hash = 0, length = floor(log10(fabs(x+0.))) + 1;
    for(int i=0; i<length; i++){
        hash = 31*hash+(x%int(pow(10.,i+0.)));
    }
    return hash;
}

__device__ unsigned NextPair(unsigned* coPrimes, const unsigned a, const unsigned b, const unsigned x, const unsigned y){
    return a * NextCoprime(coPrimes, x) * javaHashCode(x) + b * javaHashCode(y);
}

// Need circular hash function : https://stackoverflow.com/questions/2585092/is-there-a-circular-hash-function
// https://mathoverflow.net/questions/135297/general-euclid-fermat-sequences
//This function can return either positive or negative values
__device__ double circularHash(const char *kneadings, const unsigned periodLength)
{
    int H = 0;

    if (periodLength > 0)
    {
        //any arbitrary coprime numbers
        unsigned a = periodLength, b = periodLength + 1;

        //an array of Euclid-Fermat sequences to generate additional coprimes for each duplicate character occurrence
        unsigned noOfCoprimes = 255; //see if this is enough or more are needed
        unsigned coPrimes[255];

        for (int i = 1; i < noOfCoprimes; i++)
        {
            coPrimes[i] = i + 1;
        }

        //for i=0 we need to wrap around to the last character
        H = NextPair(coPrimes, a, b, kneadings[periodLength - 1], kneadings[0]);

        //for i=1...n we use the previous character
        for (int i = 1; i < periodLength; i++)
        {
            H ^= NextPair(coPrimes, a, b, kneadings[i - 1], kneadings[i]);
        }
    }
    //printf("\nCircular hash : %d\n",H);

    //return double(H);
    //H can be any integer; In order to not conflict with LZ, we make sure we return a value greater than 1 or less than -1, so LZ can be between 0 and 1, and -0.5 can be used as marker for insufficient N.
    if(H>=0) return double(H+1);
    else return double(H-1);
}


__device__ double periodicPCorChaoticLZ(const char* kneadings,const  unsigned kneadingsLength){
    //After a long transient when the periodic orbits if any have already been reached, this method computes a kneading sum that is invariant between different cyclic permutations of the period

    bool periodFound=true;
    unsigned periodLength = kneadingsLength;
    for(unsigned currPeriod=1; currPeriod < kneadingsLength/2; currPeriod++) {
        periodFound=true;
        //Check if the kneading sequence has a period with periodicity of currPeriod
        for(unsigned i=currPeriod; i < kneadingsLength-currPeriod; i+=currPeriod) {
            for ( unsigned j=0; j<currPeriod; j++) {
                if(kneadings[j] != kneadings[i+j]) {
                    periodFound=false;
                    break;
                }
            }
            if(!periodFound) {
                break;
            }
        }
        //compute kneadingSum based on period found, if any
        if(periodFound) {
            //currPeriod is the period of the kneading sequence. So this will be normalized to a sequence with the sorted period(0's followed by 1's)
            periodLength = currPeriod;
            break;
        }
    }
    //return periodLength==kneadingsLength? LZ76(kneadings, kneadingsLength) : -computePeriodNormalizedKneadingSum(kneadings, kneadingsLength, periodLength);
    //return periodLength==kneadingsLength? LZ76(kneadings, kneadingsLength) : -periodLength;

    //printf("\nPeriod Length: %d\n", periodLength);
    return periodLength==kneadingsLength? LZ76(kneadings, kneadingsLength) : -circularHash(kneadings, periodLength);

}


__device__ char getBinSymbol(const double value, const double* binBoundariesGpu, const double numberOfBinBoundaries, const char startingChar){
    unsigned i;

    if(numberOfBinBoundaries==0) return startingChar;
    for(i=0;i<numberOfBinBoundaries;i++){
        if(value < binBoundariesGpu[i])
            return char(startingChar+i);
    }
    return char(startingChar+i);
}

__device__ void integrator_rk4(double* outputMetrics, double* integratorBuffer, char* kneadingsBuffer, const double* voltageBinBoundariesGpu, const double* intervalBinBoundariesGpu, const double* parameterSet, const unsigned networkSize, const double dt, const unsigned N, const unsigned stride, const unsigned nV, const unsigned nI, const unsigned kneadingsStart, const unsigned kneadingsEnd)
{
	unsigned i, j, k, kneadingIndex=0, kneadingArrayIndex=0, N_EQ1 = N_EQ*networkSize + 2*networkSize * networkSize ; // For each cell N_EQ equations plus networkSize^2 equations each for SInh and SExc.;
	double dt2, dt6;

    //double y_current[N_EQ1]; //To be passed with initial conditions
	//double y1[N_EQ1], y2[N_EQ1], k1[N_EQ1], k2[N_EQ1], k3[N_EQ1], k4[N_EQ1];
	//double firstDerivativeCurrent[N_EQ1],firstDerivativePrevious[networkSize];
    //double previousEvent[networkSize]; //unsigned should be sufficient; making it doulbe to fit all data in one array

    double *y_current = integratorBuffer+0, *y1=integratorBuffer+N_EQ1, *y2=integratorBuffer+2*N_EQ1, *k1=integratorBuffer+3*N_EQ1, *k2=integratorBuffer+4*N_EQ1, *k3=integratorBuffer+5*N_EQ1, *k4=integratorBuffer+6*N_EQ1, *firstDerivativeCurrent=integratorBuffer+7*N_EQ1, *firstDerivativePrevious=integratorBuffer+8*N_EQ1, *previousEvent=integratorBuffer+8*N_EQ1+networkSize;

	double kneadingsWeightedSum=0;

    //char kneadings[MAX_KNEADING_LENGTH], lastSymbol[networkSize];
    char *kneadings = kneadingsBuffer+0, *lastSymbol = kneadingsBuffer+kneadingsEnd-kneadingsStart+1;

    int simultaneousEventsTimeGap = 50, firstBurstTime = 0, lastBurstTime=0, numberOfBursts=-1, spikesUptoLastBurst=0, totalSpikesSinceFirstBurst=0, totalSpikes=0, totalSpikes2=0;
    //int cellIdToPlot = 0; //plotting first cell data
    int cellIdToPlot = networkSize-1; //plotting last cell data - ideally, this has to be a parameter
    int cellIdToPlot2 = networkSize-2; //last cell data compared with second last cell for HCO tonic spiking or quiescent

	dt2 = dt/2.; dt6 = dt/6.;

	for(i=1; i<N; i++)
	{
		for(j=0; j<stride; j++)
		{
			stepper_coupled(y_current, k1, parameterSet, networkSize);
			for(k=0; k<N_EQ1; k++) y1[k] = y_current[k]+k1[k]*dt2;
			stepper_coupled(y1, k2, parameterSet, networkSize);
			for(k=0; k<N_EQ1; k++) y2[k] = y_current[k]+k2[k]*dt2;
			stepper_coupled(y2, k3, parameterSet, networkSize);
			for(k=0; k<N_EQ1; k++) y2[k] = y_current[k]+k3[k]*dt;
			stepper_coupled(y2, k4, parameterSet, networkSize);

			//Copy latest value into y_current
			for(k=0; k<N_EQ1; k++) y_current[k] += dt6*(k1[k]+2.*(k2[k]+k3[k])+k4[k]);
		}

		stepper_coupled(y_current, firstDerivativeCurrent, parameterSet, networkSize);


        for(j=0;j<networkSize;j++){
            if(firstDerivativePrevious[j] > 0 && firstDerivativeCurrent[j*N_EQ] < 0 && y_current[j*N_EQ] > voltageBinBoundariesGpu[nV-1]) {
                if(kneadingIndex<kneadingsStart){
                    //skip ahead upto the kneadingsStart symbol
                    kneadingIndex++;
                } else {
                    char c = getBinSymbol(dt * (i - previousEvent[j]), intervalBinBoundariesGpu, nI, 'A'+2*j);
                    if(kneadingArrayIndex==0 || c != lastSymbol[j]){
                        //multiple spikes in a burst will be shown by just one symbol at its start
                        if(dt * (i - previousEvent[j]) > intervalBinBoundariesGpu[nI-1])
                            kneadings[kneadingArrayIndex++] = c;
                        lastSymbol[j] = c;
                        if(j==cellIdToPlot && dt * (i - previousEvent[cellIdToPlot]) > intervalBinBoundariesGpu[nI-1]){
                            numberOfBursts++;
                            if(numberOfBursts==0){
                                firstBurstTime = i;
                            }else if(numberOfBursts>0){

                            }
                            lastBurstTime = i;
                            spikesUptoLastBurst = totalSpikesSinceFirstBurst;
                        }
                    }
                    if(j==cellIdToPlot && numberOfBursts>=0) totalSpikesSinceFirstBurst++;
                    if(j==cellIdToPlot) totalSpikes++;
                    if(j==cellIdToPlot2) totalSpikes2++;
                    kneadingIndex++;
                }
                previousEvent[j] = i;
                //TODO issues when events in two cells occur very close to each other so they sometimes alternate between two repeats of the period, so symbolic sequences are also altered, hence periodic sequence can't be detected.. at the beginning of the bursts in phase .. might not matter for HCO, but might be required for 4 cell network where two burst in phase
            }
            firstDerivativePrevious[j] = firstDerivativeCurrent[j*N_EQ];
        }
        /*
		if(kneadingIndex>kneadingsEnd){
            //print test
            //kneadings[kneadingArrayIndex++]=char('\0'); printf("%s \t length: %d \n", kneadings, kneadingArrayIndex); return;

            //KneadingSum, burst period of cell 1, spikes per burst of cell 1
			outputMetrics[0] = periodicPCorChaoticLZ(kneadings, kneadingArrayIndex);
			outputMetrics[1] = numberOfBursts<=0?0:(lastBurstTime-firstBurstTime)*dt/numberOfBursts; //Average burst period
			outputMetrics[2] = numberOfBursts<=0?totalSpikes*totalSpikes2:spikesUptoLastBurst/numberOfBursts; // Average spikes per burst
			return;
		}
        //Continuing to compute symbols even beyond kneadingsEnd; So In the end we will compute upto atleast kneadingsEnd and as many as possible for the given N
		*/
	}

    if(kneadingIndex>kneadingsEnd){
        //print test
        //kneadings[kneadingArrayIndex++]=char('\0'); printf("%s \t length: %d \n", kneadings, kneadingArrayIndex); return;

        //KneadingSum, burst period of cell 1, spikes per burst of cell 1
        outputMetrics[0] = periodicPCorChaoticLZ(kneadings, kneadingArrayIndex);
        outputMetrics[1] = numberOfBursts<=0?0:(lastBurstTime-firstBurstTime)*dt/numberOfBursts; //Average burst period
        outputMetrics[2] = numberOfBursts<=0?totalSpikes*totalSpikes2:spikesUptoLastBurst/numberOfBursts; // Average spikes per burst
    }else{
       	//print test
    	//printf("%s \t length: %d\n", kneadings, kneadingIndex); //return 0;
	    outputMetrics[0] = -0.5;outputMetrics[1] = -0.5;outputMetrics[2] = -0.5;
    }
	return;
	//return -0.5;
}

__global__ void sweepThreads(double* kneadingsWeightedSumSet, double* initialConditions,
                                        double* sweepIntegratorsBuffer,
                                        char* sweepKneadingsBuffer,
                                        const unsigned networkSize,
										double * sweepParameterSet,
										const unsigned * sweepParameter1Indices, const unsigned sweepParameter1IndicesSize, const double sweepParameter1Start, const double sweepParameter1End, const unsigned sweepParameter1Count,
										const unsigned * sweepParameter2Indices, const unsigned sweepParameter2IndicesSize, const double sweepParameter2Start, const double sweepParameter2End, const unsigned sweepParameter2Count,
										const double* voltageBinBoundariesGpu, const double* intervalBinBoundariesGpu,
										const unsigned nV, const unsigned nI,
										const double dt, const unsigned N, const unsigned stride,
										const unsigned kneadingsStart, const unsigned kneadingsEnd){

    int tx=blockIdx.x * blockDim.x + threadIdx.x;

	double a,b;
	double sweepParameter1Step = sweepParameter1Count==1?0:(sweepParameter1End - sweepParameter1Start)/(sweepParameter1Count);
	double sweepParameter2Step = sweepParameter2Count==1?0:(sweepParameter2End - sweepParameter2Start)/(sweepParameter2Count);
	int i,j,k;
    unsigned N_EQ1 = N_EQ*networkSize + 2*networkSize * networkSize, NP1 = networkSize*NP+networkSize*networkSize*7 ;

    if(tx<sweepParameter1Count*sweepParameter2Count){

        double * integratorBuffer = sweepIntegratorsBuffer+tx*(8*N_EQ1+2*networkSize), *y_current=integratorBuffer;
        char * kneadingsBuffer = sweepKneadingsBuffer+tx*(kneadingsEnd-kneadingsStart+1+networkSize);
        double * parameterSet = sweepParameterSet+tx*NP1;

        i=tx/sweepParameter1Count;
        j=tx%sweepParameter1Count;

        for(k=0;k<sweepParameter1IndicesSize;k++){
            parameterSet[sweepParameter1Indices[k]]=sweepParameter1Start+i*sweepParameter1Step;
        }
        for(k=0;k<sweepParameter2IndicesSize;k++){
            parameterSet[sweepParameter2Indices[k]]=sweepParameter2Start+j*sweepParameter2Step;
        }

        for(k=0;k<N_EQ1;k++){
            y_current[k]=initialConditions[k];
        }
        integrator_rk4(kneadingsWeightedSumSet+(i*sweepParameter1Count+j)*numberOfOutputMetrics, integratorBuffer, kneadingsBuffer, voltageBinBoundariesGpu, intervalBinBoundariesGpu, parameterSet, networkSize, dt, N, stride, nV, nI, kneadingsStart, kneadingsEnd);
    }
}

/**
 * error checking routine
 */
void checkAndDisplayErrors(char *label)
{
  // we need to synchronise first to catch errors due to
  // asynchronous operations that would otherwise
  // potentially go unnoticed

  cudaError_t err;

  err = cudaThreadSynchronize();
  if (err != cudaSuccess)
  {
    char *e = (char*) cudaGetErrorString(err);
    fprintf(stderr, "CUDA Error: %s (at %s)", e, label);
  }

  err = cudaGetLastError();
  if (err != cudaSuccess)
  {
    char *e = (char*) cudaGetErrorString(err);
    fprintf(stderr, "CUDA Error: %s (at %s)", e, label);
  }
}


extern "C" {

void sweep( double* kneadingsWeightedSumSet, double* initialConditions,
                                        const unsigned networkSize,
										double * parameterSet,
										const unsigned * sweepParameter1Indices, const unsigned sweepParameter1IndicesSize, const double sweepParameter1Start, const double sweepParameter1End, const unsigned sweepParameter1Count,
										const unsigned * sweepParameter2Indices, const unsigned sweepParameter2IndicesSize, const double sweepParameter2Start, const double sweepParameter2End, const unsigned sweepParameter2Count,
										const double* voltageBinBoundaries, const double* intervalBinBoundaries,
										const unsigned nV, const unsigned nI,
										const double dt, const unsigned N, const unsigned stride,
										const unsigned kneadingsStart, const unsigned kneadingsEnd){

        int sweepParameterSpaceSize = sweepParameter1Count*sweepParameter2Count, i;
        double *kneadingsWeightedSumSetGpu; //// using this not just for kneadingSum; for each point in the sweep we compute: KneadingSum, burst period, numberOfSpikesInBurst values for the first cell trajectory and put everything in this array sequentially
        double *initialConditionsGpu, *sweepParameterSetGpu;
        double *voltageBinBoundariesGpu, *intervalBinBoundariesGpu;
        double *sweepIntegratorBuffersGpu;
        char *sweepKneadingsBuffersGpu;
        unsigned *sweepParameter1IndicesGpu, *sweepParameter2IndicesGpu, N_EQ1 = N_EQ*networkSize + 2*networkSize * networkSize, NP1=networkSize*NP+networkSize*networkSize*7 ;

        /* For debugging
        for(i=0;i<N_EQ1;i++){
            printf("%lf ", initialConditions[i]);
        }printf("\n\n");

        for(i=0;i<sweepParameterSpaceSize*NP1;i++){
            printf("%lf ", parameterSet[i]);
        }printf("\n\n");

        for(i=0;i<sweepParameter1IndicesSize;i++){
            printf("%d ", sweepParameter1Indices[i]);
        }printf("\n\n");

        for(i=0;i<sweepParameter2IndicesSize;i++){
            printf("%d ", sweepParameter2Indices[i]);
        }printf("\n\n");

        for(i=0;i<nV;i++){
            printf("%lf ", voltageBinBoundaries[i]);
        }printf("\n\n");

        for(i=0;i<nI;i++){
            printf("%lf ", intervalBinBoundaries[i]);
        }printf("\n\n");
        */

        printf("Before malloc gpu\n");
        /*allocate device memory. */
        (cudaMalloc( (void**) &kneadingsWeightedSumSetGpu, sweepParameterSpaceSize*sizeof(double)*numberOfOutputMetrics));
        (cudaMalloc( (void**) &initialConditionsGpu, N_EQ1*sizeof(double)));
        (cudaMalloc( (void**) &sweepIntegratorBuffersGpu, sweepParameterSpaceSize*(8*N_EQ1+2*networkSize)*sizeof(double)));
        (cudaMalloc( (void**) &sweepKneadingsBuffersGpu, sweepParameterSpaceSize*(kneadingsEnd-kneadingsStart+1+networkSize)*sizeof(char)));
        (cudaMalloc( (void**) &sweepParameterSetGpu, sweepParameterSpaceSize*NP1*sizeof(double)));
        (cudaMalloc( (void**) &voltageBinBoundariesGpu, nV*sizeof(double)));
        (cudaMalloc( (void**) &intervalBinBoundariesGpu, nI*sizeof(double)));

        (cudaMalloc( (void**) &sweepParameter1IndicesGpu, sweepParameter1IndicesSize*sizeof(unsigned)));
        (cudaMalloc( (void**) &sweepParameter2IndicesGpu, sweepParameter2IndicesSize*sizeof(unsigned)));
        checkAndDisplayErrors("Memory allocation");
        printf("After malloc gpu\n");

        /*copy data from host to device */
        (cudaMemcpy( initialConditionsGpu, initialConditions, N_EQ1*sizeof(double), cudaMemcpyHostToDevice));
        for(i=0;i<sweepParameterSpaceSize;i++){
                (cudaMemcpy( sweepParameterSetGpu+i*NP1, parameterSet, NP1*sizeof(double), cudaMemcpyHostToDevice));
        }
        (cudaMemcpy( voltageBinBoundariesGpu, voltageBinBoundaries, nV*sizeof(double), cudaMemcpyHostToDevice));
        (cudaMemcpy( intervalBinBoundariesGpu, intervalBinBoundaries, nI*sizeof(double), cudaMemcpyHostToDevice));
        (cudaMemcpy( sweepParameter1IndicesGpu, sweepParameter1Indices, sweepParameter1IndicesSize*sizeof(unsigned), cudaMemcpyHostToDevice));
        (cudaMemcpy( sweepParameter2IndicesGpu, sweepParameter2Indices, sweepParameter2IndicesSize*sizeof(unsigned), cudaMemcpyHostToDevice));
        checkAndDisplayErrors("Error while copying bin boundaries from host to device.");

        /*timing*/
        cudaEvent_t start_event, stop_event;
        cudaEventCreate(&start_event) ;
        cudaEventCreate(&stop_event) ;
        cudaEventRecord(start_event, 0);

        /*Dimensions. */

        int gridXDimension = sweepParameterSpaceSize/NUM_THREADS_PER_BLOCK;
        if(sweepParameterSpaceSize%NUM_THREADS_PER_BLOCK!=0) {
                gridXDimension += 1;
        }
        dim3 dimGrid(gridXDimension,1);
        dim3 dimBlock(NUM_THREADS_PER_BLOCK,1);

        printf(" Num of blocks per grid:       %d\n", gridXDimension);
        printf(" Num of threads per block:     %d\n", NUM_THREADS_PER_BLOCK);
        printf(" Total Num of threads running: %d\n", gridXDimension*NUM_THREADS_PER_BLOCK);
        printf(" Parameters sweepParameter1Count=%d, sweepParameter2Count=%d\n",sweepParameter1Count,sweepParameter2Count);

        /*Call kernel(global function)*/
        sweepThreads<<<dimGrid, dimBlock>>>(kneadingsWeightedSumSetGpu, initialConditionsGpu, sweepIntegratorBuffersGpu, sweepKneadingsBuffersGpu, networkSize, sweepParameterSetGpu,
        								sweepParameter1IndicesGpu, sweepParameter1IndicesSize, sweepParameter1Start, sweepParameter1End, sweepParameter1Count,
										sweepParameter2IndicesGpu, sweepParameter2IndicesSize,  sweepParameter2Start, sweepParameter2End, sweepParameter2Count,
										voltageBinBoundariesGpu, intervalBinBoundariesGpu, nV, nI,
										dt, N, stride,
										kneadingsStart, kneadingsEnd);

        cudaThreadSynchronize();
        cudaEventRecord(stop_event, 0);
        cudaEventSynchronize(stop_event);

        float time_kernel;
        cudaEventElapsedTime(&time_kernel, start_event, stop_event);
        printf("Total time(sec) %f\n", time_kernel/1000);

        /*copy data from device memory to memory. */
        (cudaMemcpy( kneadingsWeightedSumSet, kneadingsWeightedSumSetGpu, sweepParameterSpaceSize*sizeof(double)*numberOfOutputMetrics, cudaMemcpyDeviceToHost));
        checkAndDisplayErrors("Error while copying kneading sum values from device to host.");

        /*Free all allocated memory. */
        cudaFree(kneadingsWeightedSumSetGpu);
        cudaFree(voltageBinBoundariesGpu);
        cudaFree(intervalBinBoundariesGpu);

}

}


int main(){
	double dt=1;
	unsigned N=30000*10;
	unsigned stride = 1;
	unsigned sweepSize = 5;
	double *kneadingsWeightedSumSet;
	int i,j, networkSize=4;
    double voltageBinBoundaries[] = {10}, intervalBinBoundaries [] = {1500};
    double initialConditions[] = {-20. ,   0.4,   0.1,   0. ,   0.9,   0. , -10. ,   0.3,   0.5,
         0.2,   0.2,   0.6, -10. ,   0.8,   0.5,   0.2,   0.2,   0.6,
       -50. ,   0.7,   0.5,   0.2,   0.2,   0.6,   0. ,   0. ,   0. ,
         0. ,   0. ,   0. ,   0. ,   0. ,   0. ,   0. ,   0. ,   0. ,
         0. ,   0. ,   0. ,   0. ,   0. ,   0. ,   0. ,   0. ,   0. ,
         0. ,   0. ,   0. ,   0. ,   0. ,   0. ,   0. ,   0. ,   0. ,
         0. ,   0.  };

    double parameterSet[] = {-8.0e+01,  0.0e+00,  0.0e+00,  4.0e-04, -5.4e+01,  3.0e-03,
       -4.5e+01, -3.0e+00, -8.0e+01,  0.0e+00,  0.0e+00,  4.0e-04,
       -5.4e+01,  3.0e-03, -4.5e+01, -3.0e+00, -8.0e+01,  0.0e+00,
        0.0e+00,  4.0e-04, -5.4e+01,  3.0e-03, -4.5e+01, -3.0e+00,
       -8.0e+01,  0.0e+00,  0.0e+00,  4.0e-04, -5.4e+01,  3.0e-03,
       -4.5e+01, -3.0e+00,  0.0e+00,  0.0e+00,  0.0e+00,  1.0e-03,
        0.0e+00,  0.0e+00,  1.0e-03,  0.0e+00,  0.0e+00,  1.0e-03,
        0.0e+00,  0.0e+00,  1.0e-03,  0.0e+00,  0.0e+00,  0.0e+00,
        0.0e+00,  1.0e-01,  0.0e+00,  1.0e-02,  1.0e-01,  0.0e+00,
        1.0e-02,  0.0e+00,  0.0e+00,  0.0e+00,  0.0e+00,  1.0e-01,
        0.0e+00,  0.0e+00,  1.0e-01,  0.0e+00,  0.0e+00,  5.0e-02,
        0.0e+00,  1.0e-01,  5.0e-02,  0.0e+00,  1.0e-01,  0.0e+00,
        0.0e+00,  0.0e+00,  0.0e+00,  5.0e-02,  0.0e+00,  0.0e+00,
        5.0e-02,  0.0e+00,  0.0e+00,  5.0e-02,  0.0e+00,  8.0e-04,
        5.0e-02,  0.0e+00,  8.0e-04,  0.0e+00,  0.0e+00,  0.0e+00,
        0.0e+00,  1.0e-02,  0.0e+00,  0.0e+00,  1.0e-02,  0.0e+00,
        0.0e+00,  0.0e+00,  0.0e+00,  0.0e+00,  0.0e+00,  0.0e+00,
        0.0e+00,  0.0e+00,  0.0e+00,  1.0e-02,  0.0e+00,  0.0e+00,
        1.0e-02,  0.0e+00,  0.0e+00,  0.0e+00,  0.0e+00,  0.0e+00,
        0.0e+00,  0.0e+00,  0.0e+00,  0.0e+00,  0.0e+00,  0.0e+00,
        0.0e+00,  5.0e-02,  0.0e+00,  0.0e+00,  5.0e-02,  0.0e+00,
        0.0e+00,  0.0e+00,  0.0e+00,  0.0e+00,  0.0e+00,  0.0e+00,
        0.0e+00,  0.0e+00,  0.0e+00,  0.0e+00,  0.0e+00,  2.0e-02,
        0.0e+00,  0.0e+00,  2.0e-02,  0.0e+00,  0.0e+00,  0.0e+00};

    const unsigned sweepParameter1Indices[] = {65, 68};
    const unsigned sweepParameter2Indices[] = {81, 84};

	printf("Enter sweep size :");
	scanf("%d",&sweepSize);
	kneadingsWeightedSumSet = (double *)malloc(sweepSize*sweepSize*sizeof(double)*numberOfOutputMetrics);

    sweep(kneadingsWeightedSumSet, initialConditions, networkSize,
										parameterSet,
										sweepParameter1Indices, 2, 0.04, 0.04, sweepSize,
										sweepParameter2Indices, 2, 0.005, 0.005, sweepSize,
										voltageBinBoundaries, intervalBinBoundaries,
										1, 1,
										dt, N, stride,
										100, 500);

	for(i=0;i<sweepSize*sweepSize*numberOfOutputMetrics;i++){
        printf("%lf \t",kneadingsWeightedSumSet[i]);
	}

}

