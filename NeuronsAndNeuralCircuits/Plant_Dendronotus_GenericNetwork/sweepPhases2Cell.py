
from __future__ import division
#to get default literal division to be floating point arithmetic
import sweepPhasesGeneric
import numpy as np

networkSize = 2

#VInh = -80, VExc = 0, Iapp = 0., gh = 0.0004, Vhh = -54, gleak = 0.003, CaShift = -75., xShift =2.
# For cells Si3s
intrinsicParameterSet = np.array([-55, 0, 0., 0.0001, -85, 0.005, -110., -3.5,
                            -55, 0, 0., 0.0001, -85, 0.005, -110., -3.5], dtype='float64')

csElec = np.asarray(np.matrix('0 0; 0 0'))
#one-sided electrical connections

##Corresponding to each synapse there would be an S variable to be computed at each step of the integration!
csGInh = np.asarray(np.matrix('0 0.04; 0.04 0 '));
csAlphaInh = np.asarray(np.matrix('0 0.01; 0.01 0'));
csBetaInh = np.asarray(np.matrix('0 0.005; 0.005 0'));

csGExc = np.asarray(np.matrix("0 0; 0 0"))
csAlphaExc = np.asarray(np.matrix("0 0 ; 0 0"))
csBetaExc = np.asarray(np.matrix("0 0; 0 0 "))

parameterSet = np.concatenate((intrinsicParameterSet,csElec,csGInh,csAlphaInh,csBetaInh,csGExc,csAlphaExc,csBetaExc), axis=None)
#intrinsicParameterSet.flatten()

#print repr(parameterSet) #repr() prints the numpy array with commas (to use in C code)
#V2 = y[0], Ca2 = y[1], h2 = y[2], n2 = y[3], x2 = y[4], y2 = y[5], s2 = y[6];
initialConditions = np.asarray([0, 1, 0., 0., 0.05, 0.,
                                       20, 1.4, 0., 0., 0.6, 0.], dtype='float64')
    #Initial condition should be set appropriately to separate tonic spiking and quiescent; For these values for Si3, cell 4 is either tonic/quiescent (which is what we are plotting), while cell 3 is always tonic spiking - therefore, plotting it cann't distinguish either case

voltageBinBoundaries = np.asarray([10], dtype='float64');
#intervalBinBoundaries = np.asarray([2500], dtype='float64');
intervalBinBoundaries = np.asarray([1000], dtype='float64');

spikesStart = 250; spikesEnd = 500; nDataPointsToShow=5
N=int(1000*10*60.*2*2/2.5); dt=2.5*.1;

fileName = '2Cell'; dataSaved = True; sweepSize = 50;


def runSweep(sweepParameter1Indices, sweepParameter1Start, sweepParameter1End, sweepParameter2Indices, sweepParameter2Start, sweepParameter2End, ax=None):
    fileName = '2CellSweep_'+str(sweepParameter1Indices)+'_'+str(sweepParameter1Start)+'_'+str(sweepParameter1End)+'_'+str(sweepParameter2Indices)+'_'+str(sweepParameter2Start)+'_'+str(sweepParameter2End)+'_'+str(sweepSize);
    print(fileName)
    sweepPhasesGeneric.plotSweep(parameterSet, initialConditions, networkSize, sweepParameter1Indices, sweepParameter1Start, sweepParameter1End, sweepParameter2Indices, sweepParameter2Start, sweepParameter2End, sweepSize, spikesStart, spikesEnd, dt, N, voltageBinBoundaries, intervalBinBoundaries, fileName, dataSaved, nDataPointsToShow, ax=ax);


#Image for paper

#Images for paper
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

matplotlib.rcParams['savefig.dpi'] = 1200 #2*sweepSize/figSize #300
defaultFontSize = 11; largeFontSize = 14; indexFontSize = 16;
matplotlib.rcParams.update({'font.size': largeFontSize,
              'axes.labelsize': largeFontSize,
              'axes.titlesize': largeFontSize,
              'font.size': defaultFontSize,
              'legend.fontsize': largeFontSize,
              'xtick.labelsize': defaultFontSize,
              'ytick.labelsize': defaultFontSize})

#comment these two for rainbow text function to run properly, along with setting fig.dpi = 1200 described near rainbow text
matplotlib.rc('text', usetex=True)
matplotlib.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
matplotlib.rcParams['savefig.dpi'] = 600
bbox_props_label = dict(boxstyle="square,pad=0.01", fc="white", ec="k", lw=0	)
bbox_props_index = dict(boxstyle="square,pad=0.2", fc="white", ec="k", lw=1., alpha=0.)
bbox_props_fig = dict(boxstyle="square,pad=0.001", fc="white", ec="k", lw=0)

figSizeX=3; figSizeY=3; gridX = 1 ; gridY = 1;

fig = plt.figure(figsize=(figSizeX, figSizeY))
gs1 = gridspec.GridSpec(gridX, gridY)
gs1.update(wspace=0.03, hspace=0.03)  # set the spacing between axes.
ax = plt.subplot(gs1[0,0])

sweepParameter1Indices = np.asarray([25, 26], dtype='uint32'); sweepParameter1Start = 0.0; sweepParameter1End = 0.3;   #alpha
sweepParameter2Indices = np.asarray([29, 30], dtype='uint32'); sweepParameter2Start = 0.0; sweepParameter2End = 0.02; #beta
runSweep(sweepParameter1Indices, sweepParameter1Start, sweepParameter1End, sweepParameter2Indices, sweepParameter2Start, sweepParameter2End, ax=ax)

plt.axis('on')
ax.set_xticks([])
ax.set_yticks([])

xlabel=r'$\mathrm{\alpha_{inh}}$'; ylabel=r'$\mathrm{\beta_{inh}}$';
ax.text(0., -0.01, sweepParameter1Start, ha='left', va='top', bbox=bbox_props_label, transform=ax.transAxes,
        fontsize=defaultFontSize)
ax.text(1., -0.01, sweepParameter1End, ha='right', va='top', bbox=bbox_props_label, transform=ax.transAxes,
        fontsize=defaultFontSize)
ax.text(0.5, -0.01, xlabel, ha='center', va='top', bbox=bbox_props_label, transform=ax.transAxes,
        fontsize=largeFontSize)

ax.text(-0.01, 0, sweepParameter2Start, ha='right', va='bottom', rotation=90, bbox=bbox_props_label,
        transform=ax.transAxes, fontsize=defaultFontSize)
ax.text(-0.01, 1., sweepParameter2End, ha='right', va='top', rotation=90, bbox=bbox_props_label,
        transform=ax.transAxes, fontsize=defaultFontSize)
ax.text(-0.01, 0.5, ylabel, ha='right', va='center', rotation=90, bbox=bbox_props_label, transform=ax.transAxes,
        fontsize=largeFontSize)
plt.subplots_adjust(left=0.1, right=0.999, top=0.999, bottom=0.1)

fig.text(0.7, 0.2, r'$\boldsymbol{tonic+suppressed}$', ha='center', bbox=bbox_props_index, color='w', fontsize=defaultFontSize, weight='roman')
fig.text(0.35, 0.9, r'$\boldsymbol{tonic*2}$', ha='center', bbox=bbox_props_index, color='w', fontsize=defaultFontSize, weight='roman')

fig.savefig('Dendronotus_Si3_HCO.png', bbox_inches='tight')



