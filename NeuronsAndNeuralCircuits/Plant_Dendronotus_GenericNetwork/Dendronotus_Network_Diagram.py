#!/usr/bin/env python
import sys
sys.path.insert(0, '../Tools')
import window as win
import numpy as np
from matplotlib.patches import FancyArrowPatch,Circle,Patch
import math
import matplotlib.cm as colormap
import fitzhugh_allSynapseTypes as fh
import system_allSynapses as sys

class network(win.window):

    title = "Generic Network"
    figsize = (7,6)

    def __init__(self, networkSize, parameterSet, info=None, position=None, system=None,
                 coupling_strengths_chlorine=None, coupling_strengths_potassium=None,
                 coupling_strengths_sodium=None, coupling_strengths_electric=None,
                 ):
        win.window.__init__(self, position)
        self.fig.clear()

        #network size, coupling_strengths, parameters for each cell all set in Network; Every other file should access them from here
        if parameterSet is None :
            self.parameterSet = fh.parameters_generic(networkSize)
        else :
            self.parameterSet = np.asarray(parameterSet)

        if(coupling_strengths_chlorine is None):
            self.coupling_strengths_chlorine = 0.*np.ones((networkSize,networkSize),float);
        else :
            self.coupling_strengths_chlorine = np.asarray(coupling_strengths_chlorine)
        if(coupling_strengths_potassium is None):
            self.coupling_strengths_potassium = 0.*np.ones((networkSize,networkSize),float);
        else :
            self.coupling_strengths_potassium = np.asarray(coupling_strengths_potassium)
        if(coupling_strengths_sodium is None):
            self.coupling_strengths_sodium = 0.*np.ones((networkSize,networkSize),float);
        else :
            self.coupling_strengths_sodium = np.asarray(coupling_strengths_sodium)
        if(coupling_strengths_electric is None):
            self.coupling_strengths_electric = 0.*np.ones((networkSize,networkSize),float);
        else :
            self.coupling_strengths_electric = np.asarray(coupling_strengths_electric)

        #no autapses
        for i in range(0,networkSize):
            self.coupling_strengths_chlorine[i,i]=0
            self.coupling_strengths_potassium[i,i]=0
            self.coupling_strengths_sodium[i,i]=0
            self.coupling_strengths_electric[i,i]=0
        self.COUPLING_LOCKED = True #where is it used?

        self.systemList = []
        for i in range(0, networkSize):
            self.systemList.append(sys.system(parameterArray=self.parameterSet[i*fh.NP:(i+1)*fh.NP]))

        self.info = info
        self.system = system

        self.networkSize = networkSize
        self.CYCLES = 50
        self.network_origin = np.array([0.5,0.5])
        self.cell_radius = 0.25/self.networkSize
        self.ax = self.fig.add_axes([-0.12, -0.1, 1.1, 1.33])
        self.draw_cells(ax=self.ax)

    def getCellPosition(self, cellIndex):
        """
        for a cell index between 0 and networkSize-1, returns its coordinates
        """
        if cellIndex==2:
            cellIndex=3
        elif cellIndex==3:
            cellIndex=2
        return self.network_origin + self.cell_radius*(self.networkSize)*\
                                     np.array([math.cos(3*math.pi/4-math.pi*2*cellIndex/self.networkSize),
                                               math.sin(3*math.pi/4-math.pi*2*cellIndex/self.networkSize)])

    def draw_cells(self, ax):
        """
        Plots the Cells and Couplings
        :param coupling_strengths: 2 dimensional array of coupling strengths; size - networkSize x networkSize; zero diagonal
        :param ax: axis
        """
        colors = ['b', 'g', 'r', 'm', 'y', 'c']
        for i in range(0,self.networkSize) :
            position = np.array(self.getCellPosition(i))
            ax.add_patch(Circle(position, self.cell_radius, fc='k'))#colormap.gist_rainbow(i/float(self.networkSize)),label='test'))
            #ax.add_patch(Circle(position, self.cell_radius, fc=colors[i]))#colormap.gist_rainbow(i/float(self.networkSize)),label='test'))
                                                    #http://wiki.scipy.org/Cookbook/Matplotlib/Show_colormaps
            ax.text(position[0], position[1], i+1, fontsize=600*self.cell_radius, color='w', ha='center', va='center')

        for i in range(0, self.networkSize ) :
            for j in range( 0, self.networkSize) :

                startCoordinates = self.getCellPosition(i)
                endCoordinates = self.getCellPosition(j)
                differenceVector = (endCoordinates - startCoordinates)
                differenceVectorLength = math.sqrt(differenceVector[0] ** 2 + differenceVector[1] ** 2)
                perpendicularVector = np.array([-differenceVector[1],differenceVector[0]])
                perpendicularVectorLength = math.sqrt(perpendicularVector[0]**2+perpendicularVector[1]**2)

                if(self.coupling_strengths_electric[i,j]!=0) :
                    shiftdifferenceVector = differenceVector*1.225*self.cell_radius/differenceVectorLength
                    shiftPerpendicularVector = perpendicularVector*0.30*self.cell_radius/perpendicularVectorLength
                    if i==0:
                        shiftPerpendicularVector = -shiftPerpendicularVector
                    ax.add_patch(FancyArrowPatch(startCoordinates-shiftdifferenceVector/8+1.2*shiftPerpendicularVector,endCoordinates-shiftdifferenceVector/8 +1.2*shiftPerpendicularVector,
                                                 arrowstyle=u'-|>',lw=2,mutation_scale=self.cell_radius*400,
                                                 shrinkA=400*self.cell_radius,shrinkB=400*self.cell_radius,
                                                 color='gray',zorder=-1))

                bbox_props = dict(boxstyle="square,pad=0.0", fc="white", ec="k", lw=0)
                bbox_props_label = dict(boxstyle="square,pad=0.0", fc="white", ec="k", lw=0, alpha=0)
                for vIndex in range(5):
                    ax.text(0.46+vIndex*0.008, .42+vIndex*0.008, 'v', ha='right', va='top', transform=ax.transAxes, fontsize=14, rotation=45, bbox=bbox_props, color='w', fontweight='normal', zorder=1)
                    ax.text(0.46+vIndex*0.008, .42+vIndex*0.008, 'v', ha='right', va='top', transform=ax.transAxes, fontsize=14, rotation=45, bbox=bbox_props_label, color='gray', fontweight='normal', zorder=2)
                for vIndex in range(5):
                    ax.text(0.54+vIndex*0.008, .45-vIndex*0.008, 'v', ha='right', va='top', transform=ax.transAxes, fontsize=14, rotation=-45, bbox=bbox_props, color='w', fontweight='normal', zorder=1)
                    ax.text(0.54+vIndex*0.008, .45-vIndex*0.008, 'v', ha='right', va='top', transform=ax.transAxes, fontsize=14, rotation=-45, bbox=bbox_props_label, color='gray', fontweight='normal', zorder=2)

                if(self.coupling_strengths_chlorine[i,j]!=0) :
                    colorHiddenOrNot = 'b'
                    alpha = 1
                    arrowstyle = u'-'
                    zorder = -1
                    linestyle='-'

                    """
                    if(i+j==5):
                        colorHiddenOrNot = 'grey'
                        alpha = 1
                        linestyle = ':'
                        zorder = -2
                    """
                    shiftdifferenceVector = differenceVector*1.225*self.cell_radius/differenceVectorLength
                    shiftPerpendicularVector = perpendicularVector*0.15*self.cell_radius/perpendicularVectorLength
                    if(self.coupling_strengths_chlorine[j,i]==0):
                        shiftPerpendicularVector=0
                    ax.add_patch(FancyArrowPatch(startCoordinates+shiftPerpendicularVector,endCoordinates+shiftPerpendicularVector,
                                                 arrowstyle=arrowstyle,lw=2,mutation_scale=self.cell_radius*300,
                                                 shrinkA=400*self.cell_radius,shrinkB=400*self.cell_radius,
                                                 color=colorHiddenOrNot, zorder=zorder, alpha=alpha, linestyle=linestyle))
                    ax.add_patch(Circle(endCoordinates-shiftdifferenceVector*0.95+shiftPerpendicularVector, 0.18*self.cell_radius, fc=colorHiddenOrNot ))
                if(self.coupling_strengths_potassium[i,j]!=0) :
                    shiftdifferenceVector = differenceVector*1.225*self.cell_radius/differenceVectorLength
                    shiftPerpendicularVector = perpendicularVector*0.30*self.cell_radius/perpendicularVectorLength
                    if i==3:
                        shiftPerpendicularVector = -shiftPerpendicularVector
                    ax.add_patch(FancyArrowPatch(startCoordinates-shiftdifferenceVector/8+shiftPerpendicularVector,endCoordinates-shiftdifferenceVector/8 +shiftPerpendicularVector,
                                                 #arrowstyle=u'-|>', lw=2, mutation_scale=self.cell_radius * 400,
                                                 arrowstyle=u'-', lw=2, mutation_scale=self.cell_radius * 400,
                                                 shrinkA=400*self.cell_radius,shrinkB=400*self.cell_radius,
                                                 color='purple',zorder=-2))
                    ax.add_patch(
                        FancyArrowPatch(endCoordinates - shiftdifferenceVector / 8 + shiftPerpendicularVector,
                                        endCoordinates - 8.5*shiftdifferenceVector/8 + shiftPerpendicularVector,
                                        arrowstyle=u'-|>', lw=2, mutation_scale=self.cell_radius * 400,
                                        #arrowstyle=u'-', lw=2, mutation_scale=self.cell_radius * 400,
                                        shrinkA=400 * self.cell_radius, shrinkB=400 * self.cell_radius,
                                        color='purple', zorder=-2))
                if(self.coupling_strengths_sodium[i,j]!=0) :
                    shiftPerpendicularVector = perpendicularVector*0.45*self.cell_radius/perpendicularVectorLength
                    ax.add_patch(FancyArrowPatch(startCoordinates+shiftPerpendicularVector,endCoordinates+shiftPerpendicularVector,
                                                 arrowstyle=u'-|>',lw=2,mutation_scale=self.cell_radius*300,
                                                 shrinkA=400*self.cell_radius,shrinkB=400*self.cell_radius,
                                                 color='r'))

#                    ax.text((startCoordinates[0]+endCoordinates[0])/2 - self.cell_radius + shiftPerpendicularVector[0],
#                            (startCoordinates[1]+endCoordinates[1])/2 + self.cell_radius/3 + shiftPerpendicularVector[1],
#                            '%.4f\n'%(coupling_strengths[i,j]), fontsize=200*self.cell_radius)

        ax.set_axis_off()

    #initialStateArray has x0,y0,x1,y1,..x{n-1},y{n-1} ; total 'networkSize' number of ordered pairs each corresponding to one neuron
    def load_initial_condition_generic(self, initialStateArray=None ):
        X = np.zeros(fh.N_EQ1*self.networkSize, float)
        for i in range(0,self.networkSize):
            initialPhase=initialStateArray[i] #if initialStateArray else None
            X[i*fh.N_EQ1:i*fh.N_EQ1+2] = self.systemList[i].load_initial_condition(initialPhase=initialPhase)
        return X


if __name__ == "__main__":

    import pylab as pl

    coupling_strengths = 0.005*np.ones((4,4),float);

    csTwoCell = np.asarray(np.matrix('0 1; 1 0'))
    # Simple one way closed loop
    csChlorineOneWayClosedLoop = np.asarray(np.matrix('0 1 0 0; 0 0 1 0; 0 0 0 1; 1 0 0 0 '))
    # Simple two way closed loop
    csChlorineTwoWayClosedLoop = np.asarray(np.matrix('0 1 0 1; 1 0 1 0; 0 1 0 1; 1 0 1 0 '))
    # Mixed
    csChlorineMixed = np.asarray(np.matrix('0 1 1 0; 1 0 0 1; 0 1 0 1; 1 0 1 0 '))
    # FullyConnected
    csChlorineFullyConnected = np.asarray(np.matrix('0 1 1 1; 1 0 1 1; 1 1 0 1; 1 1 1 0 '))

    cs6cell = np.asarray(np.matrix('0 1 1 0 0 1; 1 0 1 0 1 0; 1 1 0 1 0 0; 0 0 1 0 1 1; 0 1 0 1 0 1; 1 0 0 1 1 0 '))

    parameterSet=np.array([0.4, 0.3, 0., 10., -1.5, -1.2, 1.5, 1.,
                           0.4, 0.3, 0., 10., -1.5, -1.2, 1.5, 1.,
                           0.4, 0.3, 0., 10., -1.5, -1.2, 1.5, 1.,
                           0.4, 0.3, 0., 10., -1.5, -1.2, 1.5, 1.,
                           0.5, 0.4, 0., 10., -1.5, -1.2, 1.5, 1.,
                           0.5, 0.4, 0., 10., -1.5, -1.2, 1.5, 1.])
    #net = network(2,parameterSet, coupling_strengths_chlorine=csTwoCell)#,

    csGInh = np.asarray(np.matrix('0 0.02 0 .0055 ; 0.02 0 .0055 0; 0 0 0 .04; 0 0 .04 0 '));
    csElec = np.asarray(np.matrix('0 0 0 0.002 ; 0 0 0.002 0 ; 0 0 0 0 ; 0 0 0 0'))
    csGExc = np.asarray(np.matrix("0 0 0 0 ; 0 0 0 0 ; 0 0.08 0 0 ; 0.08 0 0 0"))

    net = network(4,parameterSet, coupling_strengths_chlorine=csGInh, coupling_strengths_potassium=csGExc, coupling_strengths_electric=csElec)#,

    pl.show()




