from __future__ import division
#to get default literal division to be floating point arithmetic
import matplotlib

matplotlib.use('Qt4Agg')
import ctypes as ct
from pylab import *
import numpy as np
import matplotlib.pyplot as plt
#import matplotlib.animation as animation
import time


lib = ct.cdll.LoadLibrary('./trajectory.so')
N_EQ=6
NP=8
lib.integrator_rk4.argtypes = [ct.POINTER(ct.c_double), ct.POINTER(ct.c_double),ct.POINTER(ct.c_double),
                          ct.c_uint, ct.c_double,ct.c_uint,ct.c_uint]
def integrator_rk4 (networkSize, y_initial, params, dt=0.001, N=30000, stride = 1):
    output = np.zeros(N*networkSize, float)
    lib.integrator_rk4(y_initial.ctypes.data_as(ct.POINTER(ct.c_double)), output.ctypes.data_as(ct.POINTER(ct.c_double)), params.ctypes.data_as(ct.POINTER(ct.c_double)),
                       ct.c_uint(networkSize), ct.c_double(dt), ct.c_uint(N), ct.c_uint(stride))
    #imshow takes fortran style array input
    return np.reshape(output, (N,networkSize), 'C')


def plotTrajectory(networkSize, parameterSet, initialConditions, N=300000, dt=1, plotIt=True):

    assert parameterSet.size == networkSize*NP+networkSize*networkSize*7

    sInhAndExc = np.zeros([2*networkSize*networkSize])
    initialConditionsAndSynapses = np.concatenate((initialConditions, sInhAndExc), axis=None)
    print(repr(initialConditionsAndSynapses))
    assert  initialConditionsAndSynapses.size == N_EQ*networkSize+2*networkSize*networkSize

    startTime = time.time()
    o = integrator_rk4(networkSize, initialConditionsAndSynapses, parameterSet, N=N,dt=dt)
    endTime = time.time()
    print('Time taken to compute (s): ', endTime-startTime)

    if plotIt:
        fig = plt.figure()
        sp = fig.add_subplot(111)
        sp.set_xlim(0, dt*N)
        sp.set_ylim(100-networkSize*200, 100)
        sp.set_xlabel('time', fontsize=20)
        sp.set_ylabel('V', fontsize=20)

        for i in range(networkSize):
            line, = sp.plot(dt*np.arange(0,N,1.), o[0:N,i] - i*200,'b-')

        plt.show()
    else:
        return o;