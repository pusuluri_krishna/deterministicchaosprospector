from __future__ import division
#to get default literal division to be floating point arithmetic

import trajectoryGeneric
import numpy as np

networkSize = 4

#This file is aligned with D6_circuit_exp_synapse_s2R_s3L_Krishna.m, which is the updated file for Dendronotus
#1,2 Si1s are quiescent while 3,4 Si3s are tonic spiking..
#If we remove excitation, then the rhythms breaks.. excittn is essential for the rhythm..
#The interplay between cross inhibition and cross excitation seems to be controlling the network rhythm - 14 23 vs 41 32

#VInh = -80, VExc = 0, Iapp = 0., gh = 0.0004, Vhh = -54, gleak = 0.003, CaShift = -75., xShift =2.
intrinsicParameterSet = np.array([-80, 0, 0., 0.0001, -85, 0.005, 0., -3.5,
                            -80, 0, 0., 0.0001, -85, 0.005, 0., -3.5,
                            -55, 0, 0., 0.0001, -85, 0.005, -110., -3.5,
                            -55, 0, 0., 0.0001, -85, 0.005, -110., -3.5], dtype='float64')

#Original parameters from D6_circuit_exp_synapse_s2R_s3L_Krishna.m
gElec=0.002; gCInh=.0055; gCExc=0.08; gExcAlpha=0.02; gExcBeta=0.004;
gInh=0.04; alphaInh=0.01; betaInh=0.005;

csElec = gElec*np.asarray(np.matrix('0 0 0 1 ; 0 0 1 0 ; 0 0 0 0 ; 0 0 0 0'))
#one-sided electrical connections

##Corresponding to each synapse there would be an S variable to be computed at each step of the integration!
csGInh = np.asarray(np.matrix('0 0.02 0 '+str(gCInh)+' ; 0.02 0 '+str(gCInh)+' 0; 0 0 0 '+str(gInh)+'; 0 0 '+str(gInh)+' 0 '));
csAlphaInh = np.asarray(np.matrix('0 0.04 0 0.01 ; 0.04 0 0.01 0; 0 0 0 '+str(alphaInh)+'; 0 0 '+str(alphaInh)+' 0 '));
csBetaInh = np.asarray(np.matrix('0 0.001 0 0.001 ; 0.001 0 0.001 0; 0 0 0 '+str(betaInh)+'; 0 0 '+str(betaInh)+' 0 '));

csGExc = np.asarray(np.matrix("0 0 0 0 ; 0 0 0 0 ; 0 "+str(gCExc)+" 0 0 ; "+str(gCExc)+" 0 0 0"))
csAlphaExc = np.asarray(np.matrix("0 0 0 0 ; 0 0 0 0 ; 0 "+str(gExcAlpha)+" 0 0 ;  "+str(gExcAlpha)+" 0 0 0"))
csBetaExc = np.asarray(np.matrix("0 0 0 0 ; 0 0 0 0 ; 0  "+str(gExcBeta)+" 0 0 ;  "+str(gExcBeta)+" 0 0 0"))

defaultParameterSet = np.concatenate((intrinsicParameterSet,csElec,csGInh,csAlphaInh,csBetaInh,csGExc,csAlphaExc,csBetaExc), axis=None)
#intrinsicParameterSet.flatten()

#print repr(parameterSet) #repr() prints the numpy array with commas (to use in C code)
#V2 = y[0], Ca2 = y[1], h2 = y[2], n2 = y[3], x2 = y[4], y2 = y[5], s2 = y[6];
initialConditions = np.asarray([-45, 1, 0., 0., 0.8, 0.0,
                                       -45, 1.1, 0., 0., 0.8, 0.,
                                       0, 1, 0., 0., 0.05, 0.,
                                       20, 1.4, 0., 0., 0.6, 0.], dtype='float64')

parameterSet = defaultParameterSet.copy()
N=int(1000*10*60.*2/2.5); dt=2.5*.1;
#trajectoryGeneric.plotTrajectory(networkSize, parameterSet, initialConditions, N, dt)


#Images for symbolicNetwork paper

#gElec=0.002; gCInh=.006; gCExc=0.04; gExcAlpha=0.02; gExcBeta=0.004; gInh=0.04;
# alphaInh=0.004; betaInh=0.008; for all tonic
# alphaInh=0.005; betaInh=0.008; for 3,4 tonic but 1,2 burst
# alphaInh=0.01; betaInh=0.008; for chaotic
# alphaInh=0.04; betaInh=0.004; for supression
## alphaInh=0.04; betaInh=0.004; for supression (show symmetric alternative) - ignore
# alphaInh=0.02; betaInh=0.008; for 14 spike burst
# alphaInh=0.04; betaInh=0.006; for 18 spike burst - for separate image side by side dendronotus network image
## alphaInh=0.08; betaInh=0.006; for 43 spike burst - ignore

dataSaved = True;

def plotTraj(alphaInh=0.004, betaInh=0.008):
    crossInhibition=0.006; crossExcitation=0.04;
    parameterSet[51] = crossInhibition; parameterSet[54] = crossInhibition;
    parameterSet[105] = crossExcitation; parameterSet[108] = crossExcitation;
    parameterSet[75] = alphaInh; parameterSet[78] = alphaInh;
    parameterSet[91] = betaInh; parameterSet[94] = betaInh;
    return trajectoryGeneric.plotTrajectory(networkSize, parameterSet, initialConditions, N, dt, plotIt=False)

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import pickle
import gzip
from pylab import *

matplotlib.rcParams['savefig.dpi'] = 1200 #2*sweepSize/figSize #300
defaultFontSize = 11; largeFontSize = 14; indexFontSize = 16;
matplotlib.rcParams.update({'font.size': largeFontSize,
              'axes.labelsize': largeFontSize,
              'axes.titlesize': largeFontSize,
              'font.size': defaultFontSize,
              'legend.fontsize': largeFontSize,
              'xtick.labelsize': defaultFontSize,
              'ytick.labelsize': defaultFontSize})

#comment these two for rainbow text function to run properly, along with setting fig.dpi = 1200 described near rainbow text
matplotlib.rc('text', usetex=True)
matplotlib.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
matplotlib.rcParams['savefig.dpi'] = 600
bbox_props = dict(boxstyle="square,pad=0.2", fc="white", ec="k", lw=.5, alpha=0.)
bbox_props_label = dict(boxstyle="square,pad=0.01", fc="white", ec="k", lw=0	)
bbox_props_index = dict(boxstyle="square,pad=0.2", fc="white", ec="k", lw=1., alpha=0.)
bbox_props_fig = dict(boxstyle="square,pad=0.001", fc="white", ec="k", lw=0)

outputdir = "Output/"


#"""
#Dendronotus_AllTrajectories
parameterSet = defaultParameterSet.copy()

figSizeX=6; figSizeY=6; gridX = 2 ; gridY = 2;
fig = plt.figure(figsize=(figSizeX, figSizeY))
gs1 = gridspec.GridSpec(gridX, gridY)
gs1.update(wspace=0.03, hspace=0.03)  # set the spacing between axes.

networkSize=4

if not dataSaved :
    pickleDataFile1 = gzip.open(outputdir+'Dendronotus_AllTrajectories.gz','wb')
else :
    pickleDataFile1 = gzip.open(outputdir+'Dendronotus_AllTrajectories.gz','rb')

axTonic = plt.subplot(gs1[0,0])
axChaotic = plt.subplot(gs1[0,1])
axBurst = plt.subplot(gs1[1,0])
axSuppress = plt.subplot(gs1[1,1])

#burst
if not dataSaved:
    plotData1 = plotTraj(alphaInh=0.02, betaInh=0.008)
    pickle.dump(plotData1, pickleDataFile1)
else:
    plotData1 = pickle.load(pickleDataFile1)

xlimMin=78000; xlimMax=108000;
tSize=int((xlimMax-xlimMin)/10)
axBurst.set_xlim(xlimMin, xlimMax)
axBurst.set_ylim(30-networkSize*120, 30+40)

plt.axis('on')
axBurst.set_xticks([])
axBurst.set_yticks([])

for i in range(networkSize):
    line, = axBurst.plot(dt * np.arange(0, N, 1.), plotData1[0:N, i] - i * 120, color='b', linestyle='-', linewidth=0.75)

#axBurst.plot(arange(xlimMin+int(tSize),xlimMin+int(tSize)+int(tSize),int(tSize/5)), -15.*np.ones(5,float),'k-')
#xbarSize=tSize/1000
#axBurst.annotate(r'$\boldsymbol{'+str('%d'%xbarSize if xbarSize == int(xbarSize) else '%s'%xbarSize)+'s}$', xy=(xlimMin+int(tSize), -18), xytext=(xlimMin+int(tSize), -18), bbox=bbox_props, color='k', va='top', fontSize=defaultFontSize)
#axBurst.plot((xlimMin+int(tSize))*np.ones(4,float), arange(-12,28,10), 'k-')
#axBurst.annotate(r'$\boldsymbol{40mV}$', xy=(xlimMin+int(tSize), -12), xytext=(xlimMin+int(tSize), -12), bbox=bbox_props, color='k', va='bottom', ha='right', rotation=90, fontsize=defaultFontSize)

axBurst.text(0.98, 0.98, r'$\boldsymbol{C}$', ha='right', va='top', bbox=bbox_props_index,
                   transform=axBurst.transAxes, fontsize=defaultFontSize)

#tonic
if not dataSaved:
    plotData1 = plotTraj(alphaInh=0.004, betaInh=0.008)
    pickle.dump(plotData1, pickleDataFile1)
else:
    plotData1 = pickle.load(pickleDataFile1)

xlimMin=78000; xlimMax=108000;
tSize=int((xlimMax-xlimMin)/10)
axTonic.set_xlim(xlimMin, xlimMax)
axTonic.set_ylim(30-networkSize*120, 30+40)

plt.axis('on')
axTonic.set_xticks([])
axTonic.set_yticks([])

for i in range(networkSize):
    line, = axTonic.plot(dt * np.arange(0, N, 1.), plotData1[0:N, i] - i * 120, color='b', linestyle='-', linewidth=0.75)

#axTonic.plot(arange(xlimMin+int(tSize),xlimMin+int(tSize)+int(tSize),int(tSize/5)), -15.*np.ones(5,float),'k-')
#xbarSize=tSize/1000
#axTonic.annotate(r'$\boldsymbol{'+str('%d'%xbarSize if xbarSize == int(xbarSize) else '%s'%xbarSize)+'s}$', xy=(xlimMin+int(tSize), -18), xytext=(xlimMin+int(tSize), -18), bbox=bbox_props, color='k', va='top', fontSize=defaultFontSize)
#axTonic.plot((xlimMin+int(tSize))*np.ones(4,float), arange(-12,28,10), 'k-')
#axTonic.annotate(r'$\boldsymbol{40mV}$', xy=(xlimMin+int(tSize), -12), xytext=(xlimMin+int(tSize), -12), bbox=bbox_props, color='k', va='bottom', ha='right', rotation=90, fontsize=defaultFontSize)

axTonic.text(0.98, 0.98, r'$\boldsymbol{A}$', ha='right', va='top', bbox=bbox_props_index,
                   transform=axTonic.transAxes, fontsize=defaultFontSize)

#chaotic
if not dataSaved:
    plotData1 = plotTraj(alphaInh=0.01, betaInh=0.008)
    pickle.dump(plotData1, pickleDataFile1)
else:
    plotData1 = pickle.load(pickleDataFile1)

xlimMin=81000; xlimMax=111000;
tSize=int((xlimMax-xlimMin)/10)
axChaotic.set_xlim(xlimMin, xlimMax)
axChaotic.set_ylim(30-networkSize*120, 30+40)

plt.axis('on')
axChaotic.set_xticks([])
axChaotic.set_yticks([])

for i in range(networkSize):
    line, = axChaotic.plot(dt * np.arange(0, N, 1.), plotData1[0:N, i] - i * 120, color='b', linestyle='-', linewidth=0.75)

#axChaotic.plot(arange(xlimMin+int(tSize),xlimMin+int(tSize)+int(tSize),int(tSize/5)), -15.*np.ones(5,float),'k-')
#xbarSize=tSize/1000
#axChaotic.annotate(r'$\boldsymbol{'+str('%d'%xbarSize if xbarSize == int(xbarSize) else '%s'%xbarSize)+'s}$', xy=(xlimMin+int(tSize), -18), xytext=(xlimMin+int(tSize), -18), bbox=bbox_props, color='k', va='top', fontSize=defaultFontSize)
#axChaotic.plot((xlimMin+int(tSize))*np.ones(4,float), arange(-12,28,10), 'k-')
#axChaotic.annotate(r'$\boldsymbol{40mV}$', xy=(xlimMin+int(tSize), -12), xytext=(xlimMin+int(tSize), -12), bbox=bbox_props, color='k', va='bottom', ha='right', rotation=90, fontsize=defaultFontSize)

axChaotic.text(0.98, 0.98, r'$\boldsymbol{B}$', ha='right', va='top', bbox=bbox_props_index,
                   transform=axChaotic.transAxes, fontsize=defaultFontSize)

#suppression
if not dataSaved:
    plotData1 = plotTraj(alphaInh=0.04, betaInh=0.004)
    pickle.dump(plotData1, pickleDataFile1)
else:
    plotData1 = pickle.load(pickleDataFile1)

xlimMin=78000; xlimMax=108000;
tSize=int((xlimMax-xlimMin)/10)
axSuppress.set_xlim(xlimMin, xlimMax)
axSuppress.set_ylim(30-networkSize*120, 30+40)

plt.axis('on')
axSuppress.set_xticks([])
axSuppress.set_yticks([])

for i in range(networkSize):
    line, = axSuppress.plot(dt * np.arange(0, N, 1.), plotData1[0:N, i] - i * 120, color='b', linestyle='-', linewidth=0.75)

axSuppress.plot(arange(xlimMin+int(tSize),xlimMin+int(tSize)+int(tSize),int(tSize/5)), -35.*np.ones(5,float),'k-')
xbarSize=tSize/1000
axSuppress.annotate(r'$\boldsymbol{'+str('%d'%xbarSize if xbarSize == int(xbarSize) else '%s'%xbarSize)+'s}$', xy=(xlimMin+int(tSize), -38), xytext=(xlimMin+int(tSize), -38), bbox=bbox_props, color='k', va='top', fontSize=defaultFontSize)
axSuppress.plot((xlimMin+int(tSize))*np.ones(4,float), arange(-32,48,20), 'k-')
axSuppress.annotate(r'$\boldsymbol{80mV}$', xy=(xlimMin+int(tSize), -32), xytext=(xlimMin+int(tSize), -32), bbox=bbox_props, color='k', va='bottom', ha='right', rotation=90, fontsize=defaultFontSize)

axSuppress.text(0.98, 0.98, r'$\boldsymbol{D}$', ha='right', va='top', bbox=bbox_props_index,
                   transform=axSuppress.transAxes, fontsize=defaultFontSize)

### whole image
plt.subplots_adjust(left=0.1, right=0.999, top=0.999, bottom=0.1)

fig.savefig('Dendronotus_AllTrajectories.png', bbox_inches='tight')
#"""

"""
#Dendronotus_Final Trajectory
parameterSet = defaultParameterSet.copy()

figSizeX=4; figSizeY=3; gridX = 1 ; gridY = 1;
fig = plt.figure(figsize=(figSizeX, figSizeY))
gs1 = gridspec.GridSpec(gridX, gridY)
gs1.update(wspace=0.03, hspace=0.03)  # set the spacing between axes.

networkSize=4

if not dataSaved :
    pickleDataFile1 = gzip.open(outputdir+'Dendronotus_FinalTrajectory.gz','wb')
else :
    pickleDataFile1 = gzip.open(outputdir+'Dendronotus_FinalTrajectory.gz','rb')

axBurst = plt.subplot(gs1[0,0])

#burst
if not dataSaved:
    plotData1 = plotTraj(alphaInh=0.04, betaInh=0.006)
    pickle.dump(plotData1, pickleDataFile1)
else:
    plotData1 = pickle.load(pickleDataFile1)

xlimMin=77500; xlimMax=107500;
tSize=int((xlimMax-xlimMin)/10)
axBurst.set_xlim(xlimMin, xlimMax)
axBurst.set_ylim(30-networkSize*120, 30+40)

plt.axis('on')
axBurst.set_xticks([])
axBurst.set_yticks([])

for i in range(networkSize):
    line, = axBurst.plot(dt * np.arange(0, N, 1.), plotData1[0:N, i] - i * 120, color='b', linestyle='-', linewidth=0.75)

axBurst.plot(arange(xlimMin+int(tSize),xlimMin+int(tSize)+int(tSize),int(tSize/5)), -35.*np.ones(5,float),'k-')
xbarSize=tSize/1000
axBurst.annotate(r'$\boldsymbol{'+str('%d'%xbarSize if xbarSize == int(xbarSize) else '%s'%xbarSize)+'s}$', xy=(xlimMin+int(tSize), -38), xytext=(xlimMin+int(tSize), -38), bbox=bbox_props, color='k', va='top', fontSize=defaultFontSize)
axBurst.plot((xlimMin+int(tSize))*np.ones(4,float), arange(-32,48,20), 'k-')
axBurst.annotate(r'$\boldsymbol{80mV}$', xy=(xlimMin+int(tSize), -32), xytext=(xlimMin+int(tSize), -32), bbox=bbox_props, color='k', va='bottom', ha='right', rotation=90, fontsize=defaultFontSize)

### whole image
plt.subplots_adjust(left=0.1, right=0.999, top=0.999, bottom=0.1)

fig.savefig('Dendronotus_FinalTrajectory.png', bbox_inches='tight')

"""