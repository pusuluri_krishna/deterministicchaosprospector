import matplotlib

matplotlib.use('Qt4Agg')
#matplotlib.use('Agg')
import ctypes as ct
from pylab import *
import numpy as np
from matplotlib import colors
from numpy.ma import masked_array
import pickle
import gzip

N_EQ=6
NP=8

lib = ct.cdll.LoadLibrary('./sweepPhases.so')
lib.sweep.argtypes = [ct.POINTER(ct.c_double), ct.POINTER((ct.c_double)),
                      ct.c_uint,
                      ct.POINTER(ct.c_double),
                      ct.POINTER(ct.c_uint), ct.c_uint, ct.c_double, ct.c_double, ct.c_uint,
                      ct.POINTER(ct.c_uint), ct.c_uint, ct.c_double, ct.c_double, ct.c_uint,
                      ct.POINTER(ct.c_double), ct.POINTER(ct.c_double),
                      ct.c_uint, ct.c_uint,
                      ct.c_double, ct.c_uint, ct.c_uint,
                      ct.c_uint, ct.c_uint]
def sweep(parameterSet, initialConditions, networkSize,
          sweepParameter1Indices, sweepParameter1Start, sweepParameter1End,
          sweepParameter2Indices, sweepParameter2Start, sweepParameter2End,
          sweepSize=100, kneadingsStart=100, kneadingsEnd=200, dt=1, N=300000, voltageBinBoundaries = np.array([10.], dtype='float64'), intervalBinBoundaries = np.array([2500], dtype='float64'), stride=1):

    kneadingsWeightedSumSet = np.zeros(sweepSize * sweepSize * 3, float)
    sInhAndExc = np.zeros([2*networkSize*networkSize])
    initialConditionsAndSynapses = np.concatenate((initialConditions, sInhAndExc), axis=None)

    assert parameterSet.size == networkSize*NP+networkSize*networkSize*7
    assert  initialConditionsAndSynapses.size == N_EQ*networkSize+2*networkSize*networkSize
    # using this not just for kneadingSum; for each point in the sweep we compute DCP KneadingSum, burst period of cell 1, spikes per burst of cell 1 for the trajectory and put everything in this array sequentially
    lib.sweep(kneadingsWeightedSumSet.ctypes.data_as(ct.POINTER(ct.c_double)), initialConditionsAndSynapses.ctypes.data_as(ct.POINTER(ct.c_double)),
              ct.c_uint(networkSize),
              parameterSet.ctypes.data_as(ct.POINTER(ct.c_double)),
              sweepParameter1Indices.ctypes.data_as(ct.POINTER(ct.c_uint)), ct.c_uint(len(sweepParameter1Indices)), ct.c_double(sweepParameter1Start), ct.c_double(sweepParameter1End), ct.c_uint(sweepSize),
              sweepParameter2Indices.ctypes.data_as(ct.POINTER(ct.c_uint)), ct.c_uint(len(sweepParameter2Indices)), ct.c_double(sweepParameter2Start), ct.c_double(sweepParameter2End), ct.c_uint(sweepSize),
              voltageBinBoundaries.ctypes.data_as(ct.POINTER(ct.c_double)), intervalBinBoundaries.ctypes.data_as(ct.POINTER(ct.c_double)),
              ct.c_uint(len(voltageBinBoundaries)), ct.c_uint(len(intervalBinBoundaries)),
              ct.c_double(dt), ct.c_uint(N), ct.c_uint(stride),
              ct.c_uint(kneadingsStart), ct.c_uint(kneadingsEnd))
    # imshow takes fortran style array input
    return np.reshape(kneadingsWeightedSumSet, (3, sweepSize, sweepSize), 'F')

#For LZ, use the returned value between 0 to 1 with a grey colormap
#For PC, return the circular hash of the period string, and use it with a custom colormap; If needed, we can also return, say, 6digits from the hash and use 2 each for R,G,B

colorMapLevels = 2 ** 8

blue = np.linspace(0.01, 1, colorMapLevels)
red = 1 - blue
min = 0.;
max = .8
green = min + np.random.random(colorMapLevels) * (max - min)
RGB = np.column_stack((red, green, blue))
customColorMap = colors.ListedColormap(RGB)

blue = np.linspace(0., 1., colorMapLevels)
red = blue
green = blue
RGB = np.column_stack((red, green, blue))
customGrayColorMap = colors.ListedColormap(RGB)

def plotSweep(parameterSet, initialConditions, networkSize,
          sweepParameter1Indices, sweepParameter1Start, sweepParameter1End,
          sweepParameter2Indices, sweepParameter2Start, sweepParameter2End,
          sweepSize=100, kneadingsStart=100, kneadingsEnd=200, dt=1, N=300000, voltageBinBoundaries = np.array([10.], dtype='float64'), intervalBinBoundaries = np.array([2500], dtype='float64'), fileName='Test', dataSaved=True, nDataPointsToShow=10, plotDCP=False, plotSpikeFrequency=False, plotBurstPeriod=False, plotSpikesPerBurst=False, plotDCPSpikes=True, ax=None, plotDataPoints=True):

    print('Running sweep')
    axPassed=False
    outputdir = 'Output/sweepPhase_'
    matplotlib.rcParams['savefig.dpi'] = 600

    if(ax == None):
        figSize = 5
        fig = figure(figsize=(figSize, figSize))
        fig.clear()
    else:
        axPassed=True


    filename = outputdir + fileName

    if not dataSaved:
        pickleDataFile1 = gzip.open(filename + '.gz', 'wb')
        sweepData = sweep(parameterSet, initialConditions, networkSize,
                          sweepParameter1Indices, sweepParameter1Start, sweepParameter1End,
                          sweepParameter2Indices, sweepParameter2Start, sweepParameter2End,
                          sweepSize, kneadingsStart, kneadingsEnd, dt, N,
                          voltageBinBoundaries, intervalBinBoundaries)
        pickle.dump(sweepData, pickleDataFile1)
    else:
        pickleDataFile1 = gzip.open(filename + '.gz', 'rb')
        sweepData = pickle.load(pickleDataFile1)
    # print sweepData
    pickleDataFile1.close()

    aMin = sweepParameter1Start;
    aMax = sweepParameter1End;
    bMin = sweepParameter2Start;
    bMax = sweepParameter2End;
    aStep = (aMax - aMin) / sweepSize
    bStep = (bMax - bMin) / sweepSize

    if plotDCP:
        # print sweepData
        # sweepData[sweepData == -1.1] = 0.

        # PC hash can be any integer value (positive/negative), LZ lies between 0 and 1, -0.5 for insufficient N
        sweepDataInsufficientN = masked_array(sweepData[0], sweepData[0] != -0.5)
        sweepDataComputed = masked_array(sweepData[0], sweepData[0] == -0.5)

        sweepDataChaotic = masked_array(sweepDataComputed, (sweepData[0] <= 0) | (sweepData[0] >= 1))
        # for tonic spiking, LZ complexity could be 1 or 2 depending on whether both or just one fire, so %1.
        # sweepDataChaotic = masked_array(sweepDataComputed, sweepData <= 0)  ## for tonic spiking,

        sweepDataPeriodic = masked_array(sweepDataComputed, (sweepData[0] > 0) & (sweepData[0] < 1))
        # sweepDataPeriodic = masked_array(sweepDataComputed, sweepData > 0)

        # to force common color scheme without scaling across images, use the vmin and vmax arguments of imshow(), and make sure they are the same for all your images

        # imshow(sweepDataPeriodic, ##hash seems to have a large range causing unclear figures. Due to this, tonic spiking
        # and bursting are shown in same color for 1500 and above, although running a single trajectory sweep prints them
        # differently.. for tonic spiking there is either 1 or 2 symbols depending on whether one or both spike tonically..
        # so LZ complexity not very meaningful.. for one symbol might just show it as periodic
        imshow(sweepDataPeriodic.astype(np.int64) % colorMapLevels,
               extent=[aMin, aMax, bMin, bMax],
               aspect=(aMax - aMin) / (bMax - bMin),
               cmap=customColorMap,
               origin='lower',  # vmin=-1., vmax=0.1
               )  # making vmax 0.1 instead of 0. to match the colormaps for short and long kneadings
        # colorbar()

        imshow(sweepDataChaotic % 1.,
               # for tonic spiking, LZ complexity could be 1 or 2 depending on whether both or just one fire, so %1.
               extent=[aMin, aMax, bMin, bMax],
               aspect=(aMax - aMin) / (bMax - bMin),
               cmap=customGrayColorMap,
               origin='lower')

        imshow(sweepDataInsufficientN,
               extent=[aMin, aMax, bMin, bMax],
               aspect=(aMax - aMin) / (bMax - bMin),
               cmap=colors.ListedColormap(['white']),
               origin='lower')

        fig.suptitle("DCP");
        fig.savefig(filename + ".png")


    # Avg. burst period
    if plotBurstPeriod:
        figSize = 5
        fig = figure(figsize=(figSize, figSize))
        ax = fig.add_subplot(111)
        ax.imshow(sweepData[1],
                  extent=[aMin, aMax, bMin, bMax],
                  aspect=(aMax - aMin) / (bMax - bMin),
                  cmap='Reds',
                  origin='lower', vmin=5000, vmax=50000
                  )  # making vmax 0.1 instead of 0. to match the colormaps for short and long kneadings
        for i in np.arange(0, sweepSize, int(sweepSize/nDataPointsToShow)):
            for j in np.arange(0, sweepSize, int(sweepSize/nDataPointsToShow)):
                ax.plot(aMin + i * aStep, bMin + j * bStep, 'o', color='k', ms=1)
                ax.text(aMin + i * aStep, bMin + j * bStep - bStep / 10.,
                        round(sweepData[1][j, i], 2), color='black', ha='center',
                        va='center', size=4)
        fig.suptitle("Avg. burst period");
        fig.savefig(filename + "_burstPeriod.png")

    # Avg. spikes per burst
    if plotSpikesPerBurst:
        figSize = 5
        fig = figure(figsize=(figSize, figSize))
        ax = fig.add_subplot(111)
        ax.imshow(sweepData[2],
                  extent=[aMin, aMax, bMin, bMax],
                  aspect=(aMax - aMin) / (bMax - bMin),
                  cmap=customColorMap,
                  origin='lower', vmin=5., vmax=50
                  )  # making vmax 0.1 instead of 0. to match the colormaps for short and long kneadings
        for i in np.arange(0, sweepSize, int(sweepSize / nDataPointsToShow)):
            for j in np.arange(0, sweepSize, int(sweepSize / nDataPointsToShow)):
                ax.plot(aMin + i * aStep, bMin + j * bStep, 'o', color='k', ms=1)
                ax.text(aMin + i * aStep, bMin + j * bStep - bStep / 10.,
                        round(sweepData[2][j,i], 2), color='black', ha='center',
                        va='center', size=4)
        fig.suptitle("Avg. spikes per burst");
        fig.savefig(filename + "_spikesPerBurst.png")

    # Avg. spike frequency within a burst
    if plotSpikeFrequency:
        figSize = 5
        fig = figure(figsize=(figSize, figSize))
        ax = fig.add_subplot(111)
        ax.imshow(sweepData[2]/sweepData[1],
                  extent=[aMin, aMax, bMin, bMax],
                  aspect=(aMax - aMin) / (bMax - bMin),
                  cmap=customColorMap,
                  origin='lower'  # , vmin=.002, vmax=0.02
                  )  # making vmax 0.1 instead of 0. to match the colormaps for short and long kneadings
        for i in np.arange(0, sweepSize, int(sweepSize / nDataPointsToShow)):
            for j in np.arange(0, sweepSize, int(sweepSize / nDataPointsToShow)):
                ax.plot(aMin + i * aStep, bMin + j * bStep, 'o', color='k', ms=1)
                ax.text(aMin + i * aStep, bMin + j * bStep - bStep / 10.,
                        round(sweepData[2][j,i]/sweepData[1][j, i], 4), color='black', ha='center',
                        va='center', size=4)
        fig.suptitle("Avg. burst spike frequency ");
        fig.savefig(filename + "_avgBurstSpikeFrequency.png")


    # Combined sweep of Avg. spikes per burst with tonic, quiescent, bursting or chaotic
    if plotDCPSpikes:
        #spikesPerBurstQuiescent = masked_array(sweepData[2], (sweepData[1] != 0) | (sweepData[2]!=0))
        spikesPerBurstQuiescent = masked_array(sweepData[2], sweepData[2] != 0)
        #spikesPerBurstTonic = masked_array(sweepData[2], (sweepData[1] != 0) | (sweepData[2]==0))
        spikesPerBurstTonic = masked_array(sweepData[2], sweepData[2] < 1000)
        burstChaotic = masked_array(sweepData[0], (sweepData[0] <= 0) | (sweepData[0] >= 1))
        spikesInsufficient= masked_array(sweepData[2], sweepData[2]>=0)

        if ax==None:
            print('Ax is None; Creating figure;')
            figSize = 5
            fig = figure(figsize=(figSize, figSize))
            ax = fig.add_subplot(111)
        else:
            axPassed=True

        blueLevels = 15
        blue = np.linspace(1, 0, blueLevels)
        red = np.linspace(0.0, 0, blueLevels)
        green = np.linspace(0.0, 0, blueLevels)
        RGB = np.column_stack((red, green, blue))
        blueColorMap = colors.ListedColormap(RGB)

        ax.imshow(sweepData[2],
                  extent=[aMin, aMax, bMin, bMax],
                  aspect=(aMax - aMin) / (bMax - bMin),
                  #cmap=blueColorMap,
                  #cmap=plt.get_cmap('tab20b'),
                  cmap=plt.get_cmap('Blues'),
                  origin='lower', vmin=-30, vmax=25.
                  )  #
        ax.imshow(spikesPerBurstTonic,
                  extent=[aMin, aMax, bMin, bMax],
                  aspect=(aMax - aMin) / (bMax - bMin),
                  cmap=colors.ListedColormap(['red']),
                  origin='lower')
        ax.imshow(spikesPerBurstQuiescent,
               extent=[aMin, aMax, bMin, bMax],
               aspect=(aMax - aMin) / (bMax - bMin),
               cmap=colors.ListedColormap(['green']), #use green here to distinguish quiscent vs where trajectory not long enough
               origin='lower')
        ax.imshow(burstChaotic % 1.,
               # for tonic spiking, LZ complexity could be 1 or 2 depending on whether both or just one fire, so %1.
               extent=[aMin, aMax, bMin, bMax],
               aspect=(aMax - aMin) / (bMax - bMin),
               cmap=customGrayColorMap,
               origin='lower', vmin=-0.2, vmax=1.)

        #white would be where trajectory not long enough or both cells quiescent
        ax.imshow(spikesInsufficient,
                  extent=[aMin, aMax, bMin, bMax],
                  aspect=(aMax - aMin) / (bMax - bMin),
                  cmap=colors.ListedColormap(['white']),
                  origin='lower')

        if(plotDataPoints):
            for i in np.arange(0, sweepSize, int(sweepSize / nDataPointsToShow)):
                for j in np.arange(0, sweepSize, int(sweepSize / nDataPointsToShow)):
                    #ax.plot(aMin + i * aStep, bMin + j * bStep, 'o', color='k', ms=1)
                    spikes=int(round(sweepData[2][j,i], 2))
                    if(spikes>1 and spikes<50):
                        ax.text(aMin + i * aStep, bMin + j * bStep - bStep / 10.,
                                spikes, color='w', ha='center',
                                va='center', fontsize=9)
        if axPassed==False:
            fig.suptitle("DCP avg. spikes per burst");
            fig.savefig(filename + "_DCPSpikesPerBurst.png")

    #show()
