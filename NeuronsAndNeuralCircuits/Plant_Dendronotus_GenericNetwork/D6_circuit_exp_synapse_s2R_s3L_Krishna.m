%D6_circuit_exp_synapse_s2R_s3L_Krishna.m is the most updated file
%1,2 Si1s are quiescent while 3,4 Si3s are tonic spiking.. 
%If we remove excitation, then the rhythms breaks.. excittn is essential for the rhythm.. 


%%% g12,g21, g34, g43, g32, g41, g23, ca_shift, x_shift - sweep parameters 

clear all 
tmax=60; % max time in seconds

% STEP 1
%Parameters of the models: 1,2 quiescent while 3,4 are tonic spikers 
Ca_shift1 =  0;
Ca_shift2 =  -0;
Ca_shift3 =  -110.0;
Ca_shift4 =  -110.0;
Ca_shift5 =  -56.5;

% avoid endogenous bursters can be made even smaller 
x_shift =  -3.5;

%STEP 2 equating 
% electric 0.01
gelec=0.002 ;

% STEP 3 Inhibition between 2s
% inhibitory coupling strength 
g12=0.02;
g21=g12;
% alpha inh synapses 
alpha2 = .04;
beta2 =  0.001;

%Inh from 2 to 3
g23=.0055% ;
g14=g23;
alpha23 = .01;
beta23 =  0.001;

%STEP 3 Inhibition between 3s
% This inhibition shoudl be weaker ... 
g34=.04% 
g43=g34;
alpha3 = .01;
beta3 =  0.005;

%STEP 4 excitation from 3s to 2s 
%exc
g32= 0.08 ; %just removing these excitations breaks the network and HCO rhythms 
%g32 = 0.;
g41=g32;
alpha_ex = .02;
beta_ex =  0.004;


%Exc synapses from S1s projected to Si2s and Si3s
alpha5 = .08;
beta5 =  0.005;
g1n=.000;

%NOISE
%----------------------
cutoff=30.0; % cutoff frequency in Hz
noise=.01;
%----------------------


%inhibitory current pulse
Iapp=-0.0

% Pulse between 40 and 100 sec 
t1=60*1000; % this time is in msec
t2=95*1000; % this time is in msec
      
% h-current      
gh    = 0.0001;
Vhh   = -85;

% Intergration  - no need to cnange   
tf=tmax*1000;   % max time in msec   
step=0.25;       % time step in msec
tsamp=1; % sampling interval to save data in msec

cut1=cutoff/1000; % cutoff frequency converted to msec
% Initial values
%Si2-3/L/R
V1= -45; V2= -45; Ca1=1.; Ca2=1.1; h1 =0; h2 =0; n1 =0; n2 =0; 
x1 =0.8; x2 =0.8; y1 =0; y2 =0; s1 =0; s2 =0; m1 =0; m2 =0;
V3= 0; V4= 0; Ca3=1; Ca4= 1; h3 =0; h4 =0; n3 =0; n4 =0; 
x3 =0.05; x4 =0.6; y3 =0; y4 =0; s3 =0; s4 =0; m3 =0; m4 =0;
s3_ex=0; s4_ex=0; s23=0; s14=0;
% Si1L/R
V5= 0; V6= 0; Ca5=-.3; Ca6=.0;  h5 =0; h6 =0; n5 =0; n6 =0; 
x5 =0.9; x6 =0.9; y5 =0; y6 =0; s5 =0; s6 =0; m5 =0; m6 =0;

tic
[rnd1,nt] = bandlimnoise (cut1,step,tf);
[rnd2,nt] = bandlimnoise (cut1,step,tf);
[rnd3,nt] = bandlimnoise (cut1,step,tf);
[rnd4,nt] = bandlimnoise (cut1,step,tf);
rnd1=rnd1*noise; rnd2=rnd2*noise;   
rnd3=rnd3*noise; rnd4=rnd4*noise;

nt1=round(tmax/tsamp)+1;
is=round(tsamp/step);

time=zeros(nt1,1);
vv1=time;vv2=time;ss1=time;ss2=time;mm1=time;mm2=time;Caa1=time;Caa2=time;xx1=time;xx2=time; 
vv3=time;vv4=time;ss3=time;ss4=time;mm3=time;mm4=time;Caa3=time;Caa4=time;xx3=time;xx4=time; 
ss3_ex=time; ss4_ex=time; yy1=time; yy2=time; ss23=time; ss14=time; 
% Si1L/R
vv5=time; vv6=time; ss5=time;ss5=time;mm5=time;mm5=time;Caa5=time;Caa6=time;xx5=time;xx6=time; 
yy5=time; yy6=time; mm5=time; mm6=time; 

%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
% CALCULATE heaviside functions in advance! This speeds up calculation!
heav1=zeros(nt,1); heav2=heav1;
j1=round(t1/step)+1; j2=round(t2/step)+1;
heav1(j1:nt)=1; heav2(j2:nt)=1; heav=heav1-heav2; 
clear heav1, heav2;

% This is a new version with shorter spikes: speeds up sodium and potasium currents and scales the v-dependent time rates  
% 8 instead of 4 for g_I and 1.3 instead of 0.3 for g_n and 12.5/2 in n1/2 % and h1 and h2

j=0;
for i=1:nt
 tt=(i-1)*step;
V1 =V1 +step*(Iapp*heav(i)+8*((0.1*(50-(127*V1/105+8265/105))/(exp((50 - (127*V1/105 ...
    +8265/105))/10) - 1))/((0.1*(50 - (127*V1/105 + 8265/105))/(exp((50 - (127*V1/105 + 8265/105))/10) - 1))+...
    (4*exp((25 - (127*V1/105 + 8265/105))/18))))^3*h1*(30 - V1) + 1.3*n1^4*(-75 - V1)+0.01*x1*(30-V1) ...
    +0.03*Ca1/(.5 + Ca1)*(-75 - V1)-0.005*(V1+50)  +gh*((1/(1+exp((V1+75)/7.8)))^3)*y1*(+120-V1)...
-g21*(V1+80)*s2 -g41*(V1-0)*s4_ex -0.0*g1n*(V1-10)*s5  +rnd1(i));

V2= V2 +step*(0*Iapp*heav(i)+8*((0.1*(50-(127*V2/105+8265/105))/(exp((50 - (127*V2/105 ...
    +8265/105))/10) - 1))/((0.1*(50 - (127*V2/105 + 8265/105))/(exp((50 - (127*V2/105 + 8265/105))/10) - 1))+...
    (4*exp((25 - (127*V2/105 + 8265/105))/18))))^3*h2*(30 - V2) + 1.3*n2^4*(-75 - V2)+0.01*x2*(30-V2) ...
    +0.03*Ca2/(.5 + Ca2)*(-75 - V2)-0.005*(V2+50) +gh*((1/(1+exp((V2+75)/7.8)))^3)*y2*(-V2+120)...
-g12*(V2+80)*s1 -g32*(V2-0)*s3_ex  -0.0*g1n*(V2-30)*s5 +rnd2(i));

V3= V3 +step*(0*Iapp*heav(i)+8*((0.1*(50-(127*V3/105+8265/105))/(exp((50 - (127*V3/105 ...
    +8265/105))/10) - 1))/((0.1*(50 - (127*V3/105 + 8265/105))/(exp((50 - (127*V3/105 + 8265/105))/10) - 1))+...
    (4*exp((25 - (127*V3/105 + 8265/105))/18))))^3*h3*(30 - V3) + 1.3*n3^4*(-75 - V3)+0.01*x3*(30-V3) ...
    +0.03*Ca3/(.5 + Ca3)*(-75 - V3)-0.005*(50+V3) +gh*((1/(1+exp((V3+65)/7.8)))^3)*y3*(-V3+120)...
-g43*(V3+55)*s4 -g23*(V3+75)*s23 +gelec*(V2-V3) -10*g1n*(V3-10)*s5  +rnd3(i));

V4= V4 +step*(0*Iapp*heav(i)+8*((0.1*(50-(127*V4/105+8265/105))/(exp((50 - (127*V4/105 ...
    +8265/105))/10) - 1))/((0.1*(50 - (127*V4/105 + 8265/105))/(exp((50 - (127*V4/105 + 8265/105))/10) - 1))+...
    (4*exp((25 - (127*V4/105 + 8265/105))/18))))^3*h4*(30 - V4) + 1.3*n4^4*(-75 - V4)+0.01*x4*(30-V4) ...
    +0.03*Ca4/(.5 + Ca4)*(-75 - V4)-0.005*(50+V4) +gh*((1/(1+exp(-(V4+75)/7.8)))^3)*y4*(-V4+120)...
-g34*(V4+55)*s3 -g14*(V4+75)*s14 +gelec*(V1-V4) -g1n*(V4-10)*s5  +rnd4(i));

V5= V5 +step*(8*((0.1*(50-(127*V5/105+8265/105))/(exp((50 - (127*V5/105 ...
    +8265/105))/10) - 1))/((0.1*(50 - (127*V5/105 + 8265/105))/(exp((50 - (127*V5/105 + 8265/105))/10) - 1))+...
    (4*exp((25 - (127*V5/105 + 8265/105))/18))))^3*h5*(30 - V5) + 1.3*n5^4*(-75 - V5)+0.01*x5*(30-V5) ...
    +0.03*Ca5/(.5 + Ca5)*(-75 - V5)-0.005*(50+V5) +gh*((1/(1+exp((V5+75)/7.8)))^3)*y5*(-V5+120) );

% 
% V6= V6 +step*(8*((0.1*(50-(127*V6/105+8265/105))/(exp((50 - (127*V6/105 ...
%     +8265/105))/10) - 1))/((0.1*(50 - (127*V6/105 + 8265/105))/(exp((50 - (127*V6/105 + 8265/105))/10) - 1))+...
%     (4*exp((25 - (127*V6/105 + 8265/105))/18))))^3*h6*(30 - V6) + 1.3*n6^4*(-75 - V6)+0.01*x6*(30-V6) ...
%     +0.03*Ca6/(.5 + Ca6)*(-75 - V6)+0.005*(-50 - V6) +gh*((1/(1+exp((V6+65)/7.8)))^3)*y6*(-V6+120));


Ca1=Ca1+step*(0.0003*(0.0085*x1*(140-V1+Ca_shift1)-Ca1));
Ca2=Ca2+step*(0.0003*(0.0085*x2*(140-V2+Ca_shift2)-Ca2));
Ca3=Ca3+step*(0.0003*(0.0085*x3*(140-V3+Ca_shift3)-Ca3));
Ca4=Ca4+step*(0.0003*(0.0085*x4*(140-V4+Ca_shift4)-Ca4));
Ca5=Ca5+step*(0.0003*(0.0085*x5*(140-V5+Ca_shift5)-Ca5));
%Ca6=Ca6+step*(0.0003*(0.0085*x6*(140-V6+Ca_shift6)-Ca6));

x1 =x1+step*(((1/(exp(0.1*(-V1-50+x_shift))+1))-x1)/235);
x2 =x2+step*(((1/(exp(0.1*(-V2-50+x_shift))+1))-x2)/235);
x3 =x3+step*(((1/(exp(0.1*(-V3-50+x_shift))+1))-x3)/235);
x4 =x4+step*(((1/(exp(0.1*(-V4-50+x_shift))+1))-x4)/235);
x5 =x5+step*(((1/(exp(0.1*(-V5-50+x_shift))+1))-x5)/235);

h1 =h1+step*(((1-h1)*(0.07*exp((25 - (127*V1/105 + 8265/105))/20))-h1*(1.0/(1 + exp((55 - (127*V1/105 + 8265/105))/10))))/(12.5/1));
h2 =h2+step*(((1-h2)*(0.07*exp((25 - (127*V2/105 + 8265/105))/20))-h2*(1.0/(1 + exp((55 - (127*V2/105 + 8265/105))/10))))/(12.5/1));
h3 =h3+step*(((1-h3)*(0.07*exp((25 - (127*V3/105 + 8265/105))/20))-h3*(1.0/(1 + exp((55 - (127*V3/105 + 8265/105))/10))))/(12.5/1));
h4 =h4+step*(((1-h4)*(0.07*exp((25 - (127*V4/105 + 8265/105))/20))-h4*(1.0/(1 + exp((55 - (127*V4/105 + 8265/105))/10))))/(12.5/1));
h5 =h5+step*(((1-h5)*(0.07*exp((25 - (127*V5/105 + 8265/105))/20))-h5*(1.0/(1 + exp((55 - (127*V5/105 + 8265/105))/10))))/(12.5/1));
%h6 =h6+step*(((1-h6)*(0.07*exp((25 - (127*V6/105 + 8265/105))/20))-h6*(1.0/(1 + exp((55 - (127*V6/105 + 8265/105))/10))))/(12.5/1));

n1 =n1+step*(((1-n1)*(0.01*(55 - (127*V1/105 + 8265/105))/(exp((55 - (127*V1/105 + 8265/105))/10) - 1))-n1*(0.125*exp((45 - (127*V1/105 + 8265/105))/80)))/(12.5/1));
n2 =n2+step*(((1-n2)*(0.01*(55 - (127*V2/105 + 8265/105))/(exp((55 - (127*V2/105 + 8265/105))/10) - 1))-n2*(0.125*exp((45 - (127*V2/105 + 8265/105))/80)))/(12.5/1));
n3 =n3+step*(((1-n3)*(0.01*(55 - (127*V3/105 + 8265/105))/(exp((55 - (127*V3/105 + 8265/105))/10) - 1))-n3*(0.125*exp((45 - (127*V3/105 + 8265/105))/80)))/(12.5/1));
n4 =n4+step*(((1-n4)*(0.01*(55 - (127*V4/105 + 8265/105))/(exp((55 - (127*V4/105 + 8265/105))/10) - 1))-n4*(0.125*exp((45 - (127*V4/105 + 8265/105))/80)))/(12.5/1));
n5 =n5+step*(((1-n5)*(0.01*(55 - (127*V5/105 + 8265/105))/(exp((55 - (127*V5/105 + 8265/105))/10) - 1))-n5*(0.125*exp((45 - (127*V5/105 + 8265/105))/80)))/(12.5/1));
%n6 =n6+step*(((1-n6)*(0.01*(55 - (127*V6/105 + 8265/105))/(exp((55 - (127*V6/105 + 8265/105))/10) - 1))-n6*(0.125*exp((45 - (127*V6/105 + 8265/105))/80)))/(12.5/1));

y1 =y1+step*(.5*((1/(1+exp(1*(V1-Vhh))))-y1)/(7.1+10.4/(1+exp((V1+75)/2.2))));
y2 =y2+step*(.5*((1/(1+exp(1*(V2-Vhh))))-y2)/(7.1+10.4/(1+exp((V2+75)/2.2))));
y3 =y3+step*(.5*((1/(1+exp(1*(V3-Vhh))))-y3)/(7.1+10.4/(1+exp((V3+75)/2.2))));
y4 =y4+step*(.5*((1/(1+exp(1*(V4-Vhh))))-y4)/(7.1+10.4/(1+exp((V4+75)/2.2))));
y5 =y5+step*(.5*((1/(1+exp(1*(V5-Vhh))))-y5)/(7.1+10.4/(1+exp((V5+75)/2.2))));
%y6 =y6+step*(.5*((1/(1+exp(1*(V6-Vhh))))-y6)/(7.1+10.4/(1+exp((V6+75)/2.2))));

s1 =s1+step*(alpha2*s1*(1-s1)/(1+exp(-10*(V1+30)))-beta2*(s1-0.01));
s2 =s2+step*(alpha2*s2*(1-s2)/(1+exp(-10*(V2+30)))-beta2*(s2-0.01));
s14 =s14+step*(s14*alpha23*(1-s14)/(1+exp(-10*(V1+30)))-beta23*(s14-.01));
s23 =s23+step*(s23*alpha23*(1-s23)/(1+exp(-10*(V2+30)))-beta23*(s23-.01));
s3 =s3+step*(alpha3*(1-s3)/(1+exp(-10*(V3+30)))-beta3*s3);
s4 =s4+step*(alpha3*(1-s4)/(1+exp(-10*(V4+30)))-beta3*s4);
s3_ex =s3_ex+step*(alpha_ex*(1-s3_ex)/(1+exp(-10*(V3+30)))-beta_ex*(s3_ex-0.01));
s4_ex =s4_ex+step*(alpha_ex*(1-s4_ex)/(1+exp(-10*(V4+30)))-beta_ex*(s4_ex-0.01));
s5 =s5+step*(alpha5*(1-s5)/(1+exp(-10*(V5+30)))-beta5*s5);%
%s6 =s6+step*(alpha5*(1-s4)/(1+exp(-10*(V6+20)))-beta5*s6);


% m1 =m1 +step*((1/(1+exp(-(V1+20)))-m1)/taum2);
% m2 =m2 +step*((1/(1+exp(-(V2+20)))-m2)/taum2);
% m23=m23+step*((1/(1+exp(-(V2+20)))-m23)/taum23);
% m14=m14+step*((1/(1+exp(-(V1+20)))-m14)/taum23);
% m3 =m3 +step*((1/(1+exp(-(V3+20)))-m3)/taum3);
% m4 =m4 +step*((1/(1+exp(-(V4+20)))-m4)/taum3);


% Do not need to save every point!
% we do not plot y,h,n
if mod(i,is) ==0
    j=j+1;
    time(j)=tt; vv1(j)=V1; vv2(j)=V2; vv3(j)=V3; vv4(j)=V4;vv5(j)=V5; vv6(j)=V6;
    ss1(j)=s1; ss2(j)=s2; ss3(j)=s3; ss4(j)=s4; ss5(j)=s5; ss6(j)=s6; 
    mm1(j)=m1; mm2(j)=m2; mm3(j)=m3; mm4(j)=m4;  mm5(j)=m5; mm6(j)=m6; 
    Caa1(j)=Ca1; Caa2(j)=Ca2; Caa3(j)=Ca3; Caa4(j)=Ca4;Caa5(j)=Ca5; Caa6(j)=Ca6;  
    xx1(j)=x1; xx2(j)=x2; xx3(j)=x3; xx4(j)=x4; ss3_ex(j)=s3_ex; ss4_ex(j)=s4_ex;
    heavy(j)=heav(i); yy1(j)=y1; yy2(j)=y2; ss23(j)=s23; ss14(j)=s14;
  
end
end  
toc    

%  
 [vp2,tp2] = findpeaks(vv2,time,'MinPeakHeight',-20);
 [vp3,tp3] = findpeaks(vv3,time,'MinPeakHeight',-20);
% spike frequency within bursts in the blue cell 
 %fr2=length(tp2)/(tp2(end)-tp2(1))*1000
 %fr3=length(tp3)/(tp3(end)-tp3(1))*1000
% convert back to sec
 tp2=tp2/1000;
 tp3=tp3/1000;

 time=time/1000;

%     
 figure(4)
 clf
 
 % 1 
subplot(6,1,1)
plot(time,vv1,'Color',[0 0  .5]','LineWidth',2.5);  hold on
plot(time,heavy*10,'-','Color',[1 0  1]','LineWidth',1.5); hold on
plot(time,30*yy1+Vhh,'Color',[1 0 0]','LineWidth',2.5);  hold on
plot(time,30*ss1-80,'Color',[0 0 0]','LineWidth',1.5);  hold on
plot(time, .1*((1./(1+exp((vv1+75)/7.8))).^3).*yy1.*(+120-vv1)+Vhh   ,'Color',[0 1 0]','LineWidth',0.5); hold on
xlim([0 time(end)]);  ylim([-90 40])
 
% 2 
subplot(6,1,2)
plot(tp2, vp2,'.','Color',[0 0 0]);  hold on
plot(time,vv2,'Color',[0 0 1]','LineWidth',1.5); hold on
plot(time,30*ss2-80,'Color',[0 0 0]','LineWidth',1.5);  hold on
% plot(time,140*yy2+Vhh,'Color',[1 0 0]','LineWidth',2.5)
% plot(time,   3*((1./(1+exp((vv2+65)/7.8))).^3).*yy2.*(+120-vv2)+Vhh
% ,'Color',[0 1 0]','LineWidth',0.5);  hold on
xlabel('Time'),ylabel('Voltage');  xlim([0 time(end)]);  ylim([-90 40])
 
 % 3 
subplot(6,1,3)
plot(tp3, vp3,'.','Color',[0 0 0]); hold on
plot(time,vv3,'Color',[0.5 0 0]','LineWidth',1.5);  hold on
plot(time,30*ss3-80,'Color',[0.5 0 0]);  hold on
xlabel('Time'),ylabel('Si3L');  xlim([0 time(end)]) ; ylim([-95 40])
 
 % 4 
subplot(6,1,4)
plot(time,vv4,'Color',[1 0 0]','LineWidth',1.5);  hold on
plot(time,-30*ss4+60,'Color',[1 0 0]','LineWidth',1.5);  hold on
plot(time,30*ss4_ex-90,'Color',[0 0 0]','LineWidth',1.5) ;  hold on
xlabel('Time'),ylabel('Voltage'); xlim([0 time(end)]) ;  ylim([-95 80])
 
 %5 
subplot(6,1,5)
%plot(time,ss1,'Color',[0.0 0.0 0.5]);  hold on
%plot(time,ss14,'Color',[0.0 0.5 0.5]);  hold on
%plot(time,ss2,'Color',[0.0 0.0 1]);   hold on
plot(time,ss23,'Color',[0.0 1 1]);   hold on
%plot(time,ss4_ex,'Color',[1 0 0]);   hold on
plot(time,ss3_ex,'Color',[.5 0 0]);   hold on
ylim([0 1]);   xlim([0 time(end)]) ;   title('s(t) probability of neuro-transmitter release', 'Fontsize', 11);
  
 %6 
subplot(6,1,6)
plot(time,vv5,'Color',[0 0 0]','LineWidth',1);  hold on
plot(time,30*ss5-90,'Color',[0 0 1],'LineWidth',1.5); hold on
%  plot(time,vv6,'Color',[.5 0.5 0.5]','LineWidth',1.5); hold on
xlabel('Time'),ylabel('Voltage Si1L/R');  xlim([0 time(end)]);  ylim([-95 40])  
  

  toc 
 
  figure(5)
  clf 
  plot(Caa1,xx1,'Color',[0 0 .5],'LineWidth',1.5)
  hold on
  plot(Caa2,xx2,'Color',[0 0 1],'LineWidth',1.5)
  hold on
  plot(Caa3,xx3,'Color',[0.5 0 0],'LineWidth',1.5)
  hold on
  plot(Caa4,xx4,'Color',[1 0 0],'LineWidth',1.5)
 hold on
  xlabel('Ca'),ylabel('x-variable')  
  title('Ca vs Ca-activaterd possasion gating varaible', 'Fontsize', 11);
  
