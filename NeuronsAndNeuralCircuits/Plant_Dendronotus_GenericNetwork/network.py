#!/usr/bin/env python
import sys
sys.path.insert(0, '../Tools')
import window as win
import numpy as np
from matplotlib.patches import FancyArrowPatch,Circle,Patch
import math
import matplotlib.cm as colormap

class network(win.window):

    title = "Generic Network"
    figsize = (7,6)

    def __init__(self, networkSize, parameterSet):
        win.window.__init__(self, position=None)
        self.fig.clear()

        assert parameterSet.size == networkSize * 8 + networkSize * networkSize * 7

        coupling_strengths_electric = parameterSet[networkSize*8:networkSize*8+networkSize*networkSize]

        if coupling_strengths_chlorine is None:
            self.coupling_strengths_chlorine = 0.*np.ones((networkSize,networkSize),float);
        else :
            self.coupling_strengths_chlorine = np.asarray(coupling_strengths_chlorine)
        if coupling_strengths_potassium is None:
            self.coupling_strengths_potassium = 0.*np.ones((networkSize,networkSize),float);
        else :
            self.coupling_strengths_potassium = np.asarray(coupling_strengths_potassium)
        if coupling_strengths_sodium is None:
            self.coupling_strengths_sodium = 0.*np.ones((networkSize,networkSize),float);
        else :
            self.coupling_strengths_sodium = np.asarray(coupling_strengths_sodium)
        if coupling_strengths_electric is None:
            self.coupling_strengths_electric = 0.*np.ones((networkSize,networkSize),float);
        else :
            self.coupling_strengths_electric = np.asarray(coupling_strengths_electric)

        self.networkSize = networkSize
        self.network_origin = np.array([0.5,0.5])
        self.cell_radius = 0.25/self.networkSize
        self.ax = self.fig.add_axes([-0.12, -0.1, 1.1, 1.33])
        self.draw_cells(ax=self.ax)

    def getCellPosition(self, cellIndex):
        """
        for a cell index between 0 and networkSize-1, returns its coordinates
        """
        return self.network_origin + self.cell_radius*(self.networkSize)*\
                                     np.array([math.cos(3*math.pi/4-math.pi*2*cellIndex/self.networkSize),
                                               math.sin(3*math.pi/4-math.pi*2*cellIndex/self.networkSize)])

    def draw_cells(self, ax):
        """
        Plots the Cells and Couplings
        :param coupling_strengths: 2 dimensional array of coupling strengths; size - networkSize x networkSize; zero diagonal
        :param ax: axis
        """
        for i in range(0,self.networkSize) :
            position = np.array(self.getCellPosition(i))
            ax.add_patch(Circle(position, self.cell_radius, fc='k'))#colormap.gist_rainbow(i/float(self.networkSize)),label='test'))
                                                    #http://wiki.scipy.org/Cookbook/Matplotlib/Show_colormaps
            ax.text(position[0], position[1], i+1, fontsize=600*self.cell_radius, color='w', ha='center', va='center')

        for i in range(0, self.networkSize ) :
            for j in range( 0, self.networkSize) :

                startCoordinates = self.getCellPosition(i)
                endCoordinates = self.getCellPosition(j)
                differenceVector = (endCoordinates - startCoordinates)
                differenceVectorLength = math.sqrt(differenceVector[0] ** 2 + differenceVector[1] ** 2)
                perpendicularVector = np.array([-differenceVector[1],differenceVector[0]])
                perpendicularVectorLength = math.sqrt(perpendicularVector[0]**2+perpendicularVector[1]**2)

                if(self.coupling_strengths_electric[i,j]!=0) :
                    ax.add_patch(FancyArrowPatch(startCoordinates,endCoordinates,
                                                 arrowstyle=u'-|>',lw=2,mutation_scale=self.cell_radius*300,
                                                 shrinkA=400*self.cell_radius,shrinkB=400*self.cell_radius,
                                                 color='k'))
                if(self.coupling_strengths_chlorine[i,j]!=0) :
                    shiftdifferenceVector = differenceVector * 1.15 * self.cell_radius / differenceVectorLength
                    shiftPerpendicularVector = perpendicularVector * 0.15 * self.cell_radius / perpendicularVectorLength
                    if (self.coupling_strengths_chlorine[j, i] == 0):
                        shiftPerpendicularVector = 0
                    ax.add_patch(FancyArrowPatch(startCoordinates + shiftPerpendicularVector,
                                                 endCoordinates + shiftPerpendicularVector,
                                                 arrowstyle=u'-', lw=2, mutation_scale=self.cell_radius * 300,
                                                 shrinkA=400 * self.cell_radius, shrinkB=400 * self.cell_radius,
                                                 color='b'))
                    ax.add_patch(Circle(endCoordinates - shiftdifferenceVector + shiftPerpendicularVector,
                                        0.15 * self.cell_radius, fc='b'))

                if(self.coupling_strengths_potassium[i,j]!=0) :
                    shiftPerpendicularVector = perpendicularVector*0.30*self.cell_radius/perpendicularVectorLength
                    ax.add_patch(FancyArrowPatch(startCoordinates+shiftPerpendicularVector,endCoordinates+shiftPerpendicularVector,
                                                 arrowstyle=u'-|>',lw=2,mutation_scale=self.cell_radius*300,
                                                 shrinkA=400*self.cell_radius,shrinkB=400*self.cell_radius,
                                                 color='g'))
                if(self.coupling_strengths_sodium[i,j]!=0) :
                    shiftPerpendicularVector = perpendicularVector*0.45*self.cell_radius/perpendicularVectorLength
                    ax.add_patch(FancyArrowPatch(startCoordinates+shiftPerpendicularVector,endCoordinates+shiftPerpendicularVector,
                                                 arrowstyle=u'-|>',lw=2,mutation_scale=self.cell_radius*300,
                                                 shrinkA=400*self.cell_radius,shrinkB=400*self.cell_radius,
                                                 color='r'))

#                    ax.text((startCoordinates[0]+endCoordinates[0])/2 - self.cell_radius + shiftPerpendicularVector[0],
#                            (startCoordinates[1]+endCoordinates[1])/2 + self.cell_radius/3 + shiftPerpendicularVector[1],
#                            '%.4f\n'%(coupling_strengths[i,j]), fontsize=200*self.cell_radius)

        ax.set_axis_off()

    """
    #initialStateArray has x0,y0,x1,y1,..x{n-1},y{n-1} ; total 'networkSize' number of ordered pairs each corresponding to one neuron
    def load_initial_condition_generic(self, initialStateArray=None ):
        X = np.zeros(fh.N_EQ1*self.networkSize, float)
        for i in range(0,self.networkSize):
            X[i*fh.N_EQ1:i*fh.N_EQ1+2] = self.systemList[i].load_initial_condition(initialPhase=initialStateArray[i])
        return X
    """

if __name__ == "__main__":

    import pylab as pl

    coupling_strengths = 0.005*np.ones((6,6),float);
    parameterSet=np.array([0.4, 0.3, 0., 10., -1.5, -1.2, 1.5, 1.,
                           0.4, 0.3, 0., 10., -1.5, -1.2, 1.5, 1.,
                           0.5, 0.4, 0., 10., -1.5, -1.2, 1.5, 1.,
                           0.4, 0.3, 0., 10., -1.5, -1.2, 1.5, 1.,
                           0.4, 0.3, 0., 10., -1.5, -1.2, 1.5, 1.,
                           0.5, 0.4, 0., 10., -1.5, -1.2, 1.5, 1.])
    net = network(6,parameterSet,
                        coupling_strengths_chlorine=coupling_strengths,
                        #coupling_strengths_potassium=coupling_strengths,
                        #coupling_strengths_sodium=coupling_strengths,
                        #coupling_strengths_electric=coupling_strengths
                 )
    #melibe
    #coupling_strengths_chlorine = 0.01 * np.asarray(np.matrix('0 1 1 0 0 0;1 0 1 0 0 0; 1 1 0 0 0 0; 0 0 0 0 1 1; 0 0 0 1 0 1; 0 0 0 1 1 0 '))
    #coupling_strengths_electric = 0.01 * np.asarray(np.matrix('0 0 0 1 0 0;0 0 0 0 0 0; 0 0 0 0 0 0; 1 0 0 0 0 0; 0 0 0 0 0 0; 0 0 0 0 0 0 '))
    #net1 = network(6, coupling_strengths_chlorine=coupling_strengths_chlorine, coupling_strengths_electric=coupling_strengths_electric);
    pl.show()




