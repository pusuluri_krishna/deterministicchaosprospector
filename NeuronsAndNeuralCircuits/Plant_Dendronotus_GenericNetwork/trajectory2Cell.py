from __future__ import division
#to get default literal division to be floating point arithmetic

import trajectoryGeneric
import numpy as np

networkSize = 2

#VInh = -80, VExc = 0, Iapp = 0., gh = 0.0004, Vhh = -54, gleak = 0.003, CaShift = -75., xShift =2.
# For cells Si3s
intrinsicParameterSet = np.array([-55, 0, 0., 0.0001, -85, 0.005, -110., -3.5,
                            -55, 0, 0., 0.0001, -85, 0.005, -110., -3.5], dtype='float64')

csElec = np.asarray(np.matrix('0 0; 0 0'))
#one-sided electrical connections

#gInh = 0.04; alpha = 0.01; beta = 0.005;
gInh = 0.04; alpha = 0.4; beta = 0.005;

##Corresponding to each synapse there would be an S variable to be computed at each step of the integration!
csGInh = gInh * np.asarray(np.matrix('0 1; 1 0 '));
csAlphaInh = alpha * np.asarray(np.matrix('0 1; 1 0'));
csBetaInh = beta * np.asarray(np.matrix('0 1; 1 0'));

csGExc = np.asarray(np.matrix("0 0; 0 0"))
csAlphaExc = np.asarray(np.matrix("0 0 ; 0 0"))
csBetaExc = np.asarray(np.matrix("0 0; 0 0 "))

parameterSet = np.concatenate((intrinsicParameterSet,csElec,csGInh,csAlphaInh,csBetaInh,csGExc,csAlphaExc,csBetaExc), axis=None)
#intrinsicParameterSet.flatten()

#print repr(parameterSet) #repr() prints the numpy array with commas (to use in C code)
#V2 = y[0], Ca2 = y[1], h2 = y[2], n2 = y[3], x2 = y[4], y2 = y[5], s2 = y[6];
initialConditions = np.asarray([0, 1, 0., 0., 0.05, 0.,
                                       20, 1.4, 0., 0., 0.6, 0.], dtype='float64')

N=2*int(600000/2.5); dt=2.5*.1;
trajectoryGeneric.plotTrajectory(networkSize, parameterSet, initialConditions, N, dt)