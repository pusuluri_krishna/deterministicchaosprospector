
from __future__ import division
#to get default literal division to be floating point arithmetic
import sweepPhasesGeneric
import numpy as np

networkSize = 4

#This file is aligned with D6_circuit_exp_synapse_s2R_s3L_Krishna.m, which is the updated file for Dendronotus
#1,2 Si1s are quiescent while 3,4 Si3s are tonic spiking..
#If we remove excitation, then the rhythms breaks.. excittn is essential for the rhythm..
#The interplay between cross inhibition and cross excitation seems to be controlling the network rhythm - 14 23 vs 41 32

#VInh = -80, VExc = 0, Iapp = 0., gh = 0.0004, Vhh = -54, gleak = 0.003, CaShift = -75., xShift =2.
intrinsicParameterSet = np.array([-80, 0, 0., 0.0001, -85, 0.005, 0., -3.5,
                            -80, 0, 0., 0.0001, -85, 0.005, 0., -3.5,
                            -55, 0, 0., 0.0001, -85, 0.005, -110., -3.5,
                            -55, 0, 0., 0.0001, -85, 0.005, -110., -3.5], dtype='float64')

csElec = np.asarray(np.matrix('0 0 0 0.002 ; 0 0 0.002 0 ; 0 0 0 0 ; 0 0 0 0'))
#one-sided electrical connections

##Corresponding to each synapse there would be an S variable to be computed at each step of the integration!
csGInh = np.asarray(np.matrix('0 0.02 0 .0055 ; 0.02 0 .0055 0; 0 0 0 .04; 0 0 .04 0 '));
csAlphaInh = np.asarray(np.matrix('0 0.04 0 0.01 ; 0.04 0 0.01 0; 0 0 0 0.01; 0 0 0.01 0 '));
csBetaInh = np.asarray(np.matrix('0 0.001 0 0.001 ; 0.001 0 0.001 0; 0 0 0 0.005; 0 0 0.005 0 '));

csGExc = np.asarray(np.matrix("0 0 0 0 ; 0 0 0 0 ; 0 0.08 0 0 ; 0.08 0 0 0"))
csAlphaExc = np.asarray(np.matrix("0 0 0 0 ; 0 0 0 0 ; 0 0.02 0 0 ; 0.02 0 0 0"))
csBetaExc = np.asarray(np.matrix("0 0 0 0 ; 0 0 0 0 ; 0 0.004 0 0 ; 0.004 0 0 0"))


defaultParameterSet = np.concatenate((intrinsicParameterSet,csElec,csGInh,csAlphaInh,csBetaInh,csGExc,csAlphaExc,csBetaExc), axis=None)
#intrinsicParameterSet.flatten()

#print repr(parameterSet) #repr() prints the numpy array with commas (to use in C code)
#V2 = y[0], Ca2 = y[1], h2 = y[2], n2 = y[3], x2 = y[4], y2 = y[5], s2 = y[6];
initialConditions = np.asarray([-45, 1, 0., 0., 0.8, 0.0,
                                       -45, 1.1, 0., 0., 0.8, 0.,
                                        0, 1, 0., 0., 0.05, 0.,
                                        20, 1.4, 0., 0., 0.6, 0.], dtype='float64')

voltageBinBoundaries = np.asarray([10], dtype='float64');
#intervalBinBoundaries = np.asarray([2500], dtype='float64');
intervalBinBoundaries = np.asarray([1000], dtype='float64');

spikesStart = 250; spikesEnd = 500; nDataPointsToShow=5
N=int(1000*10*60.*2/2.5); dt=2.5*.1;
dataSaved = True; sweepSize = 50;

def runSweep(sweepParameter1Indices, sweepParameter1Start, sweepParameter1End, sweepParameter2Indices, sweepParameter2Start, sweepParameter2End, gridParametersString='', ax=None, plotDataPoints=True):
    fileName = '4CellSweep_'+gridParametersString+str(sweepParameter1Indices)+'_'+str(sweepParameter1Start)+'_'+str(sweepParameter1End)+'_'+str(sweepParameter2Indices)+'_'+str(sweepParameter2Start)+'_'+str(sweepParameter2End)+'_'+str(sweepSize);
    print(fileName)
    sweepPhasesGeneric.plotSweep(parameterSet, initialConditions, networkSize, sweepParameter1Indices, sweepParameter1Start, sweepParameter1End, sweepParameter2Indices, sweepParameter2Start, sweepParameter2End, sweepSize, spikesStart, spikesEnd, dt, N, voltageBinBoundaries, intervalBinBoundaries, fileName, dataSaved, nDataPointsToShow, ax=ax, plotDataPoints=plotDataPoints);


#Images for paper

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

matplotlib.rcParams['savefig.dpi'] = 1200 #2*sweepSize/figSize #300
defaultFontSize = 11; largeFontSize = 14; indexFontSize = 16;
matplotlib.rcParams.update({'font.size': largeFontSize,
              'axes.labelsize': largeFontSize,
              'axes.titlesize': largeFontSize,
              'font.size': defaultFontSize,
              'legend.fontsize': largeFontSize,
              'xtick.labelsize': defaultFontSize,
              'ytick.labelsize': defaultFontSize})

#comment these two for rainbow text function to run properly, along with setting fig.dpi = 1200 described near rainbow text
matplotlib.rc('text', usetex=True)
matplotlib.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
matplotlib.rcParams['savefig.dpi'] = 600
bbox_props_label = dict(boxstyle="square,pad=0.01", fc="white", ec="k", lw=0	)
bbox_props_index = dict(boxstyle="square,pad=0.2", fc="white", ec="k", lw=1., alpha=0.)
bbox_props_fig = dict(boxstyle="square,pad=0.001", fc="white", ec="k", lw=0)


""" #Dendronotus Si3 HCO alpha vs beta sweeps grid images as cross inhibition, cross excitation are varied;  different electric connnection values; Si1 inhibitions kept constant, which can also be varied later if needed

sweepSize = 50;
parameterSet = defaultParameterSet.copy()

#default values
#electric=0.002; crossInhibition=0.0055; crossExcitation=0.08

#Initial exploration
#electricRange=[0, 0.0005, 0.001, 0.0015, 0.002, 0.003]
#crossInhibitionRange=[0.003, 0.006, 0.009];
#crossExcitationRange=[0.04, 0.08, 0.12];

#For paper
electricRange=[0.001, 0.0015, 0.002]
crossInhibitionRange=[0.003, 0.006, 0.009];
crossExcitationRange=[0.04, 0.08, 0.12];

for electric in electricRange:
    parameterSet[35]=electric; parameterSet[38]=electric;
    figSizeX = 6; figSizeY = 6;
    gridX = 3; gridY = 3;
    fig = plt.figure(figsize=(figSizeX, figSizeY))
    gs1 = gridspec.GridSpec(gridX, gridY)
    gs1.update(wspace=0.02, hspace=0.02)  # set the spacing between axes.

    for i in range(3):
        crossInhibition = crossInhibitionRange[i]
        parameterSet[51]=crossInhibition; parameterSet[54]=crossInhibition;
        for j in range(3):
            crossExcitation=crossExcitationRange[j]
            parameterSet[105]=crossExcitation; parameterSet[108]=crossExcitation;
            gridParametersString = 'electric_'+str(electric)+'_crossInhibition_' + str(crossInhibition) + '_crossExcitation_' + str(crossExcitation) + '_'
            sweepParameter1Indices = np.asarray([75, 78], dtype='uint32'); sweepParameter1Start = 0.0; sweepParameter1End = 0.1; #Si3 alpha
            sweepParameter2Indices = np.asarray([91, 94], dtype='uint32'); sweepParameter2Start = 0.0; sweepParameter2End = 0.01;  #Si3 beta

            ax = plt.subplot(gs1[i, j])

            runSweep(sweepParameter1Indices, sweepParameter1Start, sweepParameter1End, sweepParameter2Indices,
                     sweepParameter2Start, sweepParameter2End, gridParametersString, ax=ax)
            plt.axis('on')
            ax.set_xticks([])
            ax.set_yticks([])
            if (i==0):
                if(j==0):
                    ax.text(0.01, 1.01, r'$\mathrm{g_{41\_exc}=g_{32\_exc}=}$', ha='left', va='bottom', bbox=bbox_props_label, transform=ax.transAxes, fontsize=largeFontSize)
                ax.text(0.99, 1.013, crossExcitation, ha='right', va='bottom', bbox=bbox_props_label, transform=ax.transAxes,fontsize=defaultFontSize)

            if (i == 2):
                ax.text(0., -0.01, sweepParameter1Start, ha='left', va='top', bbox=bbox_props_label,
                        transform=ax.transAxes,
                        fontsize=defaultFontSize)
                ax.text(1., -0.01, sweepParameter1End, ha='right', va='top', bbox=bbox_props_label,
                        transform=ax.transAxes,
                        fontsize=defaultFontSize)
                ax.text(0.5, -0.01, r'$\mathrm{\alpha_{34\_43\_inh}}$', ha='center', va='top', bbox=bbox_props_label,
                        transform=ax.transAxes, fontsize=largeFontSize)

            if (j == 0):
                ax.text(-0.01, 0, sweepParameter2Start, ha='right', va='bottom', rotation=90, bbox=bbox_props_label,
                        transform=ax.transAxes, fontsize=defaultFontSize)
                ax.text(-0.01, 1., sweepParameter2End, ha='right', va='top', rotation=90, bbox=bbox_props_label,
                        transform=ax.transAxes, fontsize=defaultFontSize)
                ax.text(-0.01, 0.5, r'$\mathrm{\beta_{34\_43\_inh}}$', ha='right', va='center', rotation=90,
                        bbox=bbox_props_label, transform=ax.transAxes, fontsize=largeFontSize)

            if (j == 2):
                if(i==2):
                    ax.text(1.01, 0.01, r'$\mathrm{g_{14\_inh}=g_{23\_inh}=}$', ha='left', va='bottom', rotation=90, bbox=bbox_props_label, transform=ax.transAxes, fontsize=largeFontSize)
                ax.text(1.013, 0.99, crossInhibition, ha='left', va='top', rotation=90, bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)

            ax.text(0.02, 0.98, r'$\boldsymbol{' + chr(i*3+j + ord('A')) + '}$', ha='left', va='top', transform=ax.transAxes,
                    color='k', bbox=bbox_props_index, fontsize=largeFontSize)

    plt.subplots_adjust(left=0.1, right=0.999, top=0.999, bottom=0.1)

    # fig.text(0.35, 0.6, r'$\boldsymbol{tonick~spiker~}$''\n'r'$\boldsymbol{suppressed}$', ha='center', bbox=bbox_props_index, color='darkslategray', fontsize=defaultFontSize, weight='roman')

    fig.savefig('Dendronotus_Si3AlphaBetaSweeps_gExcVsGInhGrid_gElectric_'+str(electric)+'.png', bbox_inches='tight')

"""

#""" CrossExc. vs CrossInh. vs Si3Inh

#default values
#electric=0.002; crossInhibition=0.0055; crossExcitation=0.08
parameterSet = defaultParameterSet.copy()
sweepSize = 200;
#sweepSize = 200;

def plotSweep(sweepParameter1Indices, sweepParameter1Start, sweepParameter1End, sweepParameter2Indices, sweepParameter2Start, sweepParameter12End, xlabel, ylabel):
    figSizeX = 3;figSizeY = 3;
    gridX = 1;gridY = 1;
    fig = plt.figure(figsize=(figSizeX, figSizeY))
    gs1 = gridspec.GridSpec(gridX, gridY)
    gs1.update(wspace=0.02, hspace=0.02)  # set the spacing between axes.
    ax = plt.subplot(gs1[0, 0])
    runSweep(sweepParameter1Indices, sweepParameter1Start, sweepParameter1End, sweepParameter2Indices,
             sweepParameter2Start, sweepParameter2End, ax=ax)
    plt.axis('on')
    ax.set_xticks([])
    ax.set_yticks([])

    ax.text(0., -0.01, sweepParameter1Start, ha='left', va='top', bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)
    ax.text(1., -0.01, sweepParameter1End, ha='right', va='top', bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)
    ax.text(0.5, -0.01, xlabel, ha='center', va='top', bbox=bbox_props_label,transform=ax.transAxes, fontsize=largeFontSize)

    ax.text(-0.01, 0, sweepParameter2Start, ha='right', va='bottom', rotation=90, bbox=bbox_props_label,
            transform=ax.transAxes, fontsize=defaultFontSize)
    ax.text(-0.01, 1., sweepParameter2End, ha='right', va='top', rotation=90, bbox=bbox_props_label,
            transform=ax.transAxes, fontsize=defaultFontSize)
    ax.text(-0.01, 0.5, ylabel, ha='right', va='center', rotation=90, bbox=bbox_props_label, transform=ax.transAxes, fontsize=largeFontSize)
    plt.subplots_adjust(left=0.1, right=0.999, top=0.999, bottom=0.1)
    fig.savefig('Dendronotus_'+xlabel+'_vs_'+ylabel+'_'+'.png', bbox_inches='tight')

sweepParameter1Indices = np.asarray([51, 54], dtype='uint32'); sweepParameter1Start = 0.003; sweepParameter1End = 0.012; #cross inhibition
sweepParameter2Indices = np.asarray([105, 108], dtype='uint32'); sweepParameter2Start = 0.01; sweepParameter2End = 0.15; #cross excitation
xlabel=r'$\mathrm{g_{14\_23\_inh}}$'; ylabel=r'$\mathrm{g_{41\_32\_exc}}$';
plotSweep(sweepParameter1Indices, sweepParameter1Start, sweepParameter1End, sweepParameter2Indices, sweepParameter2Start, sweepParameter2End, xlabel=xlabel, ylabel=ylabel)

sweepParameter1Indices = np.asarray([59, 62], dtype='uint32'); sweepParameter1Start = 0.001; sweepParameter1End = 0.1;   #Si3 inhibition
sweepParameter2Indices = np.asarray([105, 108], dtype='uint32'); sweepParameter2Start = 0.01; sweepParameter2End = 0.3; #cross excitation
xlabel=r'$\mathrm{g_{34\_43\_inh}}$'; ylabel=r'$\mathrm{g_{41\_32\_exc}}$';
plotSweep(sweepParameter1Indices, sweepParameter1Start, sweepParameter1End, sweepParameter2Indices, sweepParameter2Start, sweepParameter2End, xlabel=xlabel, ylabel=ylabel)

sweepParameter1Indices = np.asarray([121, 124],dtype='uint32');sweepParameter1Start = 0.0;sweepParameter1End = 0.08;#alpha cross excitation
sweepParameter2Indices = np.asarray([137, 140], dtype='uint32');sweepParameter2Start = 0.0;sweepParameter2End = 0.02;#beta cross excitation
xlabel=r'$\mathrm{\alpha_{41\_32\_exc}}$'; ylabel=r'$\mathrm{\beta_{41\_32\_exc}}$';
plotSweep(sweepParameter1Indices, sweepParameter1Start, sweepParameter1End, sweepParameter2Indices, sweepParameter2Start, sweepParameter2End, xlabel=xlabel, ylabel=ylabel)

#"""
