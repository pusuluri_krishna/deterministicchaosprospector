from __future__ import division
#to get default literal division to be floating point arithmetic
import matplotlib
matplotlib.use('Qt4Agg')
import ctypes as ct
from pylab import *
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

lib = ct.cdll.LoadLibrary('./trajectory.so')

N_EQ=3
lib.integrator_rk4.argtypes = [ct.POINTER(ct.c_double), ct.POINTER(ct.c_double),ct.POINTER(ct.c_double),
                          ct.c_double,ct.c_uint,ct.c_uint]
def integrator_rk4 (y_initial = np.asarray([0.045,-.1,0.45]),
                    dt=0.001,
                    N=30000,
                    stride = 1
                ):
    output = np.zeros(N*N_EQ, float)
    #params=np.asarray([-0.0225, 0.001]) #	VK2shift, Iapp;
    params = np.asarray([0, -0.04])  # VK2shift, Iapp;
    params = np.asarray([0.000588221, -0.0639892])  # VK2shift, Iapp; //chaotic
    params = np.asarray([-0.0201796, -0.0608058])

    params = np.asarray([-0.03, -0.04])  # quiescent
    params = np.asarray([-0.03, 0.007])  # tonic spiking
    params = np.asarray([0.005, -0.06])  # burst 2 spikes
    params = np.asarray([-0.003, -0.06])  # burst 3 spikes
    params = np.asarray([-0.007, -0.06])  # burst 4 spikes

    params = np.asarray([-0.01, -0.07])  # burst 4 spikes

    lib.integrator_rk4(y_initial.ctypes.data_as(ct.POINTER(ct.c_double)), output.ctypes.data_as(ct.POINTER(ct.c_double)), params.ctypes.data_as(ct.POINTER(ct.c_double)),
                       ct.c_double(dt), ct.c_uint(N), ct.c_uint(stride))
    #imshow takes fortran style array input
    return np.reshape(output, (N,N_EQ), 'C')

N=300000; dt=0.01

animationStepSize=1000
o = integrator_rk4(N=N,dt=dt)
fig = plt.figure()
sp = fig.add_subplot(111)
sp.set_xlim(0, dt*N)
sp.set_ylim(-0.1, 0.1)
sp.set_xlabel('time', fontsize=20)
sp.set_ylabel('V', fontsize=20)
line, = sp.plot(dt*arange(0,N,1), o[:,0],'b-')

"""
#line, = sp.plot([], [],'b-')

i=0

def update(data):
    global i
    global animationStepSize
    line.set_xdata(dt*arange(0,i+animationStepSize,1.))
    line.set_ydata(o[0:i+animationStepSize,0])
    i=i+animationStepSize
    return line,
ani = animation.FuncAnimation(fig, update,frames=int(N/animationStepSize))
"""

plt.show()
