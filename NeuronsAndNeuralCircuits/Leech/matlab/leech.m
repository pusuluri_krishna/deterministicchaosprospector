%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function leech


K2shft = -0.0225; %%%%%%%


GK2 = 30;
EK = -0.07;
ENa = 0.045;
GNa = 160;
GI = 8;
Pol = 0.001;


shftNam = 0.0035;
shftNah = 0.0065;
tauK2 = 0.9;
tauhNa = 0.0405;
Esyn = -0.0625;
Thr = -0.03;
EI = -0.046;


flor = @(t,s)[-2*(GK2*s(2)*s(2)*(s(1)-EK)+GNa*1/(1+exp(-150*(s(1)+0.027+shftNam)))^3*s(3)*(s(1)-ENa)+GI*(s(1)-EI)+Pol); ...
    (1/(1+exp(-83*(s(1)+0.018+K2shft+0.0)))-s(2))/tauK2; (1/(1+exp(500*(s(1)+0.026+shftNah)))-s(3))/tauhNa];

[t,s] = ode45(flor,[0:0.001:40], [0.045,-.1,0.45]);


figure(1),subplot(2,2,1),plot(t,s(:,1),'b'),xlabel('t'),ylabel('V')
          subplot(2,2,2),plot(t,s(:,2),'b'),xlabel('t'),ylabel('m')
          subplot(2,2,3),plot(t,s(:,3),'b'),xlabel('t'),ylabel('h')
          subplot(2,2,4),plot3(s(:,2),s(:,3),s(:,1),'r'),xlabel('m2'),ylabel('h'),zlabel('V')
view(3,3)
end

