%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function leech

clf;
clear all;
set(gcf, 'PaperPositionMode','auto','color', 'white');
set(gcf,'PaperPosition',[1.5 3 5 5])
hFig=axes('Position',[0.2 0.4 .6 .6],'Visible','off','Color',[.9 .9 .9],...
         'FontName','times',...
         'FontSize',8,...
         'XColor',[0 0 .0],...
         'YColor',[0 0 .0],...
         'ZColor',[0 0 .0]);

GK2 = 30;
EK = -0.07;
ENa = 0.045;
GNa = 160;
GI = 8;
Pol = 0.006;
shftNam = 0.0035;
shftNah = 0.0065;
tauK2 = 0.99;
tauhNa = 0.0405;
Esyn = -0.0625;
Thr = -0.03;
EI = -0.046;

%bif parameter
K2shft = -0.022; %%%%%%%


flor = @(t,s)[-2*(GK2*s(2)*s(2)*(s(1)-EK)+GNa*1/(1+exp(-150*(s(1)+0.027+shftNam)))^3*s(3)*(s(1)-ENa)+GI*(s(1)-EI)+Pol); ...
    (1/(1+exp(-83*(s(1)+0.018+K2shft+0.0)))-s(2))/tauK2;(1/(1+exp(500*(s(1)+0.026+shftNah)))-s(3))/tauhNa];

options = odeset('RelTol',1e-5,'AbsTol',1e-5,'Events',@events);
[t,y,tau1,ye1,ie1]=ode45(flor,[0:0.005:100],[0.001,0.,0.],options);
plot3(y(:,2), y(:,3), y(:,1),'Color', [0.8 0.8 0.8])
hold on
plot3(ye1(:,2), ye1(:,3), ye1(:,1),'.','Color', [0.8 0.0 0.0])
hold on


[X,Z] = meshgrid(-0.06:.005:0.01-0.001, 0:.035:0.5);    
Y = 1./(1.+exp(-87.*(X+K2shft+0.018)));                                        
nul=surf(Y,Z,X);

%set(nul,'EdgeColor','y','FaceColor','b','FaceAlpha',0.001);
set(nul,'EdgeColor','none','FaceColor',[0.1,0.5,0.7],'FaceAlpha',0.25);

xlabel('m_{K2}','FontSize',20);
ylabel('h_{Na}','FontSize',20);
zlabel('V','FontSize',20);
axis on;
axis([-70 40 0 1 0 1])
axis tight 
view(10,5)

set(gca,'box','off','FontSize',8);
 hFig=axes('Position',[0.2 0.1 .6 .2],'Visible','off','Color',[.9 .9 .9],...
         'FontName','times',...
         'FontSize',8,...
         'XColor',[0 0 .7],...
         'YColor',[0 0 .7],...
         'ZColor',[0 0 .7]);
box on

plot (t,y(:,1),'Color','b','LineWidth',1, 'Color', [0.8 0.8 0.8])
hold on
plot (tau1,ye1(:,1),'.', 'MarkerSize',15,'Color',[250./255  81./255  5./255])
hold on



xlabel('time','FontSize',20);
ylabel('x','FontSize',20);
axis tight
axis on



function [value,isterminal,direction] = events(t,s);
vthreshold=s(1)-(-0.026);
    direction= [1];
%    direction= [-1];
value= vthreshold;
isterminal=[0];
end 



end

