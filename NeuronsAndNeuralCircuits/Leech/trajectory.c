#include <sys/time.h>
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include <sys/time.h>
#include<string.h>
#include<stdbool.h>

#define N_EQ1 3
#define MAX_KNEADING_LENGTH 10000
#define NUM_THREADS_PER_BLOCK 512

double mNa(double V){
    return 1/(1+exp(-150*(0.0305+V)));
}

double INa(double gNa, double hNa, double V, double ENa){
    return gNa*pow(mNa(V),3)*hNa*(V-ENa);
}

double IK2(double gK2, double mK2, double V, double EK ){
    return gK2*mK2*mK2*(V-EK);
}

double IL(double gL, double V, double EL){
    return gL*(V-EL);
}

void stepper(const double* y, double* dydt, const double* params)
{
    double V = y[0], mK2 = y[1], hNa = y[2];
    double K2shft = params[0], Iapp = params[1];

    double C=0.5, gK2 = 30, gNa = 200, gL = 8, EK = -0.07, ENa = 0.045, EL = -0.046, tauK2 = 0.9, tauhNa = 0.0405,
    Isyn = 0;


    dydt[0] = (-1/C)*(
                        IK2(gK2,mK2,V,EK)
                        + INa(gNa, hNa, V, ENa)
                        + IL(gL, V, EL)
                        + Iapp
                        + Isyn
                    );
    dydt[1] = (1/(1+exp(-83*(0.018+V+K2shft)))-y[1])/tauK2;
    dydt[2] = (1/(1+exp(500*(0.0333+V)))-y[2])/tauhNa;
}

void computeFirstDerivative(const double *y, double *dydt, const double* params){
	stepper(y, dydt, params);
	return;
}

double integrator_rk4(double* y_current, double *output, const double* params, const double dt, const unsigned N, const unsigned stride)
{
	unsigned i, j, k, kneadingIndex=0;
	double dt2, dt6;
	double y1[N_EQ1], y2[N_EQ1], k1[N_EQ1], k2[N_EQ1], k3[N_EQ1], k4[N_EQ1];
	double firstDerivativeCurrent[N_EQ1],firstDerivativePrevious;

	dt2 = dt/2.; dt6 = dt/6.;

	for(i=1; i<N; i++)
	{
		for(j=0; j<stride; j++)
		{
			stepper(y_current, k1, params);
			for(k=0; k<N_EQ1; k++) y1[k] = y_current[k]+k1[k]*dt2;
			stepper(y1, k2, params);
			for(k=0; k<N_EQ1; k++) y2[k] = y_current[k]+k2[k]*dt2;
			stepper(y2, k3, params);
			for(k=0; k<N_EQ1; k++) y2[k] = y_current[k]+k3[k]*dt;
			stepper(y2, k4, params);

			//Copy latest value into y_current
			for(k=0; k<N_EQ1; k++) y_current[k] += dt6*(k1[k]+2.*(k2[k]+k3[k])+k4[k]);
		}
        for(k=0; k<N_EQ1; k++) output[i*N_EQ1+k] = y_current[k];
	}
	return 0;
}

int main(){
    /*
	double y_initial[]={0.01,0.,0.,0.,0.,0.};
	double dt=0.01;
	unsigned N=30000;
	unsigned stride = 1;
	unsigned maxKneadings = 20;
	unsigned sweepSize = 2;
	double g = 50, sigma = 1.5;
	double *kneadingsWeightedSumSet;
	double alpha;
	int i,j;
	printf("Enter alpha :");
	scanf("%lf",&alpha);
	kneadingsWeightedSumSet = (double *)malloc(sweepSize*sweepSize*sizeof(double));
	*/
	/*
	for(i=0;i<sweepSize;i++){
	    for(j=0;j<sweepSize;j++){
	        printf("%f \n",kneadingsWeightedSumSet[i*sweepSize+j]);
	    }
	}
	*/
}
