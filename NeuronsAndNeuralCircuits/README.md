
The code is presented for all the computational methods described and to generate the images in the following publications:

	K. Pusuluri, “Complex dynamics in dedicated/multifunctional neural networks and chaotic nonlinear systems,” Ph.D. Thesis, GSU (2020).
	Pusuluri K., Ju H., Shilnikov A. (2020) Chaotic Dynamics in Neural Systems. In: Meyers R. (eds) Encyclopedia of Complexity and Systems Science. Springer, Berlin, Heidelberg
	Pusuluri K, Shilnikov A. Symbolic representation of neuronal dynamics. Advances on Nonlinear Dynamics of Electronic Systems, World Scientific. 2019:97-102.

If you make use of this repository in your research, please consider citing the above articles.
