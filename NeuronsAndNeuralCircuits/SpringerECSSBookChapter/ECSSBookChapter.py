#All labels adjusted for Smaug workstation at lab

from __future__ import division
import matplotlib
#matplotlib.use('TkAgg')
matplotlib.use('Qt4Agg')
import ctypes as ct
from pylab import *
import numpy as np
from matplotlib import colors
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from mpl_toolkits.axes_grid.inset_locator import inset_axes
from mpl_toolkits.axes_grid.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid.inset_locator import mark_inset
import matplotlib.patches as patches
import pickle
import gzip
from scipy.integrate import ode
from matplotlib import transforms
import matplotlib.image as mpimg
from numpy.ma import masked_array

colorMapLevels = 2**8; dataSaved = True; sweepSize = 1000; outputdir = 'Output/'

blue = np.linspace(0.01, 1, colorMapLevels)
red = 1 - blue
min = 0.;
max = .8
green = min + np.random.random(colorMapLevels) * (max - min)
RGB = np.column_stack((red, green, blue))
customColorMap = colors.ListedColormap(RGB)

blue = np.linspace(0.8, 0, colorMapLevels)
red = blue
green = blue
RGB = np.column_stack((red, green, blue))
customGrayColorMap = colors.ListedColormap(RGB)

figSize = 5

matplotlib.rcParams['savefig.dpi'] = 1200 #2*sweepSize/figSize #300
defaultFontSize = 11; largeFontSize = 14; indexFontSize = 16;
matplotlib.rcParams.update({'font.size': largeFontSize,
              'axes.labelsize': largeFontSize,
              'axes.titlesize': largeFontSize,
              'font.size': defaultFontSize,
              'legend.fontsize': largeFontSize,
              'xtick.labelsize': defaultFontSize,
              'ytick.labelsize': defaultFontSize})

#comment these two for rainbow text function to run properly, along with setting fig.dpi = 1200 described near rainbow text
matplotlib.rc('text', usetex=True)
matplotlib.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]

sweepData = [[0.1,0.2],[0.3,0.4]]                           #### For testing purposes

bbox_props = dict(boxstyle="square,pad=0.2", fc="white", ec="k", lw=.5, alpha=0.)
arrowprops = dict(arrowstyle="->", connectionstyle="arc3", facecolor='white', lw=1.,
                  ec="white")  # ,shrink=0.05, width=defaultFontSize/10.)
arrowpropsblack = dict(arrowstyle="->", connectionstyle="arc3", facecolor='k', lw=1.,
                  ec="k")  # ,shrink=0.05, width=defaultFontSize/10.)
bbox_props_fig = dict(boxstyle="square,pad=0.001", fc="white", ec="k", lw=0)
bbox_props_index = dict(boxstyle="square,pad=0.2", fc="white", ec="k", lw=1., alpha=0.)
bbox_props_label = dict(boxstyle="square,pad=0.01", fc="white", ec="k", lw=0	)


def processSweepData(axis, sweepData, aMin, aMax, bMin, bMax):

    # PC hash can be any integer value (positive/negative), LZ lies between 0 and 1, -0.5 for insufficient N
    sweepDataInsufficientN = masked_array(sweepData, sweepData != -0.5)
    sweepDataComputed = masked_array(sweepData, sweepData == -0.5)
    sweepDataChaotic = masked_array(sweepDataComputed, (sweepData <= 0) | (sweepData >= 1))
    sweepDataPeriodic = masked_array(sweepDataComputed, (sweepData > 0) & (sweepData < 1))

    # to force common color scheme without scaling across images, use the vmin and vmax arguments of imshow(), and make sure they are the same for all your images
    axis.imshow(sweepDataPeriodic,
           extent=[aMin, aMax, bMin, bMax],
           aspect=(aMax - aMin) / (bMax - bMin),
           cmap=customColorMap,
           origin='lower',  # vmin=-1., vmax=0.1
           )  # making vmax 0.1 instead of 0. to match the colormaps for short and long kneadings

    axis.imshow(sweepDataChaotic,
           extent=[aMin, aMax, bMin, bMax],
           aspect=(aMax - aMin) / (bMax - bMin),
           cmap=customGrayColorMap,
           origin='lower')

    axis.imshow(sweepDataInsufficientN,
           extent=[aMin, aMax, bMin, bMax],
           aspect=(aMax - aMin) / (bMax - bMin),
           cmap=colors.ListedColormap(['white']),
           origin='lower')


"""
############################# Image2: Leech, Hair celll, HR sweeps ########################################################################

####### Whole Image

print 'Generating Leech Image'

if not dataSaved :
    pickleDataFile1 = gzip.open(outputdir+'LeechImage.gz','wb')
else :
    pickleDataFile1 = gzip.open(outputdir+'LeechImage.gz','rb')

matplotlib.rcParams['savefig.dpi'] = 600

figSizeX=4; figSizeY=4; rows = 1 ; columns = 1;

fig = plt.figure(figsize=(figSizeX, figSizeY))
gs1 = gridspec.GridSpec(rows, columns)
gs1.update(wspace=0.03, hspace=0.03)  # set the spacing between axes.

axLeft = plt.subplot(gs1[:,:])

######### Leech sweep

print 'Leech sweep'

libLeech = ct.cdll.LoadLibrary('./leech_sweep_PCLZ.so')
libLeech.sweep.argtypes = [ct.POINTER(ct.c_double),
                        ct.POINTER(ct.c_double), ct.POINTER(ct.c_double),
                        ct.c_double, ct.c_double, ct.c_uint,
                        ct.c_double, ct.c_double, ct.c_uint,
                        ct.c_double, ct.c_uint, ct.c_uint,
                        ct.c_uint, ct.c_uint,
                        ct.c_uint, ct.c_uint]

def sweepLeech(dt=1, N=300000, stride=1,
            VK2ShiftMin=-0.035, VK2ShiftMax=0.02,
            IappMin=-0.07, IappMax=0.01,
            voltageBinBoundaries=np.array([], dtype='float64'),
            intervalBinBoundaries=np.array([], dtype='float64'),
            kneadingsStart=100,
            kneadingsEnd=200,
            sweepSize=100
            ):
    kneadingsWeightedSumSet = np.zeros(sweepSize * sweepSize, float)
    libLeech.sweep(kneadingsWeightedSumSet.ctypes.data_as(ct.POINTER(ct.c_double)),
              voltageBinBoundaries.ctypes.data_as(ct.POINTER(ct.c_double)),
              intervalBinBoundaries.ctypes.data_as(ct.POINTER(ct.c_double)),
              ct.c_double(VK2ShiftMin), ct.c_double(VK2ShiftMax), ct.c_uint(sweepSize),
              ct.c_double(IappMin), ct.c_double(IappMax), ct.c_uint(sweepSize),
              ct.c_double(dt), ct.c_uint(N), ct.c_uint(stride),
              ct.c_uint(len(voltageBinBoundaries)), ct.c_uint(len(intervalBinBoundaries)),
              ct.c_uint(kneadingsStart), ct.c_uint(kneadingsEnd))
    # imshow takes fortran style array input
    return np.reshape(kneadingsWeightedSumSet, (sweepSize, sweepSize), 'F')

VK2ShiftMin = -0.03; VK2ShiftMax = 0.015; IappMin = -0.06; IappMax = 0.012;
N = 300000; dt=0.01;
voltageBinBoundaries = np.array([-0.04], dtype='float64')
kneadingsStart=2000; kneadingsEnd=4000;

plt.axis('on')
axLeft.set_xticks([])
axLeft.set_yticks([])
axLeft.text( 0., -0.02, VK2ShiftMin, ha='left', va='top', bbox=bbox_props_label, transform=axLeft.transAxes, fontsize=defaultFontSize)
axLeft.text( 1., -0.02, VK2ShiftMax, ha='right', va='top', bbox=bbox_props_label, transform=axLeft.transAxes, fontsize=defaultFontSize)
axLeft.text(-0.02, 0, IappMin, ha='right', va='bottom', rotation=90, bbox=bbox_props_label,
            transform=axLeft.transAxes, fontsize=defaultFontSize)
axLeft.text(-0.02, 1., IappMax, ha='right', va='top', rotation=90, bbox=bbox_props_label, transform=axLeft.transAxes,
            fontsize=defaultFontSize)

if not dataSaved:
    sweepData = sweepLeech(dt=dt, N=N, stride=1, VK2ShiftMin=VK2ShiftMin, VK2ShiftMax=VK2ShiftMax, IappMin=IappMin,
                            IappMax=IappMax, kneadingsStart=kneadingsStart, kneadingsEnd=kneadingsEnd,
                            voltageBinBoundaries= voltageBinBoundaries,
                            sweepSize=sweepSize)
    pickle.dump(sweepData, pickleDataFile1)
else:
    sweepData = pickle.load(pickleDataFile1)

processSweepData(axLeft, sweepData, VK2ShiftMin, VK2ShiftMax, IappMin, IappMax);

axLeft.autoscale(False)
axLeft.set_adjustable('box-forced')

axLeft.annotate(r'$\boldsymbol{bursting~with}$''\n'r'$\boldsymbol{spike~adding}$', xy=(-0.012, -0.028), xytext=(-0.003, -0.030), arrowprops=arrowprops, bbox=bbox_props, color='k' )
axLeft.annotate(r'$\boldsymbol{quiescent}$', xy=(-0.027,-0.055), xytext=(-0.027,-0.055), bbox=bbox_props, color='k' )
axLeft.annotate(r'$\boldsymbol{tonic~spiking}$', xy=(-0.0285,0.003), xytext=(-0.0285,0.003), bbox=bbox_props, color='k', rotation=90)
axLeft.annotate(r'$\boldsymbol{chaos}$', xy=(0.0011, -0.007), xytext=(0.0055, 0.005), arrowprops=arrowpropsblack, bbox=bbox_props, color='k' )
axLeft.annotate('', xy=(-0.006, -0.0055), xytext=(0.0055, 0.005), arrowprops=arrowpropsblack, bbox=bbox_props, color='k' )
axLeft.annotate('2', xy=(0.005, -0.04), xytext=(0.005, -0.04), bbox=bbox_props, color='k' )
axLeft.annotate('3', xy=(-0.003, -0.04), xytext=(-0.003, -0.04), bbox=bbox_props, color='k' )
axLeft.annotate('4', xy=(-0.0075, -0.04), xytext=(-0.0075, -0.04), bbox=bbox_props, color='k' )

######## Whole image

xlabel = r'$\mathrm{V_{K_2~shift}}$';
ylabel = r'$\mathrm{I_{app}}$'
fig.text(0.5, 0.05, xlabel, ha='center', bbox=bbox_props_fig, color='darkslategray',
         fontsize=largeFontSize,
         weight='roman')
fig.text(0.05, 0.5, ylabel, va='center', rotation='vertical', bbox=bbox_props_fig, color='darkslategray',
         fontsize=largeFontSize, weight='roman')

plt.subplots_adjust(left=0.1, right=0.999, top=0.999, bottom=0.1)

fig.savefig(outputdir+'LeechSweep.jpg', bbox_inches='tight')

pickleDataFile1.close()

############################# End Image2: Leech, Hair celll, HR sweeps ####################################################################

"""
############################# Image1: Plant trajectory: Updated after book chapter ####################################################################

libPlantTrajectory = ct.cdll.LoadLibrary('./plantTrajectory.so')

N_EQ=6
libPlantTrajectory.integrator_rk4.argtypes = [ct.POINTER(ct.c_double), ct.POINTER(ct.c_double),ct.POINTER(ct.c_double),
                          ct.c_double,ct.c_uint,ct.c_uint]
def integrator_rk4 (params = np.asarray([-8.61873, -1.94959, 0., 0., -55., 0.003], float),  # shift_Ca2, shift_x2, Iapp, gh, Vhh, gleak; #chaotic second loop
                    y_initial = np.asarray([-39.99, 0.9, 0.5, 0.2, 0.7, 0.2]),
                    dt=0.001,
                    N=30000,
                    stride = 1
                ):
    output = np.zeros(N*N_EQ, float)
    libPlantTrajectory.integrator_rk4(y_initial.ctypes.data_as(ct.POINTER(ct.c_double)), output.ctypes.data_as(ct.POINTER(ct.c_double)), params.ctypes.data_as(ct.POINTER(ct.c_double)),
                       ct.c_double(dt), ct.c_uint(N), ct.c_uint(stride))
    #imshow takes fortran style array input
    return np.reshape(output, (N,N_EQ), 'C')

print 'Generating Plant Trajectories'

if not dataSaved :
    pickleDataFile1 = gzip.open(outputdir+'PlantTrajectories.gz','wb')
else :
    pickleDataFile1 = gzip.open(outputdir+'PlantTrajectories.gz','rb')

matplotlib.rcParams['savefig.dpi'] = 600

figSizeX=6; figSizeY=3; rows = 3 ; columns = 5;

fig = plt.figure(figsize=(figSizeX, figSizeY))
gs1 = gridspec.GridSpec(rows, columns)
gs1.update(wspace=0.03, hspace=0.2)  # set the spacing between axes.

axTop = plt.subplot(gs1[0,:])

axLeft = plt.subplot(gs1[1:3,0:3])

from mpl_toolkits.mplot3d import Axes3D
axRight = plt.subplot(gs1[1:3,3:5], projection='3d')

N=430000; dt=1




### Chaotic trajectory (MMOs)
plt.axis('on')
axLeft.set_xticks([])
axLeft.set_yticks([])
#axLeft.text( 0., -0.02, VK2ShiftMin, ha='left', va='top', bbox=bbox_props_label, transform=axLeft.transAxes, fontsize=defaultFontSize)
#axLeft.text( 1., -0.02, VK2ShiftMax, ha='right', va='top', bbox=bbox_props_label, transform=axLeft.transAxes, fontsize=defaultFontSize)
#axLeft.text(-0.02, 0, IappMin, ha='right', va='bottom', rotation=90, bbox=bbox_props_label, transform=axLeft.transAxes, fontsize=defaultFontSize)
#axLeft.text(-0.02, 1., IappMax, ha='right', va='top', rotation=90, bbox=bbox_props_label, transform=axLeft.transAxes, fontsize=defaultFontSize)

if not dataSaved:
    plotData1 = integrator_rk4(N=N, dt=dt)
    pickle.dump(plotData1, pickleDataFile1)
    plotData2 = integrator_rk4(y_initial=np.asarray([-63.0481, 0.972129, 0.5, 0.05, 0.809502, 0.]), N=N, dt=dt)
    pickle.dump(plotData2, pickleDataFile1)

else:
    plotData1 = pickle.load(pickleDataFile1)
    plotData2 = pickle.load(pickleDataFile1)

#axLeft.set_xlim(0, dt*N)
axLeft.set_xlim(310000, 430000)

axLeft.set_ylim(-75, 40)
#axLeft.set_xlabel('time', fontsize=20)
#xLeft.set_ylabel('V', fontsize=20)

line, = axLeft.plot(dt*arange(0,N,1.), plotData1[0:N,0], color='seagreen', linestyle='-', linewidth=0.75)
line, = axLeft.plot(dt*(arange(0,N,1.)+300000), plotData2[0:N,0]-20,'r-')

axLeft.autoscale(False)
axLeft.set_adjustable('box-forced')

axLeft.text( -0.02, 0., -75, ha='right', va='bottom', bbox=bbox_props_label, transform=axLeft.transAxes,
                    fontsize=defaultFontSize, rotation=90)
axLeft.text( -0.02, 1., 40, ha='right', va='top', bbox=bbox_props_label, transform=axLeft.transAxes,
                    fontsize=defaultFontSize, rotation=90)
axLeft.spines['right'].set_visible(False)
axLeft.spines['top'].set_visible(False)
axLeft.spines['bottom'].set_visible(False)

line, = axLeft.plot(dt*arange(420000,430000,2000), -56*np.ones(5,float),'k-')
axLeft.annotate(r'$\boldsymbol{10s}$', xy=(420000, -66.), xytext=(420000, -66.), bbox=bbox_props,
                color='k')

############ 3D phase space
plt.axis('on')

axRight.autoscale(False)
axRight.set_adjustable('box-forced')
axRight.set_xticks([])
axRight.set_yticks([])
axRight.set_zticks([])
axRight.set_xlim(0.6,1.)
axRight.set_ylim(0.1,1.)
axRight.set_zlim(-60,20.)
axRight.plot3D(plotData1[:,1], plotData1[:,4], plotData1[:,0], color='seagreen', linestyle='-', linewidth=0.2) #Ca, x, V
axRight.plot3D(plotData2[int(N/4):N,1], plotData2[int(N/4):N,4], plotData2[int(N/4):N,0], 'r-', linewidth=0.2) #Ca, x, V

#axRight.set_xlabel('Ca', fontsize=defaultFontSize)
#axRight.set_ylabel('X', fontsize=defaultFontSize)
#axRight.set_zlabel('V', fontsize=defaultFontSize)

### Chaotic trajectory (top)

plt.axis('on')
axTop.set_xticks([])
axTop.set_yticks([])

if not dataSaved:
    params = np.asarray([-104.72, 48.2885, 0., 0., -55., 0.003], float)
    plotData0 = integrator_rk4(params=params, N=N, dt=dt)
    pickle.dump(plotData0, pickleDataFile1)

else:
    plotData0 = pickle.load(pickleDataFile1)

axTop.set_xlim(160000, 240000)
axTop.set_ylim(-60, 40)
#axLeft.set_xlabel('time', fontsize=20)
#xLeft.set_ylabel('V', fontsize=20)

line, = axTop.plot(dt*arange(0,N,1.), plotData0[0:N,0], color='seagreen', linestyle='-', linewidth=0.75)

axTop.autoscale(False)
axTop.set_adjustable('box-forced')

axTop.text( -0.0133, 0., -60, ha='right', va='bottom', bbox=bbox_props_label, transform=axTop.transAxes,
                    fontsize=defaultFontSize, rotation=90)
axTop.text( -0.0133, 1., 40, ha='right', va='top', bbox=bbox_props_label, transform=axTop.transAxes,
                    fontsize=defaultFontSize, rotation=90)
axTop.spines['right'].set_visible(False)
axTop.spines['top'].set_visible(False)
axTop.spines['bottom'].set_visible(False)

line, = axTop.plot(dt*arange(230000,233000,1000), -60*np.ones(3,float),'k-', linewidth=3.)
#axTop.annotate(r'$\boldsymbol{3s}$', xy=(230000, -80.), xytext=(230000, -80.), bbox=bbox_props,
#                color='b')
fig.text(.912, .73, r'$\boldsymbol{3s}$', ha='right', va='top', bbox=bbox_props_index, fontsize=defaultFontSize, color='k')

#Whole image
xlabelX=0.35; xlabelY=0.01; ylabelX=0.06; ylabelY=0.4;
xlabel = r'$\mathrm{time}$';
ylabel = r'$\mathrm{V (mV)}$'
#fig.text(xlabelX, xlabelY, xlabel, ha='center', bbox=bbox_props_fig, color='darkslategray', fontsize=defaultFontSize,
         #weight='roman')
fig.text(ylabelX, ylabelY, ylabel, va='center', rotation='vertical', bbox=bbox_props_fig, color='darkslategray',
         fontsize=defaultFontSize, weight='roman')

#fig.text(xlabelX, 0.715, xlabel, ha='center', bbox=bbox_props_fig, color='darkslategray', fontsize=defaultFontSize,
         #weight='roman')
fig.text(ylabelX, 0.87, r'$\mathrm{V}$', va='center', rotation='vertical', bbox=bbox_props_fig, color='darkslategray',
         fontsize=defaultFontSize, weight='roman')

fig.text(0.2, 0.18, r'$\mathrm{V-20}$', va='center', bbox=bbox_props_fig, color='r',
         fontsize=defaultFontSize, weight='roman')

fig.text(0.05, 1., r'$\boldsymbol{A}$', ha='right', va='top', bbox=bbox_props_index, fontsize=defaultFontSize)
fig.text(0.05, .7, r'$\boldsymbol{B}$', ha='right', va='top', bbox=bbox_props_index, fontsize=defaultFontSize)
fig.text(.7, .7, r'$\boldsymbol{C}$', ha='right', va='top', bbox=bbox_props_index, fontsize=defaultFontSize)

fig.text(0.77, 0.14, 'Ca', ha='center', color='darkslategray', fontsize=defaultFontSize, weight='roman')
fig.text(0.92, 0.17, 'X', ha='center', color='darkslategray', fontsize=defaultFontSize, weight='roman')
fig.text(0.94, 0.4, 'V', va='center', color='darkslategray', fontsize=defaultFontSize, weight='roman')

plt.subplots_adjust(left=0.1, right=0.999, top=0.999, bottom=0.1)

fig.savefig(outputdir+'PlantTrajectories.jpg', bbox_inches='tight')

pickleDataFile1.close()

############################# End Image1: Plant trajectory : Updated After Book Chapter ####################################################################


show()
