---------------------------------------------
Deterministic Chaos Prospector
---------------------------------------------

This is a collection of GPU computing tools based on symbolic dynamics for the study of Lorenz like systems as well as models of neurons and neural circuits. The code is presented for all the computational methods described and to generate the images in the following publications:

	Pusuluri K., Ju H., Shilnikov A. (2020) Chaotic Dynamics in Neural Systems. In: Meyers R. (eds) Encyclopedia of Complexity and Systems Science. Springer, Berlin, Heidelberg
	Pusuluri K, Shilnikov A. Symbolic representation of neuronal dynamics. Advances on Nonlinear Dynamics of Electronic Systems, World Scientific. 2019:97-102.
	Pusuluri K, Shilnikov A. Homoclinic chaos and its organization in a nonlinear optics model. Physical Review E. 2018 Oct 30;98(4):040202.
	Pusuluri K., Pikovsky A., Shilnikov A. (2017) Unraveling the Chaos-Land and Its Organization in the Rabinovich System. In: Aranson I., Pikovsky A., Rulkov N., Tsimring L. (eds) Advances in Dynamics, Patterns, Cognition. Nonlinear Systems and Complexity, vol 20. Springer, Cham

If you make use of this repository in your research, please consider citing the above articles.
