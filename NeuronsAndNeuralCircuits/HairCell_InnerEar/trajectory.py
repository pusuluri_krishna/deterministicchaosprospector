from __future__ import division
#to get default literal division to be floating point arithmetic
import matplotlib
matplotlib.use('Qt4Agg')
import ctypes as ct
from pylab import *
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

lib = ct.cdll.LoadLibrary('./trajectory.so')

N_EQ=12
lib.integrator_rk4.argtypes = [ct.POINTER(ct.c_double), ct.POINTER(ct.c_double),ct.POINTER(ct.c_double),
                          ct.c_double,ct.c_uint,ct.c_uint]
def integrator_rk4 (#y_initial = np.asarray([12,0,0,0,0,0,0,0,0,0,0,0], dtype='float64'),
                    y_initial= np.asarray([-6.405504935885031E-002 ,
                                1.510925113459757E-002 ,
                                1.503835478594321E-002,
                                0.156789443830350       ,
                                0.165144360467404      ,
                                0.322644807411062     ,
                                6.257830798697624E-002,
                                2.317122358369597E-003,
                                0.167388800546701,
                                2.927998693440382E-003,
                                9.362823433735715E-007,
                                0.595924681516093], dtype='float64'),
                    dt=0.001,
                    N=30000,
                    stride = 1
                ):
    output = np.zeros(N*N_EQ, float)
    params=np.asarray([0.01, 29.5], float) #	BK = 0.01, gk1 = 29.5
    params=np.asarray([0.001, 40.], float) #	BK = 0.01, gk1 = 29.5

    params = np.asarray([0.01, 29.], float)  # BK = 0.01, gk1 = 29.5
    params = np.asarray([0.01, 29.2], float)  # BK = 0.01, gk1 = 29.5
    params = np.asarray([0.01, 29.213], float)  # BK = 0.01, gk1 = 29.5
    params = np.asarray([0.01, 29.24], float)  # BK = 0.01, gk1 = 29.5
    params = np.asarray([0.01, 40.])  # BK = 0.01, gk1 = 29.5
    params = np.asarray([0.01, 33.])  # BK = 0.01, gk1 = 29.5

    lib.integrator_rk4(y_initial.ctypes.data_as(ct.POINTER(ct.c_double)), output.ctypes.data_as(ct.POINTER(ct.c_double)), params.ctypes.data_as(ct.POINTER(ct.c_double)),
                       ct.c_double(dt), ct.c_uint(N), ct.c_uint(stride))
    #imshow takes fortran style array input
    return np.reshape(output, (N,N_EQ), 'C')

N=300000; dt=1E-4

animationStepSize=10000
o = integrator_rk4(N=N,dt=dt)
fig = plt.figure()
sp = fig.add_subplot(111)
sp.set_xlim(0, dt*N)
sp.set_ylim(-0.1, -0.02)
sp.set_xlabel('time', fontsize=20)
sp.set_ylabel('V', fontsize=20)

line, = sp.plot(dt*arange(0,N,1.), o[0:N,0],'b-')

"""
line, = sp.plot([], [],'b-')
i=0

def update(data):
    global i
    global animationStepSize
    line.set_xdata(dt*arange(0,i+animationStepSize,1.))
    line.set_ydata(o[0:i+animationStepSize,0])
    i=i+animationStepSize
    return line,
ani = animation.FuncAnimation(fig, update,frames=int(N/animationStepSize))
"""

plt.show()
