      program model
!
!     modelling the non-oscillatory cells: bifurcation diagram
! gK1 - > par(1)
! gL  - > par(2)
! Ihold-> par(3)
! gh   -> par(4)
! bks  -> par(5)
! gt   -> par(6)
! gca  -> par(7)
! DRK  -> par(8) 

      implicit none
      integer i,n,nmax,ntm,ntu,j,maxper,iflag,itr1,is,ipar,npr,ipr,itr2,ix,NGS,init,np,isave
      parameter (maxper=200000)
      double precision :: t,y(12),y0(12),h,aa, x0f, x1f, fcc, y00(12)
      double precision :: gK1, gh, Pa, Pdrk, gCa, PBKT, PBKS, gL,xS, k1
      double precision :: tm,tu,ti,taudrk,bks,bkt, x0,x,dd, po,gt
      double precision :: pk3,pk5,pk1,pk2,vm,ca,m
      double precision :: ymax,ymin,ppamp,freq,tsr,yth1,yth2
      double precision :: vsrmi, vsrma
      double precision :: v0, c0, c1, c2, o2, o3, p0,pk
      double precision :: pIk1, pIh, pIdrk, pIca, pIBKS, pIBKT
      double precision :: sIk1, sIh, sIdrk, sIca, sIBKS, sIBKT, zzz, fsr, hp1
      real gasdev, sig1
      real*8 tt,Ihold,par(10),pin,pfin,hp,pip
      real*8 kcom, lambda, Zgain, kb, temp, pi, tsamp, fc
      real*8 ts(maxper)
      real*8, allocatable, dimension (:) :: por
      character*128 name1, name2
      parameter (ti=2.d-4, tu=10.d0, tsamp=2.d-3)
      parameter (kcom=1350.d-6, lambda=2.8d-6, Zgain=7.d-13, kb=1.3807d-23)
      parameter (temp=297.d0, xS=12.d-9, NGS=50)
      common /param/ gK1,gh,Pa,Pdrk,gCa,PBKT,PBKS,gL,taudrk,tt,Ihold
      common /currents/ pIk1, pIh, pIdrk, pIca, pIBKS, pIBKT


      open (11,file='det_trace.in')
      read (11,*) p0
      read (11,*) par(2)
      read (11,*) par(3)
      read (11,*) par(4)
      read (11,*) par(5)
      read (11,*) par(6)
      read (11,*) par(7)
      read (11,*) par(8)
      read (11,*) tm
      read (11,*) name1
      close (11)
      tt=1.d0
      ix=-17
!
      y0(1)=-6.405504935885031E-002  
      y0(2)=1.510925113459757E-002  
      y0(3)=1.503835478594321E-002
      y0(4)=0.156789443830350       
      y0(5)=0.165144360467404       
      y0(6)=0.322644807411062     
      y0(7)=6.257830798697624E-002  
      y0(8)=2.317122358369597E-003  
      y0(9)=0.167388800546701     
      y0(10)=2.927998693440382E-003  
      y0(11)=9.362823433735715E-007  
      y0(12)=0.595924681516093     
!      open (11,file='y0_torus.dat')
!      read (11,*) y0
!      close (11)
      aa=1.d12
      ntm=int(tm/ti)+1
      ntu=int(tu/ti)+1

      k1=kcom/lambda
      x0=0.0
      gt=par(6)*1.d-9
      pi=4*atan(1.d0)

      gh=par(4)*1.d-9
      bks=par(5)
      bkt=par(5)
      Pdrk=2.4d-14*par(8)
      gCa=par(7)*1.d-9
      PBKT=14.d-13*bkt
      PBKS=2.d-13*bks
      gL=par(2)*1.d-9
      Ihold=par(3)*1.d-12

      hp1=0.0001
      par(1)=28.3

      do while (par(1).le.p0) 
        gK1=par(1)*1.d-9
        do i=1,ntu
          call rkmn(t,y0,y,ti)
          y0=y        
        enddo
        par(1)=par(1)+hp1
      enddo

      is=tsamp/ti
      open (11,file=name1)
        par(1)=p0
        gK1=par(1)*1.d-9
        do i=1,5*ntu
           call rkmn(t,y0,y,ti)           
           y0=y 
        enddo
        
        do i=1,floor(tm/tsamp)+1
         do j=1,is
           call rkmn(t,y0,y,ti)           
           y0=y 
         enddo
         write (11,1) (i-1)*tsamp,y0(1)*1000
        enddo
1     format(2(g17.9,1x))
      close (11)
      
      stop
      end
 


      real*8 function minf(v,v12,k)
      implicit none
      real*8 v,v12,k
      minf=1.d0/(1.d0+exp((v-v12)/k))
      return
      end

       SUBROUTINE RKMN(t,y0,y,h)
      IMPLICIT REAL*8(A-H,O-Z)
      parameter (nm=12)
      dimension rk1(nm),rk2(nm),rk3(nm),rk4(nm),y1(nm),f(nm)
      dimension y0(nm), y(nm)      

      CALL derivs(t,y0,f)
      rk1=h*f
      y1=y0+rk1/2
      call derivs(t+h/2,y1,f)
      rk2=h*f
      y1=y0+rk2/2
      call derivs(t+h/2,y1,f)
      rk3=h*f
      y1=y0+rk3
      call derivs(t+h,y1,f)
      rk4=h*f
      y=y0+(rk1+2*(rk2+rk3)+rk4)/6.d0
      RETURN
      END

!-----------------------------------------------------------------------
      subroutine derivs(t,y,f)
      implicit none
      real*8 t, x, y(12), f(12)
      real*8 v,minf, Cm, Temp
      parameter (Cm=10.d-12)
! 1-> v
! 2-> mk1f
! 3-> mk1s
! 4 -> mh
! 5 -> mdrk
! 6 -> mCa
! 7 -> c1
! 8 -> c2
! 9 -> o2
! 10 -> o3
! 11 -> [Ca]
! 12 -> hbkt
! 
! Ik1 (SI)
      real*8 gk1, Ik1, tk1f, tk1s, mk1sinf, ek 
      parameter (ek=-95.d-3)
! Ih  (SI)
      real*8 gh, eh, mhinf, th, Ih, y7
      parameter (eh=-45.d-3) 

! Ia
      real*8 Ia, mainf, a1, taua, h1inf,tau1h, tau2h, const1,const2
      real*8 Ca, v12a,k1a,farad, R, a,b,c, Pa, ex, Ki, Ko, permiab
      parameter (farad=96485.d0, R=8.3144, Temp=295.15)
      parameter (const1=3793550.768)
      parameter (v12a=-21.5d-3, k1a=15.6d-3, Ca=0.54d0,tau2h=300.d-3)
      parameter (const2=39.317, Ki=0.112d0, Ko=0.002d0)
! const1=F^2/RT
! const2=F/RT       

! Idrk (SI)
      real*8 Idrk, mdrkinf, taudrk, alfdrk, betdrk, Pdrk
! ICa (SI)
      real*8 ICa, gCa, ECa, mCainf, tauCa
      parameter (ECa=42.5d-3)

! IL  (SI)
      real*8 IL, gL
! IBK (SI)
      real*8 IBKS, IBKT, k1, k2, k3, k01, k02, k03
      real*8 km1, km2, km3, del1,del2,del3
      real*8 alphc, betc, alphc0, VA, p1,p2,p3,p4,gkca
      real*8 z, U, Cvol, eps, Ks, PBKS, hbktinf, taubkt, PBKT
      real*8 ex1
      real*8 probk1, probh, probdrk, probca, probbks, probbkt
      parameter (km1=300.d0, km2=5000.d0, km3=1500.d0)
      parameter (k01=6.d-6, k02=45.d-6, k03=20.d-6, del1=0.2)
      parameter (del2=0.d0, del3=0.2d0, alphc0=450.d0, VA=33.d-3, betc=2500.d0)
      parameter (Ks=2800.d0, Cvol=1.25d-12, eps=3.4d-5, z=2, U=0.005)
! Transduction (SI)
      real*8 G1, G2, Z1, Z2, RT, at, bt, gt, ghb,tt,Ihold
      parameter (G1=3.14d+3, G2=1.05d+3, Z1=41.9d0,Z2=8.4d0, RT=2454.d0)      
      parameter (ghb=1.d-9)
      common /param/ gK1,gh,Pa,Pdrk,gCa,PBKT,PBKS,gL,taudrk,tt,Ihold
      common /currents/ probk1, probh, probdrk, probca, probbks, probbkt

! Ik1 (SI)
      v=y(1)
      mk1sinf=minf(v,-110.d-3,11.d-3)
      tk1f=0.7d-3*exp((v+120.d-3)/(-43.8d-3))+0.04d-3
      tk1s=14.1d-3*exp((v+120.d-3)/(-28.d-3))+0.04d-3
      f(2)=(mk1sinf-y(2))/tk1f
      f(3)=(mk1sinf-y(3))/tk1s
      probk1=(0.7d0*y(2)+0.3d0*y(3))
      Ik1=gk1*(v-ek)*probk1
! Ih (SI)
      probh=(3*y(4)**2*(1.d0-y(4))+y(4)**3)
      Ih=gh*(v-eh)*probh
      mhinf=minf(v,-87.d-3,16.7d-3)
      th=63.7d-3+135.7d-3*exp(-((v+91.4d-3)/21.2d-3)**2) 
      f(4)=(mhinf-y(4))/th
! Ia (SI) ---- ignored
      ex=exp(-const2*v)
      permiab=const1*v*(Ki-Ko*ex)/(1.d0-ex)
!
! Idrk (SI Units)
      mdrkinf=1.d0/sqrt(1.d0+exp(-(v+48.3d-3)/4.19d-3))
      alfdrk=1.d0/(0.0032*exp(-v/20.9d-3)+0.003)
      betdrk=1.d0/(1.467*exp(v/5.96d-3)+0.009)
      taudrk=tt/(alfdrk+betdrk)  
      f(5)=(mdrkinf-y(5))/taudrk
      probdrk=y(5)*y(5)
      Idrk=probdrk*Pdrk*permiab
!
! ICa (SI Units)
      mCainf=1.d0/(1.d0+exp(-(v+55.d-3)/12.2d-3))
      tauCa=0.046d-3+0.325d-3*exp(-((v+77.d-3)/51.67d-3)**2)
      f(6)=(mCainf-y(6))/tauCa
      probca=y(6)*y(6)*y(6)
      ICa=gCa*(v-ECa)*probca
!
! IL  (SI Units)
      IL=gL*v
!
! IBK (SI)
      ex1=exp(del1*z*const2*v)
      k1=km1/k01*ex1
      k2=km2/k02
      k3=km3/k03*ex1
      alphc=alphc0*exp(v/VA)
      y7=1.d0-(y(7)+y(8)+y(9)+y(10))
      f(7)=k1*y(11)*y7+km2*y(8)-(km1+k2*y(11))*y(7)
      f(8)=k2*y(11)*y(7)+alphc*y(9)-(km2+betc)*y(8)
      f(9)=betc*y(8)+km3*y(10)-(alphc+k3*y(11))*y(9)
      f(10)=k3*y(11)*y(9)-km3*y(10)
      f(11)=-U*Ica/(z*farad*Cvol*eps)-Ks*y(11)
      hbktinf=1.d0/(1.d0+exp((v+61.6d-3)/3.65d-3))
      taubkt=2.1d-3+9.4d-3*exp(-((v+66.9d-3)/17.7d-3)**2)
      f(12)=(hbktinf-y(12))/taubkt
      probbks=(y(9)+y(10))
      probbkt=probbks*y(12)
      IBKS=PBKS*permiab*probbks
      IBKT=PBKT*permiab*probbkt
      f(1)=-(Ik1+Ih+Idrk+Ica+IBKS+IBKT+IL-Ihold)/Cm

      return
      end

