import matplotlib

matplotlib.use('Qt4Agg')
#matplotlib.use('Agg')
import ctypes as ct
from pylab import *
import numpy as np
from matplotlib import colors
from numpy.ma import masked_array
import pickle
import gzip

dataSaved = False;
bkMin = 0.001; bkMax = 0.1;
gk1Min = 15; gk1Max =   45; ghList=[0];
sweepSize = 500; N = 300000*10; dt=1E-4;
kneadingsStart = 1000; kneadingsEnd = 2000;

# Adjust these bins to change precision depending on the system; Bins do not have to be equally sized; Have more bins in the region where more precision is required and less bins where its not; Using 10 bins for a start (9 bin boundaries); Best represented by char to compute PC, may also be represented by the digits 0 to 9 if number of bins < 10

voltageBinBoundaries = np.array([-0.08], dtype='float64')
#voltageBinBoundaries = np.array([-0.08, -0.065, -0.055, -0.51, -0.048, -0.047, -0.046,-0.045,-0.044,-0.043,-0.042],
# dtype='float64')

#intervalBinBoundaries = np.array([0.5, 1, 2, 3, 5, 10, 20, 40, 80], dtype='float64')
#intervalBinBoundaries = np.array([1, 10, 40], dtype='float64')
#intervalBinBoundaries = np.array([0.01, 0.05], dtype='float64')
#intervalBinBoundaries = np.array([0.001, 0.01, 0.05], dtype='float64')
intervalBinBoundaries = np.array([], dtype='float64')

# arrays must be created with double values not integers (default) since that's how it is passed to C function later;
#  Otherwise giving errors and memory copied incorrectly

lib = ct.cdll.LoadLibrary('./sweep_PCLZ.so')
lib.sweep.argtypes = [ct.POINTER(ct.c_double),
                        ct.POINTER(ct.c_double), ct.POINTER(ct.c_double),
                        ct.c_double, ct.c_double, ct.c_uint,
                        ct.c_double, ct.c_double, ct.c_uint,
                        ct.c_double, ct.c_uint, ct.c_uint,
                        ct.c_uint, ct.c_uint,
                        ct.c_uint, ct.c_uint]

def sweep(dt=1, N=300000, stride=1,
            bkMin=0.001, bkMax=1.,
            gk1Min=15, gk1Max=45,
            kneadingsStart=100,
            kneadingsEnd=200,
            sweepSize=100
            ):
    kneadingsWeightedSumSet = np.zeros(sweepSize * sweepSize, float)
    lib.sweep(kneadingsWeightedSumSet.ctypes.data_as(ct.POINTER(ct.c_double)),
              voltageBinBoundaries.ctypes.data_as(ct.POINTER(ct.c_double)),
              intervalBinBoundaries.ctypes.data_as(ct.POINTER(ct.c_double)),
              ct.c_double(log10(bkMin)), ct.c_double(log10(bkMax)), ct.c_uint(sweepSize),
              ct.c_double(gk1Min), ct.c_double(gk1Max), ct.c_uint(sweepSize),
              ct.c_double(dt), ct.c_uint(N), ct.c_uint(stride),
              ct.c_uint(len(voltageBinBoundaries)), ct.c_uint(len(intervalBinBoundaries)),
              ct.c_uint(kneadingsStart), ct.c_uint(kneadingsEnd))
    # imshow takes fortran style array input
    return np.reshape(kneadingsWeightedSumSet, (sweepSize, sweepSize), 'F')

#For LZ, use the returned value between 0 to 1 with a grey colormap
#For PC, return the circular hash of the period string, and use it with a custom colormap; If needed, we can also return, say, 6digits from the hash and use 2 each for R,G,B

print 'Running sweepHairCell'

colorMapLevels=2**8

blue = np.linspace(0.01, 1, colorMapLevels)
red = 1 - blue
min = 0.;
max = .8
green = min + np.random.random(colorMapLevels) * (max - min)
RGB = np.column_stack((red, green, blue))
customColorMap = colors.ListedColormap(RGB)

blue = np.linspace(0.8, 0, colorMapLevels)
red = blue
green = blue
RGB = np.column_stack((red, green, blue))
customGrayColorMap = colors.ListedColormap(RGB)


figSize = 5
fig = figure(figsize=(figSize, figSize))
outputdir = 'Output/Plant_PCLZ'
matplotlib.rcParams['savefig.dpi'] = 600


fig.clear()

for gh in ghList:

    filename = outputdir + '_bkMin_'+str(bkMin) + '_bkMax_' + str(bkMax) + '_gk1Min_' + str(gk1Min) + '_gk1Max_' + str(gk1Max) + '_gh_' + str(gh) + '_voltageBins_' + str(voltageBinBoundaries) + '_intervalBins_' + str(intervalBinBoundaries) + '_dt_' + str(dt) + '_N_' + str(N) + '_sweepSize_' + str(sweepSize)

    if not dataSaved:
        pickleDataFile1 = gzip.open(filename + '.gz', 'wb')
        sweepData = sweep(dt=dt, N=N, stride=1, bkMin=bkMin, bkMax=bkMax, gk1Min=gk1Min,
                          gk1Max=gk1Max, kneadingsStart=kneadingsStart, kneadingsEnd=kneadingsEnd, sweepSize=sweepSize)
        pickle.dump(sweepData, pickleDataFile1)
    else:
        pickleDataFile1 = gzip.open(filename + '.gz', 'rb')
        sweepData = pickle.load(pickleDataFile1)
    pickleDataFile1.close()

    # print sweepData
    # sweepData[sweepData == -1.1] = 0.

    # PC hash can be any integer value (positive/negative), LZ lies between 0 and 1, -0.5 for insufficient N
    sweepDataInsufficientN = masked_array(sweepData, sweepData != -0.5)
    sweepDataComputed = masked_array(sweepData, sweepData == -0.5)
    sweepDataChaotic = masked_array(sweepDataComputed, (sweepData <= 0) | (sweepData >= 1))
    sweepDataPeriodic = masked_array(sweepDataComputed, (sweepData > 0) & (sweepData < 1))

    aMin = log10(bkMin); aMax = log10(bkMax); bMin = gk1Min; bMax = gk1Max;
    # to force common color scheme without scaling across images, use the vmin and vmax arguments of imshow(), and make sure they are the same for all your images
    imshow(sweepDataPeriodic,
           extent=[aMin, aMax, bMin, bMax],
           aspect=(aMax - aMin) / (bMax - bMin),
           cmap=customColorMap,
           origin='lower',  # vmin=-1., vmax=0.1
           )  # making vmax 0.1 instead of 0. to match the colormaps for short and long kneadings

    imshow(sweepDataChaotic,
           extent=[aMin, aMax, bMin, bMax],
           aspect=(aMax - aMin) / (bMax - bMin),
           cmap=customGrayColorMap,
           origin='lower')

    imshow(sweepDataInsufficientN,
           extent=[aMin, aMax, bMin, bMax],
           aspect=(aMax - aMin) / (bMax - bMin),
           cmap=colors.ListedColormap(['white']),
           origin='lower')

    xlabel('b');
    ylabel('gK1');
    fig.savefig(filename + ".jpg")

    show()


