#include <sys/time.h>
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include <sys/time.h>
#include<string.h>
#include<stdbool.h>

#define N_EQ1	12
#define MAX_KNEADING_LENGTH 10000
#define NUM_THREADS_PER_BLOCK 512

void stepper(const double* y, double* dydt, const double* params)
{
    double gL = 0.174, gh = 2.2, gCa = 1.2, Ek=-95.e-3, Eh=-45.e-3, farad=96485.0, const1=3793550.768, const2=39.317, Ki=0.112, Ko=0.002, Eca=42.e-3, km1=300.0, km2=5000.0, km3=1500.0, k01=6.e-6, k02=45.e-6, k03=20.e-6, ddel1=0.2, alphc0=450.0, VA=33.e-3, betc=2500.0, Ks=2800.0, Cvol=1.25e-12, eeps=3.4e-5, z=2, U=0.005, Cm=10.e-12, PBKS=2.e-13, PBKT=14.e-13, Pdrk=2.4e-14;

    //BK = 0.01, gk1 = 29.5
    double BK = params[0], gk1 = params[1];

    double mk1sinf=1.0/(1.0+exp((y[0]+0.110)/0.011));
    double tk1f=0.7e-3*exp((y[0]+0.120)/(-0.0438))+0.04e-3;
    double tk1s=14.1e-3*exp((y[0]+0.120)/(-0.028))+0.04e-3;
    double Ik1=gk1*1e-9*(y[0]-Ek)*(0.7*y[1]+0.3*y[2]);
    double Ih=gh*1e-9*(y[0]-Eh)*(3*y[3]*y[3]*(1.0-y[3])+y[3]*y[3]*y[3]);
    double mhinf=1.0/(1+exp((y[0]+0.087)/0.0167));
    double th=0.0637+0.1357*exp(-((y[0]+0.0914)/0.0212)*((y[0]+0.0914)/0.0212));
    double ex=exp(-const2*y[0]);
    double permiab=const1*y[0]*(Ki-Ko*ex)/(1.0-ex);
    double mdrkinf=1.0/sqrt(1.0+exp(-(y[0]+0.0483)/0.00419));
    double alfdrk=1.0/(0.0032*exp(-y[0]/0.0209)+0.003);
    double betdrk=1.0/(1.467*exp(y[0]/0.00596)+0.009);
    double taudrk=1.0/(alfdrk+betdrk);
    double Idrk=y[4]*y[4]*Pdrk*permiab;
    double mCainf=1.0/(1.0+exp(-(y[0]+0.055)/0.0122));
    double tauCa=0.046e-3+0.325e-3*exp(-((y[0]+0.077)/0.05167)*((y[0]+0.077)/0.05167));
    double Ica=gCa*1e-9*(y[0]-Eca)*y[5]*y[5]*y[5];
    double ex1=exp(ddel1*z*const2*y[0]);
    double k1=km1/k01*ex1;
    double k2=km2/k02;
    double k3=km3/k03*ex1;
    double alphc=alphc0*exp(y[0]/VA);
    double yy6=1.0-(y[6]+y[7]+y[8]+y[9]);
    double hbktinf=1.0/(1.0+exp((y[0]+61.6e-3)/3.65e-3));
    double taubkt=2.1e-3+9.4e-3*exp(-((y[0]+66.9e-3)/17.7e-3)*(y[0]+66.9e-3)/17.7e-3);
    double IBKS=PBKS*BK*permiab*(y[8]+y[9]);
    double IBKT=PBKT*BK*permiab*(y[8]+y[9])*y[11];
    double IL=gL*1e-9*y[0];

    dydt[0]=-(Ik1+Ih+Idrk+Ica+IBKS+IBKT+IL)/Cm;
    dydt[1]=(mk1sinf-y[1])/tk1f;
    dydt[2]=(mk1sinf-y[2])/tk1s;
    dydt[3]=(mhinf-y[3])/th;
    dydt[4]=(mdrkinf-y[4])/taudrk;
    dydt[5]=(mCainf-y[5])/tauCa;
    dydt[6]=k1*y[10]*yy6+km2*y[7]-(km1+k2*y[10])*y[6];
    dydt[7]=k2*y[10]*y[6]+alphc*y[8]-(km2+betc)*y[7];
    dydt[8]=betc*y[7]+km3*y[9]-(alphc+k3*y[10])*y[8];
    dydt[9]=k3*y[10]*y[8]-km3*y[9];
    dydt[10]=-U*Ica/(z*farad*Cvol*eeps)-Ks*y[10];
    dydt[11]=(hbktinf-y[11])/taubkt;

    //printf("%lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf \n", dydt[0], dydt[1], dydt[2], dydt[3], dydt[4], dydt[5], dydt[6], dydt[7], dydt[8], dydt[9], dydt[10], dydt[11]);

}

void computeFirstDerivative(const double *y, double *dydt, const double* params){
	stepper(y, dydt, params);
	return;
}

double integrator_rk4(double* y_current, double *output, const double* params, const double dt, const unsigned N, const unsigned stride)
{
	unsigned i, j, k, kneadingIndex=0;
	double dt2, dt6;
	double y1[N_EQ1], y2[N_EQ1], k1[N_EQ1], k2[N_EQ1], k3[N_EQ1], k4[N_EQ1];
	double firstDerivativeCurrent[N_EQ1],firstDerivativePrevious;

	dt2 = dt/2.; dt6 = dt/6.;
    printf("%lf ", y_current[0]);
    for(i=1; i<N; i++)
	{
		for(j=0; j<stride; j++)
		{
			stepper(y_current, k1, params);
			for(k=0; k<N_EQ1; k++) y1[k] = y_current[k]+k1[k]*dt2;
			stepper(y1, k2, params);
			for(k=0; k<N_EQ1; k++) y2[k] = y_current[k]+k2[k]*dt2;
			stepper(y2, k3, params);
			for(k=0; k<N_EQ1; k++) y2[k] = y_current[k]+k3[k]*dt;
			stepper(y2, k4, params);

			//Copy latest value into y_current
			for(k=0; k<N_EQ1; k++) y_current[k] += dt6*(k1[k]+2.*(k2[k]+k3[k])+k4[k]);
		}
        for(k=0; k<N_EQ1; k++) output[i*N_EQ1+k] = y_current[k];

        //printf("%lf ", y_current[0]);
        //if(isnan(y_current[0]))return 0;
	}
	return 0;
}

int main(){
    /*
	double y_initial[]={0.01,0.,0.,0.,0.,0.};
	double dt=0.01;
	unsigned N=30000;
	unsigned stride = 1;
	unsigned maxKneadings = 20;
	unsigned sweepSize = 2;
	double g = 50, sigma = 1.5;
	double *kneadingsWeightedSumSet;
	double alpha;
	int i,j;
	printf("Enter alpha :");
	scanf("%lf",&alpha);
	kneadingsWeightedSumSet = (double *)malloc(sweepSize*sweepSize*sizeof(double));
	*/
	/*
	for(i=0;i<sweepSize;i++){
	    for(j=0;j<sweepSize;j++){
	        printf("%f \n",kneadingsWeightedSumSet[i*sweepSize+j]);
	    }
	}
	*/
}
