#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include <sys/time.h>
#include<string.h>
#include<stdbool.h>

#define N_EQ1	12
#define MAX_KNEADING_LENGTH 10000
#define NUM_THREADS_PER_BLOCK 512

__device__ void stepper(const double* y, double* dydt, const double* params)
{
    double gL = 0.174, gh = 2.2, gCa = 1.2, Ek=-95.e-3, Eh=-45.e-3, farad=96485.0, const1=3793550.768, const2=39.317, Ki=0.112, Ko=0.002, Eca=42.e-3, km1=300.0, km2=5000.0, km3=1500.0, k01=6.e-6, k02=45.e-6, k03=20.e-6, ddel1=0.2, alphc0=450.0, VA=33.e-3, betc=2500.0, Ks=2800.0, Cvol=1.25e-12, eeps=3.4e-5, z=2, U=0.005, Cm=10.e-12, PBKS=2.e-13, PBKT=14.e-13, Pdrk=2.4e-14;

    //BK = 0.01, gk1 = 29.5
    double BK = pow(10.,params[0]), gk1 = params[1];

    double mk1sinf=1.0/(1.0+exp((y[0]+0.110)/0.011));
    double tk1f=0.7e-3*exp((y[0]+0.120)/(-0.0438))+0.04e-3;
    double tk1s=14.1e-3*exp((y[0]+0.120)/(-0.028))+0.04e-3;
    double Ik1=gk1*1e-9*(y[0]-Ek)*(0.7*y[1]+0.3*y[2]);
    double Ih=gh*1e-9*(y[0]-Eh)*(3*y[3]*y[3]*(1.0-y[3])+y[3]*y[3]*y[3]);
    double mhinf=1.0/(1+exp((y[0]+0.087)/0.0167));
    double th=0.0637+0.1357*exp(-((y[0]+0.0914)/0.0212)*((y[0]+0.0914)/0.0212));
    double ex=exp(-const2*y[0]);
    double permiab=const1*y[0]*(Ki-Ko*ex)/(1.0-ex);
    double mdrkinf=1.0/sqrt(1.0+exp(-(y[0]+0.0483)/0.00419));
    double alfdrk=1.0/(0.0032*exp(-y[0]/0.0209)+0.003);
    double betdrk=1.0/(1.467*exp(y[0]/0.00596)+0.009);
    double taudrk=1.0/(alfdrk+betdrk);
    double Idrk=y[4]*y[4]*Pdrk*permiab;
    double mCainf=1.0/(1.0+exp(-(y[0]+0.055)/0.0122));
    double tauCa=0.046e-3+0.325e-3*exp(-((y[0]+0.077)/0.05167)*((y[0]+0.077)/0.05167));
    double Ica=gCa*1e-9*(y[0]-Eca)*y[5]*y[5]*y[5];
    double ex1=exp(ddel1*z*const2*y[0]);
    double k1=km1/k01*ex1;
    double k2=km2/k02;
    double k3=km3/k03*ex1;
    double alphc=alphc0*exp(y[0]/VA);
    double yy6=1.0-(y[6]+y[7]+y[8]+y[9]);
    double hbktinf=1.0/(1.0+exp((y[0]+61.6e-3)/3.65e-3));
    double taubkt=2.1e-3+9.4e-3*exp(-((y[0]+66.9e-3)/17.7e-3)*(y[0]+66.9e-3)/17.7e-3);
    double IBKS=PBKS*BK*permiab*(y[8]+y[9]);
    double IBKT=PBKT*BK*permiab*(y[8]+y[9])*y[11];
    double IL=gL*1e-9*y[0];

    dydt[0]=-(Ik1+Ih+Idrk+Ica+IBKS+IBKT+IL)/Cm;
    dydt[1]=(mk1sinf-y[1])/tk1f;
    dydt[2]=(mk1sinf-y[2])/tk1s;
    dydt[3]=(mhinf-y[3])/th;
    dydt[4]=(mdrkinf-y[4])/taudrk;
    dydt[5]=(mCainf-y[5])/tauCa;
    dydt[6]=k1*y[10]*yy6+km2*y[7]-(km1+k2*y[10])*y[6];
    dydt[7]=k2*y[10]*y[6]+alphc*y[8]-(km2+betc)*y[7];
    dydt[8]=betc*y[7]+km3*y[9]-(alphc+k3*y[10])*y[8];
    dydt[9]=k3*y[10]*y[8]-km3*y[9];
    dydt[10]=-U*Ica/(z*farad*Cvol*eeps)-Ks*y[10];
    dydt[11]=(hbktinf-y[11])/taubkt;

    //printf("%lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf \n", dydt[0], dydt[1], dydt[2], dydt[3], dydt[4], dydt[5], dydt[6], dydt[7], dydt[8], dydt[9], dydt[10], dydt[11]);

}

__device__ void computeFirstDerivative(const double *y, double *dydt, const double* params){
	stepper(y, dydt, params);
	return;
}

//from: https://github.com/benjaminfrot/LZ76/blob/master/C/LZ76.c
__device__ double LZ76(const char * s, const int n) {
  int c=1,l=1,i=0,k=1,kmax = 1,stop=0;
  while(stop ==0) {
    if (s[i+k-1] != s[l+k-1]) {
      if (k > kmax) {
        kmax=k;
      }
      i++;

      if (i==l) {
        c++;
        l += kmax;
        if (l+1>n)
          stop = 1;
        else {
          i=0;
          k=1;
          kmax=1;
        }
      } else {
        k=1;
      }
    } else {
      k++;
      if (l+k > n) {
        c++;
        stop =1;
      }
    }
  }
  return double(c)/n;
}



__device__ unsigned NextCoprime(unsigned* coPrimes,unsigned x){
    coPrimes[x] = (coPrimes[x] - x) * coPrimes[x] + x;
    return coPrimes[x];
}

//https://stackoverflow.com/questions/40303333/how-to-replicate-java-hashcode-in-c-language
__device__ unsigned javaHashCode(const unsigned x) {
    int hash = 0, length = floor(log10(fabs(x+0.))) + 1;
    for(int i=0; i<length; i++){
        hash = 31*hash+(x%int(pow(10.,i+0.)));
    }
    return hash;
}

__device__ unsigned NextPair(unsigned* coPrimes, const unsigned a, const unsigned b, const unsigned x, const unsigned y){
    return a * NextCoprime(coPrimes, x) * javaHashCode(x) + b * javaHashCode(y);
}

// Need circular hash function : https://stackoverflow.com/questions/2585092/is-there-a-circular-hash-function
// https://mathoverflow.net/questions/135297/general-euclid-fermat-sequences
//This function can return either positive or negative values
__device__ double circularHash(const char *kneadings, const unsigned periodLength)
{
    int H = 0;

    if (periodLength > 0)
    {
        //any arbitrary coprime numbers
        unsigned a = periodLength, b = periodLength + 1;

        //an array of Euclid-Fermat sequences to generate additional coprimes for each duplicate character occurrence
        unsigned noOfCoprimes = 255; //see if this is enough or more are needed
        unsigned coPrimes[255];

        for (int i = 1; i < noOfCoprimes; i++)
        {
            coPrimes[i] = i + 1;
        }

        //for i=0 we need to wrap around to the last character
        H = NextPair(coPrimes, a, b, kneadings[periodLength - 1], kneadings[0]);

        //for i=1...n we use the previous character
        for (int i = 1; i < periodLength; i++)
        {
            H ^= NextPair(coPrimes, a, b, kneadings[i - 1], kneadings[i]);
        }
    }
    //printf("\nCircular hash : %d\n",H);

    //return double(H);
    //H can be any integer; In order to not conflict with LZ, we make sure we return a value greater than 1 or less than -1, so LZ can be between 0 and 1, and -0.5 can be used as marker for insufficient N.
    if(H>=0) return double(H+1);
    else return double(H-1);
}

/*
__device__ double  computePeriodNormalizedKneadingSum(const char *kneadings, const unsigned kneadingsLength, const unsigned
periodLength ){
    double kneadingSum=0, minPeriodSum=0, currPeriodSum=0;
    unsigned i=0, normalizedPeriodIndex=0;

    char s[200];unsigned si=0;
    //Also normalizing symmetric orbits -- so 00000000.. and 1111111111.. are treated identically
    double minPeriodSumSymmetric=0, currPeriodSumSymmetric=0;
    unsigned normalizedPeriodIndexSymmetric=0;

    if(periodLength<kneadingsLength){
        for(i=0; i<periodLength; i++) {
            currPeriodSum=0;
            for(unsigned j=0; j<periodLength; j++) {
                currPeriodSum+= 2*currPeriodSum + kneadings[i+j];
            }
            if(minPeriodSum==0 || currPeriodSum < minPeriodSum) {
                minPeriodSum = currPeriodSum;
                normalizedPeriodIndex=i;
            }
        }
        for(i=0; i<periodLength; i++) {
            currPeriodSumSymmetric=0;
            for(unsigned j=0; j<periodLength; j++) {
                currPeriodSumSymmetric+= 2*currPeriodSumSymmetric + 1-kneadings[i+j];
            }
            if(minPeriodSumSymmetric==0 || currPeriodSumSymmetric < minPeriodSumSymmetric) {
                minPeriodSumSymmetric = currPeriodSumSymmetric;
                normalizedPeriodIndexSymmetric=i;
            }
        }
    }

    //filling kneading sequence with normalized period
    for(i=0; i<kneadingsLength; i++) {
        if(minPeriodSum < minPeriodSumSymmetric){
            kneadingSum = kneadingSum + kneadings[normalizedPeriodIndex+periodLength-1-i%periodLength]/pow(2.,double(-i+kneadingsLength));
    	    s[si++]='0'+int(kneadings[normalizedPeriodIndex+periodLength-1-i%periodLength]);
        } else {
            kneadingSum = kneadingSum + (1-kneadings[normalizedPeriodIndexSymmetric+periodLength-1-i%periodLength])/pow(2.,double(-i+kneadingsLength));
    	    s[si++]='0'+int(kneadings[normalizedPeriodIndexSymmetric+periodLength-1-i%periodLength]);
        }
    }
    if(periodLength<kneadingsLength){
        //printf("\nPeriod length: %d, symbolic sequence: %s ",periodLength,s);
    }

    //not filling kneading sequence with normalized period, just using a single period
    //for(i=0; i<periodLength; i++) {
      //  if(minPeriodSum < minPeriodSumSymmetric){
        //    kneadingSum = kneadingSum + kneadings[normalizedPeriodIndex+periodLength-1-i%periodLength]/pow(2.,double
        //(-i+periodLength));
        //} else {
        //    kneadingSum = kneadingSum + (1-kneadings[normalizedPeriodIndexSymmetric+periodLength-1-i%periodLength])
        ///pow(2.,double(-i+periodLength));
        //}
    //}

    return kneadingSum;

}
*/

__device__ double periodicPCorChaoticLZ(const char* kneadings,const  unsigned kneadingsLength){
    //After a long transient when the periodic orbits if any have already been reached, this method computes a kneading sum that is invariant between different cyclic permutations of the period

    bool periodFound=true;
    unsigned periodLength = kneadingsLength;
    for(unsigned currPeriod=1; currPeriod < kneadingsLength/2; currPeriod++) {
        periodFound=true;
        //Check if the kneading sequence has a period with periodicity of currPeriod
        for(unsigned i=currPeriod; i < kneadingsLength-currPeriod; i+=currPeriod) {
            for ( unsigned j=0; j<currPeriod; j++) {
                if(kneadings[j] != kneadings[i+j]) {
                    periodFound=false;
                    break;
                }
            }
            if(!periodFound) {
                break;
            }
        }
        //compute kneadingSum based on period found, if any
        if(periodFound) {
            //currPeriod is the period of the kneading sequence. So this will be normalized to a sequence with the sorted period(0's followed by 1's)
            periodLength = currPeriod;
            break;
        }
    }
    //return periodLength==kneadingsLength? LZ76(kneadings, kneadingsLength) : -computePeriodNormalizedKneadingSum(kneadings, kneadingsLength, periodLength);
    //return periodLength==kneadingsLength? LZ76(kneadings, kneadingsLength) : -periodLength;

    //printf("\nPeriod Length: %d\n", periodLength);
    return periodLength==kneadingsLength? LZ76(kneadings, kneadingsLength) : -circularHash(kneadings, periodLength);

}


__device__ char getBinSymbol(const double value, const double* binBoundariesGpu, const double numberOfBinBoundaries){
    unsigned i;

    if(numberOfBinBoundaries==0) return char('a');
    for(i=0;i<numberOfBinBoundaries;i++){
        if(value < binBoundariesGpu[i])
            return char('a'+i);
    }
    return char('a'+i);
}

__device__ double integrator_rk4(double* y_current, const double* voltageBinBoundariesGpu, const double* intervalBinBoundariesGpu, const double* params, const double dt, const unsigned N, const unsigned stride, const unsigned nV, const unsigned nI, const unsigned kneadingsStart, const unsigned kneadingsEnd)
{
	unsigned i, j, k, kneadingIndex=0, kneadingArrayIndex=0, previousEvent=0;
	double dt2, dt6;
	double y1[N_EQ1], y2[N_EQ1], k1[N_EQ1], k2[N_EQ1], k3[N_EQ1], k4[N_EQ1];
	double firstDerivativeCurrent[N_EQ1],firstDerivativePrevious;
	double kneadingsWeightedSum=0;
    char kneadings[MAX_KNEADING_LENGTH];

	bool isPreviousEventOne=false, isDerviative2FirstTimeOnThisSidePositive=0, isPreviousDerivative2Positive=0, isCurrentDerivate2Positive=0;

	dt2 = dt/2.; dt6 = dt/6.;

	for(i=1; i<N; i++)
	{
		for(j=0; j<stride; j++)
		{
			stepper(y_current, k1, params);
			for(k=0; k<N_EQ1; k++) y1[k] = y_current[k]+k1[k]*dt2;
			stepper(y1, k2, params);
			for(k=0; k<N_EQ1; k++) y2[k] = y_current[k]+k2[k]*dt2;
			stepper(y2, k3, params);
			for(k=0; k<N_EQ1; k++) y2[k] = y_current[k]+k3[k]*dt;
			stepper(y2, k4, params);

			//Copy latest value into y_current
			for(k=0; k<N_EQ1; k++) y_current[k] += dt6*(k1[k]+2.*(k2[k]+k3[k])+k4[k]);
		}

        /*if(i*dt < 200){
         continue;
        }*/

		computeFirstDerivative(y_current, firstDerivativeCurrent, params);

        //if(firstDerivativePrevious * firstDerivativeCurrent[0] < 0) { //to avoid tiny oscillations due to numerical
         error around a fixed point, helps especially detect AH bifurcation
        if(firstDerivativePrevious * firstDerivativeCurrent[0] < -1.E-12) {
            if(kneadingIndex<kneadingsStart){
                //skip ahead upto the kneadingsStart symbol
                kneadingIndex++;
            } else if(kneadingArrayIndex==0 and firstDerivativePrevious<0){
                //First symbol is always a maxima to maintain the order of maxima,time,minima,time... and to reuse the same symbols for all four items in the sequence; We ignore the first minima if any until we encounter the first maxima (which should immediately follow the minima)
                kneadingIndex++;
            } else {
               if(kneadingArrayIndex!=0){
                    //Except for the first maxima, every other current event will be preceded by the time interval from the last interval
                    kneadings[kneadingArrayIndex++] = getBinSymbol(dt * (i - previousEvent), intervalBinBoundariesGpu, nI)+13; //intervalBin; using a to m for voltage bins, and n to z for time bins so that when computing circular permutations, there is no confusion
                    //printf("%c,",kneadings[kneadingArrayIndex-1]);
                    kneadingIndex++;
               }
               kneadings[kneadingArrayIndex++] = getBinSymbol(y_current[0], voltageBinBoundariesGpu, nV); //voltageBin
               //printf("%c,",kneadings[kneadingArrayIndex-1]);
               kneadingIndex++;
            }
            previousEvent = i;
        }
		firstDerivativePrevious = firstDerivativeCurrent[0];

		if(kneadingIndex>kneadingsEnd){
            //print test
            //kneadings[kneadingArrayIndex++]=char('\0'); printf("%lf %lf %s\n length: %d", params[0], params[1], kneadings, kneadingArrayIndex); //return 0;

			return periodicPCorChaoticLZ(kneadings, kneadingArrayIndex);
			//return LZ(kneadings, kneadingArrayIndex); //temporarily only using LZ
		}
	}
	//print test
	//printf("%lf %lf %s \t length: %d\n", params[0], params[1], kneadings, kneadingIndex); //return 0;
	return -0.5;
}

__global__ void sweepThreads(double* kneadingsWeightedSumSet, const double* voltageBinBoundariesGpu, const double* intervalBinBoundariesGpu,
                                        const double alphaStart, const double alphaEnd, const unsigned alphaCount,
										const double lStart, const double lEnd, const unsigned lCount,
										const double dt, const unsigned N, const unsigned stride,
										const unsigned nV, const unsigned nI,
										const unsigned kneadingsStart, const unsigned kneadingsEnd){

    int tx=blockIdx.x * blockDim.x + threadIdx.x;

	double params[2],a,b;
	double alphaStep = alphaCount==1?0:(alphaEnd - alphaStart)/(alphaCount-1);
	double lStep = lCount==1?0:(lEnd-lStart)/(lCount -1);
	int i,j,k;


    if(tx<alphaCount*lCount){

        i=tx/alphaCount;
        j=tx%alphaCount;

        params[0]=alphaStart+i*alphaStep;
        params[1]=lStart+j*lStep;

        //V2=y[0], Ca2=y[1], h2=y[2], n2=y[3], x2=y[4], y2=y[5]
      	double y_initial[N_EQ1]={-6.405504935885031E-002 ,
                                1.510925113459757E-002 ,
                                1.503835478594321E-002,
                                0.156789443830350       ,
                                0.165144360467404      ,
                                0.322644807411062     ,
                                6.257830798697624E-002,
                                2.317122358369597E-003,
                                0.167388800546701,
                                2.927998693440382E-003,
                                9.362823433735715E-007,
                                0.595924681516093};

        kneadingsWeightedSumSet[i*lCount+j] = integrator_rk4(y_initial, voltageBinBoundariesGpu, intervalBinBoundariesGpu, params, dt, N, stride, nV, nI, kneadingsStart, kneadingsEnd);

    }
}

/**
 * error checking routine
 */
void checkAndDisplayErrors(char *label)
{
  // we need to synchronise first to catch errors due to
  // asynchronous operations that would otherwise
  // potentially go unnoticed

  cudaError_t err;

  err = cudaThreadSynchronize();
  if (err != cudaSuccess)
  {
    char *e = (char*) cudaGetErrorString(err);
    fprintf(stderr, "CUDA Error: %s (at %s)", e, label);
  }

  err = cudaGetLastError();
  if (err != cudaSuccess)
  {
    char *e = (char*) cudaGetErrorString(err);
    fprintf(stderr, "CUDA Error: %s (at %s)", e, label);
  }
}


extern "C" {

void sweep(double* kneadingsWeightedSumSet,const double* voltageBinBoundaries,const double* intervalBinBoundaries,
                                        const double alphaStart, const double alphaEnd, const unsigned alphaCount,
										const double lStart, const double lEnd, const unsigned lCount,
										const double dt, const unsigned N, const unsigned stride,
										const unsigned nV, const unsigned nI,
										const unsigned kneadingsStart, const unsigned kneadingsEnd){

        int totalParameterSpaceSize = alphaCount*lCount;
        double *kneadingsWeightedSumSetGpu, *voltageBinBoundariesGpu, *intervalBinBoundariesGpu;

        printf("Before malloc gpu\n");
        /*allocate device memory. */
        (cudaMalloc( (void**) &kneadingsWeightedSumSetGpu, totalParameterSpaceSize*sizeof(double)));
        (cudaMalloc( (void**) &voltageBinBoundariesGpu, nV*sizeof(double)));
        (cudaMalloc( (void**) &intervalBinBoundariesGpu, nI*sizeof(double)));
        checkAndDisplayErrors("Memory allocation");
        printf("After malloc gpu\n");

        /*copy data from host to device */
        (cudaMemcpy( voltageBinBoundariesGpu, voltageBinBoundaries, nV*sizeof(double), cudaMemcpyHostToDevice));
        (cudaMemcpy( intervalBinBoundariesGpu, intervalBinBoundaries, nI*sizeof(double), cudaMemcpyHostToDevice));
        checkAndDisplayErrors("Error while copying bin boundaries from host to device.");

        /*timing*/
        cudaEvent_t start_event, stop_event;
        cudaEventCreate(&start_event) ;
        cudaEventCreate(&stop_event) ;
        cudaEventRecord(start_event, 0);

        /*Dimensions. */

        int gridXDimension = totalParameterSpaceSize/NUM_THREADS_PER_BLOCK;
        if(totalParameterSpaceSize%NUM_THREADS_PER_BLOCK!=0) {
                gridXDimension += 1;
        }
        dim3 dimGrid(gridXDimension,1);
        dim3 dimBlock(NUM_THREADS_PER_BLOCK,1);

        printf(" Num of blocks per grid:       %d\n", gridXDimension);
        printf(" Num of threads per block:     %d\n", NUM_THREADS_PER_BLOCK);
        printf(" Total Num of threads running: %d\n", gridXDimension*NUM_THREADS_PER_BLOCK);
        printf(" Parameters alphaCount=%d, lCount=%d\n",alphaCount,lCount);

        /*Call kernel(global function)*/
        sweepThreads<<<dimGrid, dimBlock>>>(kneadingsWeightedSumSetGpu, voltageBinBoundariesGpu, intervalBinBoundariesGpu, alphaStart, alphaEnd, alphaCount, lStart, lEnd, lCount, dt, N, stride, nV, nI, kneadingsStart, kneadingsEnd);

        cudaThreadSynchronize();
        cudaEventRecord(stop_event, 0);
        cudaEventSynchronize(stop_event);

        float time_kernel;
        cudaEventElapsedTime(&time_kernel, start_event, stop_event);
        printf("Total time(sec) %f\n", time_kernel/1000);

        /*copy data from device memory to memory. */
        (cudaMemcpy( kneadingsWeightedSumSet, kneadingsWeightedSumSetGpu, totalParameterSpaceSize*sizeof(double), cudaMemcpyDeviceToHost));
        checkAndDisplayErrors("Error while copying kneading sum values from device to host.");

        /*Free all allocated memory. */
        cudaFree(kneadingsWeightedSumSetGpu);
        cudaFree(voltageBinBoundariesGpu);
        cudaFree(intervalBinBoundariesGpu);

}

}


int main(){
	double dt=1e-4;
	unsigned N=30000*10;
	unsigned stride = 1;
	unsigned maxKneadings = 201;
	unsigned sweepSize = 1;
	double *kneadingsWeightedSumSet;
	int i,j;
	double voltageBinBoundaries[] = {-0.058, -0.052, -0.05}, intervalBinBoundaries [] = {};

	printf("Enter sweep size :");
	scanf("%d",&sweepSize);
	kneadingsWeightedSumSet = (double *)malloc(sweepSize*sweepSize*sizeof(double));


	sweep(kneadingsWeightedSumSet, voltageBinBoundaries, intervalBinBoundaries, -1, -1, sweepSize, 15., 15.,
	sweepSize,
	 dt, N, stride, 3, 0, 100, 200);

	for(i=0;i<sweepSize;i++){
	    for(j=0;j<sweepSize;j++){
	        printf("%f \n",kneadingsWeightedSumSet[i*sweepSize+j]);
	    }
	}

}

