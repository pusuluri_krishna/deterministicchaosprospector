
The code is presented for all the computational methods described and to generate the images in the following publications:

	Pusuluri K, Shilnikov A. Symbolic representation of neuronal dynamics. Advances on Nonlinear Dynamics of Electronic Systems, World Scientific. 2019:97-102.

If you make use of this repository in your research, please consider citing the above articles.
