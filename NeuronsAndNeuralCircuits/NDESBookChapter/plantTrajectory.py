from __future__ import division
#to get default literal division to be floating point arithmetic
import matplotlib
matplotlib.use('Qt4Agg')
import ctypes as ct
from pylab import *
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

lib = ct.cdll.LoadLibrary('./plantTrajectory.so')

N_EQ=6
lib.integrator_rk4.argtypes = [ct.POINTER(ct.c_double), ct.POINTER(ct.c_double),ct.POINTER(ct.c_double),
                          ct.c_double,ct.c_uint,ct.c_uint]
def integrator_rk4 (y_initial = np.asarray([-39.99, 0.9, 0.5, 0.2, 0.7, 0.2]),
                    dt=0.001,
                    N=30000,
                    stride = 1
                ):
    output = np.zeros(N*N_EQ, float)
    params=np.asarray([100, 40, 0., 0., -55, 0.003]) #	shift_Ca2, shift_x2, Iapp, gh, Vhh, gleak;
    params = np.asarray([-8., -1.97, 0., 0., -55., 0.003], float)  # shift_Ca2, shift_x2, Iapp, gh, Vhh, gleak; #chaotic second loop
    params = np.asarray([-8.61873, -1.94959, 0., 0., -55., 0.003], float)  # shift_Ca2, shift_x2, Iapp, gh, Vhh, gleak; #chaotic second loop
    lib.integrator_rk4(y_initial.ctypes.data_as(ct.POINTER(ct.c_double)), output.ctypes.data_as(ct.POINTER(ct.c_double)), params.ctypes.data_as(ct.POINTER(ct.c_double)),
                       ct.c_double(dt), ct.c_uint(N), ct.c_uint(stride))
    #imshow takes fortran style array input
    return np.reshape(output, (N,N_EQ), 'C')

N=300000; dt=1

animationStepSize=10000
o = integrator_rk4(N=N,dt=dt)
fig = plt.figure()
sp = fig.add_subplot(111)
sp.set_xlim(0, dt*N)
sp.set_ylim(-100, 100)
sp.set_xlabel('time', fontsize=20)
sp.set_ylabel('V', fontsize=20)
#line, = sp.plot(o[:,0], o[:,5],'b-')
#line, = sp.plot([], [],'b-')

line, = sp.plot(dt*arange(0,N,1.), o[0:N,0],'b-')

"""
i=0

def update(data):
    global i
    global animationStepSize
    line.set_xdata(dt*arange(0,i+animationStepSize,1.))
    line.set_ydata(o[0:i+animationStepSize,0])
    i=i+animationStepSize
    return line,
ani = animation.FuncAnimation(fig, update,frames=int(N/animationStepSize))
"""

plt.show()
