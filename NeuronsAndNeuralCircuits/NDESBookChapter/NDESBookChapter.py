#All labels adjusted for Smaug workstation at lab

from __future__ import division
import matplotlib
matplotlib.use('TkAgg')
#matplotlib.use('Qt4Agg')
import ctypes as ct
from pylab import *
import numpy as np
from matplotlib import colors
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from mpl_toolkits.axes_grid.inset_locator import inset_axes
from mpl_toolkits.axes_grid.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid.inset_locator import mark_inset
import matplotlib.patches as patches
import pickle
import gzip
from scipy.integrate import ode
from matplotlib import transforms
import matplotlib.image as mpimg
from numpy.ma import masked_array

colorMapLevels = 2**8; dataSaved = True; sweepSize = 1000; outputdir = 'Output/'

blue = np.linspace(0.01, 1, colorMapLevels)
red = 1 - blue
min = 0.;
max = .8
green = min + np.random.random(colorMapLevels) * (max - min)
RGB = np.column_stack((red, green, blue))
customColorMap = colors.ListedColormap(RGB)

blue = np.linspace(0.8, 0, colorMapLevels)
red = blue
green = blue
RGB = np.column_stack((red, green, blue))
customGrayColorMap = colors.ListedColormap(RGB)

figSize = 5

matplotlib.rcParams['savefig.dpi'] = 1200 #2*sweepSize/figSize #300
defaultFontSize = 11; largeFontSize = 14; indexFontSize = 16;
matplotlib.rcParams.update({'font.size': largeFontSize,
              'axes.labelsize': largeFontSize,
              'axes.titlesize': largeFontSize,
              'font.size': defaultFontSize,
              'legend.fontsize': largeFontSize,
              'xtick.labelsize': defaultFontSize,
              'ytick.labelsize': defaultFontSize})

#comment these two for rainbow text function to run properly, along with setting fig.dpi = 1200 described near rainbow text
matplotlib.rc('text', usetex=True)
matplotlib.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]

sweepData = [[0.1,0.2],[0.3,0.4]]                           #### For testing purposes

bbox_props = dict(boxstyle="square,pad=0.2", fc="white", ec="k", lw=.5, alpha=0.)
arrowprops = dict(arrowstyle="->", connectionstyle="arc3", facecolor='white', lw=1.,
                  ec="white")  # ,shrink=0.05, width=defaultFontSize/10.)
bbox_props_fig = dict(boxstyle="square,pad=0.001", fc="white", ec="k", lw=0)
bbox_props_index = dict(boxstyle="square,pad=0.1", fc="white", ec="k", lw=1.)
bbox_props_label = dict(boxstyle="square,pad=0.01", fc="white", ec="k", lw=0	)


def processSweepData(axis, sweepData, aMin, aMax, bMin, bMax):

    # PC hash can be any integer value (positive/negative), LZ lies between 0 and 1, -0.5 for insufficient N
    sweepDataInsufficientN = masked_array(sweepData, sweepData != -0.5)
    sweepDataComputed = masked_array(sweepData, sweepData == -0.5)
    sweepDataChaotic = masked_array(sweepDataComputed, (sweepData <= 0) | (sweepData >= 1))
    sweepDataPeriodic = masked_array(sweepDataComputed, (sweepData > 0) & (sweepData < 1))

    # to force common color scheme without scaling across images, use the vmin and vmax arguments of imshow(), and make sure they are the same for all your images
    axis.imshow(sweepDataPeriodic,
           extent=[aMin, aMax, bMin, bMax],
           aspect=(aMax - aMin) / (bMax - bMin),
           cmap=customColorMap,
           origin='lower',  # vmin=-1., vmax=0.1
           )  # making vmax 0.1 instead of 0. to match the colormaps for short and long kneadings

    axis.imshow(sweepDataChaotic,
           extent=[aMin, aMax, bMin, bMax],
           aspect=(aMax - aMin) / (bMax - bMin),
           cmap=customGrayColorMap,
           origin='lower')

    axis.imshow(sweepDataInsufficientN,
           extent=[aMin, aMax, bMin, bMax],
           aspect=(aMax - aMin) / (bMax - bMin),
           cmap=colors.ListedColormap(['white']),
           origin='lower')



"""
############################# Image2: Leech, Hair celll, HR sweeps ########################################################################

####### Whole Image

print 'Generating Image2'

if not dataSaved :
    pickleDataFile1 = gzip.open(outputdir+'Image2.gz','wb')
else :
    pickleDataFile1 = gzip.open(outputdir+'Image2.gz','rb')

matplotlib.rcParams['savefig.dpi'] = 600

figSizeX=6; figSizeY=4; gridX = 2 ; gridY = 3;

fig = plt.figure(figsize=(figSizeX, figSizeY))
gs1 = gridspec.GridSpec(gridX, gridY)
gs1.update(wspace=0.03, hspace=0.03)  # set the spacing between axes.

#Quick fix to shift first and third plot positions
axRightBottom = plt.subplot(gs1[0:2,0:2])
axRightTop = plt.subplot(gs1[0,2])
axLeft = plt.subplot(gs1[1,2])

######### Leech sweep

print 'Leech sweep'

libLeech = ct.cdll.LoadLibrary('./leech_sweep_PCLZ.so')
libLeech.sweep.argtypes = [ct.POINTER(ct.c_double),
                        ct.POINTER(ct.c_double), ct.POINTER(ct.c_double),
                        ct.c_double, ct.c_double, ct.c_uint,
                        ct.c_double, ct.c_double, ct.c_uint,
                        ct.c_double, ct.c_uint, ct.c_uint,
                        ct.c_uint, ct.c_uint,
                        ct.c_uint, ct.c_uint]

def sweepLeech(dt=1, N=300000, stride=1,
            VK2ShiftMin=-0.035, VK2ShiftMax=0.02,
            IappMin=-0.07, IappMax=0.01,
            voltageBinBoundaries=np.array([], dtype='float64'),
            intervalBinBoundaries=np.array([], dtype='float64'),
            kneadingsStart=100,
            kneadingsEnd=200,
            sweepSize=100
            ):
    kneadingsWeightedSumSet = np.zeros(sweepSize * sweepSize, float)
    libLeech.sweep(kneadingsWeightedSumSet.ctypes.data_as(ct.POINTER(ct.c_double)),
              voltageBinBoundaries.ctypes.data_as(ct.POINTER(ct.c_double)),
              intervalBinBoundaries.ctypes.data_as(ct.POINTER(ct.c_double)),
              ct.c_double(VK2ShiftMin), ct.c_double(VK2ShiftMax), ct.c_uint(sweepSize),
              ct.c_double(IappMin), ct.c_double(IappMax), ct.c_uint(sweepSize),
              ct.c_double(dt), ct.c_uint(N), ct.c_uint(stride),
              ct.c_uint(len(voltageBinBoundaries)), ct.c_uint(len(intervalBinBoundaries)),
              ct.c_uint(kneadingsStart), ct.c_uint(kneadingsEnd))
    # imshow takes fortran style array input
    return np.reshape(kneadingsWeightedSumSet, (sweepSize, sweepSize), 'F')

VK2ShiftMin = -0.03; VK2ShiftMax = 0.015; IappMin = -0.06; IappMax = 0.012;
N = 300000; dt=0.01;
voltageBinBoundaries = np.array([-0.04], dtype='float64')
kneadingsStart=2000; kneadingsEnd=4000;

plt.axis('on')
axLeft.set_xticks([])
axLeft.set_yticks([])
axLeft.text( 0., -0.02, VK2ShiftMin, ha='left', va='top', bbox=bbox_props_label, transform=axLeft.transAxes, fontsize=defaultFontSize)
axLeft.text( 1., -0.02, VK2ShiftMax, ha='right', va='top', bbox=bbox_props_label, transform=axLeft.transAxes, fontsize=defaultFontSize)
axLeft.text(1.02, 0, IappMin, ha='left', va='bottom', rotation=90, bbox=bbox_props_label,
            transform=axLeft.transAxes, fontsize=defaultFontSize)
axLeft.text(1.02, 1., IappMax, ha='left', va='top', rotation=90, bbox=bbox_props_label, transform=axLeft.transAxes,
            fontsize=defaultFontSize)

if not dataSaved:
    sweepData = sweepLeech(dt=dt, N=N, stride=1, VK2ShiftMin=VK2ShiftMin, VK2ShiftMax=VK2ShiftMax, IappMin=IappMin,
                            IappMax=IappMax, kneadingsStart=kneadingsStart, kneadingsEnd=kneadingsEnd,
                            voltageBinBoundaries= voltageBinBoundaries,
                            sweepSize=sweepSize)
    pickle.dump(sweepData, pickleDataFile1)
else:
    sweepData = pickle.load(pickleDataFile1)

processSweepData(axLeft, sweepData, VK2ShiftMin, VK2ShiftMax, IappMin, IappMax);

axLeft.autoscale(False)
axLeft.set_adjustable('box-forced')

axLeft.text(1., 1., chr(2 + ord('a')), ha='right', va='top', bbox=bbox_props_index,
                   transform=axLeft.transAxes, fontsize=largeFontSize)

######### Hair cell sweep

print 'Hair cell sweep'

libHairCell = ct.cdll.LoadLibrary('./hairCell_sweep_PCLZ.so')
libHairCell.sweep.argtypes = [ct.POINTER(ct.c_double),
                        ct.POINTER(ct.c_double), ct.POINTER(ct.c_double),
                        ct.c_double, ct.c_double, ct.c_uint,
                        ct.c_double, ct.c_double, ct.c_uint,
                        ct.c_double, ct.c_uint, ct.c_uint,
                        ct.c_uint, ct.c_uint,
                        ct.c_uint, ct.c_uint]

def sweepHairCell(dt=1, N=300000, stride=1,
            bkMin=0.001, bkMax=1.,
            gk1Min=15, gk1Max=45,
            voltageBinBoundaries=np.array([], dtype='float64'),
            intervalBinBoundaries=np.array([], dtype='float64'),
            kneadingsStart=100,
            kneadingsEnd=200,
            sweepSize=100
            ):
    kneadingsWeightedSumSet = np.zeros(sweepSize * sweepSize, float)
    libHairCell.sweep(kneadingsWeightedSumSet.ctypes.data_as(ct.POINTER(ct.c_double)),
              voltageBinBoundaries.ctypes.data_as(ct.POINTER(ct.c_double)),
              intervalBinBoundaries.ctypes.data_as(ct.POINTER(ct.c_double)),
              ct.c_double(log10(bkMin)), ct.c_double(log10(bkMax)), ct.c_uint(sweepSize),
              ct.c_double(gk1Min), ct.c_double(gk1Max), ct.c_uint(sweepSize),
              ct.c_double(dt), ct.c_uint(N), ct.c_uint(stride),
              ct.c_uint(len(voltageBinBoundaries)), ct.c_uint(len(intervalBinBoundaries)),
              ct.c_uint(kneadingsStart), ct.c_uint(kneadingsEnd))
    # imshow takes fortran style array input
    return np.reshape(kneadingsWeightedSumSet, (sweepSize, sweepSize), 'F')

bkMin = 10**-3; bkMax = 10**-1.5; gk1Min = 24; gk1Max = 44;
N = 300000*10; dt=1E-4;
kneadingsStart = 1000; kneadingsEnd = 2000;
voltageBinBoundaries = np.array([-0.08], dtype='float64')

plt.axis('on')
axRightTop.set_xticks([])
axRightTop.set_yticks([])
axRightTop.text( 0.01, .01, bkMin, ha='left', va='bottom', transform=axRightTop.transAxes,
                 fontsize=defaultFontSize)
axRightTop.text( .99, .01, "%.3f"%round(bkMax,3), ha='right', va='bottom', transform=axRightTop.transAxes,
                 fontsize=defaultFontSize, color='w')
axRightTop.text(1.02, 0, gk1Min, ha='left', va='bottom', rotation=90, bbox=bbox_props_label,
                transform=axRightTop.transAxes, fontsize=defaultFontSize)
axRightTop.text(1.02, 1., gk1Max, ha='left', va='top', rotation=90, bbox=bbox_props_label,
                transform=axRightTop.transAxes, fontsize=defaultFontSize)

if not dataSaved:
    sweepData = sweepHairCell(dt=dt, N=N, stride=1, bkMin=bkMin, bkMax=bkMax, gk1Min=gk1Min,
                      gk1Max=gk1Max, kneadingsStart=kneadingsStart, kneadingsEnd=kneadingsEnd,
                      voltageBinBoundaries=voltageBinBoundaries,
                      sweepSize=sweepSize)
    pickle.dump(sweepData, pickleDataFile1)
else:
    sweepData = pickle.load(pickleDataFile1)

#Note the logarithmic range of bk parameter
processSweepData(axRightTop, sweepData, log10(bkMin), log10(bkMax), gk1Min, gk1Max);

axRightTop.autoscale(False)
axRightTop.set_adjustable('box-forced')

axRightTop.text(1., 1., chr(1 + ord('a')), ha='right', va='top', bbox=bbox_props_index,
                   transform=axRightTop.transAxes, fontsize=largeFontSize)

######### HR sweep

print 'HR sweep'

libHR = ct.cdll.LoadLibrary('./HR_sweep_PCLZ.so')
libHR.sweepHR.argtypes = [ct.POINTER(ct.c_double),
                        ct.POINTER(ct.c_double), ct.POINTER(ct.c_double),
                        ct.c_double, ct.c_double, ct.c_uint,
                        ct.c_double, ct.c_double, ct.c_uint,
                        ct.c_double, ct.c_uint, ct.c_uint,
                        ct.c_uint, ct.c_uint,
                        ct.c_uint, ct.c_uint]

def sweepHR(dt=0.1, N=N, stride=1,
            bmin=2.5, bmax=3.3,
            Imin=2.2, Imax=4.4,
            voltageBinBoundaries=np.array([], dtype='float64'),
            intervalBinBoundaries=np.array([], dtype='float64'),
            kneadingsStart=1000,
            kneadingsEnd=2000,
            sweepSize=100
            ):
    kneadingsWeightedSumSet = np.zeros(sweepSize * sweepSize, float)
    libHR.sweepHR(kneadingsWeightedSumSet.ctypes.data_as(ct.POINTER(ct.c_double)),
                voltageBinBoundaries.ctypes.data_as(ct.POINTER(ct.c_double)),
                intervalBinBoundaries.ctypes.data_as(ct.POINTER(ct.c_double)),
                ct.c_double(bmin), ct.c_double(bmax), ct.c_uint(sweepSize),
                ct.c_double(Imin), ct.c_double(Imax), ct.c_uint(sweepSize),
                ct.c_double(dt), ct.c_uint(N), ct.c_uint(stride),
                ct.c_uint(len(voltageBinBoundaries)), ct.c_uint(len(intervalBinBoundaries)),
                ct.c_uint(kneadingsStart), ct.c_uint(kneadingsEnd))
    # imshow takes fortran style array input
    return np.reshape(kneadingsWeightedSumSet, (sweepSize, sweepSize), 'F')

bmin=2.5; bmax=3.1; Imin=2.5; Imax=4.5;
N = 540000*20; dt=0.1;
kneadingsStart = 5000; kneadingsEnd = 10000;
voltageBinBoundaries = np.array([-1.2], dtype='float64')

plt.axis('on')
axRightBottom.set_xticks([])
axRightBottom.set_yticks([])
axRightBottom.text( 0., -0.01, bmin, ha='left', va='top', bbox=bbox_props_label, transform=axRightBottom.transAxes,
                    fontsize=defaultFontSize)
axRightBottom.text( 1., -0.01, bmax, ha='right', va='top', bbox=bbox_props_label, transform=axRightBottom.transAxes,
                    fontsize=defaultFontSize)
axRightBottom.text(-0.01, 0, Imin, ha='right', va='bottom', rotation=90, bbox=bbox_props_label,
                   transform=axRightBottom.transAxes, fontsize=defaultFontSize)
axRightBottom.text(-0.01, 1., Imax, ha='right', va='top', rotation=90, bbox=bbox_props_label,
                   transform=axRightBottom.transAxes, fontsize=defaultFontSize)

if not dataSaved:
    sweepData = sweepHR(dt=0.1, N=N, stride=1, bmin=bmin, bmax=bmax, Imin=Imin, Imax=Imax,
                        kneadingsStart=kneadingsStart, kneadingsEnd=kneadingsEnd,
                        voltageBinBoundaries=voltageBinBoundaries,
                        sweepSize=sweepSize)

    pickle.dump(sweepData, pickleDataFile1)
else:
    sweepData = pickle.load(pickleDataFile1)

#Note the logarithmic nature of
processSweepData(axRightBottom, sweepData, bmin, bmax, Imin, Imax);

axRightBottom.autoscale(False)
axRightBottom.set_adjustable('box-forced')

axRightBottom.text(1., 1., chr(0 + ord('a')), ha='right', va='top', bbox=bbox_props_index,
                   transform=axRightBottom.transAxes, fontsize=largeFontSize)

axRightBottom.annotate(r'$\boldsymbol{tonic~spiking}$', xy=(2.85,4.), xytext=(2.85,4.), bbox=bbox_props,
                color='w')
axRightBottom.annotate(r'$\boldsymbol{square~wave~burst}$', xy=(2.6,2.6), xytext=(2.6,2.6), bbox=bbox_props,
                color='w')
axRightBottom.annotate(r'$\boldsymbol{plateau~burst}$', xy=(2.52,4.3), xytext=(2.52,4.3), bbox=bbox_props,
                color='w', rotation=90)
axRightBottom.annotate(r'$\boldsymbol{chaos}$', xy=(2.96, 3.27), xytext=(2.96, 3.5), arrowprops=arrowprops,
             bbox=bbox_props, color='white' )

######## Whole image

xlabelX=0.38; xlabel1X = 0.84; xlabelY=0.059; ylabelX=0.065; ylabelY=0.5;
xlabel = r'$\mathrm{b}$';
xlabel1 = r'$\mathrm{V_{K_2~shift}}$';
ylabel = r'$\mathrm{I_{app}}$'
fig.text(xlabelX, xlabelY, xlabel, ha='center', bbox=bbox_props_fig, color='darkslategray', fontsize=largeFontSize,
         weight='roman')
fig.text(xlabel1X, 0.053, xlabel1, ha='center', bbox=bbox_props_fig, color='darkslategray',
         fontsize=largeFontSize,
         weight='roman')
fig.text(ylabelX, ylabelY, ylabel, va='center', rotation='vertical', bbox=bbox_props_fig, color='darkslategray',
         fontsize=largeFontSize, weight='roman')

fig.text(xlabel1X, 0.57, r'$\mathrm{log(b)}$', ha='center', bbox=bbox_props_fig, color='darkslategray',
         fontsize=largeFontSize,
         weight='roman')
fig.text(1.019, 0.77, r'$\mathrm{g_{K_1}}$', ha='center', bbox=bbox_props_fig, color='darkslategray',
         fontsize=largeFontSize, weight='roman', rotation=90)
fig.text(1.021, 0.32, r'$\mathrm{I_{app}}$', ha='center', bbox=bbox_props_fig, color='darkslategray',
         fontsize=largeFontSize, weight='roman', rotation=90)

fig.text(xlabelX, xlabelY, xlabel, ha='center', bbox=bbox_props_fig, color='darkslategray', fontsize=largeFontSize,
         weight='roman')

plt.subplots_adjust(left=0.1, right=0.999, top=0.999, bottom=0.1)

fig.savefig(outputdir+'Image2.jpg', bbox_inches='tight')

pickleDataFile1.close()

############################# End Image2: Leech, Hair celll, HR sweeps ####################################################################
"""

"""
############################# Image0: Plant trajectory ####################################################################

libPlantTrajectory = ct.cdll.LoadLibrary('./plantTrajectory.so')

N_EQ=6
libPlantTrajectory.integrator_rk4.argtypes = [ct.POINTER(ct.c_double), ct.POINTER(ct.c_double),ct.POINTER(ct.c_double),
                          ct.c_double,ct.c_uint,ct.c_uint]
def integrator_rk4 (y_initial = np.asarray([-39.99, 0.9, 0.5, 0.2, 0.7, 0.2]),
                    dt=0.001,
                    N=30000,
                    stride = 1
                ):
    output = np.zeros(N*N_EQ, float)
    params=np.asarray([100, 40, 0., 0., -55, 0.003]) #	shift_Ca2, shift_x2, Iapp, gh, Vhh, gleak;
    params = np.asarray([-8., -1.97, 0., 0., -55., 0.003], float)  # shift_Ca2, shift_x2, Iapp, gh, Vhh, gleak; #chaotic second loop
    params = np.asarray([-8.61873, -1.94959, 0., 0., -55., 0.003], float)  # shift_Ca2, shift_x2, Iapp, gh, Vhh, gleak; #chaotic second loop
    libPlantTrajectory.integrator_rk4(y_initial.ctypes.data_as(ct.POINTER(ct.c_double)), output.ctypes.data_as(ct.POINTER(ct.c_double)), params.ctypes.data_as(ct.POINTER(ct.c_double)),
                       ct.c_double(dt), ct.c_uint(N), ct.c_uint(stride))
    #imshow takes fortran style array input
    return np.reshape(output, (N,N_EQ), 'C')

print 'Generating Image0'

if not dataSaved :
    pickleDataFile1 = gzip.open(outputdir+'Image0.gz','wb')
else :
    pickleDataFile1 = gzip.open(outputdir+'Image0.gz','rb')

matplotlib.rcParams['savefig.dpi'] = 600

figSizeX=6; figSizeY=2; gridX = 1 ; gridY = 2;

fig = plt.figure(figsize=(figSizeX, figSizeY))
gs1 = gridspec.GridSpec(gridX, gridY)
gs1.update(wspace=0.03, hspace=0.03)  # set the spacing between axes.

axLeft = plt.subplot(gs1[0,0])
axRight = plt.subplot(gs1[0,1])

N=300000; dt=1


### Chaotic trajectory
plt.axis('on')
axLeft.set_xticks([])
axLeft.set_yticks([])
#axLeft.text( 0., -0.02, VK2ShiftMin, ha='left', va='top', bbox=bbox_props_label, transform=axLeft.transAxes, fontsize=defaultFontSize)
#axLeft.text( 1., -0.02, VK2ShiftMax, ha='right', va='top', bbox=bbox_props_label, transform=axLeft.transAxes, fontsize=defaultFontSize)
#axLeft.text(-0.02, 0, IappMin, ha='right', va='bottom', rotation=90, bbox=bbox_props_label, transform=axLeft.transAxes, fontsize=defaultFontSize)
#axLeft.text(-0.02, 1., IappMax, ha='right', va='top', rotation=90, bbox=bbox_props_label, transform=axLeft.transAxes, fontsize=defaultFontSize)

if not dataSaved:
    plotData = integrator_rk4(N=N, dt=dt)
    pickle.dump(plotData, pickleDataFile1)
else:
    plotData = pickle.load(pickleDataFile1)

axLeft.set_xlim(0, dt*N)
axLeft.set_ylim(-75, 50)
#axLeft.set_xlabel('time', fontsize=20)
#xLeft.set_ylabel('V', fontsize=20)

axLeft.axhline(y=-57.,color='r', linestyle='dashed', linewidth=0.75)
axLeft.axhline(y=-40.,color='r', linestyle='dashed', linewidth=0.75)
axLeft.axhline(y=10.,color='r', linestyle='dashed', linewidth=0.75)

line, = axLeft.plot(dt*arange(0,N,1.), plotData[0:N,0],'b-', linewidth=0.75)

axLeft.autoscale(False)
axLeft.set_adjustable('box-forced')

axLeft.text(1., 1., chr(0 + ord('a')), ha='right', va='top', bbox=bbox_props_index,
                   transform=axLeft.transAxes, fontsize=largeFontSize)

axLeft.text( -0.02, 0.14, -60, ha='right', va='center', bbox=bbox_props_label, transform=axLeft.transAxes,
                    fontsize=defaultFontSize)
axLeft.text( -0.02, 0.28, -40, ha='right', va='center', bbox=bbox_props_label, transform=axLeft.transAxes,
                    fontsize=defaultFontSize)
axLeft.text( -0.02, 0.69, 10, ha='right', va='center', bbox=bbox_props_label, transform=axLeft.transAxes,
                    fontsize=defaultFontSize)

line, = axLeft.plot(dt*arange(230*N/300,250*N/300,4*N/300), -63*np.ones(5,float),'b-')
axLeft.annotate(r'$\boldsymbol{20s}$', xy=(230000, -73.), xytext=(230000, -73.), bbox=bbox_props,
                color='b')

axLeft.annotate(r'$\boldsymbol{a}$', xy=(130000,-67.), xytext=(130000,-67.), bbox=bbox_props,
                color='r')
axLeft.annotate(r'$\boldsymbol{b}$', xy=(130000,-48.), xytext=(130000,-48.), bbox=bbox_props,
                color='r')
axLeft.annotate(r'$\boldsymbol{c}$', xy=(130000,-14.), xytext=(130000,-14.), bbox=bbox_props,
                color='r')
axLeft.annotate(r'$\boldsymbol{d}$', xy=(130000, 24.), xytext=(130000, 24.), bbox=bbox_props,
                color='r')



############ Symbols
plt.axis('on')
axRight.set_xticks([])
axRight.set_yticks([])
axRight.set_xlim(6.7*N/10, dt*N*6.73/10)
axRight.set_ylim(-75, 50)
#axRight.set_xlabel('time', fontsize=20)
#axRight.set_ylabel('V', fontsize=20)

axRight.autoscale(False)
axRight.set_adjustable('box-forced')

line, = axRight.plot(dt*arange(6.725*N/10,6.727*N/10,N*0.0002/5), -63.*np.ones(5,float),'b-')
axRight.annotate(r'$\boldsymbol{60ms}$', xy=(6.725*N/10, -73.), xytext=(6.725*N/10, -73.), bbox=bbox_props,
                color='b')

axRight.axvline(x=201222, color='gray', linestyle='dashed', linewidth=0.75)
axRight.axvline(x=201291, color='gray', linestyle='dashed', linewidth=0.75)
axRight.axvline(x=201647, color='gray', linestyle='dashed', linewidth=0.75)
axRight.axvline(x=201716, color='gray', linestyle='dashed', linewidth=0.75)
#axRight.axvline(x=202133, color='gray', linestyle='dashed')
#axRight.axvline(x=202201, color='gray', linestyle='dashed')
#axRight.axvline(x=202716, color='gray', linestyle='dashed')
#axRight.axvline(x=202784, color='gray', linestyle='dashed')
#axRight.axvline(x=203483, color='gray', linestyle='dashed')
#axRight.axvline(x=203551, color='gray', linestyle='dashed')

#line, = axRight.plot(dt*arange(0,N,N/5), -40.*np.ones(5,float),'r--')
axRight.axhline(y=-40.,color='r', linestyle='dashed', linewidth=0.75)

line, = axRight.plot(dt*arange(0,N,1.), plotData[0:N,0],'b-', linewidth=0.75)

axRight.plot(201222, 27.976, 'o', color='r', ms=4)
axRight.plot(201291, -51.0472, 'o', color='r', ms=4)
axRight.plot(201647, 27.976, 'o', color='r', ms=4)
axRight.plot(201716, -51.0472, 'o', color='r', ms=4)
#axRight.plot(202133, 27.976, 'o', color='r', ms=4)
#axRight.plot(202201, -51.0472, 'o', color='r', ms=4)
#axRight.plot(202716, 27.976, 'o', color='r', ms=4)
#axRight.plot(202784, -51.0472, 'o', color='r', ms=4)
#axRight.plot(203483, 27.976, 'o', color='r', ms=4)
#axRight.plot(203551, -51.0472, 'o', color='r', ms=4)

axRight.text(1., 1., chr(1 + ord('a')), ha='right', va='top', bbox=bbox_props_index,
                   transform=axRight.transAxes, fontsize=largeFontSize)

axRight.annotate(r'$\boldsymbol{d}$', xy=(201170, 27.976), xytext=(201170, 27.976), bbox=bbox_props,
                color='r')
axRight.annotate(r'$\boldsymbol{b}$', xy=(201321, -57), xytext=(201321, -57), bbox=bbox_props,
                color='r')
axRight.annotate(r'$\boldsymbol{d}$', xy=(201598, 27.976), xytext=(201598, 27.976), bbox=bbox_props,
                color='r')
axRight.annotate(r'$\boldsymbol{b}$', xy=(201746, -57), xytext=(201746, -57), bbox=bbox_props,
                color='r')

axRight.annotate(r'$\boldsymbol{B}$', xy=(201110, 38), xytext=(201110, 38), bbox=bbox_props,
                color='gray')
axRight.annotate(r'$\boldsymbol{A}$', xy=(201240, 38), xytext=(201240, 38), bbox=bbox_props,
                color='gray')
axRight.annotate(r'$\boldsymbol{B}$', xy=(201460, 38), xytext=(201460, 38), bbox=bbox_props,
                color='gray')
axRight.annotate(r'$\boldsymbol{A}$', xy=(201660, 38), xytext=(201660, 38), bbox=bbox_props,
                color='gray')
axRight.annotate(r'$\boldsymbol{B}$', xy=(201780, 38), xytext=(201780, 38), bbox=bbox_props,
                color='gray')

#Whole image
xlabelX=0.3; xlabel1X = 0.76; xlabelY=0.002; ylabelX=0.07; ylabelY=0.5;
xlabel = r'$\mathrm{time}$';
ylabel = r'$\mathrm{V}$'
fig.text(xlabelX, xlabelY, xlabel, ha='center', bbox=bbox_props_fig, color='darkslategray', fontsize=largeFontSize,
         weight='roman')
fig.text(xlabel1X, xlabelY, xlabel, ha='center', bbox=bbox_props_fig, color='darkslategray', fontsize=largeFontSize,
         weight='roman')
fig.text(ylabelX, ylabelY, ylabel, va='center', rotation='vertical', bbox=bbox_props_fig, color='darkslategray',
         fontsize=largeFontSize, weight='roman')

plt.subplots_adjust(left=0.1, right=0.999, top=0.999, bottom=0.1)

fig.savefig(outputdir+'Image0.jpg', bbox_inches='tight')

pickleDataFile1.close()

############################# End Image0: Plant trajectory ####################################################################
"""


"""
############################# Image0: Plant trajectory: Updated after book chapter ####################################################################

libPlantTrajectory = ct.cdll.LoadLibrary('./plantTrajectory.so')

N_EQ=6
libPlantTrajectory.integrator_rk4.argtypes = [ct.POINTER(ct.c_double), ct.POINTER(ct.c_double),ct.POINTER(ct.c_double),
                          ct.c_double,ct.c_uint,ct.c_uint]
def integrator_rk4 (y_initial = np.asarray([-39.99, 0.9, 0.5, 0.2, 0.7, 0.2]),
                    dt=0.001,
                    N=30000,
                    stride = 1
                ):
    output = np.zeros(N*N_EQ, float)
    params=np.asarray([100, 40, 0., 0., -55, 0.003]) #	shift_Ca2, shift_x2, Iapp, gh, Vhh, gleak;
    params = np.asarray([-8., -1.97, 0., 0., -55., 0.003], float)  # shift_Ca2, shift_x2, Iapp, gh, Vhh, gleak; #chaotic second loop
    params = np.asarray([-8.61873, -1.94959, 0., 0., -55., 0.003], float)  # shift_Ca2, shift_x2, Iapp, gh, Vhh, gleak; #chaotic second loop
    libPlantTrajectory.integrator_rk4(y_initial.ctypes.data_as(ct.POINTER(ct.c_double)), output.ctypes.data_as(ct.POINTER(ct.c_double)), params.ctypes.data_as(ct.POINTER(ct.c_double)),
                       ct.c_double(dt), ct.c_uint(N), ct.c_uint(stride))
    #imshow takes fortran style array input
    return np.reshape(output, (N,N_EQ), 'C')

print 'Generating Image0'

if not dataSaved :
    pickleDataFile1 = gzip.open(outputdir+'Image0.gz','wb')
else :
    pickleDataFile1 = gzip.open(outputdir+'Image0.gz','rb')

matplotlib.rcParams['savefig.dpi'] = 600

figSizeX=6; figSizeY=2; gridX = 1 ; gridY = 2;

fig = plt.figure(figsize=(figSizeX, figSizeY))
gs1 = gridspec.GridSpec(gridX, gridY)
gs1.update(wspace=0.03, hspace=0.03)  # set the spacing between axes.

axLeft = plt.subplot(gs1[0,0])
axRight = plt.subplot(gs1[0,1])

N=430000; dt=1


### Chaotic trajectory
plt.axis('on')
axLeft.set_xticks([])
axLeft.set_yticks([])
#axLeft.text( 0., -0.02, VK2ShiftMin, ha='left', va='top', bbox=bbox_props_label, transform=axLeft.transAxes, fontsize=defaultFontSize)
#axLeft.text( 1., -0.02, VK2ShiftMax, ha='right', va='top', bbox=bbox_props_label, transform=axLeft.transAxes, fontsize=defaultFontSize)
#axLeft.text(-0.02, 0, IappMin, ha='right', va='bottom', rotation=90, bbox=bbox_props_label, transform=axLeft.transAxes, fontsize=defaultFontSize)
#axLeft.text(-0.02, 1., IappMax, ha='right', va='top', rotation=90, bbox=bbox_props_label, transform=axLeft.transAxes, fontsize=defaultFontSize)

if not dataSaved:
    plotData = integrator_rk4(N=N, dt=dt)
    pickle.dump(plotData, pickleDataFile1)
else:
    plotData = pickle.load(pickleDataFile1)

#axLeft.set_xlim(0, dt*N)
axLeft.set_xlim(310000, 430000)

axLeft.set_ylim(-75, 50)
#axLeft.set_xlabel('time', fontsize=20)
#xLeft.set_ylabel('V', fontsize=20)

axLeft.axhline(y=-57.,color='r', linestyle='dashed', linewidth=0.75)
axLeft.axhline(y=-40.,color='r', linestyle='dashed', linewidth=0.75)
axLeft.axhline(y=10.,color='r', linestyle='dashed', linewidth=0.75)

line, = axLeft.plot(dt*arange(0,N,1.), plotData[0:N,0],'b-', linewidth=0.75)

axLeft.autoscale(False)
axLeft.set_adjustable('box-forced')

axLeft.text(1., 1., chr(0 + ord('a')), ha='right', va='top', bbox=bbox_props_index,
                   transform=axLeft.transAxes, fontsize=largeFontSize)

axLeft.text( -0.02, 0.14, -60, ha='right', va='center', bbox=bbox_props_label, transform=axLeft.transAxes,
                    fontsize=defaultFontSize)
axLeft.text( -0.02, 0.28, -40, ha='right', va='center', bbox=bbox_props_label, transform=axLeft.transAxes,
                    fontsize=defaultFontSize)
axLeft.text( -0.02, 0.69, 10, ha='right', va='center', bbox=bbox_props_label, transform=axLeft.transAxes,
                    fontsize=defaultFontSize)

line, = axLeft.plot(dt*arange(418000,428000,2000), -63*np.ones(5,float),'b-')
axLeft.annotate(r'$\boldsymbol{20s}$', xy=(418000, -73.), xytext=(418000, -73.), bbox=bbox_props,
                color='b')

axLeft.annotate(r'$\boldsymbol{a}$', xy=(340000,-67.), xytext=(340000,-67.), bbox=bbox_props,
                color='r')
axLeft.annotate(r'$\boldsymbol{b}$', xy=(340000,-48.), xytext=(340000,-48.), bbox=bbox_props,
                color='r')
axLeft.annotate(r'$\boldsymbol{c}$', xy=(340000,-14.), xytext=(340000,-14.), bbox=bbox_props,
                color='r')
axLeft.annotate(r'$\boldsymbol{d}$', xy=(340000, 24.), xytext=(340000, 24.), bbox=bbox_props,
                color='r')



############ Symbols
plt.axis('on')
axRight.set_xticks([])
axRight.set_yticks([])
axRight.set_xlim(201000, 201900)
axRight.set_ylim(-75, 50)
#axRight.set_xlabel('time', fontsize=20)
#axRight.set_ylabel('V', fontsize=20)

axRight.autoscale(False)
axRight.set_adjustable('box-forced')

line, = axRight.plot(dt*arange(201750,201810,12), -63.*np.ones(5,float),'b-')
axRight.annotate(r'$\boldsymbol{60ms}$', xy=(201750, -73.), xytext=(201750, -73.), bbox=bbox_props,
                color='b')

axRight.axvline(x=201222, color='gray', linestyle='dashed', linewidth=0.75)
axRight.axvline(x=201291, color='gray', linestyle='dashed', linewidth=0.75)
axRight.axvline(x=201647, color='gray', linestyle='dashed', linewidth=0.75)
axRight.axvline(x=201716, color='gray', linestyle='dashed', linewidth=0.75)
#axRight.axvline(x=202133, color='gray', linestyle='dashed')
#axRight.axvline(x=202201, color='gray', linestyle='dashed')
#axRight.axvline(x=202716, color='gray', linestyle='dashed')
#axRight.axvline(x=202784, color='gray', linestyle='dashed')
#axRight.axvline(x=203483, color='gray', linestyle='dashed')
#axRight.axvline(x=203551, color='gray', linestyle='dashed')

#line, = axRight.plot(dt*arange(0,N,N/5), -40.*np.ones(5,float),'r--')
axRight.axhline(y=-40.,color='r', linestyle='dashed', linewidth=0.75)

line, = axRight.plot(dt*arange(0,N,1.), plotData[0:N,0],'b-', linewidth=0.75)

axRight.plot(201222, 27.976, 'o', color='r', ms=4)
axRight.plot(201291, -51.0472, 'o', color='r', ms=4)
axRight.plot(201647, 27.976, 'o', color='r', ms=4)
axRight.plot(201716, -51.0472, 'o', color='r', ms=4)
#axRight.plot(202133, 27.976, 'o', color='r', ms=4)
#axRight.plot(202201, -51.0472, 'o', color='r', ms=4)
#axRight.plot(202716, 27.976, 'o', color='r', ms=4)
#axRight.plot(202784, -51.0472, 'o', color='r', ms=4)
#axRight.plot(203483, 27.976, 'o', color='r', ms=4)
#axRight.plot(203551, -51.0472, 'o', color='r', ms=4)

axRight.text(1., 1., chr(1 + ord('a')), ha='right', va='top', bbox=bbox_props_index,
                   transform=axRight.transAxes, fontsize=largeFontSize)

axRight.annotate(r'$\boldsymbol{d}$', xy=(201170, 27.976), xytext=(201170, 27.976), bbox=bbox_props,
                color='r')
axRight.annotate(r'$\boldsymbol{b}$', xy=(201321, -57), xytext=(201321, -57), bbox=bbox_props,
                color='r')
axRight.annotate(r'$\boldsymbol{d}$', xy=(201598, 27.976), xytext=(201598, 27.976), bbox=bbox_props,
                color='r')
axRight.annotate(r'$\boldsymbol{b}$', xy=(201746, -57), xytext=(201746, -57), bbox=bbox_props,
                color='r')

axRight.annotate(r'$\boldsymbol{B}$', xy=(201110, 38), xytext=(201110, 38), bbox=bbox_props,
                color='gray')
axRight.annotate(r'$\boldsymbol{A}$', xy=(201240, 38), xytext=(201240, 38), bbox=bbox_props,
                color='gray')
axRight.annotate(r'$\boldsymbol{B}$', xy=(201460, 38), xytext=(201460, 38), bbox=bbox_props,
                color='gray')
axRight.annotate(r'$\boldsymbol{A}$', xy=(201660, 38), xytext=(201660, 38), bbox=bbox_props,
                color='gray')
axRight.annotate(r'$\boldsymbol{B}$', xy=(201780, 38), xytext=(201780, 38), bbox=bbox_props,
                color='gray')

#Whole image
xlabelX=0.3; xlabel1X = 0.76; xlabelY=0.002; ylabelX=0.07; ylabelY=0.5;
xlabel = r'$\mathrm{time}$';
ylabel = r'$\mathrm{V}$'
fig.text(xlabelX, xlabelY, xlabel, ha='center', bbox=bbox_props_fig, color='darkslategray', fontsize=largeFontSize,
         weight='roman')
fig.text(xlabel1X, xlabelY, xlabel, ha='center', bbox=bbox_props_fig, color='darkslategray', fontsize=largeFontSize,
         weight='roman')
fig.text(ylabelX, ylabelY, ylabel, va='center', rotation='vertical', bbox=bbox_props_fig, color='darkslategray',
         fontsize=largeFontSize, weight='roman')

plt.subplots_adjust(left=0.1, right=0.999, top=0.999, bottom=0.1)

fig.savefig(outputdir+'Image0.jpg', bbox_inches='tight')

pickleDataFile1.close()

############################# End Image0: Plant trajectory : Updated After Book Chapter ####################################################################
"""

############################# Image7: Plant 3D trajectory  : Updated After Book Chapter ####################################################################

libPlantTrajectory = ct.cdll.LoadLibrary('./plantTrajectory.so')

N_EQ=6
libPlantTrajectory.integrator_rk4.argtypes = [ct.POINTER(ct.c_double), ct.POINTER(ct.c_double),ct.POINTER(ct.c_double),
                          ct.c_double,ct.c_uint,ct.c_uint]
def integrator_rk4 (y_initial = np.asarray([-39.99, 0.9, 0.5, 0.2, 0.7, 0.2]),
                    dt=0.001,
                    N=30000,
                    stride = 1
                ):
    output = np.zeros(N*N_EQ, float)
    params = np.asarray([-8.61873, -1.94959, 0., 0., -55., 0.003], float)  # shift_Ca2, shift_x2, Iapp, gh, Vhh, gleak; #chaotic second loop
    libPlantTrajectory.integrator_rk4(y_initial.ctypes.data_as(ct.POINTER(ct.c_double)), output.ctypes.data_as(ct.POINTER(ct.c_double)), params.ctypes.data_as(ct.POINTER(ct.c_double)),
                       ct.c_double(dt), ct.c_uint(N), ct.c_uint(stride))
    #imshow takes fortran style array input
    return np.reshape(output, (N,N_EQ), 'C')

print 'Generating Image7'

if not dataSaved :
    pickleDataFile1 = gzip.open(outputdir+'Image7.gz','wb')
else :
    pickleDataFile1 = gzip.open(outputdir+'Image7.gz','rb')

matplotlib.rcParams['savefig.dpi'] = 600

figSizeX=4; figSizeY=4*2.4/2.85; gridX = 1 ; gridY = 1;
N=30000*30; dt=1


from mpl_toolkits.mplot3d import Axes3D

fig = plt.figure(figsize=(figSizeX, figSizeY))
ax = fig.add_subplot(111, projection='3d')

if not dataSaved:
    plotData = integrator_rk4(N=N, dt=dt)
    pickle.dump(plotData, pickleDataFile1)
else:
    plotData = pickle.load(pickleDataFile1)

ax.plot3D(plotData[int(N/3):N,1], plotData[int(N/3):N,4], plotData[int(N/3):N,0], 'b-', linewidth=0.2) #Ca, x, V
ax.set_xticks([])
ax.set_yticks([])
ax.set_zticks([])

#ax.set_xlabel('Ca', fontsize=largeFontSize)
#ax.set_ylabel('X', fontsize=largeFontSize)
#ax.set_zlabel('V', fontsize=largeFontSize)

fig.text(0.45, 0.17, 'Ca', ha='center', color='darkslategray', fontsize=defaultFontSize, weight='roman')
fig.text(0.75, 0.29, 'X', ha='center', color='darkslategray', fontsize=defaultFontSize, weight='roman')
fig.text(0.765, 0.6, 'V', va='center', color='darkslategray', fontsize=defaultFontSize, weight='roman')

ax.view_init(45,-70)
plt.axis('on')
ax.autoscale(False)
ax.set_adjustable('box-forced')

#ax.set_xlim(310000, 430000)
#ax.set_ylim(-75, 50)

#plt.subplots_adjust(left=0.1, right=0.999, top=0.999, bottom=0.1)
#gca().set_axis_off()

bbox = fig.bbox_inches.from_bounds(0.85, 0.5, 2.4, 2.4) #x0, y0, width, height
fig.savefig(outputdir+'Image7.jpg', bbox_inches=bbox, pad=0.)

pickleDataFile1.close(),

############################# End Image7: Plant 3D trajectory  : Updated After Book Chapter ####################################################################

show()
