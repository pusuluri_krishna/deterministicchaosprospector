#!/bin/bash

nvcc --ptxas-options=-v -arch=sm_30 --compiler-options '-fPIC' -o plant_sweep_PCLZ.so --shared plant_sweep_PCLZ.cu
nvcc --ptxas-options=-v -arch=sm_30 --compiler-options '-fPIC' -o leech_sweep_PCLZ.so --shared leech_sweep_PCLZ.cu
nvcc --ptxas-options=-v -arch=sm_30 --compiler-options '-fPIC' -o HR_sweep_PCLZ.so --shared HR_sweep_PCLZ.cu
nvcc --ptxas-options=-v -arch=sm_30 --compiler-options '-fPIC' -o hairCell_sweep_PCLZ.so --shared hairCell_sweep_PCLZ.cu

gcc -fPIC -shared -o plantTrajectory.so plantTrajectory.c