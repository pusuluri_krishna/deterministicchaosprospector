import matplotlib
matplotlib.use('Agg')
import ctypes as ct
from pylab import *
import numpy as np
from matplotlib import colors
from matplotlib import rc

lib = ct.cdll.LoadLibrary('./pikovskyCuda.so')


N=300000
lib.sweep.argtypes = [ct.POINTER(ct.c_double),
                          ct.c_double,ct.c_double,ct.c_uint,
                          ct.c_double,ct.c_double,ct.c_uint,
                          ct.c_double,ct.c_uint,
                          ct.c_double, ct.c_uint, ct.c_uint,
                          ct.c_uint, ct.c_uint]
def sweepPikovsky ( dt=0.01,
                    N=N,
                    stride = 1,
                    xmin = 3, xmax = 5,
                    ymin = 1, ymax = 7,
                    parameter3 = 1., whichSweep = 2,
                    kneadingsStart = 0,
                    kneadingsEnd = 10,
                    sweepSize = 100
                ):
    kneadingsWeightedSumSet = np.zeros(sweepSize*sweepSize, float)
    lib.sweep(kneadingsWeightedSumSet.ctypes.data_as(ct.POINTER(ct.c_double)),
                  ct.c_double(xmin), ct.c_double(xmax), ct.c_uint(sweepSize),
                  ct.c_double(ymin), ct.c_double(ymax), ct.c_uint(sweepSize),
                  ct.c_double(parameter3), ct.c_uint(whichSweep),
                  ct.c_double(dt), ct.c_uint(N), ct.c_uint(stride),
                  ct.c_uint(kneadingsStart), ct.c_uint(kneadingsEnd))
    #imshow takes fortran style array input
    return np.reshape(kneadingsWeightedSumSet, (sweepSize,sweepSize), 'F')


colorMapLevels = 2**24
blue=np.linspace(0.01,1,colorMapLevels)
red = 1 - blue
#blue = np.random.random(colorMapLevels)
#red = np.random.random(colorMapLevels)
green = np.random.random(colorMapLevels)
RGB = np.column_stack((red, green, blue))
customColorMap = colors.ListedColormap(RGB)

# resolution not good with PdfPages writing to a single file

sweepSize = 5000
figSize = 5
fig = figure(figsize=(figSize,figSize))
outputdir = 'Output/pikovsky_'

matplotlib.rcParams['savefig.dpi'] = 600 #2*sweepSize/figSize #300


#TODO : create a data file and use that to compute different kneading ranges; for sigma=2 too noisy.. only smaller kneadings ranges 0-6,1-7 etc might be better

# nu1 - 0 to 10; nu2 - 0 to 20 ; h - 0 to 40
"""
#Larger exploration
kneadingRangesAndParametersToCompute=[[10, 17, 5., 0, 0., 10., 0., 20.],[10, 17, 15., 0, 0., 10., 0., 20.],[10, 17, 25., 0, 0., 10., 0., 20.],[10, 17, 35., 0, 0., 10., 0., 20.],
                                      [10, 17, 4., 1, 0., 20., 0., 100.],[10, 17, 8., 1, 0., 20., 0., 100.],[10, 17, 12., 1, 0., 20., 0., 100.],[10, 17, 16., 1, 0., 20., 0., 100.],
                                      [10, 17, 1., 2, 0, 20., 0., 40.],[10, 17 , 3., 2, 0, 20., 0., 40.],[10, 17, 5., 2, 0, 20., 0., 40.],[10, 17, 7., 2, 0, 20., 0., 40.]]

#Larger exploration Kneading Evolution
kneadingRangesAndParametersToCompute=[
                                        [0,1, 5., 0, 0., 10., 0., 20.],[0,2, 5., 0, 0., 10., 0., 20.],[0,3, 5., 0, 0., 10., 0., 20.],[0,4, 5., 0, 0., 10., 0., 20.],[0,5, 5., 0, 0., 10., 0., 20.],[0,6, 5., 0, 0., 10., 0., 20.],[0,7, 5., 0, 0., 10., 0., 20.],
                                        [0, 1, 15., 0, 0., 10., 0., 20.],[0, 2, 15., 0, 0., 10., 0., 20.],[0, 3, 15., 0, 0., 10., 0., 20.],[0, 4, 15., 0, 0., 10., 0., 20.],[0, 5, 15., 0, 0., 10., 0., 20.],[0, 6, 15., 0, 0., 10., 0., 20.],[0, 7, 15., 0, 0., 10., 0., 20.],
                                        [0, 1, 25., 0, 0., 10., 0., 20.],[0, 2, 25., 0, 0., 10., 0., 20.],[0, 3, 25., 0, 0., 10., 0., 20.],[0, 4, 25., 0, 0., 10., 0., 20.],[0, 5, 25., 0, 0., 10., 0., 20.],[0, 6, 25., 0, 0., 10., 0., 20.],[0, 7, 25., 0, 0., 10., 0., 20.],
                                        [0, 1, 35., 0, 0., 10., 0., 20.],[0, 2, 35., 0, 0., 10., 0., 20.],[0, 3, 35., 0, 0., 10., 0., 20.],[0, 4, 35., 0, 0., 10., 0., 20.],[0, 5, 35., 0, 0., 10., 0., 20.],[0, 6, 35., 0, 0., 10., 0., 20.],[0, 7, 35., 0, 0., 10., 0., 20.],
                                        [0, 1, 4., 1, 0., 20., 0., 100.],[0, 2, 4., 1, 0., 20., 0., 100.],[0, 3, 4., 1, 0., 20., 0., 100.],[0, 4, 4., 1, 0., 20., 0., 100.],[0, 5, 4., 1, 0., 20., 0., 100.],[0, 6, 4., 1, 0., 20., 0., 100.],[0, 7, 4., 1, 0., 20., 0., 100.],
                                      [0, 1, 8., 1, 0., 20., 0., 100.],[0, 2, 8., 1, 0., 20., 0., 100.],[0, 3, 8., 1, 0., 20., 0., 100.],[0, 4, 8., 1, 0., 20., 0., 100.],[0, 5, 8., 1, 0., 20., 0., 100.],[0, 6, 8., 1, 0., 20., 0., 100.],[0, 7, 8., 1, 0., 20., 0., 100.],
                                      [0, 1, 12., 1, 0., 20., 0., 100.],[0, 2, 12., 1, 0., 20., 0., 100.],[0, 3, 12., 1, 0., 20., 0., 100.],[0, 4, 12., 1, 0., 20., 0., 100.],[0, 5, 12., 1, 0., 20., 0., 100.],[0, 6, 12., 1, 0., 20., 0., 100.],[0, 7, 12., 1, 0., 20., 0., 100.],
                                      [0, 1, 16., 1, 0., 20., 0., 100.],[0, 2, 16., 1, 0., 20., 0., 100.],[0, 3, 16., 1, 0., 20., 0., 100.],[0, 4, 16., 1, 0., 20., 0., 100.],[0, 5, 16., 1, 0., 20., 0., 100.],[0, 6, 16., 1, 0., 20., 0., 100.],[0, 7, 16., 1, 0., 20., 0., 100.],
                                      [0, 1, 1., 2, 0, 20., 0., 40.],[0, 2, 1., 2, 0, 20., 0., 40.],[0, 3, 1., 2, 0, 20., 0., 40.],[0, 4, 1., 2, 0, 20., 0., 40.],[0, 5, 1., 2, 0, 20., 0., 40.],[0, 6, 1., 2, 0, 20., 0., 40.],[0, 7, 1., 2, 0, 20., 0., 40.],
                                      [0, 1 , 3., 2, 0, 20., 0., 40.],[0, 2 , 3., 2, 0, 20., 0., 40.],[0, 3 , 3., 2, 0, 20., 0., 40.],[0, 4 , 3., 2, 0, 20., 0., 40.],[0, 5 , 3., 2, 0, 20., 0., 40.],[0, 6 , 3., 2, 0, 20., 0., 40.],[0, 7 , 3., 2, 0, 20., 0., 40.],
                                      [0,1, 5., 2, 0, 20., 0., 40.],[0,2, 5., 2, 0, 20., 0., 40.],[0,3, 5., 2, 0, 20., 0., 40.],[0,4, 5., 2, 0, 20., 0., 40.],[0,5, 5., 2, 0, 20., 0., 40.],[0,6, 5., 2, 0, 20., 0., 40.],[0,7, 5., 2, 0, 20., 0., 40.],
                                      [0, 1, 7., 2, 0, 20., 0., 40.],[0, 2, 7., 2, 0, 20., 0., 40.],[0, 3, 7., 2, 0, 20., 0., 40.],[0, 4, 7., 2, 0, 20., 0., 40.],[0, 5, 7., 2, 0, 20., 0., 40.],[0, 6, 7., 2, 0, 20., 0., 40.],[0, 7, 7., 2, 0, 20., 0., 40.]
                                      ]
"""

#To use for paper
kneadingRangesAndParametersToCompute=[[0, 2, 1., 2, 2, 10., 7., 12.],[0, 3, 1., 2, 2, 10., 7., 12.],[0, 4, 1., 2, 2, 10., 7., 12.],[0, 5, 1., 2, 2, 10., 7., 12.],[0, 6, 1., 2, 2, 10., 7., 12.],[0, 7, 1., 2, 2, 10., 7., 12.],[1, 8, 1., 2, 2, 10., 7., 12.],[2, 9, 1., 2, 2, 10., 7., 12.],[3, 10, 1., 2, 2, 10., 7., 12.],[4, 11, 1., 2, 2, 10., 7., 12.],[104, 127, 1., 2, 2, 10., 7., 12.],
[4, 11, 1., 2, 0, 20., 0., 40.],[4, 11, 3., 2, 0, 20., 0., 40.],[4, 11, 5., 2, 0, 20., 0., 40.],
[104, 127, 1., 2, 0, 20., 0., 40.],
[4, 11, 15., 0, 0, 10., 0., 20.],[4, 11, 25., 0, 0, 10., 0., 20.],[4, 11, 35., 0, 0, 10., 0., 20.],
[104, 127, 35., 0, 0, 10., 0., 20.] ]

for kneadingRangesAndParameters in kneadingRangesAndParametersToCompute :
    fig.clear()
    xmin=kneadingRangesAndParameters[4]
    xmax=kneadingRangesAndParameters[5]
    ymin=kneadingRangesAndParameters[6]
    ymax=kneadingRangesAndParameters[7]
    whichSweep=kneadingRangesAndParameters[3]
    imshow(sweepPikovsky(kneadingsStart=kneadingRangesAndParameters[0], kneadingsEnd=kneadingRangesAndParameters[1], sweepSize=sweepSize, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax, parameter3=kneadingRangesAndParameters[2], whichSweep=whichSweep),
           extent=[xmin, xmax, ymin, ymax], aspect=(xmax - xmin)/ (ymax-ymin),
           cmap=customColorMap,
           origin='lower')
    #colorbar(ticks=np.arange(-0.2,1,0.1))
    parameterString=""
    if whichSweep==0:
        parameterStr = "nu1_%0.2f-%0.2f___nu2_%0.2f-%0.2f___h_%0.2f" % (xmin, xmax, ymin, ymax, kneadingRangesAndParameters[2]);
	xlabel(r'$ \nu_1 $', fontsize='xx-large');ylabel(r'$ \nu_2 }$', fontsize='xx-large');
    elif whichSweep==1:
        parameterStr = "nu1_%0.2f-%0.2f___h_%0.2f-%0.2f___nu2_%0.2f" % (xmin, xmax, ymin, ymax, kneadingRangesAndParameters[2]);
	xlabel(r'$ \nu_1 $', fontsize='xx-large');ylabel('h', fontsize='xx-large');
    elif whichSweep==2:
        parameterStr = "nu2_%0.2f-%0.2f___h_%0.2f-%0.2f___nu1_%0.2f" % (xmin, xmax, ymin, ymax, kneadingRangesAndParameters[2]);
	xlabel(r'$ \nu_2 $', fontsize='xx-large');ylabel('h', fontsize='xx-large');

    kneadingRangeStr =  "%0.2f-%0.2f" % (kneadingRangesAndParameters[0], kneadingRangesAndParameters[1]);
    #fig.suptitle(parameterStr + "\nKneading Range : " + kneadingRangeStr, fontsize=2*figSize)

    fig.savefig(outputdir+parameterStr+'_kneadingRange-'+kneadingRangeStr+"_sweepSize-"+str(sweepSize)+".jpg")
    #fig.savefig(outputdir+parameterStr+'_kneadingRange-'+kneadingRangeStr+"_sweepSize-"+str(sweepSize)+".pdf")





