
clf
%set(gcf, 'PaperPositionMode','auto','color', 'white');
%set(gcf,'PaperPosition',[1.5 3 5 4])

% 
%  hFig=axes('Position',[0.2 0.4 .6 .5],'Visible','off','Color',[.9 .9 .9],...
%          'FontName','times',...
%          'FontSize',8,...
%          'XColor',[0 0 .0],...
%          'YColor',[0 0 .0],...
%          'ZColor',[0 0 .0]);

% text(-0.08,0, 1., '(A)','FontSize',11);
% text(-0.08,0, -0.1, '(B)','FontSize',11);


nu1=1.6;
nu2=3.6;
h=6.8;

nu1=1
nu2=2
h=10;

 
 J=[-nu1 h 0; h -nu2 0; 0 0 -1];
 [a,b]=eig(J);


tk=5;
flor = @(t,s) [ -nu1*s(1)+h*s(2)-s(2).*s(3); h*s(1)-nu2*s(2)+s(1).*s(3); -s(3)+s(1).*s(2)];
options = odeset('RelTol',1e-5,'AbsTol',1e-5);
[t1,y1,tau1]=ode45(flor,[0:0.01:tk],[0.01,0.,0.]);

figure(1)
plot3(y1(:,1), y1(:,2), y1(:,3),'Color','b')
hold on

% 
% flor = @(t,s)[ -nu1*s(1)+h*s(2)-s(2).*s(3); h*s(1)-nu2*s(2)+s(1).*s(3); -s(3)+s(1).*s(2)];
% options = odeset('RelTol',1e-5,'AbsTol',1e-5);
% [t2,y2,tau2]=ode45(flor,[0:0.001:tk],[0.01,0.,.1]);
% 
% plot3(y2(:,1), y2(:,2), y2(:,3),'Color','green')
% hold on


% %plot3( [y1(i+ish,1),y2(i,1)], [y1(i+ish,2),y2(i,2)], [y1(i+ish,3),y2(i,3)],'Color','red')
% factor=30;
% jsp=1;
% len1=length(y1(:,1))-400;
% len2=length(y2(:,1));
%  for i=200:1:len1
%      distmin=10^6;
%      for j=1:len2;
%      if ( y1(i,1)-y2(j,1))^2 + (y1(i,2)-y2(j,2))^2 + (y1(i,3)-y2(j,3))^2 <= distmin ;
%          jsp=j;
%      distmin=(y1(i,1)-y2(jsp,1))^2+(y1(i,2)-y2(jsp,2))^2+(y1(i,3)-y2(jsp,3))^2;
%       end 
%      end
%  plot3( [y1(i,1),y1(i,1)+(y2(jsp,1)-y1(i,1))*factor], [y1(i,2),y1(i,2)+(y2(jsp,2)-y1(i,2))*factor], [y1(i,3),y1(i,3)+(y2(jsp,3)-y1(i,3))*factor],'Color','blue')
%  hold on
%  plot3( [y1(i,1),y2(jsp,1)], [y1(i,2),y2(jsp,2)],[y1(i,3),y2(jsp,3)],'Color','red')
%  hold on
%    
%  end

 
axis tight
axis on
%axis([-1.5 2 -1.5 2 -0 2.5])


% xlabel('x','FontSize',12);
%  ylabel('y','FontSize',12);
%  zlabel('z','FontSize',12);
 view(54,2)

% 
%print -depsc2 -r600 lorenz_run1.eps 
%print('-dpdf','-r600','lorenz_run3.pdf');
%print -djpeg -r600 lor.jpg
%print -dpng -r600 lor.png




