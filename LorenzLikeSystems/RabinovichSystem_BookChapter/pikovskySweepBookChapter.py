import matplotlib
matplotlib.use('Agg')
import ctypes as ct
from pylab import *
import numpy as np
from matplotlib import colors
from matplotlib import rc
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib.ticker import MaxNLocator

lib = ct.cdll.LoadLibrary('./pikovskyCuda.so')
lib1 = ct.cdll.LoadLibrary('./pikovskyCudaPeriodicKneadings.so')


N=300000
lib.sweep.argtypes = [ct.POINTER(ct.c_double),
                          ct.c_double,ct.c_double,ct.c_uint,
                          ct.c_double,ct.c_double,ct.c_uint,
                          ct.c_double,ct.c_uint,
                          ct.c_double, ct.c_uint, ct.c_uint,
                          ct.c_uint, ct.c_uint]
def sweepPikovsky ( dt=0.01,
                    N=N,
                    stride = 1,
                    xmin = 3, xmax = 5,
                    ymin = 1, ymax = 7,
                    parameter3 = 1., whichSweep = 2,
                    kneadingsStart = 0,
                    kneadingsEnd = 10,
                    sweepSize = 100
                ):
    kneadingsWeightedSumSet = np.zeros(sweepSize*sweepSize, float)
    lib.sweep(kneadingsWeightedSumSet.ctypes.data_as(ct.POINTER(ct.c_double)),
                  ct.c_double(xmin), ct.c_double(xmax), ct.c_uint(sweepSize),
                  ct.c_double(ymin), ct.c_double(ymax), ct.c_uint(sweepSize),
                  ct.c_double(parameter3), ct.c_uint(whichSweep),
                  ct.c_double(dt), ct.c_uint(N), ct.c_uint(stride),
                  ct.c_uint(kneadingsStart), ct.c_uint(kneadingsEnd))
    #imshow takes fortran style array input
    return np.reshape(kneadingsWeightedSumSet, (sweepSize,sweepSize), 'F')

    lib.sweep.argtypes = [ct.POINTER(ct.c_double),
                          ct.c_double, ct.c_double, ct.c_uint,
                          ct.c_double, ct.c_double, ct.c_uint,
                          ct.c_double, ct.c_uint,
                          ct.c_double, ct.c_uint, ct.c_uint,
                          ct.c_uint, ct.c_uint]

def sweepPikovsky1(dt=0.01,
                  N=N,
                  stride=1,
                  xmin=3, xmax=5,
                  ymin=1, ymax=7,
                  parameter3=1., whichSweep=2,
                  kneadingsStart=0,
                  kneadingsEnd=10,
                  sweepSize=100
                  ):
    kneadingsWeightedSumSet = np.zeros(sweepSize * sweepSize, float)
    lib1.sweep(kneadingsWeightedSumSet.ctypes.data_as(ct.POINTER(ct.c_double)),
              ct.c_double(xmin), ct.c_double(xmax), ct.c_uint(sweepSize),
              ct.c_double(ymin), ct.c_double(ymax), ct.c_uint(sweepSize),
              ct.c_double(parameter3), ct.c_uint(whichSweep),
              ct.c_double(dt), ct.c_uint(N), ct.c_uint(stride),
              ct.c_uint(kneadingsStart), ct.c_uint(kneadingsEnd))
    # imshow takes fortran style array input
    return np.reshape(kneadingsWeightedSumSet, (sweepSize, sweepSize), 'F')


colorMapLevels = 2**24
blue=np.linspace(0.01,1,colorMapLevels)
red = 1 - blue
green = np.random.random(colorMapLevels)
RGB = np.column_stack((red, green, blue))
customColorMap = colors.ListedColormap(RGB)

# resolution not good with PdfPages writing to a single file

sweepSize = 5000

#http://stackoverflow.com/questions/20057260/how-to-remove-gaps-between-subplots-in-matplotlib
#http://matplotlib.org/examples/pylab_examples/subplots_demo.html
#http://stackoverflow.com/questions/18266642/multiple-imshow-subplots-each-with-colorbar
#http://matplotlib.org/users/gridspec.html
#http://stackoverflow.com/questions/22511550/gridspec-with-shared-axes-in-python
#http://stackoverflow.com/questions/15773049/remove-overlapping-tick-marks-on-subplot-in-matplotlib
#http://matplotlib.org/users/annotations_guide.html#plotting-guide-annotation

outputdir = 'Output/pikovsky_'

matplotlib.rcParams['savefig.dpi'] = 1200 #2*sweepSize/figSize #300
defaultFontSize = 8; largeFontSize = 11
matplotlib.rcParams.update({'font.size': largeFontSize,
              'axes.labelsize': largeFontSize,
              'axes.titlesize': largeFontSize,
              'text.fontsize': defaultFontSize,
              'legend.fontsize': largeFontSize,
              'xtick.labelsize': defaultFontSize,
              'ytick.labelsize': defaultFontSize})

def plotFigureWithMultipleSubplots(kneadingRangesAndParametersToCompute, gridX, gridY, filename, figSizeX=6, figSizeY=6, xlabelX=0.5,xlabelY=0.07, ylabelX=0.07, ylabelY=0.5):
	fig = plt.figure(figsize=(figSizeX, figSizeY))
	gs1 = gridspec.GridSpec(gridX, gridY)
	gs1.update(wspace=0.03, hspace=0.03)  # set the spacing between axes.

	i = 0
	for kneadingRangesAndParameters in kneadingRangesAndParametersToCompute:
		# fig.clear()
		xmin = kneadingRangesAndParameters[4]
		xmax = kneadingRangesAndParameters[5]
		ymin = kneadingRangesAndParameters[6]
		ymax = kneadingRangesAndParameters[7]
		whichSweep = kneadingRangesAndParameters[3]

		ax = plt.subplot(gs1[i])
		plt.axis('on')

		numberOfTicks = 5
		xStep = round((xmax - xmin) / numberOfTicks)
		yStep = round((ymax - ymin) / numberOfTicks)

		ax.set_xticks(arange(xmin, xmax + xStep, xStep))
		ax.set_yticks(arange(ymin, ymax + yStep, yStep))

		if (kneadingRangesAndParameters[8] == 0):
		    sweepData = sweepPikovsky(kneadingsStart=kneadingRangesAndParameters[0],
				              kneadingsEnd=kneadingRangesAndParameters[1], sweepSize=sweepSize, xmin=xmin,
				              xmax=xmax, ymin=ymin, ymax=ymax, parameter3=kneadingRangesAndParameters[2],
				              whichSweep=whichSweep)
		elif (kneadingRangesAndParameters[8] == 1):
		    sweepData = sweepPikovsky1(kneadingsStart=kneadingRangesAndParameters[0],
				               kneadingsEnd=kneadingRangesAndParameters[1], sweepSize=sweepSize, xmin=xmin,
				               xmax=xmax, ymin=ymin, ymax=ymax, parameter3=kneadingRangesAndParameters[2],
				               whichSweep=whichSweep)
		ax.imshow(sweepData,
			  extent=[xmin, xmax, ymin, ymax],
			  aspect=(xmax - xmin) / (ymax - ymin),
			  cmap=customColorMap,
			  origin='lower')
		ax.autoscale(False)
		ax.set_adjustable('box-forced')

		if (i % gridY != 0):
		    plt.setp(ax.get_yticklabels(), visible=False)
		if (i / gridY < gridX - 1):
		    plt.setp(ax.get_xticklabels(), visible=False)

		if (i % gridY < gridY - 1):
		    nbins = len(ax.get_xticklabels())
		    ax.xaxis.set_major_locator(MaxNLocator(nbins=nbins, prune='upper'))
		if (i / gridY > 0):
		    nbins = len(ax.get_yticklabels())
		    ax.yaxis.set_major_locator(MaxNLocator(nbins=nbins, prune='upper'))

		bbox_props = dict(boxstyle="square,pad=0.2", fc="white", ec="k", lw=.5	)
		if len(kneadingRangesAndParametersToCompute)>1:
			ax.text(1., 1., chr(i + ord('a')), ha='right', va='top', bbox=bbox_props, transform=ax.transAxes, fontsize=largeFontSize)

		bbox_props = dict(boxstyle="square,pad=0.2", fc="white", ec="k", lw=.5)
		arrowprops = dict(arrowstyle="-",connectionstyle="arc3", facecolor='white', lw=.5, ec="white")#,shrink=0.05, width=defaultFontSize/10.)

		#For the TPoint figure, annotate the primary and secondary T points here itself
		if kneadingRangesAndParameters==[4, 11, 1., 2, 2, 10., 7., 12., 0]:

			ax.plot(4.45173,9.80738,'o',color='white',ms=3)
			ax.annotate('T1', xy=(4.45173, 9.80738), xytext=(5.5, 9.80738), arrowprops=arrowprops, bbox=bbox_props)
	
			ax.plot(6.16393, 11.4923,'o',color='white',ms=3)
			ax.annotate('T2', xy=(6.16393, 11.4923), xytext=(5, 11.4923), arrowprops=arrowprops, bbox=bbox_props)
			ax.plot(6.06375,11.3215,'o',color='white',ms=3)
			ax.annotate('T2\'', xy=(6.06375,11.3215), xytext=(7, 11.3215), arrowprops=arrowprops, bbox=bbox_props)

			ax.plot(5.11658,8.78848,'o',color='white',ms=3)
			ax.annotate('T3', xy=(5.11658,8.78848), xytext=(4,8.78848), arrowprops=arrowprops, bbox=bbox_props)
			ax.plot(5.12568,8.72587,'o',color='white',ms=3)
			ax.annotate('T3\'', xy=(5.12568,8.72587), xytext=(6, 8.72587), arrowprops=arrowprops, bbox=bbox_props)

			ax.plot(4.75228,7.93465,'o',color='white',ms=3)
			ax.annotate('T4', xy=(4.75228,7.93465), xytext=(3.5, 7.93465), arrowprops=arrowprops, bbox=bbox_props)
			ax.plot(4.7796,7.88342,'o',color='white',ms=3)
			ax.annotate('T4\'', xy=(4.7796,7.88342), xytext=(6, 7.88342), arrowprops=arrowprops, bbox=bbox_props)

			# nu1 = 1; 			nu2 = 2.31148; 			h = 8.72017;			tk = 5; % Saddle			near			T		point
			ax.plot(2.31148, 8.72017, 'o', color='white', ms=3)
			ax.annotate('S', xy=(2.31148, 8.72017), xytext=(2.31148, 8.), arrowprops=arrowprops, bbox=bbox_props)

		#For nu2 vs h plots, mark all the T-point like structures
		if kneadingRangesAndParameters[2:]==[1., 2, 0, 20., 0., 40., 0]:
			### T1 - 4.45838,9.79438; T2 - 4.90973,14.007; T3 - 5.28586, 18.4203; T4 - 5.51153, 22.683; T5 - 5.71214, 27.0461; T6 - 5.83751, 31.5095; T7 - 5.93781, 36.1234

			ax.plot(4.45838,9.79438,'o',color='white',ms=3)
			ax.annotate('T1', xy=(4.45838,9.79438), xytext=(0.5,9.79438), arrowprops=arrowprops, bbox=bbox_props)

			ax.plot(4.90973,14.007,'o',color='white',ms=3)
			ax.annotate('T2', xy=(4.90973,14.007), xytext=(0.5,14.007), arrowprops=arrowprops, bbox=bbox_props)

			ax.plot(5.28586, 18.4203,'o',color='white',ms=3)
			ax.annotate('T3', xy=(5.28586, 18.4203), xytext=(0.5, 18.4203), arrowprops=arrowprops, bbox=bbox_props)

			ax.plot(5.51153, 22.683,'o',color='white',ms=3)
			ax.annotate('T4', xy=(5.51153, 22.683), xytext=(0.5, 22.683), arrowprops=arrowprops, bbox=bbox_props)

			ax.plot(5.71214, 27.0461,'o',color='white',ms=3)
			ax.annotate('T5', xy=(5.71214, 27.0461), xytext=(0.5, 27.0461), arrowprops=arrowprops, bbox=bbox_props)

			ax.plot(5.83751+0.2, 31.5095,'o',color='white',ms=3)
			ax.annotate('T6', xy=(5.83751+0.2, 31.5095), xytext=(0.5, 31.5095), arrowprops=arrowprops, bbox=bbox_props)

			ax.plot(5.93781+0.3, 36.1234+0.1,'o',color='white',ms=3)
			ax.annotate('T7', xy=(5.93781+0.3, 36.1234+.1), xytext=(0.5, 36.1234), arrowprops=arrowprops, bbox=bbox_props)

		"""
		if whichSweep==0:
			xlabel = r'$\longrightarrow \nu_1$'; ylabel = r'$\longrightarrow \nu_2$'

		elif whichSweep==1:
			xlabel = r'$\longrightarrow \nu_1$'; ylabel = r'$\longrightarrow h$'

		elif whichSweep==2:
			xlabel = r'$\longrightarrow \nu_2$'; ylabel = r'$\longrightarrow h$'
	    
		if i%gridY==0:
			ax.text(0.02, 0.8, ylabel, ha='left', va='center', rotation='vertical', transform=ax.transAxes, bbox=bbox_props)
		if i/gridY==gridX-1:
			ax.text(0.8, 0.02, xlabel, ha='center', va='bottom', transform=ax.transAxes, bbox=bbox_props)
		"""

		i = i + 1

	#http://matplotlib.org/users/mathtext.html

	if whichSweep == 0:
		xlabel = r'$ \mathrm{\nu_1}$ - parameter ';
		ylabel = r'$ \mathrm{\nu_2}$ - parameter '

	elif whichSweep == 1:
		xlabel = r'$ \mathrm{\nu_1}$ - parameter ';
		ylabel = r'$ \mathrm{h}$ - parameter '

	elif whichSweep == 2:
		xlabel = r'$ \mathrm{\nu_2}$ - parameter ';
		ylabel = r'$ \mathrm{h}$ - parameter '

	bbox_props = dict(boxstyle="square,pad=0.2", fc="white", ec="k", lw=0)
	fig.text(xlabelX, xlabelY, xlabel, ha='center', bbox=bbox_props,color='darkslategray', fontsize=largeFontSize, weight='roman')
	fig.text(ylabelX, ylabelY, ylabel, va='center', rotation='vertical',bbox=bbox_props,color='darkslategray', fontsize=largeFontSize, weight='roman')

	plt.subplots_adjust(left=0.1, right=0.9, top=0.9, bottom=0.1)
	fig.savefig(filename, bbox_inches='tight')
	return

# Chaos evolution
kneadingRangesAndParametersToCompute=[[0, 2, 1., 2, 2, 10., 7., 12., 0],[0, 3, 1., 2, 2, 10., 7., 12., 0],[0, 4, 1., 2, 2, 10., 7., 12., 0],[0, 5, 1., 2, 2, 10., 7., 12., 0],[0, 7, 1., 2, 2, 10., 7., 12., 0],[104, 127, 1., 2, 2, 10., 7., 12., 0]]
gridX=2; gridY=3
filename = outputdir+'chaosEvolution.jpg'
plotFigureWithMultipleSubplots(kneadingRangesAndParametersToCompute, gridX, gridY, filename, figSizeY=4, xlabelX=0.49,xlabelY=0.058,ylabelX=0.065,ylabelY=0.5)

# T point
kneadingRangesAndParametersToCompute=[[4, 11, 1., 2, 2, 10., 7., 12., 0]]
gridX=gridY=1
filename = outputdir+'TPoint.jpg'
#annotate the primary and secondary T points here itself
plotFigureWithMultipleSubplots(kneadingRangesAndParametersToCompute, gridX, gridY, filename, xlabelX=0.5,xlabelY=0.07,ylabelX=0.065,ylabelY=0.55)

# global - nu2 vs h big		### T1 - 4.45838,9.79438; T2 - 4.90973,14.007; T3 - 5.28586, 18.4203; T4 - 5.51153, 22.683; T5 - 5.71214, 27.0461; T6 - 5.83751, 31.5095; T7 - 5.93781, 36.1234
kneadingRangesAndParametersToCompute=[[4, 11, 1., 2, 0, 20., 0., 40., 0]]
gridX=gridY=1
filename = outputdir+'global_nu2h_1big.jpg'
plotFigureWithMultipleSubplots(kneadingRangesAndParametersToCompute, gridX, gridY, filename, xlabelX=0.5,xlabelY=0.07,ylabelX=0.065,ylabelY=0.55)

# global - nu2 vs h long range chaos
kneadingRangesAndParametersToCompute=[[104, 127, 1., 2, 0, 20., 0., 40., 0], [104, 127, 1., 2, 0, 20., 0., 40., 1]]
#					[4, 11, 3., 2, 0, 20., 0., 40., 0],[104, 127, 3., 2, 0, 20., 0., 40., 1],
#					[4, 11, 5., 2, 0, 20., 0., 40., 0],[104, 127, 5., 2, 0, 20., 0., 40., 1]]
gridX=2; gridY=1
filename = outputdir+'global_nu2h_longRangeChaos.jpg'
plotFigureWithMultipleSubplots(kneadingRangesAndParametersToCompute, gridX, gridY, filename,figSizeY=9, xlabelX=0.5,xlabelY=0.083,ylabelX=0.17,ylabelY=0.5)

# global - nu1 vs nu2 big
kneadingRangesAndParametersToCompute=[[4, 11, 35., 0, 0, 10., 0., 20., 0],[4, 11, 15., 0, 0, 10., 0., 20., 0]]
gridX=2;gridY=1
filename = outputdir+'global_nu1nu2_all.jpg'
plotFigureWithMultipleSubplots(kneadingRangesAndParametersToCompute, gridX, gridY, filename, figSizeY=9, xlabelX=0.5,xlabelY=0.083,ylabelX=0.17,ylabelY=0.5)

# global - nu1 vs nu2 long range chaos
kneadingRangesAndParametersToCompute=[[104, 127, 35., 0, 0, 10., 0., 20.,0], [104, 127, 15., 0, 0, 10., 0., 20.,0],
					[104, 127, 35., 0, 0, 10., 0., 20.,1],[104, 127, 15., 0, 0, 10., 0., 20.,1]]
gridX=2;gridY=2
filename = outputdir+'global_nu1nu2_longRangeChaos.jpg'
plotFigureWithMultipleSubplots(kneadingRangesAndParametersToCompute, gridX, gridY, filename, xlabelX=0.55,xlabelY=0.07,ylabelX=0.065,ylabelY=0.55)

