
clf;

%https://dgleich.github.io/hq-matlab-figs/
%http://www.mathworks.com/help/matlab/ref/annotation.html
%https://www.mathworks.com/matlabcentral/answers/12987-how-to-save-a-matlab-graphic-in-a-right-size-pdf

alw = 0.75;    % AxesLineWidth
fsz = 10;      % Fontsize
dpi = 600;
set(gca, 'FontSize', fsz, 'LineWidth', alw); %<- Set properties
set(0, 'DefaultFigureVisible', 'off');

echo off;
pointSize = fsz/2.;

%not used
%nu1=1;nu2=2;h=10;tk=4;

%nu1=1;nu2=4;h=9;tk=10;

%nu1=1;nu2=4;h=7.5;tk=3.8; %0-2 homoclinic left
%nu1=1;nu2=4;h=7.6;tk=3; %0-2 homoclinic
%nu1=1;nu2=4;h=7.7;tk=3.8; %0-2 homoclinic right

%not used
%nu1=1;nu2=4;h=7.08;tk=5; %0-3 homoclinic

%nu1=1;nu2=4;h=3;tk=100; %Right fixed point stable
%nu1=1;nu2=4;h=3.95;tk=100; %Right fixed point larger
%nu1=1;nu2=4;h=3.99;tk=5; %Homoclinic in between
%nu1=1;nu2=4;h=4.1;tk=100; %Right to left fixed point stable
%nu1=1;nu2=4;h=8;tk=100; %Strange attractor
%nu1=1;nu2=4;h=18;tk=100; %Limit cycle

%nu1=1;nu2=4.45173;h=9.80738;tk=10; %T point
%nu1=1;nu2=6.16393;h=11.4923;tk=8; %T point above up        1011011111...
%nu1=1;nu2=6.06375;h=11.3215;tk=8; %T point above down      1011100000...
%nu1=1;nu2=5.11658;h=8.78848;tk=8; %T point below1 up       101100000...
%nu1=1;nu2=5.12568;h=8.72587;tk=8; %T point below1 down     101011111...
%nu1=1;nu2=4.75228;h=7.93465;tk=8; %T point below2 up       10100000...
%nu1=1;nu2=4.7796;h=7.88342;tk=8; %T point below2 down      10011111...
%nu1=1;nu2=4.45173;h=7.23452;tk=10; %T point below3 up       10010000...
%nu1=1;nu2=4.45173;h=7.21175;tk=10; %T point below3 down        10001111...

%not used
%nu1=1;nu2=2.31148;h=8.72017;tk=5; %Saddle near T point


width = 6; height = 4;
figure;
set(gcf,'Units','Inches');
set(gcf,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[width*dpi, height*dpi])
pikovskyPlotBookChapterPlotMultiple([1,4,3,100],[2,3,1],false,pointSize,'a')
pikovskyPlotBookChapterPlotMultiple([1,4,3.95,100],[2,3,2],false,pointSize,'b')
pikovskyPlotBookChapterPlotMultiple([1,4,3.99,5],[2,3,3],false,pointSize,'c')
pikovskyPlotBookChapterPlotMultiple([1,4,4.1,100],[2,3,4],false,pointSize,'d')
pikovskyPlotBookChapterPlotMultiple([1,4,8,100;],[2,3,5],false,pointSize,'e')
pikovskyPlotBookChapterPlotMultiple([1,4,18,100],[2,3,6],false,pointSize,'f')
pikovskyPlotBookChapterTightfig;
print('Output/exampleConfigurationsTrajectories','-dpdf','-r0');

width = 6; height = 2;
figure;
set(gcf,'Units','Inches');
set(gcf,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[width*dpi, height*dpi])
pikovskyPlotBookChapterPlotMultiple([1,4,9,10],[1,2,1,2],true,pointSize,'a','b');
pikovskyPlotBookChapterTightfig;
print('Output/symbolicComputationsTrajectories','-dpdf','-r0');

width = 6; height = 2;
figure;
set(gcf,'Units','Inches');
set(gcf,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[width*dpi, height*dpi])
pikovskyPlotBookChapterPlotMultiple([1,4,7.5,3.8],[1,3,1],false,pointSize,'a');
pikovskyPlotBookChapterPlotMultiple([1,4,7.6,3],[1,3,2],false,pointSize,'b');
pikovskyPlotBookChapterPlotMultiple([1,4,7.7,3.8],[1,3,3],false,pointSize,'c');
pikovskyPlotBookChapterTightfig;
print('Output/0_2_homoclinicTrajectories','-dpdf','-r0');

width = 6; height = 4;
figure;
set(gcf,'Units','Inches');
set(gcf,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[width*dpi, height*dpi])
pikovskyPlotBookChapterPlotMultiple([1,4.45173,9.80738,10],[2,3,1,4],true,pointSize,'a','b');
pikovskyPlotBookChapterPlotMultiple([1,4.75228,7.93465,8],[2,3,2,5],true,pointSize,'c','d');
pikovskyPlotBookChapterPlotMultiple([1,4.7796,7.88342,8],[2,3,3,6],true,pointSize,'e','f');
pikovskyPlotBookChapterTightfig;
print('Output/tPointTrajectories','-dpdf','-r0');
