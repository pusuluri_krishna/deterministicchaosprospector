import matplotlib
matplotlib.use('TkAgg')
from scipy.integrate import ode
import numpy as n
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib import transforms
#from matplotlib import rc

""" plot a trajectory
def f(t, s, nu1, nu2, h):
    return [-nu1 * s[0] + h * s[1] - s[1] * s[2],
                h * s[0] - nu2 * s[1] + s[0]* s[2],
                -s[2] + s[0] * s[1]]

r = ode(f).set_integrator('rk4')

y0, t0 = [0.01,0.,0.], 0
dt = 0.001
[nu1,nu2,h,tk] = [1,4.45173,9.80738,15]#[1,4,7.5,3.8]

r.set_initial_value(y0, t0).set_f_params(nu1,nu2,h)

output = n.zeros(((tk-t0)/dt+3,4))
output[0,:]=n.concatenate([[t0],y0],0)
i=0;
while r.successful() and r.t < tk:
    output[i,:]=n.concatenate([[r.t+dt],r.integrate(r.t+dt)],0)
    i=i+1

yPositive = output[:,2]>0
plt.plot(output[yPositive,2],output[yPositive,1],'ro', ms=2, mec='r')
plt.plot(output[~yPositive,2],output[~yPositive,1],'bo', ms=2, mec='b')
plt.plot(output[0,2],output[0,1],'ko',ms=10,mec='k')
plt.xlabel('y')
plt.ylabel('x')


plt.show()
"""

outputdir = 'Output/'

dpi = 1200
defaultFontSize = 8; largeFontSize = 11
matplotlib.rcParams['savefig.dpi'] = dpi #2*sweepSize/figSize #300
matplotlib.rcParams.update({'font.size': largeFontSize,
              'axes.labelsize': largeFontSize,
              'axes.titlesize': largeFontSize,
              'text.fontsize': defaultFontSize,
              'legend.fontsize': largeFontSize,
              'xtick.labelsize': defaultFontSize,
              'ytick.labelsize': defaultFontSize})

#rc('text',usetex=True)
#rc('text.latex', preamble='\usepackage{color}')

y0, t0 = [0.01, 0., 0.], 0
dt = 0.001

def f(t, s, nu1, nu2, h):
    return [-nu1 * s[0] + h * s[1] - s[1] * s[2],
                h * s[0] - nu2 * s[1] + s[0]* s[2],
                -s[2] + s[0] * s[1]]

#Text with multiple substrings each having a different color, either horizontally or vertically
def rainbow_text(fig, ax, x,y,textList,colorList, isVertical=False):
    t = ax.transAxes
    if(not isVertical):
        for text,color in zip(textList, colorList):
            text = ax.text(x,y,text,color=color, transform=t)
            ex = text.get_window_extent(fig.canvas.get_renderer())
            t = transforms.offset_copy(t, x=ex.width, units='dots', fig=fig)
    else:
        for text,color in zip(textList, colorList):
            text = ax.text(x,y,text,color=color, transform=t,rotation='vertical',va='bottom',ha='center')
            ex = text.get_window_extent(fig.canvas.get_renderer())
            t = transforms.offset_copy(t, y=ex.height, units='dots')

def plotFigureWithMultipleSubplots(parameterSetArray, gridX, gridY, filename, figSizeX=6,
                                   figSizeY=6, plotTime=False, labelCoordinates=(0.5,0.01,0.01,), labelCoordinates1=None, plotSymbols=False):
    fig = plt.figure(figsize=(figSizeX, figSizeY))
    fig.dpi=1200
    gs1 = gridspec.GridSpec(gridX, gridY)
    if(plotTime==False):
        gs1.update(wspace=0.03, hspace=0.03)  # set the spacing between axes.
    else:
        gs1.update(wspace=0.03, hspace=0.2)  # set the spacing between axes.
    if(parameterSetArray.__len__()==1 & plotTime==True) :
        gs1.update(wspace=0.2, hspace=0.03)

    for parameterSet in parameterSetArray:

        r = ode(f).set_integrator('rk4')

        #parameterSet [nu1, nu2, h, tk]
        r.set_initial_value(y0, t0).set_f_params(parameterSet[0], parameterSet[1], parameterSet[2])
        tk = parameterSet[3]
        output = n.zeros(((tk - t0) / dt + 3, 4))
        output[0, :] = n.concatenate([[t0], y0], 0)
        j = 0;
        while r.successful() and r.t < tk:
            output[j, :] = n.concatenate([[r.t + dt], r.integrate(r.t + dt)], 0)
            j = j + 1
        yPositive = output[:, 2] > 0


        ax = plt.subplot(gs1[parameterSet[4]])
        plt.axis('on')
        ax.margins(x=0.1, y=0.1)
        ax.plot(output[yPositive, 2], output[yPositive, 1], 'ro', ms=.5, mec='r')
        ax.plot(output[~yPositive, 2], output[~yPositive, 1], 'bo', ms=.5, mec='b')
        ax.plot(output[0, 2], output[0, 1], 'ko', ms=2, mec='k')
        ax.autoscale(False)
        ax.set_adjustable('box-forced')

        bbox_props = dict(boxstyle="square,pad=0.2", fc="white", ec="k", lw=.5)
        ax.text(1., 1., chr(parameterSet[4] + ord('a')), ha='right', va='top', bbox=bbox_props,
                transform=ax.transAxes, fontsize=largeFontSize)
        bbox_props = dict(boxstyle="square,pad=0.1", fc="white", ec="k", lw=0)
        fig.text(labelCoordinates[0], labelCoordinates[1], r'$y$ - variable', ha='center', va='center', transform=ax.transAxes, bbox=bbox_props, color='darkslategray', fontsize=largeFontSize, weight='roman')  # xlabel
        fig.text(labelCoordinates[2], labelCoordinates[3], r'$x$ - variable', ha='center', va='center', rotation='vertical', transform=ax.transAxes, bbox=bbox_props, color='darkslategray', fontsize=largeFontSize, weight='roman')  # ylabel
        if(plotSymbols):
            rainbow_text(fig, ax, 0.02, 0.9, parameterSet[6], parameterSet[7])
        ax.set_xticks([0])
        ax.set_yticks([0])
        plt.setp(ax.get_yticklabels(), visible=False)
        plt.setp(ax.get_xticklabels(), visible=False)


        if(plotTime==True):
            ax1 = plt.subplot(gs1[parameterSet[5]])
            plt.axis('on')
            ax1.margins(x=0.,y=0.1)
            ax1.plot(output[yPositive, 0], output[yPositive, 2], 'ro', ms=.5, mec='r')
            ax1.plot(output[~yPositive, 0], output[~yPositive, 2], 'bo', ms=.5, mec='b')
            ax1.plot(output[0, 0],output[0, 2], 'ko', ms=2, mec='k')
            bbox_props = dict(boxstyle="square,pad=0.2", fc="white", ec="k", lw=.5)
            ax1.text(1.0, 1.0, chr(parameterSet[5] + ord('a')), ha='right', va='top', bbox=bbox_props,
                     transform=ax1.transAxes, fontsize=largeFontSize)
            ax1.autoscale(False)
            ax1.set_adjustable('box-forced')

            bbox_props = dict(boxstyle="square,pad=0.1", fc="white", ec="k", lw=0)
            fig.text(labelCoordinates1[0], labelCoordinates1[1], r'time', ha='center', va='center', transform=ax.transAxes, bbox=bbox_props, color='darkslategray', fontsize=largeFontSize, weight='roman')  # xlabel
            fig.text(labelCoordinates1[2], labelCoordinates1[3], r'$y$ - variable', ha='center', va='center', rotation='vertical', transform=ax.transAxes, bbox=bbox_props, color='darkslategray', fontsize=largeFontSize, weight='roman')  # ylabel

            if(plotSymbols):
                rainbow_text(fig, ax1, 0.15, 0.9, parameterSet[6], parameterSet[7])

            ax1.set_xticks([0])
            ax1.set_yticks([0])
            plt.setp(ax1.get_yticklabels(), visible=False)
            plt.setp(ax1.get_xticklabels(), visible=False)

    plt.subplots_adjust(left=0.1, right=0.9, top=0.9, bottom=0.1)
    fig.savefig(filename, bbox_inches='tight')
    return

parameterSetArray=[[1,4,3,100,0,0, ['{',r'$\overline{1}$','}'],['k','red','k'] ],
                   [1,4,3.95,100,1,0, ['{',r'$\overline{1}$','}'],['k','red','k'] ],
                   [1,4,3.99,5,2,0, ['{','$1$','}-homoclinic'],['k','red','k'] ],
                   [1,4,4.1,100,3,0, ['{','$1$',r'$\overline{0}$','}'],['k','red','blue','k'] ] ,
                   [1,4,8,100,4,0, ['  chaos'],['k'] ],
                   [1,4,18,100,5,0, ['{...',r'$\overline{1}$',r'$\overline{0}$','}-periodic'],['k','red','blue','k'] ]]
gridX=2; gridY=3
filename = outputdir+'exampleConfigurationsTrajectories.jpg'
plotFigureWithMultipleSubplots(parameterSetArray, gridX, gridY, filename, figSizeY=4., plotTime=False, labelCoordinates=(0.5,0.07,0.08,0.5), plotSymbols=True)

parameterSetArray=[[1,4,9,10,0,1,['{','$1$','$0$','$1111$','$00$','$11$','...}'],['k','red','blue','red','blue','red','k']]]
gridX=1; gridY=2
filename = outputdir+'symbolicComputationsTrajectories.jpg'
plotFigureWithMultipleSubplots(parameterSetArray, gridX, gridY, filename, figSizeY=2., plotTime=True, labelCoordinates=(0.27,0.051,0.08,0.5), labelCoordinates1=(0.71,0.051,0.51,0.5), plotSymbols=True)

parameterSetArray=[[1,4,7.5,3.8,0,0, ['{','$1$','$00$','...}'],['k','red','blue','k'] ],
                   [1,4,7.6,2.9,1,0, ['{','$1$','$0$','}-homoclinic'],['k','red','blue','k'] ],
                   [1,4,7.7,3.8,2,0, ['{','$1$','$0$','$1$','...}'],['k','red','blue','red','k']] ]
gridX=1; gridY=3
filename = outputdir+'0_2_homoclinicTrajectories.jpg'
plotFigureWithMultipleSubplots(parameterSetArray, gridX, gridY, filename, figSizeY=2., plotTime=False, labelCoordinates=(0.5,0.051,0.08,0.5), plotSymbols=True)

parameterSetArray=[[1,4.45173,9.80738,15,0,4, ['{','$1$','$0$',r'$\overline{1}$','}'],['k','red','blue','red','k'] ],
                   [1,4.75228,7.93465,17,1,5, ['{','$1$','$0$','$1$', r'$\overline{0}$','}'],['k','red','blue','red','blue','k'] ],
                   [1,4.7796,7.88342,15,2,6, ['{','$1$','$00$',r'$\overline{1}$','}'],['k','red','blue','red','k'] ],
                   [1,6.16393,11.4923,10,3,7, ['{','$1$','$0$','$11$','$0$',r'$\overline{1}$','}'],['k','red','blue','red','blue','red','k'] ] ] #T1, T4, T4', T2
gridX=2; gridY=4
filename = outputdir+'tPointTrajectories.jpg'
plotFigureWithMultipleSubplots(parameterSetArray, gridX, gridY, filename, figSizeY=4., plotTime=True, labelCoordinates=(0.5,0.51,0.08,0.73), labelCoordinates1=(0.5,0.07,0.08,0.26), plotSymbols=True)
