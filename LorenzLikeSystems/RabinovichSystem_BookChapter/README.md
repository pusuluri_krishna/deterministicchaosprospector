
The code is presented for all the computational methods described and to generate the images in the following publications:

	Pusuluri K., Pikovsky A., Shilnikov A. (2017) Unraveling the Chaos-Land and Its Organization in the Rabinovich System. In: Aranson I., Pikovsky A., Rulkov N., Tsimring L. (eds) Advances in Dynamics, Patterns, Cognition. Nonlinear Systems and Complexity, vol 20. Springer, Cham

If you make use of this repository in your research, please consider citing the above article.
