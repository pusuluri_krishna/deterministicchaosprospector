function [] = pikovskyPlotBookChapterPlotMultiple(parameterArray,positionArray,plotTime, pointSize, id1, id2)
    
    nu1=parameterArray(1);
    nu2=parameterArray(2);
    h=parameterArray(3);
    tk=parameterArray(4);
    flor = @(t,s) [ -nu1*s(1)+h*s(2)-s(2).*s(3); h*s(1)-nu2*s(2)+s(1).*s(3); -s(3)+s(1).*s(2)];
    options = odeset('RelTol',1e-5,'AbsTol',1e-5);
    [t1,y1,tau1]=ode45(flor,0:0.001:tk,[0.01,0.,0.]);
    
    pikovskyPlotBookChapterSubaxis(positionArray(1),positionArray(2),positionArray(3),'sv',0,'sh',0,'p',.02,'m',.04)
    k1 = find(y1(:,2)>0);
    k2 = find(y1(:,2)<=0);
    hold on
    plot(y1(k1,2), y1(k1,1),'.','MarkerSize', pointSize,'Color','b');
    plot(y1(k2,2), y1(k2,1),'.','MarkerSize', pointSize,'Color','r');
    plot(0,0,'O','Color','k','MarkerSize', pointSize*1.5,'MarkerFaceColor','k');
    %if(plotTime==true || mod(positionArray(3),positionArray(2))==1)
        ylabel('x');
    %end
    %if(plotTime==true || ceil(positionArray(3)/positionArray(2))==positionArray(1))
        xlabel('y');
    %end
    
    pikovskyPlotBookChapterNtitle(id1, 'edgecolor','k','backgroundcolor','w');
    axis tight;
    axis on;

    if(plotTime==true)
        pikovskyPlotBookChapterSubaxis(positionArray(1),positionArray(2),positionArray(4),'sv',0,'sh',0,'p',.02,'m',.04);
        hold on;
        plot(t1(k1), y1(k1,2), '.', 'MarkerSize',pointSize,'Color', 'b');
        plot(t1(k2), y1(k2,2), '.', 'MarkerSize',pointSize,'Color', 'r');
        xlabel('time');
        ylabel('y');
        pikovskyPlotBookChapterNtitle(id2', 'edgecolor','k','backgroundcolor','w');
        axis tight;
        axis on;
    end
    
end

