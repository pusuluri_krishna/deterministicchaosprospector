#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include <sys/time.h>
#include<string.h>

#define N_EQ1	3
#define NUM_THREADS_PER_BLOCK 512

__device__ void stepper(const double* y, double* dydt, const double* params)
{
	double nu1=params[0];
	double nu2=params[1];
	double h=params[2];

	dydt[0] = -nu1*y[0]+h*y[1]-y[1]*y[2];
	dydt[1] = h*y[0]-nu2*y[1]+y[0]*y[2];
	dydt[2] = -y[2]+y[0]*y[1];
}

/*
__device__ void computeFirstDerivative(const double *y, double *dydt, const double* params){
	stepper(y, dydt, params);
	return;
}
*/


__device__ double computeFixedPointDistance(const double* params){
    double v1 = params[0], v2 = params[1], h = params[2];
    double z, l;
    if(h*h-v1*v2 > 0){
        z = sqrt(h*h-v1*v2);
        l = (h - z)/v1;
        return sqrt(z*l);
    }
    return 0;
}

__device__ double integrator_rk4(double* y_current, const double* params, const double dt, const unsigned N, const unsigned stride,
                                            const unsigned kneadingsStart, const unsigned kneadingsEnd)
{
	unsigned i, j, k, kneadingIndex=0;
	double dt2, dt6;
	double y1[N_EQ1], y2[N_EQ1], k1[N_EQ1], k2[N_EQ1], k3[N_EQ1], k4[N_EQ1];
	double firstDerivativeCurrent[N_EQ1],firstDerivativePrevious;
	double kneadingsWeightedSum=0;
    double fixedPointDistance = computeFixedPointDistance(params);

    bool isPreviousEventOne=false, isDerviative2FirstTimeOnThisSidePositive=0, isPreviousDerivative2Positive=0, isCurrentDerivate2Positive=0;
    bool isDerviative1FirstTimeOnThisSidePositive=0, isPreviousDerivative1Positive=0, isCurrentDerivate1Positive=0;

	dt2 = dt/2.; dt6 = dt/6.;
	if(fixedPointDistance==0)return -0.1;
	for(i=1; i<N; i++)
	{
		for(j=0; j<stride; j++)
		{
			stepper(y_current, k1, params);
			for(k=0; k<N_EQ1; k++) y1[k] = y_current[k]+k1[k]*dt2;
			stepper(y1, k2, params);
			for(k=0; k<N_EQ1; k++) y2[k] = y_current[k]+k2[k]*dt2;
			stepper(y2, k3, params);
			for(k=0; k<N_EQ1; k++) y2[k] = y_current[k]+k3[k]*dt;
			stepper(y2, k4, params);

			//Copy latest value into y_current
			for(k=0; k<N_EQ1; k++) y_current[k] += dt6*(k1[k]+2.*(k2[k]+k3[k])+k4[k]);
		}


		//computeFirstDerivative(y_current, firstDerivativeCurrent, params);
        stepper(y_current, firstDerivativeCurrent, params);

/* Normal event check

        // event y - check x; event x check y not good since the symmetry seems to be about x-axis; check with z not giving useful results; Also, without fixedPointDistance check, results not good
        if(firstDerivativePrevious * firstDerivativeCurrent[1] < 0) {

        	if( y_current[1] > fixedPointDistance && firstDerivativeCurrent[0]<0 ) {
				if(kneadingIndex>=kneadingsStart){
					//printf("1");
					kneadingsWeightedSum+=powf(2,kneadingIndex-kneadingsStart+1);
				}
				kneadingIndex++;

        	} else if(y_current[1]< -fixedPointDistance && firstDerivativeCurrent[0]>0 ) {
        			//printf("0");
        			kneadingIndex++;
        	}
        }
		firstDerivativePrevious = firstDerivativeCurrent[1];

		if(kneadingIndex>kneadingsEnd)
			return kneadingsWeightedSum/powf(2,kneadingIndex-kneadingsStart+1);;
	}
*/

///* Event check with loop correction

        // event y[1] - check y[0] (event y[0] - check y[1] not right since symmetry about x-axis not y-axis) (check with y[2] not giving useful results either by itself or along with check with y[1])

        if(firstDerivativePrevious * firstDerivativeCurrent[1] < 0) {

            isCurrentDerivate2Positive = (firstDerivativeCurrent[0]>0);
            //isCurrentDerivate1Positive = (firstDerivativeCurrent[2]>0);

            if(y_current[1] > 0){

                if(y_current[1] > fixedPointDistance ){//&& y_current[0] > fixedPointDistance){
                    if(!isPreviousEventOne){

                        if(kneadingIndex>=kneadingsStart){
                            //printf("1");
                            kneadingsWeightedSum+=powf(2,kneadingIndex-kneadingsStart+1);
                        }
                        kneadingIndex++;
                        isDerviative2FirstTimeOnThisSidePositive = isCurrentDerivate2Positive;
                        //isDerviative1FirstTimeOnThisSidePositive = isCurrentDerivate1Positive;

                    }else{

//                        if(isCurrentDerivate2Positive==isDerviative2FirstTimeOnThisSidePositive && isCurrentDerivate2Positive!=isPreviousDerivative2Positive &&
//                        isCurrentDerivate1Positive==isDerviative1FirstTimeOnThisSidePositive && isCurrentDerivate1Positive!=isPreviousDerivative1Positive){
                        if(isCurrentDerivate2Positive==isDerviative2FirstTimeOnThisSidePositive && isCurrentDerivate2Positive!=isPreviousDerivative2Positive ){
                            if(kneadingIndex>=kneadingsStart){
                                //printf("1");
                                kneadingsWeightedSum+=powf(2,kneadingIndex-kneadingsStart+1);
                            }
                            kneadingIndex++;
                        }

                    }

                    isPreviousEventOne=true;
                }

                isPreviousDerivative2Positive = isCurrentDerivate2Positive;
                //isPreviousDerivative1Positive = isCurrentDerivate1Positive;

            }else{

                if(y_current[1] < -fixedPointDistance){ //&& y_current[0] > fixedPointDistance){
                    if(isPreviousEventOne){

                        kneadingIndex++;
                        isDerviative2FirstTimeOnThisSidePositive = isCurrentDerivate2Positive;
                        //isDerviative1FirstTimeOnThisSidePositive = isCurrentDerivate1Positive;

                    }else{

//                        if(isCurrentDerivate2Positive==isDerviative2FirstTimeOnThisSidePositive && isCurrentDerivate2Positive!=isPreviousDerivative2Positive &&
//                        isCurrentDerivate1Positive==isDerviative1FirstTimeOnThisSidePositive && isCurrentDerivate1Positive!=isPreviousDerivative1Positive){
                        if(isCurrentDerivate2Positive==isDerviative2FirstTimeOnThisSidePositive && isCurrentDerivate2Positive!=isPreviousDerivative2Positive){
                            kneadingIndex++;
                        }

                    }

                    isPreviousEventOne=false;
                }

                isPreviousDerivative2Positive = isCurrentDerivate2Positive;
                //isPreviousDerivative1Positive = isCurrentDerivate1Positive;
            }

        }
		firstDerivativePrevious = firstDerivativeCurrent[1];

		if(kneadingIndex>kneadingsEnd)
			return kneadingsWeightedSum/powf(2,kneadingIndex-kneadingsStart+1);;
	}
//*/

	return -0.05;
}


__global__ void sweepThreads(double* kneadingsWeightedSumSet,
                                        double parameter1Start, double parameter1End, unsigned parameter1Count,
										double parameter2Start, double parameter2End, unsigned parameter2Count,
										double parameter3, unsigned whichSweep,
										double dt, unsigned N, unsigned stride,
										unsigned kneadingsStart, unsigned kneadingsEnd){

    int tx=blockIdx.x * blockDim.x + threadIdx.x;

	double params[3];
	double parameter1Step = (parameter1End - parameter1Start)/(parameter1Count-1);
	double parameter2Step = (parameter2End-parameter2Start)/(parameter2Count -1);
	int i,j,k;


    if(tx<parameter1Count*parameter2Count){

        i=tx/parameter1Count;
        j=tx%parameter1Count;

        if(whichSweep==0){                          //sweep of nu1 vs nu2

            params[0]=parameter1Start+i*parameter1Step;
            params[1]=parameter2Start+j*parameter2Step;
            params[2]=parameter3;

        }else if(whichSweep==1){                    //sweep of nu1 vs h

            params[0]=parameter1Start+i*parameter1Step;
            params[1]=parameter3;
            params[2]=parameter2Start+j*parameter2Step;

        }else if(whichSweep==2){                    //sweep of nu2 vs h

            params[0]=parameter3;
            params[1]=parameter1Start+i*parameter1Step;
            params[2]=parameter2Start+j*parameter2Step;

        }

      	double y_initial[N_EQ1]={0+0.00000001L,0,0};
        kneadingsWeightedSumSet[i*parameter2Count+j] = integrator_rk4(y_initial, params, dt, N, stride,
                                                                kneadingsStart, kneadingsEnd);
    }
}

/**
 * error checking routine
 */
void checkAndDisplayErrors(char *label)
{
  // we need to synchronise first to catch errors due to
  // asynchronous operations that would otherwise
  // potentially go unnoticed

  cudaError_t err;

  err = cudaThreadSynchronize();
  if (err != cudaSuccess)
  {
    char *e = (char*) cudaGetErrorString(err);
    fprintf(stderr, "CUDA Error: %s (at %s)", e, label);
  }

  err = cudaGetLastError();
  if (err != cudaSuccess)
  {
    char *e = (char*) cudaGetErrorString(err);
    fprintf(stderr, "CUDA Error: %s (at %s)", e, label);
  }
}


extern "C" {

void sweep(double* kneadingsWeightedSumSet,
                                        double parameter1Start, double parameter1End, unsigned parameter1Count,
										double parameter2Start, double parameter2End, unsigned parameter2Count,
										double parameter3, unsigned whichSweep,
										double dt, unsigned N, unsigned stride,
										unsigned kneadingsStart, unsigned kneadingsEnd){

        int totalParameterSpaceSize = parameter1Count*parameter2Count;
        double *kneadingsWeightedSumSetGpu;

        /*allocate device memory. */
        (cudaMalloc( (void**) &kneadingsWeightedSumSetGpu, totalParameterSpaceSize*sizeof(double)));
        checkAndDisplayErrors("Memory allocation");

        /*timing*/
        cudaEvent_t start_event, stop_event;
        cudaEventCreate(&start_event) ;
        cudaEventCreate(&stop_event) ;
        cudaEventRecord(start_event, 0);

        /*Dimensions. */

        int gridXDimension = totalParameterSpaceSize/NUM_THREADS_PER_BLOCK;
        if(totalParameterSpaceSize%NUM_THREADS_PER_BLOCK!=0) {
                gridXDimension += 1;
        }
        dim3 dimGrid(gridXDimension,1);
        dim3 dimBlock(NUM_THREADS_PER_BLOCK,1);

        printf(" Num of blocks per grid:       %d\n", gridXDimension);
        printf(" Num of threads per block:     %d\n", NUM_THREADS_PER_BLOCK);
        printf(" Total Num of threads running: %d\n", gridXDimension*NUM_THREADS_PER_BLOCK);
        printf(" Parameters alphaCount=%d, lCount=%d\n",parameter1Count,parameter2Count);

        /*Call kernel(global function)*/
        sweepThreads<<<dimGrid, dimBlock>>>(kneadingsWeightedSumSetGpu, parameter1Start, parameter1End, parameter1Count, parameter2Start, parameter2End, parameter2Count, parameter3, whichSweep, dt, N, stride, kneadingsStart, kneadingsEnd);

        cudaThreadSynchronize();
        cudaEventRecord(stop_event, 0);
        cudaEventSynchronize(stop_event);

        float time_kernel;
        cudaEventElapsedTime(&time_kernel, start_event, stop_event);
        printf("Total time(sec) %f\n", time_kernel/1000);

        /*copy data from device memory to memory. */
        (cudaMemcpy( kneadingsWeightedSumSet, kneadingsWeightedSumSetGpu, totalParameterSpaceSize*sizeof(double), cudaMemcpyDeviceToHost));
        checkAndDisplayErrors("Error while copying kneading sum values from device to host.");

        /*Free all allocated memory. */
        cudaFree(kneadingsWeightedSumSetGpu);

}

}


int main(){
	double dt=0.01;
	unsigned N=30000;
	unsigned stride = 1;
	unsigned maxKneadings = 20;
	unsigned sweepSize = 5;
	double g = 50, sigma = 1.5;
	double *kneadingsWeightedSumSet;
	int i,j;
	printf("Enter sweep size :");
	scanf("%d",&sweepSize);
	kneadingsWeightedSumSet = (double *)malloc(sweepSize*sweepSize*sizeof(double));
	sweep(kneadingsWeightedSumSet, 3, 5, sweepSize, 1, 7, sweepSize, 1, 2, dt, N, stride, 0, 19);

	for(i=0;i<sweepSize;i++){
	    for(j=0;j<sweepSize;j++){
	        printf("%f \n",kneadingsWeightedSumSet[i*sweepSize+j]);
	    }
	}

}





