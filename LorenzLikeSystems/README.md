
The code is presented for all the computational methods described and to generate the images in the following publications:

	Yu. Bakharova, A. Kazakov, S. Malykh, K. Pusuluri and AL. Shilnikov. Homoclinic chaos in the Rossler model. J. Chaos, 2020
	T. Xing, K. Pusuluri, and A. L. Shilnikov. Ordered intricacy of homoclinics of the L. Shilnikov saddle-focus in symmetric systems. J. Chaos, in review (2020).

	K. Pusuluri, H. G. E. Meijer, and A. L. Shilnikov. Homoclinic puzzles and chaos in a nonlinear laser model. J. Communications in Nonlinear Science and Numerical Simulations (2020).
	Pusuluri K, Shilnikov A. Homoclinic chaos and its organization in a nonlinear optics model. Physical Review E. 2018 Oct 30;98(4):040202.

	Pusuluri K., Pikovsky A., Shilnikov A. (2017) Unraveling the Chaos-Land and Its Organization in the Rabinovich System. In: Aranson I., Pikovsky A., Rulkov N., Tsimring L. (eds) Advances in Dynamics, Patterns, Cognition. Nonlinear Systems and Complexity, vol 20. Springer, Cham

If you make use of this repository in your research, please consider citing the above articles.
