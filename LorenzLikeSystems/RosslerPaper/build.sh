#!/usr/bin/env bash
nvcc --ptxas-options=-v -arch=sm_30 --compiler-options '-fPIC' -o rosslerSweep.so --shared rosslerSweep.cu
nvcc --ptxas-options=-v -arch=sm_30 --compiler-options '-fPIC' -o rosslerLongKneadings.so --shared rosslerLongKneadings.cu
nvcc --ptxas-options=-v -arch=sm_30 --compiler-options '-fPIC' -o rosslerLongKneadings_LoopCorrection.so --shared rosslerLongKneadings_LoopCorrection.cu
