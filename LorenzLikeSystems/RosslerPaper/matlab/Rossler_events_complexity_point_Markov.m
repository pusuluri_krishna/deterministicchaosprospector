
function Rossler_run

clf;
%close all 
clear all;

figure(1)
set(gcf, 'PaperPositionMode','auto','color', 'white');
set(gcf,'PaperPosition',[1.5 3 5 5])
hFig=axes('Position',[0.2 0.4 .6 .6],'Visible','off','Color',[.9 .9 .9],...
         'FontName','times',...
         'FontSize',8,...
         'XColor',[0 0 .0],...
         'YColor',[0 0 .0],...
         'ZColor',[0 0 .0]);



%Search for Shilnikov
bb=0.2;
aa=0.2;
%'homoclinic loop'
%cc=10.25505474;
cc=10.05;


p=(cc-sqrt(cc^2-4*aa*bb))/(2*aa);
xeq(1)=aa.*p;  
xeq(2)=-p;
xeq(3)=p;


J=[0 -1 -1; 1 aa 0; xeq(3) 0 xeq(1)-cc];
[ve,va]=eig(J);


%  xrev=ve(:,3)'*0.0001+xeq
% % 
%  flor1 = @(t,s) [ -(-s(2)-s(3)); -(s(1)+aa*s(2)); -(bb+s(3)*(s(1)-cc))];
%  options = odeset('RelTol',1e-8,'AbsTol',1e-10);
%  [tb,yb]=ode45(flor1,[0:0.01:4.2],xrev,options);
%  plot3(yb(:,1), yb(:,2), yb(:,3),'Color',[0.1 0.1 0.9],'LineWidth',2)
%  hold on

flor = @(t,s) [ -(s(2)+s(3)); s(1)+aa*s(2); bb+s(3)*(s(1)-cc)];
options = odeset('RelTol',1e-6,'AbsTol',1e-5,'Events',@events);
%options = odeset('Events',@events);

% Initial Transient 
[t,y1,tau1,ye1,ie1]=ode45(flor,[0:0.01:500],xeq,options);
plot3(y1(:,1), y1(:,2), y1(:,3),'Color',[0.0 0.8 0.8],'LineWidth',.1)
hold on
%after transient 
[t,y,tau1,ye1,ie1]=ode45(flor,[0:0.01:5600],y1(end,:),options);
plot3(y(:,1), y(:,2), y(:,3),'Color',[0.8 0.8 0.8],'LineWidth',.1)
hold on
plot3(ye1(:,1), ye1(:,2), ye1(:,3),'.','MarkerSize',5,'Color', [0.8 0.4 0.4])
hold on
% ll=length(y(:,1))
% plot3(y(ll-10000:ll,1), y(ll-10000:ll,2), y(ll-10000:ll,3),'Color',[0. 0.8 0],'LineWidth',3)
% hold on


[xclosest,index]=min((y(:,1)-xeq(1)).^2+(y(:,2)-xeq(2)).^2+(y(:,3)-xeq(3)).^2)

%  k=4500;
% plot3(y(index:index+k,1), y(index:index+k,2), y(index:index+k,3),'Color',[0.0 0.1 0.4],'LineWidth',3)
%  hold on
  k=-4500;
  plot3(y(index+k:index,1), y(index+k:index,2), y(index+k:index,3),'MarkerSize',5,'Color', [0.0 0. 0.4],'LineWidth',3)
  hold on



xlabel('x','FontSize',20);
ylabel('y','FontSize',20);
zlabel('z','FontSize',20);
axis on;
axis([-50 50 -30 30 -10 55])
axis([-2 5 -5 5 -3 5])
axis tight 
view(-44,13)


set(gca,'box','off','FontSize',8);
 hFig=axes('Position',[0.2 0.1 .6 .2],'Visible','off','Color',[.9 .9 .9],...
         'FontName','times',...
         'FontSize',8,...
         'XColor',[0 0 .7],...
         'YColor',[0 0 .7],...
         'ZColor',[0 0 .7]);
box on

plot (t,y(:,3),'Color','b','LineWidth',1, 'Color', [0.8 0.8 0.8])
 hold on
plot (tau1,ye1(:,3),'.', 'MarkerSize',5,'Color',[250./255  81./255  5./255])
hold on


bin = logical(heaviside(ye1(:,1)'));
b1=bin+1;
b2 = heaviside(ye1(:,1)')+1;


lookup_string1 = '01';
% this is a banary sequence 
bin1 = lookup_string1(bin + 1);

 % find changes in vector,
% forcing a change at the beginning and at the end
%changes = find(diff([~b(1); b(:); ~b(end)]) ~= 0)


[TRANS, EMIS] = hmmestimate(b1,b1);
hmmestimate(b1,b1)';
TRANS'
%[vec val] = eig(TRANS_EST');


%c=heaviside(ye1(:,1)');
%heaviside(ye1(:,1)');

xlabel('time','FontSize',20);
ylabel('z','FontSize',20);
axis tight
axis on



complex=calc_lz_complexity1(bin,'primitive',0);
complex=calc_lz_complexity1(bin,'primitive',0)/length(bin);
[q1,q2]=calc_lz_complexity1(bin,'primitive',0);
q1;
%celldisp(q2);
length(bin);

T={'a','c','complex';aa,cc,complex};
disp(T)


% figure(2)
% 
% plot (ye1(:,3),circshift(ye1(:,3),-2),'.')
% hold on 

function [value,isterminal,direction] = events(t,s);
if s(3)>.03
    direction= [-1];
else
    direction= [1];
end

value= bb+s(3)*(s(1)-cc);
% this means x'=0 as x'=y in the SM model 
isterminal=[0];
end 

end

