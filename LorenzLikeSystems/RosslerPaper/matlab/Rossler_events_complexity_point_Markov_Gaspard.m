
function sm_run

clf;
%close all 
clear all;

figure(1)
set(gcf, 'PaperPositionMode','auto','color', 'white');
set(gcf,'PaperPosition',[1.5 3 5 5])
hFig=axes('Position',[0.2 0.4 .6 .6],'Visible','off','Color',[.9 .9 .9],...
         'FontName','times',...
         'FontSize',8,...
         'XColor',[0 0 .0],...
         'YColor',[0 0 .0],...
         'ZColor',[0 0 .0]);



%Search for Shilnikov
b=0.3;
%DO NOT TOUCH
a=0.380205;
c=4.8200;


a=0.32;
c=4.5;

%DO NOT TOUCH
a=0.38;
c=4.821651860268419742538981154212;

A=[0 -1 -1; 1 a 0; b 0 -c];
[ve,va]=eig(A);
xrev=ve(:,3)*0.0001;

flor1 = @(t,s) [ -(-s(2)-s(3)); -(s(1)+a*s(2)); -(b*s(1)+s(3)*(s(1)-c))];
options = odeset('RelTol',1e-4,'AbsTol',1e-5);
[tb,yb]=ode45(flor1,[0:0.01:5],[xrev],options);

plot3(yb(:,1), yb(:,2), yb(:,3),'Color',[0.1 0.1 0.9],'LineWidth',2)
hold on

flor = @(t,s) [ -s(2)-s(3); s(1)+a*s(2); b*s(1)+s(3)*(s(1)-c)];
options = odeset('RelTol',1e-4,'AbsTol',1e-5,'Events',@events);
%options = odeset('Events',@events);

[t,y,tau1,ye1,ie1]=ode45(flor,[0:0.01:50],[0,0,0.1],options);

plot3(y(:,1), y(:,2), y(:,3),'Color',[0.9 0.9 0.9],'LineWidth',.1)
hold on
plot3(ye1(:,1), ye1(:,2), ye1(:,3),'.','MarkerSize',5,'Color', [0.8 0.4 0.4])
hold on



[xclosest,index]=min(y(:,1).^2+y(:,2).^2+y(:,3).^2);

% k=3500;
% %plot3(y(index:index+k,1), y(index:index+k,2), y(index:index+k,3),'.','MarkerSize',5,'Color', [0.0 0.1 0.4])
% hold on
% k=-2000;
% plot3(y(index+k:index,1), y(index+k:index,2), y(index+k:index,3),'.','MarkerSize',5,'Color', [0.0 0.9 0.4])
% hold on



xlabel('x','FontSize',20);
ylabel('y','FontSize',20);
zlabel('z','FontSize',20);
axis on;
axis([-25 25 -30 30 0 55])
axis tight 
view(21,11)


set(gca,'box','off','FontSize',8);
 hFig=axes('Position',[0.2 0.1 .6 .2],'Visible','off','Color',[.9 .9 .9],...
         'FontName','times',...
         'FontSize',8,...
         'XColor',[0 0 .7],...
         'YColor',[0 0 .7],...
         'ZColor',[0 0 .7]);
box on

plot (t,y(:,3),'Color','b','LineWidth',1, 'Color', [0.8 0.8 0.8])
 hold on
plot (tau1,ye1(:,3),'.', 'MarkerSize',5,'Color',[250./255  81./255  5./255])
hold on


bin = logical(heaviside(ye1(:,1)'));
b1=bin+1;
b2 = heaviside(ye1(:,1)')+1;


lookup_string1 = '01';
bin1 = lookup_string1(bin + 1)

 % find changes in vector,
% forcing a change at the beginning and at the end
%changes = find(diff([~b(1); b(:); ~b(end)]) ~= 0)


[TRANS, EMIS] = hmmestimate(b1,b1);
hmmestimate(b1,b1)'
TRANS'
%[vec val] = eig(TRANS_EST');


%c=heaviside(ye1(:,1)');
%heaviside(ye1(:,1)');

xlabel('time','FontSize',20);
ylabel('z','FontSize',20);
axis tight
axis on

complex=calc_lz_complexity1(bin,'primitive',0);
complex=calc_lz_complexity1(bin,'primitive',0)/length(bin);
[q1,q2]=calc_lz_complexity1(bin,'primitive',0);
q1;
%celldisp(q2);
length(bin);

T={'a','b','complex';a,b,complex};
disp(T)


% figure(2)
% 
% plot (ye1(:,3),circshift(ye1(:,3),-2),'.')
% hold on 

function [value,isterminal,direction] = events(t,s);
if s(3)>.03
    direction= [-1];
else
    direction= [1];
end

value= b*s(1)+s(3)*(s(1)-c);
% this means x'=0 as x'=y in the SM model 
isterminal=[0];
end 

end

