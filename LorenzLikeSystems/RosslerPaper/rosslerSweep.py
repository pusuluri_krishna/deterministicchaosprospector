import matplotlib
matplotlib.use('Agg')
import ctypes as ct
from pylab import *
import numpy as np
from matplotlib import colors
from numpy.ma import masked_array
import pickle
import gzip

sweepSize = 5000; dataSaved = True

lib1 = ct.cdll.LoadLibrary('./rosslerSweep.so')
#lib2 = ct.cdll.LoadLibrary('./rosslerLongKneadings.so')
lib2 = ct.cdll.LoadLibrary('./rosslerLongKneadings_LoopCorrection.so')

lib1.sweepRossler.argtypes = [ct.POINTER(ct.c_double),
                          ct.c_double,ct.c_double,ct.c_uint,
                          ct.c_double,ct.c_double,ct.c_uint,
                          ct.c_double,
                          ct.c_double, ct.c_uint, ct.c_uint,
                          ct.c_uint, ct.c_uint]
def sweepRossler (     dt=0.01,
                    N=30000,
                    stride = 1,
                    param1min = 0.8, param1max= 1.1,
                    param2min = 0, param2max = 15,
                    param3 = 0.3,
                    kneadingsStart = 0,
                    kneadingsEnd = 19,
                    sweepSize = 100
                ):
    kneadingsWeightedSumSet = np.zeros(sweepSize*sweepSize, float)
    lib1.sweepRossler(kneadingsWeightedSumSet.ctypes.data_as(ct.POINTER(ct.c_double)),
                  ct.c_double(param1min), ct.c_double(param1max), ct.c_uint(sweepSize),
                  ct.c_double(param2min), ct.c_double(param2max), ct.c_uint(sweepSize),
                  ct.c_double(param3),
                  ct.c_double(dt), ct.c_uint(N), ct.c_uint(stride),
                  ct.c_uint(kneadingsStart), ct.c_uint(kneadingsEnd))
    return np.reshape(kneadingsWeightedSumSet, (sweepSize,sweepSize), 'F')


lib2.sweepRossler.argtypes = [ct.POINTER(ct.c_double),
                          ct.c_double,ct.c_double,ct.c_uint,
                          ct.c_double,ct.c_double,ct.c_uint,
                          ct.c_double,
                          ct.c_double, ct.c_uint, ct.c_uint,
                          ct.c_uint, ct.c_uint]
def sweepRosslerLongKneadings (     dt=0.01,
                    N=30000,
                    stride = 1,
                    param1min = 0.8, param1max = 1.1,
                    param2min = 0, param2max = 15,
                    param3 = 0.3,
                    kneadingsStart = 0,
                    kneadingsEnd = 19,
                    sweepSize = 100
                ):
    kneadingsWeightedSumSet = np.zeros(sweepSize*sweepSize, float)
    lib2.sweepRossler(kneadingsWeightedSumSet.ctypes.data_as(ct.POINTER(ct.c_double)),
                  ct.c_double(param1min), ct.c_double(param1max), ct.c_uint(sweepSize),
                  ct.c_double(param2min), ct.c_double(param2max), ct.c_uint(sweepSize),
                  ct.c_double(param3),
                  ct.c_double(dt), ct.c_uint(N), ct.c_uint(stride),
                  ct.c_uint(kneadingsStart), ct.c_uint(kneadingsEnd))
    return np.reshape(kneadingsWeightedSumSet, (sweepSize,sweepSize), 'F')

colorMapLevels=2**8
blue = np.linspace(0.01, 1, colorMapLevels)
red = 1 - blue
min = 0.; max = .8
green = min + np.random.random(colorMapLevels) * (max - min)
RGB = np.column_stack((red, green, blue))
customColorMap = colors.ListedColormap(RGB)

colorMapLevels=2**8
blue = np.linspace(0.8, 0, colorMapLevels)
red = blue
green = blue
RGB = np.column_stack((red, green, blue))
customGrayColorMap = colors.ListedColormap(RGB)

defaultFontSize = 11;
largeFontSize = 14
matplotlib.rcParams.update({'font.size': defaultFontSize,
                            'axes.labelsize': largeFontSize,
                            'axes.titlesize': largeFontSize,
                            #'text.fontsize': defaultFontSize, //deprecated, uses font.size instead
                            'legend.fontsize': largeFontSize,
                            'xtick.labelsize': defaultFontSize,
                            'ytick.labelsize': defaultFontSize})

#comment these two for rainbow text function to run properly, along with setting fig.dpi = 1200 described near rainbow text
matplotlib.rc('text', usetex=True)
matplotlib.rcParams['text.latex.preamble'] = [r"\usepackage{amsmath}"]

sweepData = [[0.1, 0.2], [0.3, 0.4]]  #### For testing purposes

bbox_props = dict(boxstyle="square,pad=0.2", fc="white", ec="k", lw=.5, alpha=0.)
arrowprops = dict(arrowstyle="-", connectionstyle="arc3", facecolor='white', lw=1.,
                  ec="white")  # ,shrink=0.05, width=defaultFontSize/10.)
bbox_props_fig = dict(boxstyle="square,pad=0.001", fc="white", ec="k", lw=0)
bbox_props_index = dict(boxstyle="square,pad=0.2", fc="white", ec="k", lw=.5)
bbox_props_label = dict(boxstyle="square,pad=0.01", fc="white", ec="k", lw=0)


figSize = 10
fig = figure(figsize=(0.8*figSize, 0.8*figSize))

outputdir1 = 'Output/rosslerSweep'
outputdir2 = 'Output/rosslerSweep_PCLZ_LC'

matplotlib.rcParams['savefig.dpi'] = 2*sweepSize/figSize


"""
#TransientSweeps
#cmin = 20; cmax = 45; amin = 0.2; amax = 0.3; b = 0.3 ;
cmin = 2; cmax = 7; amin = 0.3; amax = 0.5; b = 0.3 ;
kneadingRangesToCompute=[[2,6],[7,14], [2,21], [99,199]];

for kneadingRange in kneadingRangesToCompute :
    fig.clear()
    kneadingRangeStr =  "%d-%d" % (kneadingRange[0], kneadingRange[1]);
    if not dataSaved:
        pickleDataFile1 = gzip.open(outputdir1+"_kneadingRange-"+kneadingRangeStr+'_sweepSize-'+str(sweepSize)+'.pickle.gz', 'wb')
        sweepData = sweepRossler(kneadingsStart=kneadingRange[0], kneadingsEnd=kneadingRange[1], sweepSize=sweepSize, param1min=cmin, param1max=cmax, param2min=amin, param2max=amax, param3=b, N=30000*10)
        pickle.dump(sweepData, pickleDataFile1)
    else:
        pickleDataFile1 = gzip.open(outputdir1+"_kneadingRange-"+kneadingRangeStr+'_sweepSize-'+str(sweepSize)+'.pickle.gz', 'rb')
        sweepData = pickle.load(pickleDataFile1)

    imshow(sweepData,
           extent=[cmin, cmax, amin, amax], aspect=(cmax - cmin)/ (amax-amin),
           cmap=customColorMap,
           origin='lower')
    #colorbar()
    #fig.suptitle("Kneading Range : " + kneadingRangeStr, fontsize=2*figSize)
    fig.savefig(outputdir1+"_kneadingRange-"+kneadingRangeStr+'_sweepSize-'+str(sweepSize)+".jpg")
"""


#LongRangeSweeps
kneadingRange=[999,1999]

sweepRanges = [[3.8, 8.2, 0.25, 0.425, 0.3]]#,[2, 7, 0.3, 0.55, 0.3]]
                    ##,[4.01, 5.84, 0.307, 0.365, 0.3],[3.1, 5.05, 0.427, 0.496, 0.3], [3.755 - 0.05, 3.755 + 0.05,
# 0.478 - 0.002, 0.478 + 0.002, 0.3]
                    ##, [3.46 - 0.05, 3.46 + 0.05, 0.492 - 0.002, 0.492 + 0.002, 0.3], [3.2, 4, 0.47, 0.51, 0.3]]

"""
numberOfSteps = 49
sweepRanges = []
cmin =0.; cmax = 20; amin = -0.; amax = 1.3; b = 0.3 ;
for i in arange(0,numberOfSteps):
    sweepRanges.append([cmin, cmax, amin, amax-(numberOfSteps-i-1)*0.5/(numberOfSteps-1), b-0.28+i*0.02])
"""

for [cmin, cmax, amin, amax, currentB] in sweepRanges :
    fig.clear()
    kneadingRangeStr =  "%d-%d" % (kneadingRange[0], kneadingRange[1]);
    if not dataSaved:
        pickleDataFile1 = gzip.open(outputdir2+"_b-"+str(currentB)+"_cmin-"+str(cmin)+"_cmax-"+str(cmax)+"_amin-"+str(amin)+"_amax-"+str(amax)+"_kneadingRange-"+kneadingRangeStr+'_sweepSize-'+str(sweepSize)+'.pickle.gz', 'wb')
        sweepData = sweepRosslerLongKneadings(kneadingsStart=kneadingRange[0], kneadingsEnd=kneadingRange[1], sweepSize=sweepSize, param1min=cmin, param1max=cmax, param2min=amin, param2max=amax, param3=currentB, N=30000*200)
        pickle.dump(sweepData, pickleDataFile1)
    else:
        pickleDataFile1 = gzip.open(outputdir2+"_b-"+str(currentB)+"_cmin-"+str(cmin)+"_cmax-"+str(cmax)+"_amin-"+str(amin)+"_amax-"+str(amax)+"_kneadingRange-"+kneadingRangeStr+'_sweepSize-'+str(sweepSize)+'.pickle.gz', 'rb')
        sweepData = pickle.load(pickleDataFile1)

    ax = plt.gca();
    ax.set_xticks([]);
    ax.set_yticks([]);
    ax.text(0., -0.02, cmin, ha='left', va='top', bbox=bbox_props_label, transform=ax.transAxes,
            fontsize=defaultFontSize)
    ax.text(1., -0.02, cmax, ha='right', va='top', bbox=bbox_props_label, transform=ax.transAxes,
            fontsize=defaultFontSize)
    ax.text(-0.02, 1., amax, ha='right', va='top', rotation=90, bbox=bbox_props_label, transform=ax.transAxes,
            fontsize=defaultFontSize)
    ax.text(-0.02, 0, amin, ha='right', va='bottom', rotation=90, bbox=bbox_props_label, transform=ax.transAxes,
            fontsize=defaultFontSize)

    sweepDataRunToInfinity = masked_array(sweepData, sweepData != -1.2)
    sweepDataInsufficientN = masked_array(sweepData, sweepData != -1.1)

    sweepDataComputed = masked_array(sweepData, sweepData <= -1.1)
    sweepDataChaotic = masked_array(sweepDataComputed, sweepData <= 0)
    sweepDataPeriodic = masked_array(sweepDataComputed, sweepData > 0)

    imshow(sweepDataPeriodic, extent=[cmin, cmax, amin, amax], aspect=(cmax - cmin)/ (amax-amin),
           #cmap=colors.ListedColormap(['orange']),
           cmap=customColorMap,
           origin='lower')
    imshow(sweepDataChaotic, extent=[cmin, cmax, amin, amax], aspect=(cmax - cmin)/ (amax-amin),
           cmap=customGrayColorMap,
           origin='lower')

    imshow(sweepDataRunToInfinity, extent=[cmin, cmax, amin, amax], aspect=(cmax - cmin)/ (amax-amin),
           cmap=colors.ListedColormap(['white']), origin='lower')
    imshow(sweepDataInsufficientN, extent=[cmin, cmax, amin, amax], aspect=(cmax - cmin)/ (amax-amin),
           cmap=colors.ListedColormap(['white']), origin='lower')
    #fig.suptitle("Kneading Range : " + kneadingRangeStr, fontsize=2 * figSize)

    xlabelX = 0.55;
    xlabelY = 0.07;
    ylabelX = 0.065;
    ylabelY = 0.55;
    xlabel = r'$ \mathrm{c}$ - parameter ';
    ylabel = r'$ \mathrm{a}$ - parameter '
    fig.text(xlabelX, xlabelY, xlabel, ha='center', bbox=bbox_props_fig, color='darkslategray', fontsize=largeFontSize,
             weight='roman')
    fig.text(ylabelX, ylabelY, ylabel, va='center', rotation='vertical', bbox=bbox_props_fig, color='darkslategray',
             fontsize=largeFontSize, weight='roman')

    plt.subplots_adjust(left=0.1, right=0.999, top=0.999, bottom=0.1)

    fig.savefig(outputdir2+"_b-"+str(currentB)+"_cmin-"+str(cmin)+"_cmax-"+str(cmax)+"_amin-"+str(amin)+"_amax-"+str(amax)+"_kneadingRange-"+kneadingRangeStr+'_sweepSize-'+str(sweepSize)+".jpg", bbox_inches='tight')
    #show()



