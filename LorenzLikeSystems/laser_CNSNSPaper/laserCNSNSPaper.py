#All labels adjusted for home workstation

from __future__ import division
import matplotlib

matplotlib.use('Agg')
#matplotlib.use('Qt4Agg')
import ctypes as ct
from pylab import *
import numpy as np
from matplotlib import colors
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from mpl_toolkits.axes_grid.inset_locator import inset_axes
from mpl_toolkits.axes_grid.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid.inset_locator import mark_inset
import matplotlib.patches as patches
import pickle
import gzip
from scipy.integrate import ode
from matplotlib import transforms
import matplotlib.image as mpimg
from numpy.ma import masked_array
import matplotlib.image as mpimg

colorMapLevels = 2 ** 8;
N = 2*30000;
dataSaved = True;
sweepSize = 2000

lib1 = ct.cdll.LoadLibrary('./laserCuda1.so')
lib1.sweepLaser.argtypes = [ct.POINTER(ct.c_double),
                            ct.c_double, ct.c_double, ct.c_uint,
                            ct.c_double, ct.c_double, ct.c_uint,
                            ct.c_double, ct.c_double,
                            ct.c_double, ct.c_uint, ct.c_uint,
                            ct.c_uint, ct.c_uint]


def sweepLaser1(dt=0.01,
                N=N,
                stride=1,
                xmin=3, xmax=5,
                ymin=0.3, ymax=0.7,
                g=50., sigma=1.5,
                kneadingsStart=0,
                kneadingsEnd=16,
                sweepSize=100
                ):
    kneadingsWeightedSumSet = np.zeros(sweepSize * sweepSize, float)
    lib1.sweepLaser(kneadingsWeightedSumSet.ctypes.data_as(ct.POINTER(ct.c_double)),
                    ct.c_double(xmin), ct.c_double(xmax), ct.c_uint(sweepSize),
                    ct.c_double(ymin), ct.c_double(ymax), ct.c_uint(sweepSize),
                    ct.c_double(g), ct.c_double(sigma),
                    ct.c_double(dt), ct.c_uint(N), ct.c_uint(stride),
                    ct.c_uint(kneadingsStart), ct.c_uint(kneadingsEnd))
    # imshow takes fortran style array input
    return np.reshape(kneadingsWeightedSumSet, (sweepSize, sweepSize), 'F')


lib11 = ct.cdll.LoadLibrary('./laserCuda11.so')
lib11.sweepLaser.argtypes = [ct.POINTER(ct.c_double),
                             ct.c_double, ct.c_double, ct.c_uint,
                             ct.c_double, ct.c_double, ct.c_uint,
                             ct.c_double, ct.c_double,
                             ct.c_double, ct.c_uint, ct.c_uint,
                             ct.c_uint, ct.c_uint]


def sweepLaser11(dt=0.01,
                 N=N,
                 stride=1,
                 xmin=3, xmax=5,
                 ymin=0.3, ymax=0.7,
                 g=50., sigma=1.5,
                 kneadingsStart=0,
                 kneadingsEnd=16,
                 sweepSize=100
                 ):
    kneadingsWeightedSumSet = np.zeros(sweepSize * sweepSize, float)
    lib11.sweepLaser(kneadingsWeightedSumSet.ctypes.data_as(ct.POINTER(ct.c_double)),
                     ct.c_double(xmin), ct.c_double(xmax), ct.c_uint(sweepSize),
                     ct.c_double(ymin), ct.c_double(ymax), ct.c_uint(sweepSize),
                     ct.c_double(g), ct.c_double(sigma),
                     ct.c_double(dt), ct.c_uint(N), ct.c_uint(stride),
                     ct.c_uint(kneadingsStart), ct.c_uint(kneadingsEnd))
    # imshow takes fortran style array input
    return np.reshape(kneadingsWeightedSumSet, (sweepSize, sweepSize), 'F')


libPCLZ11 = ct.cdll.LoadLibrary('./laserSweepPCLZ11.so')
libPCLZ11.sweepLaser.argtypes = [ct.POINTER(ct.c_double),
                          ct.c_double,ct.c_double,ct.c_uint,
                          ct.c_double,ct.c_double,ct.c_uint,
                          ct.c_double,ct.c_double,
                          ct.c_double, ct.c_uint, ct.c_uint,
                          ct.c_uint, ct.c_uint]
def sweepLaserPCLZ11 (    dt=0.01,
                    N=N,
                    stride = 1,
                    xmin = 3, xmax = 5,
                    ymin = 0.3, ymax = 0.7,
                    g = 50., sigma = 1.5,
                    kneadingsStart = 0,
                    kneadingsEnd = 16,
                    sweepSize = 100
                ):
    kneadingsWeightedSumSet = np.zeros(sweepSize*sweepSize, float)
    libPCLZ11.sweepLaser(kneadingsWeightedSumSet.ctypes.data_as(ct.POINTER(ct.c_double)),
                  ct.c_double(xmin), ct.c_double(xmax), ct.c_uint(sweepSize),
                  ct.c_double(ymin), ct.c_double(ymax), ct.c_uint(sweepSize),
                  ct.c_double(g), ct.c_double(sigma),
                  ct.c_double(dt), ct.c_uint(N), ct.c_uint(stride),
                  ct.c_uint(kneadingsStart), ct.c_uint(kneadingsEnd))
    #imshow takes fortran style array input
    return np.reshape(kneadingsWeightedSumSet, (sweepSize,sweepSize), 'F')

libPCLZ1 = ct.cdll.LoadLibrary('./laserSweepPCLZ1.so')
libPCLZ1.sweepLaser.argtypes = [ct.POINTER(ct.c_double),
                          ct.c_double,ct.c_double,ct.c_uint,
                          ct.c_double,ct.c_double,ct.c_uint,
                          ct.c_double,ct.c_double,
                          ct.c_double, ct.c_uint, ct.c_uint,
                          ct.c_uint, ct.c_uint]
def sweepLaserPCLZ1 (    dt=0.01,
                    N=N,
                    stride = 1,
                    xmin = 3, xmax = 5,
                    ymin = 0.3, ymax = 0.7,
                    g = 50., sigma = 1.5,
                    kneadingsStart = 0,
                    kneadingsEnd = 16,
                    sweepSize = 100
                ):
    kneadingsWeightedSumSet = np.zeros(sweepSize*sweepSize, float)
    libPCLZ1.sweepLaser(kneadingsWeightedSumSet.ctypes.data_as(ct.POINTER(ct.c_double)),
                  ct.c_double(xmin), ct.c_double(xmax), ct.c_uint(sweepSize),
                  ct.c_double(ymin), ct.c_double(ymax), ct.c_uint(sweepSize),
                  ct.c_double(g), ct.c_double(sigma),
                  ct.c_double(dt), ct.c_uint(N), ct.c_uint(stride),
                  ct.c_uint(kneadingsStart), ct.c_uint(kneadingsEnd))
    #imshow takes fortran style array input
    return np.reshape(kneadingsWeightedSumSet, (sweepSize,sweepSize), 'F')

blue = np.linspace(0.01, 1, colorMapLevels)
red = 1 - blue
min = 0.;
max = .8
green = min + np.random.random(colorMapLevels) * (max - min)
RGB = np.column_stack((red, green, blue))
customColorMap = colors.ListedColormap(RGB)

colorMapLevels=2**8
blue = np.linspace(0.8, 0, colorMapLevels)
red = blue
green = blue
RGB = np.column_stack((red, green, blue))
customGrayColorMap = colors.ListedColormap(RGB)

figsize = 10

matplotlib.rcParams['savefig.dpi'] = 1200  # 2*sweepSize/figSize #300
defaultFontSize = 11;
largeFontSize = 14
matplotlib.rcParams.update({'font.size': defaultFontSize,
                            'axes.labelsize': largeFontSize,
                            'axes.titlesize': largeFontSize,
                            #'text.fontsize': defaultFontSize, //deprecated, uses font.size instead
                            'legend.fontsize': largeFontSize,
                            'xtick.labelsize': defaultFontSize,
                            'ytick.labelsize': defaultFontSize})

#comment these two for rainbow text function to run properly, along with setting fig.dpi = 1200 described near rainbow text
matplotlib.rc('text', usetex=True)
matplotlib.rcParams['text.latex.preamble'] = [r"\usepackage{amsmath}"]

sweepData = [[0.1, 0.2], [0.3, 0.4]]  #### For testing purposes

bbox_props = dict(boxstyle="square,pad=0.2", fc="white", ec="k", lw=.5, alpha=0.)
arrowprops = dict(arrowstyle="-", connectionstyle="arc3", facecolor='white', lw=1.,
                  ec="white")  # ,shrink=0.05, width=defaultFontSize/10.)
bbox_props_fig = dict(boxstyle="square,pad=0.001", fc="white", ec="k", lw=0)
bbox_props_index = dict(boxstyle="square,pad=0.2", fc="white", ec="k", lw=.5)
bbox_props_label = dict(boxstyle="square,pad=0.01", fc="white", ec="k", lw=0)


def computeAndPlotImageOnAxis(ax, kneadingRangesAndParameters, plotAxesData=True):
    imageName = kneadingRangesAndParameters[8]
    print 'Generating Image ' + imageName

    xmin = kneadingRangesAndParameters[4]
    xmax = kneadingRangesAndParameters[5]
    ymin = kneadingRangesAndParameters[6]
    ymax = kneadingRangesAndParameters[7]

    if not dataSaved:
        pickleDataFile1 = gzip.open('3OPL_' + imageName + '.gz', 'wb')
    else:
        pickleDataFile1 = gzip.open('3OPL_' + imageName + '.gz', 'rb')


    numberOfTicks = 4
    xStep = round((xmax - xmin) / numberOfTicks, 2)
    yStep = round((ymax - ymin) / numberOfTicks, 2)

    ax.set_xticks([])
    ax.set_yticks([])
    if plotAxesData:
        ax.text(0., -0.02, xmin, ha='left', va='top', bbox=bbox_props_label, transform=ax.transAxes,
                fontsize=defaultFontSize)
        ax.text(1., -0.02, xmax, ha='right', va='top', bbox=bbox_props_label, transform=ax.transAxes,
                fontsize=defaultFontSize)
        ax.text(-0.02, 1., ymax, ha='right', va='top', rotation=90, bbox=bbox_props_label, transform=ax.transAxes,
                fontsize=defaultFontSize)
        ax.text(-0.02, 0, ymin, ha='right', va='bottom', rotation=90, bbox=bbox_props_label, transform=ax.transAxes,
                fontsize=defaultFontSize)

    sweepType = kneadingRangesAndParameters[9]

    if not dataSaved:
        if sweepType==0:
            sweepData = sweepLaser1(kneadingsStart=kneadingRangesAndParameters[0],
                                     kneadingsEnd=kneadingRangesAndParameters[1],
                                     sweepSize=sweepSize, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax,
                                     g=kneadingRangesAndParameters[2], sigma=kneadingRangesAndParameters[3])
        elif sweepType==1:
            sweepData = sweepLaser11(kneadingsStart=kneadingRangesAndParameters[0],
                                    kneadingsEnd=kneadingRangesAndParameters[1],
                                    sweepSize=sweepSize, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax,
                                    g=kneadingRangesAndParameters[2], sigma=kneadingRangesAndParameters[3])
        elif sweepType==2:
            sweepData = sweepLaserPCLZ11(kneadingsStart=kneadingRangesAndParameters[0],
                                         kneadingsEnd=kneadingRangesAndParameters[1], sweepSize=int(sweepSize), xmin=xmin,
                                         xmax=xmax, ymin=ymin, ymax=ymax, g=kneadingRangesAndParameters[2],
                                         sigma=kneadingRangesAndParameters[3],
                                         N=200*N)
        elif sweepType==3:
            sweepData = sweepLaserPCLZ1(kneadingsStart=kneadingRangesAndParameters[0],
                                         kneadingsEnd=kneadingRangesAndParameters[1], sweepSize=int(sweepSize), xmin=xmin,
                                         xmax=xmax, ymin=ymin, ymax=ymax, g=kneadingRangesAndParameters[2],
                                         sigma=kneadingRangesAndParameters[3],
                                         N=200*N)
        pickle.dump(sweepData, pickleDataFile1)
    else:
        sweepData = pickle.load(pickleDataFile1)

    if sweepType<2:
        ax.imshow(sweepData, extent=[xmin, xmax, ymin, ymax], aspect=(xmax - xmin) / (ymax - ymin),
              vmin=-0.1, vmax=1, cmap=customColorMap, origin='lower')
    else:
        #replacing insufficient N data with right sided loops
        sweepData[sweepData == -1.1] = 0.

        sweepDataInsufficientN = masked_array(sweepData, sweepData != -1.1)
        sweepDataComputed = masked_array(sweepData, sweepData <= -1.1)
        sweepDataChaotic = masked_array(sweepDataComputed, sweepData <= 0)
        sweepDataPeriodic = masked_array(sweepDataComputed, sweepData > 0)
        imshow(sweepDataPeriodic,
               extent=[xmin, xmax, ymin, ymax], aspect=(xmax - xmin) / (ymax - ymin),
               cmap=customColorMap,
               origin='lower',
               vmin=-1., vmax=0.1) #making vmax 0.1 instead of 0. to match the colormaps for short and long kneadings
        imshow(sweepDataChaotic,
               extent=[xmin, xmax, ymin, ymax], aspect=(xmax - xmin) / (ymax - ymin),
               cmap=customGrayColorMap,
               origin='lower')
        imshow(sweepDataInsufficientN,
               extent=[xmin, xmax, ymin, ymax], aspect=(xmax - xmin) / (ymax - ymin),
               cmap=colors.ListedColormap(['white']),
               origin='lower')

    ax.autoscale(False)
    ax.set_adjustable('box-forced')

    pickleDataFile1.close()

"""
# Single Images Large
kneadingRangesAndParametersArray = [[5, 12, 50., 1.5, 3., 5., 0.2, .7, 'Image1', 1],
                                    [2, 8, 50., 10, 0.94, 1.17, 0.08, 0.24, 'Image3', 1],
                                    ]

for kneadingRangesAndParameters in kneadingRangesAndParametersArray:
    figSizeX = 0.8 * figsize;
    figSizeY = 0.8 * figsize;
    gridX = 3;
    gridY = 3;
    imageName = kneadingRangesAndParameters[8]

    fig = plt.figure(figsize=(figSizeX, figSizeY))
    gs1 = gridspec.GridSpec(gridX, gridY)
    gs1.update(wspace=0.025, hspace=0.025)  # set the spacing between axes.

    ax = plt.subplot(gs1[:, :])
    plt.axis('on')

    computeAndPlotImageOnAxis(ax, kneadingRangesAndParameters)

    if imageName == 'Image1':
        pf1 = np.loadtxt('laser_pf_1.5_50.dat')
        pf2 = np.loadtxt('laser_hopf1_1.5_50.dat')
        pf3 = np.loadtxt('laser_hopf2_1.5_50.dat')
        ax.plot(pf1[:, 0], pf1[:, 1], 'w-', linewidth=1.0)
        ax.plot(pf2[:, 0], pf2[:, 1], 'w-', linewidth=1.0)
        ax.plot(pf3[:, 0], pf3[:, 1], 'w-', linewidth=1.0)

        ax.plot(3.95902, 0.438616, 'o', color='white', ms=8)
        ax.annotate(r'$\boldsymbol{T_0}$', xy=(3.95902, 0.438616), xytext=(3.95902, 0.48), arrowprops=arrowprops,
                    bbox=bbox_props, color='white')
        ax.plot(3.37386, 0.269558, 'o', color='white', ms=8)
        ax.annotate(r'$\boldsymbol{S}$', xy=(3.37386, 0.269558), xytext=(3.29, 0.269558), bbox=bbox_props, color='white')
        ax.plot(3.827, 0.5189, 'o', color='white', ms=8)
        ax.annotate(r'$\boldsymbol{IF_1}$', xy=(3.827, 0.5189), xytext=(3.75, 0.53), bbox=bbox_props, color='white')
        ax.plot(3.98, .4003, 'o', color='white', ms=8)
        ax.annotate(r'$\boldsymbol{IF_2}$', xy=(3.98, .4003), xytext=(3.96, 0.375), bbox=bbox_props, color='white')
        ax.annotate(r'$\boldsymbol{PF}$', xy=(4.47131, 0.535383), xytext=(4.36, 0.535383), bbox=bbox_props,
                    color='white')
        ax.annotate(r'$\boldsymbol{AH_0}$', xy=(4.63525, 0.535383), xytext=(4.66, 0.535383), bbox=bbox_props,
                    color='white')
        ax.annotate(r'$\boldsymbol{AH_{1,2}}$', xy=(3.26685, 0.409016), xytext=(3.1, 0.41), bbox=bbox_props,
                    color='white')
        ax.add_patch(patches.Rectangle((3.46, .33), 3.8 - 3.46, 0.36 - .33, fill=False, linewidth=1., color='white'))


    elif imageName == 'Image3':

        ax.plot(1.157, 0.1935, 'o', color='white', ms=8)
        ax.annotate(r'$\boldsymbol{IF_2}$', xy=(1.157, 0.1935), xytext=(1.1585, 0.19), bbox=bbox_props, color='white')
        # Main T-point
        ax.plot(1.10054, 0.192854, 'o', color='white', ms=8)
        ax.annotate(r'$\boldsymbol{T_0}$', xy=(1.10054, 0.192854), xytext=(1.11, 0.205), arrowprops=arrowprops,
                    bbox=bbox_props, color='white')
        # lower pair T-point 1.05398, .0885933
        ax.plot(1.05398, .0885933, 'o', color='white', ms=8)
        ax.annotate(r'$\boldsymbol{T_2^2}$', xy=(1.05398, .0885933), xytext=(1.11, .0885933), arrowprops=arrowprops,
                    bbox=bbox_props, color='white')
        # upper pair T-point 1.06744, .122637
        ax.plot(1.06744, .122637, 'o', color='white', ms=8)
        ax.annotate(r'$\boldsymbol{T_2^1}$', xy=(1.06744, .122637), xytext=(1.115, .134), arrowprops=arrowprops,
                    bbox=bbox_props, color='white')
        # middle T-point 1.07446, .138992
        ax.plot(1.07446, .138992, 'o', color='white', ms=8)
        ax.annotate(r'$\boldsymbol{T_1}$', xy=(1.07446, .138992), xytext=(1.107, .145), arrowprops=arrowprops,
                    bbox=bbox_props, color='white')
        # false T-point 1.13485, .109461
        ax.plot(1.13485, .109461, 'o', color='white', ms=8)
        ax.annotate(r'$\boldsymbol{T_g}$', xy=(1.13485, .109461), xytext=(1.14, .109461), bbox=bbox_props,
                    color='white')
        # circle near false T-point 1.10738, .106295
        ax.plot(1.10738, .106295, 'o', color='white', ms=8)
        ax.annotate(r'$\boldsymbol{C}$', xy=(1.10738, .106295), xytext=(1.109, .103295), arrowprops=arrowprops,
                    bbox=bbox_props, color='white')
        # saddle pair 1.03528, .0975449
        ax.plot(1.03528, .0975449, 'o', color='white', ms=8)
        ax.annotate(r'$\boldsymbol{S}$', xy=(1.03528, .0975449), xytext=(1.029, .096), bbox=bbox_props, color='white')

    xlabelX = 0.55;
    xlabelY = 0.07;
    ylabelX = 0.065;
    ylabelY = 0.55;
    xlabel = r'$ \mathrm{a}$ - parameter ';
    ylabel = r'$ \mathrm{b}$ - parameter '
    fig.text(xlabelX, xlabelY, xlabel, ha='center', bbox=bbox_props_fig, color='darkslategray', fontsize=largeFontSize,
             weight='roman')
    fig.text(ylabelX, ylabelY, ylabel, va='center', rotation='vertical', bbox=bbox_props_fig, color='darkslategray',
             fontsize=largeFontSize, weight='roman')

    plt.subplots_adjust(left=0.1, right=0.999, top=0.999, bottom=0.1)
    fig.savefig('3OPL_' + imageName + '.jpg', bbox_inches='tight')
"""

"""
# Single Images Small
kneadingRangesAndParametersArray = [[5, 12, 50., 1.5, 3.70, 3.90, 0.47, 0.53, 'Image1_IF1', 0],
                                    [3, 10, 50., 1.5, 3.46, 3.8, 0.33, 0.36, 'Image1_T1T2', 1],
                                    [3, 10, 50., 1.5, 3.75, 4.05, 0.38, .45, 'Image1_IFs_Curve_Zoom', 1],
                                    [3, 10, 50., 2., 2., 4., 0.1, 0.8, 'Image2', 1],
                                    [16, 23, 50., 10., 1.15, 1.16, 0.188, 0.194, 'Image3_IF2', 1],
                                    [4, 11, 50., 1.344, 3.68, 3.86, 0.23, 0.255, 'Image4_Saddle3D_TopSurface',1],
                                    [4, 11, 50., 1.374985, 3.57, 3.86, 0.235, 0.27, 'Image4_Saddle3D_BottomSurface', 1],
                                    [2, 9, 50., 1.2, 4.4, 4.65, 0.18, .26, 'Image7_1', 1],
                                    [1000, 1999, 50., 1.5, 3.70, 3.80, 0.44, 0.50, 'Image1_IF1_PCLZ', 3],
                                    [1000, 1999, 50., 10, 1.127, 1.160, 0.1655, 0.1956, 'Image3_IF2_PCLZ', 2],
                                    [1000, 1999, 50., 1.2, 4.4, 4.65, 0.18, .26, 'Image7_1_IF2_PCLZ', 2]
                                    ]

for kneadingRangesAndParameters in kneadingRangesAndParametersArray:
    figSizeX = 0.5*figsize;
    figSizeY = 0.5*figsize;
    gridX = 3;
    gridY = 3;
    imageName = kneadingRangesAndParameters[8]

    fig = plt.figure(figsize=(figSizeX, figSizeY))
    gs1 = gridspec.GridSpec(gridX, gridY)
    gs1.update(wspace=0.03, hspace=0.03)  # set the spacing between axes.

    ax = plt.subplot(gs1[:, :])
    plt.axis('on')

    computeAndPlotImageOnAxis(ax, kneadingRangesAndParameters)

    if imageName == 'Image1':
        pf1 = np.loadtxt('laser_pf_1.5_50.dat')
        pf2 = np.loadtxt('laser_hopf1_1.5_50.dat')
        pf3 = np.loadtxt('laser_hopf2_1.5_50.dat')
        ax.plot(pf1[:, 0], pf1[:, 1], 'w-', linewidth=1.0)
        ax.plot(pf2[:, 0], pf2[:, 1], 'w-', linewidth=1.0)
        ax.plot(pf3[:, 0], pf3[:, 1], 'w-', linewidth=1.0)

        ax.plot(3.95902, 0.438616, 'o', color='white', ms=8)
        ax.annotate(r'$\boldsymbol{T_0}$', xy=(3.95902, 0.438616), xytext=(3.95902, 0.50), arrowprops=arrowprops, bbox=bbox_props, color='white')
        ax.plot(3.37386, 0.269558, 'o', color='white', ms=8)
        ax.annotate(r'$\boldsymbol{S}$', xy=(3.37386, 0.269558), xytext=(3.27, 0.269558), bbox=bbox_props,
                    color='white')
        ax.plot(3.827, 0.5189, 'o', color='white', ms=8)
        ax.annotate(r'$\boldsymbol{IF_1}$', xy=(3.827, 0.5189), xytext=(3.67, 0.534), bbox=bbox_props, color='white')
        ax.plot(3.98, .4003, 'o', color='white', ms=8)
        ax.annotate(r'$\boldsymbol{IF_2}$', xy=(3.98, .4003), xytext=(3.96, 0.36), bbox=bbox_props, color='white')
        ax.add_patch(patches.Rectangle((3.46, .33), 3.8 - 3.46, 0.36 - .33, fill=False, linewidth=1., color='white'))

    elif imageName == 'Image2':
        ax.annotate(r'$\boldsymbol{H_0}$', xy=(3.45, .53), xytext=(3.45, 0.53), bbox=bbox_props, color='white')

    elif imageName == 'Image1_IFs_Curve_Zoom':
        ax.plot(3.9548, 0.4363, 'o', color='white', ms=8)
        ax.annotate(r'$\boldsymbol{T_0}$', xy=(3.9548, 0.4363), xytext=(4.02, 0.4363), arrowprops=arrowprops, bbox=bbox_props, color='white')
        ax.plot(3.98, .4003, 'o', color='white', ms=8)
        ax.annotate(r'$\boldsymbol{IF_2}$', xy=(3.980, 0.4003), xytext=(3.99, 0.399), bbox=bbox_props, color='white')
        #ax.plot(3.97, .4225, 'o', color='white', ms = 5)
        ax.plot(3.969, .4233, 'o', color='white', ms=8)
        ax.plot(3.967, .4250, 'o', color='white', ms=8) #interpolated
	ax.plot(3.961, .431, 'o', color='white', ms=8)
        ax.plot(3.957, .4343, 'o', color='white', ms=8)
        ax.plot(3.953, .4379, 'o', color='white', ms=8)
        ax.plot(3.948, .4411, 'o', color='white', ms=8)
        #ax.plot(3.932, .4523, 'o', color='white', ms = 5) #not visible
        #ax.plot(3.929, .4541, 'o', color='white', ms = 5) #not visible
        ax.text(1., 1., chr(0 + ord('a')), ha='right', va='top', bbox=bbox_props_index, transform=ax.transAxes,
                fontsize=largeFontSize)

    elif imageName == 'Image1_T1T2':

        ax.plot(3.68179, .351831, 'o', color='white', ms=8)
        ax.annotate(r'$\boldsymbol{T_1}$', xy=(3.68179, .351831), xytext=(3.59, .356), arrowprops=arrowprops,
                             bbox=bbox_props, color='white')
        ax.plot(3.6516, .341926, 'o', color='white', ms=8)
        ax.annotate(r'$\boldsymbol{T_2}$', xy=(3.6516, .341926), xytext=(3.55, .343), arrowprops=arrowprops,
                             bbox=bbox_props, color='white')
        ax.text(1., 1., chr(1 + ord('a')), ha='right', va='top', bbox=bbox_props_index, transform=ax.transAxes,
                fontsize=largeFontSize)

    elif imageName == 'Image1_IF1':
        ax.plot(3.827, 0.5189, 'o', color='white', ms=8)
        ax.annotate(r'$\boldsymbol{IF_1}$', xy=(3.827, 0.5189), xytext=(3.809, 0.52), bbox=bbox_props,
                             color='white')
        ax.text(1., 1., chr(0 + ord('a')), ha='right', va='top', bbox=bbox_props_index,
                         transform=ax.transAxes, fontsize=largeFontSize)

    elif imageName == 'Image1_IF1_PCLZ':
        ax.text(1., 1., chr(1 + ord('a')), ha='right', va='top', bbox=bbox_props_index,
                transform=ax.transAxes, fontsize=largeFontSize)

    elif imageName == 'Image3':
        # A=0 point
        ax.plot(1.157, 0.1935, 'o', color='white', ms=8)
        ax.annotate(r'$\boldsymbol{IF_2}$', xy=(1.157, 0.1935), xytext=(1.149, 0.175), bbox=bbox_props, color='white')
        # Main T-point
        ax.plot(1.10054, 0.192854, 'o', color='white', ms=8)
        ax.annotate(r'$\boldsymbol{T_0}$', xy=(1.10054, 0.192854), xytext=(1.108, 0.202), bbox=bbox_props, color='white')
        # lower pair T-point 1.05398, .0885933
        ax.plot(1.05398, .0885933, 'o', color='white', ms=8)
        #ax.annotate(r'$\boldsymbol{T_2^2}$', xy=(1.05398, .0885933), xytext=(0.96, .0885933), arrowprops=arrowprops, bbox=bbox_props, color='white')
        # upper pair T-point 1.06744, .122637
        ax.plot(1.06744, .122637, 'o', color='white', ms=8)
        #ax.annotate(r'$\boldsymbol{T_2^1}$', xy=(1.06744, .122637), xytext=(0.96, .122637), arrowprops=arrowprops, bbox=bbox_props, color='white')
        # middle T-point 1.07446, .138992
        ax.plot(1.07446, .138992, 'o', color='white', ms=8)
        ax.annotate(r'$\boldsymbol{T_1}$', xy=(1.07446, .138992), xytext=(1.10, .139), arrowprops=arrowprops, bbox=bbox_props, color='white')
        # false T-point 1.13485, .109461
        ax.plot(1.13485, .109461, 'o', color='white', ms=8)
        ax.annotate(r'$\boldsymbol{T_g}$', xy=(1.13485, .109461), xytext=(1.14, .109461), bbox=bbox_props, color='white')
        # circle near false T-point 1.10738, .106295
        ax.plot(1.10738, .106295, 'o', color='white', ms=8)
        ax.annotate(r'$\boldsymbol{C}$', xy=(1.10738, .106295), xytext=(1.111, .103295), arrowprops=arrowprops, bbox=bbox_props, color='white')
        # saddle pair 1.03528, .0975449
        ax.plot(1.03528, .0975449, 'o', color='white', ms=8)
        ax.annotate(r'$\boldsymbol{S}$', xy=(1.03528, .0975449), xytext=(1.018, .096), bbox=bbox_props, color='white')

    elif imageName == 'Image3_IF2':
        ax.plot(1.1569, 0.1935, 'o', color='white', ms=8)
        ax.annotate(r'$\boldsymbol{IF_2}$', xy=(1.1569, 0.1935), xytext=(1.1571, 0.1932), bbox=bbox_props, color='white')
        ax.text(1., 1., chr(0 + ord('a')), ha='right', va='top', bbox=bbox_props_index, transform=ax.transAxes,
                fontsize=largeFontSize)

    elif imageName == 'Image3_IF2_PCLZ':
        ax.plot(1.157, 0.1935, 'o', color='white', ms=8)
        ax.annotate(r'$\boldsymbol{IF_2}$', xy=(1.157, 0.1935), xytext=(1.1565, 0.19), bbox=bbox_props, color='white')
        ax.text(1., 1., chr(1 + ord('a')), ha='right', va='top', bbox=bbox_props_index, transform=ax.transAxes,
                fontsize=largeFontSize)

    elif imageName == 'Image7_1':
        ax.text(1., 1., chr(0 + ord('a')), ha='right', va='top', bbox=bbox_props_index, transform=ax.transAxes, fontsize=largeFontSize)
        ax.plot(4.612, 0.2046, 'o', color='white', ms=8)
        ax.annotate(r'$\boldsymbol{IF_2}$', xy=(4.616, 0.201), xytext=(4.616, 0.201), bbox=bbox_props, color='white')
        ax.plot(4.589, 0.2163, 'o', color='white', ms=8)
        ax.plot(4.577, 0.223, 'o', color='white', ms=8)
        ax.plot(4.57, 0.2252, 'o', color='white', ms=8)
        ax.plot(4.557, 0.2298, 'o', color='white', ms=8)
        ax.plot(4.549, 0.2344, 'o', color='white', ms=8)
        ax.plot(4.499, 0.2516, 'o', color='white', ms=8)
        ax.plot(4.333, 0.2956, 'o', color='white', ms=8)

    elif imageName == 'Image7_1_IF2_PCLZ':
        ax.text(1., 1., chr(1 + ord('a')), ha='right', va='top', bbox=bbox_props_index, transform=ax.transAxes, fontsize=largeFontSize)
        ax.plot(4.612, 0.2046, 'o', color='white', ms=8)
        ax.annotate(r'$\boldsymbol{IF_2}$', xy=(4.616, 0.201), xytext=(4.616, 0.201), bbox=bbox_props, color='white')
        ax.plot(4.589, 0.2163, 'o', color='white', ms=8)
        ax.plot(4.577, 0.223, 'o', color='white', ms=8)
        ax.plot(4.57, 0.2252, 'o', color='white', ms=8)
        ax.plot(4.557, 0.2298, 'o', color='white', ms=8)
        ax.plot(4.549, 0.2344, 'o', color='white', ms=8)
        ax.plot(4.499, 0.2516, 'o', color='white', ms=8)
        ax.plot(4.333, 0.2956, 'o', color='white', ms=8)

    elif imageName == 'Image4_Saddle3D_TopSurface':
        ax.text(1., 1., chr(1 + ord('a')), ha='right', va='top', bbox=bbox_props_index, transform=ax.transAxes, fontsize=largeFontSize)

    elif imageName == 'Image4_Saddle3D_BottomSurface':
        ax.text(1., 1., chr(0 + ord('a')), ha='right', va='top', bbox=bbox_props_index, transform=ax.transAxes, fontsize=largeFontSize)

    xlabelX = 0.55;
    xlabelY = 0.065;
    ylabelX = 0.055;
    ylabelY = 0.55;

    xlabel = r'$ \mathrm{a}$ - parameter ';
    ylabel = r'$ \mathrm{b}$ - parameter '
    fig.text(xlabelX, xlabelY, xlabel, ha='center', bbox=bbox_props_fig, color='darkslategray', fontsize=largeFontSize,
             weight='roman')
    fig.text(ylabelX, ylabelY, ylabel, va='center', rotation='vertical', bbox=bbox_props_fig, color='darkslategray',
             fontsize=largeFontSize, weight='roman')

    plt.subplots_adjust(left=0.1, right=0.999, top=0.999, bottom=0.1)
    fig.savefig('3OPL_' + imageName + '.jpg', bbox_inches='tight')
"""

"""
# Grid Images Sigma Changes - Short term
kneadingRangesAndParametersArray5 = [[4, 11, 50., 1.2, 3.8, 4.8, 0.13, .31, 'Image5_1', 1],
                                    [4, 11, 50., 1.32, 3.36, 4.76, 0.15, .47, 'Image5_2', 1],
                                    [4, 11, 50., 1.5, 2.7, 4.7, 0.18, .7, 'Image5_3', 1],
                                    [4, 11, 50., 1.72, 2.39, 4.09, 0.15, .7, 'Image5_4', 1],
                                    [4, 11, 50., 1.76, 2.33, 4.03, 0.14, .74, 'Image5_5', 1],
                                    [4, 11, 50., 2., 2., 3.6, 0.1, .76, 'Image5_6', 1],
                                    [4, 11, 50., 6., 0.5, 1.7, 0.01, .4, 'Image5_7', 1],
                                    [4, 11, 50., 7.375, 0.5, 1.6, 0.01, .41, 'Image5_8', 1],
                                    [4, 11, 50., 10., 0.5, 1.25, 0.01, .35, 'Image5_9', 1]]

ImageDataSet = [kneadingRangesAndParametersArray5]#,
ImageNames = ['Image5']

for j in range(len(ImageDataSet)):
    figSizeX = 1.0 * figsize;
    figSizeY = 1.0 * figsize;
    gridX = 3;
    gridY = 3;
    imageName = ImageNames[j]
    fig = plt.figure(figsize=(figSizeX, figSizeY))
    gs1 = gridspec.GridSpec(gridX, gridY)
    gs1.update(wspace=0.02, hspace=0.02)  # set the spacing between axes.

    kneadingRangesAndParametersArray = ImageDataSet[j]
    for i in range(len(kneadingRangesAndParametersArray)):
        ax = plt.subplot(gs1[int(i / 3), i % 3])
        plt.axis('on')
        computeAndPlotImageOnAxis(ax, kneadingRangesAndParametersArray[i], plotAxesData=False)
        if i == 2:
            ax.plot(3.827, 0.5189, 'o', color='white', ms=8)
            ax.annotate(r'$\boldsymbol{IF_1}$', xy=(3.827, 0.5189), xytext=(3.55, 0.534), bbox=bbox_props,
                        color='white')
            ax.plot(3.98, .4003, 'o', color='white', ms=8)
            ax.annotate(r'$\boldsymbol{IF_2}$', xy=(3.98, .4003), xytext=(3.96, 0.35), bbox=bbox_props, color='white')
            ax.annotate(r'$\boldsymbol{H_0}$', xy=(4.15682, 0.518533), xytext=(4.18, 0.518533), bbox=bbox_props,
                        color='white')
        ax.text(1., 1., chr(i + ord('a')), ha='right', va='top', bbox=bbox_props_index, transform=ax.transAxes,
                fontsize=defaultFontSize)

    xlabelX = 0.55;
    xlabelY = 0.072;
    ylabelX = 0.066;
    ylabelY = 0.55;
    xlabel = r'$ \mathrm{a}$ - parameter ';
    ylabel = r'$ \mathrm{b}$ - parameter '
    fig.text(xlabelX, xlabelY, xlabel, ha='center', bbox=bbox_props_fig, color='darkslategray', fontsize=largeFontSize,
             weight='roman')
    fig.text(ylabelX, ylabelY, ylabel, va='center', rotation='vertical', bbox=bbox_props_fig, color='darkslategray',
             fontsize=largeFontSize, weight='roman')

    plt.subplots_adjust(left=0.1, right=0.999, top=0.999, bottom=0.1)
    fig.savefig('3OPL_' + imageName + '.jpg', bbox_inches='tight')
"""

"""
# Grid Images Saddle S1
kneadingRangesAndParametersArray4_S = [[4, 11, 50., 1.372, 3.384, 4.02, 0.2172, .2852, 'Image4_S5', 1],
                                    [4, 11, 50., 1.352, 3.444, 4.07, 0.2152, .2782, 'Image4_S6', 1],
                                    [4, 11, 50., 1.288, 3.648, 4.224, 0.204, .254, 'Image4_S8', 1],
                                    [4, 11, 50., 1.264, 3.744, 4.272, 0.192, .242, 'Image4_S9', 1]]

# Grid Images Sigma Changes - Long term
kneadingRangesAndParametersArray6 = [[999, 1999, 50., 1.2, 3.8, 4.8, 0.13, .31, 'Image6_1', 2],
                                    [999, 1999, 50., 1.5, 2.7, 4.7, 0.18, .7, 'Image6_3', 2],
                                    [999, 1999, 50., 7.375, 0.5, 1.6, 0.01, .41, 'Image6_8', 2],
                                    [999, 1999, 50., 10., 0.5, 1.25, 0.01, .35, 'Image6_9', 2]]

# Grid Images Semi-annuli
kneadingRangesAndParametersArray9 = [[1, 9, 50., 11.1253, 1.06, 1.101464, 0.0693, .08932, 'Image9_1', 1],
                                    [1, 9, 50., 11.2459, 1.06, 1.097096, 0.0602, .08048, 'Image9_2', 1],
                                    [1, 9, 50., 11.4501, 1.06, 1.089704, 0.0448, .06552, 'Image9_3', 1],
                                    [1, 9, 50., 11.5522, 1.06, 1.086008, 0.0371, .05804, 'Image9_4', 1],
                                    ]

ImageDataSet = [kneadingRangesAndParametersArray9,kneadingRangesAndParametersArray4_S, kneadingRangesAndParametersArray6]
ImageNames = ['Image9','Image4_S', 'Image6']

for j in range(len(ImageDataSet)):
    figSizeX = 0.8 * figsize;
    figSizeY = 0.8 * figsize;
    gridX = 2;
    gridY = 2;
    imageName = ImageNames[j]
    fig = plt.figure(figsize=(figSizeX, figSizeY))
    gs1 = gridspec.GridSpec(gridX, gridY)
    gs1.update(wspace=0.02, hspace=0.02)  # set the spacing between axes.

    kneadingRangesAndParametersArray = ImageDataSet[j]
    for i in range(len(kneadingRangesAndParametersArray)):
        ax = plt.subplot(gs1[int(i / 2), i % 2])
        plt.axis('on')
        computeAndPlotImageOnAxis(ax, kneadingRangesAndParametersArray[i], plotAxesData=False)

        if j==2 and i == 1:
            ax.plot(3.827, 0.5189, 'o', color='white', ms=8)
            ax.annotate(r'$\boldsymbol{IF_1}$', xy=(3.827, 0.5189), xytext=(3.6, 0.52), bbox=bbox_props, color='white')
            ax.plot(3.98, .4003, 'o', color='white', ms=8)
            ax.annotate(r'$\boldsymbol{IF_2}$', xy=(3.98, .4003), xytext=(4., 0.385), bbox=bbox_props, color='white')
            ax.plot(3.95902, 0.438616, 'o', color='white', ms=8)
            ax.annotate(r'$\boldsymbol{T_0}$', xy=(3.95902, 0.438616), xytext=(4., 0.435), bbox=bbox_props, color='white')
            ax.add_patch( patches.Rectangle((3.70, 0.44), 3.80 - 3.70, 0.50 - 0.44, fill=False, linewidth=1., color='white'))

        if j==1 and i ==0:
            ax.plot(3.7, 0.252, 'o', color='white', ms=8)
            ax.annotate(r'$\boldsymbol{S}$', xy=(3.7, 0.252), xytext=(3.7, 0.248), bbox=bbox_props, color='white')

        ax.text(1., 1., chr(i + ord('a')), ha='right', va='top', bbox=bbox_props_index, transform=ax.transAxes,
                fontsize=defaultFontSize)

    xlabelX = 0.55;
    xlabelY = 0.07;
    ylabelX = 0.065;
    ylabelY = 0.55;

    xlabel = r'$ \mathrm{a}$ - parameter ';
    ylabel = r'$ \mathrm{b}$ - parameter '
    fig.text(xlabelX, xlabelY, xlabel, ha='center', bbox=bbox_props_fig, color='darkslategray', fontsize=largeFontSize,
             weight='roman')
    fig.text(ylabelX, ylabelY, ylabel, va='center', rotation='vertical', bbox=bbox_props_fig, color='darkslategray',
             fontsize=largeFontSize, weight='roman')

    plt.subplots_adjust(left=0.1, right=0.999, top=0.999, bottom=0.1)
    fig.savefig('3OPL_' + imageName + '.jpg', bbox_inches='tight')
"""
"""
#HomMerge Images with Annotations
HomMergeImages = ['3OPL_Image8_HomMerge_Full', '3OPL_Image8_HomMerge_Zoom1', '3OPL_Image8_HomMerge_Zoom2']

for imageName in HomMergeImages:
    print imageName

    fig = plt.figure(figsize=(0.5*figsize, 0.5*figsize))
    gridX = 3;
    gridY = 3;
    gs1 = gridspec.GridSpec(gridX, gridY)
    gs1.update(wspace=0.0, hspace=0.0)  # set the spacing between axes.
    ax = plt.subplot(gs1[:, :])
    ax.set_xticks([]);
    ax.set_yticks([]);

    img = mpimg.imread(imageName+'.jpg')
    ax.imshow(img)
    ax.autoscale(False)
    ax.set_adjustable('box-forced')

    if imageName == '3OPL_Image8_HomMerge_Full':
        ax.text(2075, 1000, r'$\boldsymbol{P}$', va='center', bbox=bbox_props, color='white')
        ax.text(1700, 1950, r'$\boldsymbol{H_0}$', va='center', bbox=bbox_props, color='white')
        ax.text(1000, 2375, r'$\boldsymbol{a}$', va='center', rotation=350, bbox=bbox_props, color='white', fontsize=largeFontSize)
        ax.text(2300, 2300, r'$\boldsymbol{b}$', va='center', rotation=0, bbox=bbox_props, color='white', fontsize=largeFontSize)
        ax.text(570, 250, r'$\boldsymbol{\sigma}$', va='center', rotation=0, bbox=bbox_props, color='white',
                fontsize=largeFontSize)

    if imageName == '3OPL_Image8_HomMerge_Zoom1':
        ax.text(2175, 1225, r'$\boldsymbol{P}$', va='center', bbox=bbox_props, color='white')
        ax.text(2500, 2300, r'$\boldsymbol{H_0}$', va='center', bbox=bbox_props, color='white')
        ax.text(800, 3100, r'$\boldsymbol{a}$', va='center', rotation=350, bbox=bbox_props, color='white', fontsize=largeFontSize)
        ax.text(150, 2200, r'$\boldsymbol{b}$', va='center', rotation=350, bbox=bbox_props, color='white', fontsize=largeFontSize)
        ax.text(1000, 200, r'$\boldsymbol{\sigma}$', va='center', rotation=0, bbox=bbox_props, color='white', fontsize=largeFontSize)

    if imageName == '3OPL_Image8_HomMerge_Zoom2':
        ax.text(1850, 1250, r'$\boldsymbol{P}$', va='center', bbox=bbox_props, color='white')
        ax.text(2500, 2300, r'$\boldsymbol{H_0}$', va='center', bbox=bbox_props, color='white')
        ax.text(1000, 3000, r'$\boldsymbol{a}$', va='center', rotation=350, bbox=bbox_props, color='white', fontsize=largeFontSize)
        ax.text(250, 1100, r'$\boldsymbol{b}$', va='center', rotation=340, bbox=bbox_props, color='white', fontsize=largeFontSize)
        ax.text(1450, 500, r'$\boldsymbol{\sigma}$', va='center', rotation=0, bbox=bbox_props, color='white', fontsize=largeFontSize)

    fig.savefig(imageName+'_Annotated.jpg', bbox_inches='tight', pad_inches=0.0)
"""

"""
# Grid Images 3D Saddle S1 : Image4_Saddle3D
#saddle3DImages = ['3dSaddle1', '3dSaddle2', '3dSaddle3', '3dSaddle4']

figSizeX = 1.0*figsize;
figSizeY = 2.0*figsize/3.0;
gridX = 2;
gridY = 3;
fig = plt.figure(figsize=(figSizeX, figSizeY))
gs1 = gridspec.GridSpec(gridX, gridY)
gs1.update(wspace=0.03, hspace=0.03)  # set the spacing between axes.

ax1Left = plt.subplot(gs1[0, 0])
ax2Left = plt.subplot(gs1[1, 0])
axRight = plt.subplot(gs1[0:2, 1:3])

ax1Left.set_xticks([]); ax1Left.set_yticks([]);
ax2Left.set_xticks([]); ax2Left.set_yticks([]);
axRight.set_xticks([]); axRight.set_yticks([])

img = mpimg.imread('3dSaddle2.jpg')
ax1Left.imshow(img)

img = mpimg.imread('3dSaddle3.jpg')
ax2Left.imshow(img)

img = mpimg.imread('3dSaddle4.jpg')
axRight.imshow(img)

ax1Left.autoscale(False)
ax1Left.set_adjustable('box-forced')
ax2Left.autoscale(False)
ax2Left.set_adjustable('box-forced')
axRight.autoscale(False)
axRight.set_adjustable('box-forced')

axRight.text(1200, 300, r'$\boldsymbol{b}$', va='center', bbox=bbox_props, color='white')
axRight.text(350, 1200, r'$\boldsymbol{a}$', va='center', bbox=bbox_props, color='white')
axRight.text(1050, 2700, r'$\boldsymbol{\sigma}$', va='center', bbox=bbox_props, color='white')

ax1Left.text(1500, 3100, r'$\boldsymbol{b}$', va='center', bbox=bbox_props, color='white')
ax1Left.text(250, 600, r'$\boldsymbol{a}$', va='center', bbox=bbox_props, color='white')
ax1Left.text(400, 2200, r'$\boldsymbol{\sigma}$', va='center', bbox=bbox_props, color='white')

ax2Left.text(1200, 250, r'$\boldsymbol{b}$', va='center', bbox=bbox_props, color='white')
ax2Left.text(260, 1100, r'$\boldsymbol{a}$', va='center', bbox=bbox_props, color='white')
ax2Left.text(800, 2600, r'$\boldsymbol{\sigma}$', va='center', bbox=bbox_props, color='white')

################# Entire image ########################
ax1Left.text(1., 1., chr(0 + ord('a')), ha='right', va='top', bbox=bbox_props_index, transform=ax1Left.transAxes, fontsize=defaultFontSize)
ax2Left.text(1., 1., chr(1 + ord('a')), ha='right', va='top', bbox=bbox_props_index, transform=ax2Left.transAxes, fontsize=defaultFontSize)
axRight.text(1., 1., chr(2 + ord('a')), ha='right', va='top', bbox=bbox_props_index, transform=axRight.transAxes, fontsize=defaultFontSize)

#plt.subplots_adjust(left=0.1, right=0.99, top=0.99, bottom=0.1)
fig.savefig('3OPL_Image4_Saddle3D.jpg', bbox_inches='tight')
"""

#Phase space images
def f(t, y, g, sigma, a, b):
    return [g * y[2] - sigma * y[0], - y[1] - y[0] * y[3] + a * y[4], - y[2] + y[0] * y[5] - a * y[3], - y[3] + y[0] * y[1] + a * y[2], - b * (y[4] + 1) - 2 * y[0] * y[2] - 4 * a * y[1], - b * y[5] - 4 * y[0] * y[2] - 2 * a * y[1]]

r = ode(f).set_integrator('dopri5')
dt = 0.001

def rainbow_text(fig, ax, x,y,textList,colorList, isVertical=False):
    t = ax.transAxes
    if(not isVertical):
        for text,color in zip(textList, colorList):
            text = ax.text(x,y,text,color=color, transform=t, va='bottom',ha='left', fontsize=defaultFontSize)
            ex = text.get_window_extent(fig.canvas.get_renderer())
            t = transforms.offset_copy(t, x=ex.width, units='dots', fig=fig)
    else:
        for text,color in zip(textList, colorList):
            text = ax.text(x,y,text,color=color, transform=t,rotation='vertical',va='bottom',ha='left', fontsize=defaultFontSize)
            ex = text.get_window_extent(fig.canvas.get_renderer())
            t = transforms.offset_copy(t, y=ex.height, units='dots')

def plotTrajectory(g, sigma, a, b, tk, axLeft, axRight= None, color1='r', color2='b', plotTrajectory=True, plotSymbols = True, plotTime = False, kneadingsText=[''], kneadingsColors=['k'], t0=0, positiveSide=True):
    if(positiveSide):
        y0 = [0 + 0.00000001, -a * b / (b + 4 * a * a), 0, 0, -b / (b + 4 * a * a), 2 * a * a / (b + 4 * a * a)]
    else :
        y0 = [0 - 0.00000001, -a * b / (b + 4 * a * a), 0, 0, -b / (b + 4 * a * a), 2 * a * a / (b + 4 * a * a)]
    #r.set_initial_value(y0, t0).set_f_params(g, sigma, a, b)
    r.set_initial_value(y0, 0).set_f_params(g, sigma, a, b)
    output = np.zeros(((tk - t0) / dt + 3, 7))
    #output[0, :] = n.concatenate([[t0], y0], 0)
    i = 0;
    while r.successful() and r.t < tk:
        x = np.concatenate([[r.t + dt], r.integrate(r.t + dt)], 0)
        if(r.t > t0) :
            output[i, :] = x
            i = i + 1

    yPositive = output[:, 1] > 0

    if(plotTrajectory):
        axLeft.plot(output[yPositive, 1], -output[yPositive, 6], marker='o', mec=color1, ms=2., mfc = color1, ls='None')
        axLeft.plot(output[~yPositive, 1], -output[~yPositive, 6], marker='o', mec = color2, ms=2., mfc= color2, ls='None')
        print y0
        #axLeft.plot(-y0[0], y0[5], 'ko', ms=8, mec='k')
        if plotSymbols :
            rainbow_text(fig, axLeft, 0.03, 0.9, kneadingsText, kneadingsColors)

    if(plotTime):
        axRight.plot(output[yPositive, 0], output[yPositive, 1], marker='o', ms=1., mec=color1, mfc = color1, ls='None')
        axRight.plot(output[~yPositive, 0], output[~yPositive, 1], marker='o', ms=1., mec=color2, mfc = color2, ls='None')
        #axRight.plot(0, y0[0], 'ko', ms=5, mec='k')
        if plotSymbols:
            rainbow_text(fig, axRight, 0.01, 0.9, kneadingsText, kneadingsColors)

"""
# Fixed point, Periodic orbit, Homoclinic, T-point
print 'Generating Images 10'

figSizeX = 0.5*figsize;
figSizeY = 0.5*figsize;
gridX = 2;
gridY = 2;
fig = plt.figure(figsize=(figSizeX, figSizeY))
fig.dpi = 1200 ## need to set this to see the rainbow_text properly

gs1 = gridspec.GridSpec(gridX, gridY)
gs1.update(wspace=0.03, hspace=0.03)  # set the spacing between axes.

ax0 = plt.subplot(gs1[0, 0])
ax1 = plt.subplot(gs1[0, 1])
ax2 = plt.subplot(gs1[1, 0])
ax3 = plt.subplot(gs1[1, 1])

ax0.set_xlim([-3,3]); ax0.set_ylim([-.52, -.1])
ax1.set_xlim([-3,3]); ax1.set_ylim([-.52, -.1])
ax2.set_xlim([-3,3]); ax2.set_ylim([-.52, -.1])
ax3.set_xlim([-3,3]); ax3.set_ylim([-.52, -.1])

ax0.set_xticks([]); ax0.set_yticks([]);
ax1.set_xticks([]); ax1.set_yticks([]);
ax2.set_xticks([]); ax2.set_yticks([]);
ax3.set_xticks([]); ax3.set_yticks([]);

ax2.text( 0., -.02, -3, ha='left', va='top', bbox=bbox_props_label, transform=ax2.transAxes, fontsize=defaultFontSize)
ax2.text( 1., -.02, 3, ha='right', va='top', bbox=bbox_props_label, transform=ax2.transAxes, fontsize=defaultFontSize)
ax3.text( 0., -.02, -3, ha='left', va='top', bbox=bbox_props_label, transform=ax3.transAxes, fontsize=defaultFontSize)
ax3.text( 1., -.02, 3, ha='right', va='top', bbox=bbox_props_label, transform=ax3.transAxes, fontsize=defaultFontSize)

ax0.text(-0.02, 1., -0.1, ha='right', va='top', rotation=90, bbox=bbox_props_label, transform=ax0.transAxes, fontsize=defaultFontSize)
ax0.text(-0.02, 0,  -0.52, ha='right', va='bottom', rotation=90, bbox=bbox_props_label, transform=ax0.transAxes, fontsize=defaultFontSize)
ax2.text(-0.02, 1., -0.1, ha='right', va='top', rotation=90, bbox=bbox_props_label, transform=ax2.transAxes, fontsize=defaultFontSize)
ax2.text(-0.02, 0,  -0.52, ha='right', va='bottom', rotation=90, bbox=bbox_props_label, transform=ax2.transAxes, fontsize=defaultFontSize)

#right side fixed point
plotTrajectory(50, 1.5, 3.7, 0.52, 200, axLeft=ax0, color1='firebrick', color2='firebrick', plotSymbols=False)
#ax0.arrow(0.5, -0.481, 0.7, -0.48, head_width=0.05, head_length=0.1, fc='k', ec='k')
ax0.plot(0.7, -0.473, marker=(3,0,65), ms=10, mfc='firebrick', mec='firebrick')
ax0.text(0.72, 0.1, r'$ \mathrm{\Gamma_1}$', ha='right', va='top', rotation=0, bbox=bbox_props, transform=ax0.transAxes, fontsize=largeFontSize)
#left side fixed point
plotTrajectory(50, 1.5, 3.7, 0.52, 200, axLeft=ax0, color1='mediumblue', color2='mediumblue', plotSymbols=False, positiveSide=False)
rainbow_text(fig, ax0, 0.03, 0.03, ['{',r'$\overline{1}$','}'], ['k','firebrick','k'])

#Periodic big band near IF1
plotTrajectory(50, 1.5, 3.37326, 0.313333, 221, axLeft=ax1, color1='firebrick', color2='firebrick', plotSymbols=False, t0=200)
plotTrajectory(50, 1.5, 4.2, 0.583, 150, axLeft=ax1, color1='mediumblue', color2='mediumblue', plotSymbols=False, t0=100)
rainbow_text(fig, ax1, 0.03, 0.14, ['{',r'$\overline{01}$','}'], ['k','mediumblue','k'])
rainbow_text(fig, ax1, 0.03, 0.03, ['{',r'$\overline{0011}$','}'], ['k','firebrick','k'])

#3.765, 0.41 chaotic lorenz attractor; 0.42 with lacunae; 0.43 bigger lacunae;
plotTrajectory(50, 1.5, 3.765, .41, 1500, axLeft=ax2, color1='lightgray', color2='lightgray', plotSymbols=False, t0=300)
#Just before and after homoclinic - convergence to asymmetric periodic orbit
#plotTrajectory(50, 1.5, 3.827, 0.51902, 200, axLeft=ax2, color1='lightgray', color2='lightgray', plotSymbols=False, t0=180)
#plotTrajectory(50, 1.5, 3.827, 0.51904, 200, axLeft=ax2, color1='lightgray', color2='lightgray', plotSymbols=False, t0=180)
plotTrajectory(50, 1.5, 3.827, 0.54, 14.5, axLeft=ax2, color1='mediumblue', color2='mediumblue', plotSymbols=False)
plotTrajectory(50, 1.5, 3.827, 0.50, 14.8, axLeft=ax2, color1='darkgreen', color2='darkgreen', plotSymbols=False)
# Homoclinic
plotTrajectory(50, 1.5, 3.827, 0.51903, 17, axLeft=ax2, color1='firebrick', color2='firebrick', plotSymbols=False)
rainbow_text(fig, ax2, 0.03, 0.03, ['{','$11$','...}'], ['k','mediumblue','k'])
rainbow_text(fig, ax2, 0.03, 0.12, ['{','$1$','}'], ['k','firebrick','k'])
rainbow_text(fig, ax2, 0.03, 0.21, ['{','$10$','...}'], ['k','darkgreen','k'])

#T-point above : T1
plotTrajectory(50, 1.5, 3.681989209, .351707034621031785, 1500, axLeft=ax3, color1='lightgray', color2='lightgray', plotSymbols=False)
plotTrajectory(50, 1.5, 3.681989209, .351707034621031785, 52.2, axLeft=ax3, color1 = 'mediumblue', color2 = 'mediumblue', plotSymbols=False, positiveSide=False)
ax3.plot([0.0,0.0],[-0.49,-0.446],color='mediumblue',mec='mediumblue',mfc='mediumblue',marker='o',ms=1.,ls='-',lw=1.5)
plotTrajectory(50, 1.5, 3.681989209, .351707034621031785, 52.2, axLeft=ax3, color1 = 'firebrick', color2 = 'firebrick', plotSymbols=False)
ax3.plot([0.0,0.0],[-0.49,-0.446],color='firebrick',mec='firebrick',mfc='firebrick',marker='o',ms=1.,ls='-',lw=1.5)
rainbow_text(fig, ax3, 0.03, 0.03, ['{','$1$','$0$',r'$\overline{1}$','}'], ['k','firebrick','firebrick','firebrick','k'])

################# Entire image ########################
ax0.text(1., 1., chr(0 + ord('a')), ha='right', va='top', bbox=bbox_props_index, transform=ax0.transAxes, fontsize=defaultFontSize)
ax1.text(1., 1., chr(1 + ord('a')), ha='right', va='top', bbox=bbox_props_index, transform=ax1.transAxes, fontsize=defaultFontSize)
ax2.text(1., 1., chr(2 + ord('a')), ha='right', va='top', bbox=bbox_props_index, transform=ax2.transAxes, fontsize=defaultFontSize)
ax3.text(1., 1., chr(3 + ord('a')), ha='right', va='top', bbox=bbox_props_index, transform=ax3.transAxes, fontsize=defaultFontSize)

ax0.plot(0, -0.497, 'ko', ms=8, mec='k')
ax1.plot(0, -0.497, 'ko', ms=8, mec='k')
ax2.plot(0, -0.497, 'ko', ms=8, mec='k')
ax2.plot(.9, -0.335, 'bo', ms=8, mec='b')
ax2.plot(-.9, -0.335, 'go', ms=8, mec='g')
ax3.plot(0, -0.497, 'ko', ms=8, mec='k')

xlabelY=0.067; ylabelX=0.057;

xlabel = r'$ \mathrm{\beta}$';
ylabel = r'$ \mathrm{-D_{23}}$'
fig.text(0.28, xlabelY, xlabel, ha='center', bbox=bbox_props_fig, color='darkslategray', fontsize=largeFontSize,
         weight='roman')
fig.text(.76, xlabelY, xlabel, ha='center', bbox=bbox_props_fig, color='darkslategray', fontsize=largeFontSize,
         weight='roman')
fig.text(ylabelX, 0.28, ylabel, va='center', rotation='vertical', bbox=bbox_props_fig, color='darkslategray',
         fontsize=largeFontSize, weight='roman')
fig.text(ylabelX, 0.76, ylabel, va='center', rotation='vertical', bbox=bbox_props_fig, color='darkslategray',
         fontsize=largeFontSize, weight='roman')

plt.subplots_adjust(left=0.1, right=0.99, top=0.99, bottom=0.1)
fig.savefig('3OPL_Image10.jpg', bbox_inches='tight')
"""

"""
# IF homoclinic single to double for 1.5, 2, 1.5 IF 3d surface
print 'Generating Images 11'

figSizeX = 1.0*figsize;
figSizeY = 0.5*figsize;
gridX = 1;
gridY = 2;
fig = plt.figure(figsize=(figSizeX, figSizeY))
gs1 = gridspec.GridSpec(gridX, gridY)
gs1.update(wspace=0.02, hspace=0.02)  # set the spacing between axes.


ax1Left = plt.subplot(gs1[0, 0])
ax1Right = plt.subplot(gs1[0, 1])

ax1Left.set_xlim([-1.7,2.7]); ax1Left.set_ylim([-.52, -.15])
ax1Right.set_xlim([-2.5,2.8]); ax1Right.set_ylim([-.52, -0.02])

ax1Left.set_xticks([]); ax1Left.set_yticks([]);
ax1Right.set_xticks([]); ax1Right.set_yticks([])

PHC = [
        4.124910, 0.595354, 23,
        3.827, 0.51903, 17,  # IF1
        #3.546020, 0.353647, 17,
        3.76, 0.354, 19,
        #3.98, .40037, 23.,  # IF2
        4.125120, 0.479283, 21,
        4.241910, 0.598092, 25,
]
#colors = [ cm.Greens(x) for x in linspace(0, 1., int(len(PHC)/3)) ]
colors = [ 'red','firebrick','darkgreen','dodgerblue','mediumblue']
for i in xrange(0,int(len(PHC)/3)).__reversed__():
    plotTrajectory(50, 1.5, PHC[3*i], PHC[3*i+1], PHC[3*i+2], axLeft=ax1Left, color1=colors[i], color2=colors[i], plotSymbols=False)

PHC = [
        3.701, 0.8734, 16.6,
        3.321, 0.69885, 15., #IF1
        #2.762, 0.41821, 15.4,
        3.03, 0.39331, 15,
        #3.306, 0.48292, 18,
        3.276000000018863, 0.5186857531, 60.75, #Do it on either side from the T-point to get left symmetric loop Homoclinic
]
#colors = [ cm.Greens(x) for x in linspace(0, 1., int(len(PHC)/3)) ]
colors = [ 'red','firebrick','darkgreen','dodgerblue','mediumblue']
for i in xrange(0,int(len(PHC)/3)).__reversed__():
    plotTrajectory(50, 2., PHC[3*i], PHC[3*i+1], PHC[3*i+2], axLeft=ax1Right, color1=colors[i], color2=colors[i], plotSymbols=False)

################# Entire image ########################
ax1Left.text(1., 1., chr(0 + ord('a')), ha='right', va='top', bbox=bbox_props_index, transform=ax1Left.transAxes, fontsize=defaultFontSize)
ax1Right.text(1., 1., chr(1 + ord('a')), ha='right', va='top', bbox=bbox_props_index, transform=ax1Right.transAxes, fontsize=defaultFontSize)

ax1Left.plot(0, -0.497, 'ko', ms=8, mec='k')
ax1Right.plot(0, -0.497, 'ko', ms=8, mec='k')

xlabelX=0.85; xlabelY=0.055; ylabelX=0.07; ylabelY=0.53;
xlabel = r'$ \mathrm{\beta}$';
ylabel = r'$ \mathrm{-D_{23}}$'

fig.text(.76, xlabelY, xlabel, ha='center', bbox=bbox_props_fig, color='darkslategray', fontsize=largeFontSize,
         weight='roman')
fig.text(.28, xlabelY, xlabel, ha='center', bbox=bbox_props_fig, color='darkslategray', fontsize=largeFontSize,
         weight='roman')
fig.text(ylabelX, ylabelY, ylabel, va='center', rotation='vertical', bbox=bbox_props_fig, color='darkslategray',
         fontsize=largeFontSize, weight='roman')

plt.subplots_adjust(left=0.1, right=0.99, top=0.99, bottom=0.1)
fig.savefig('3OPL_Image11.jpg', bbox_inches='tight')
"""

"""
# IF1 loops, surface, loop3d, one sided chaos - sigma 1.5
print 'Generating Images 12'

figSizeX = 0.8*figsize;
figSizeY = 0.8*figsize;
gridX = 2;
gridY = 2;
fig = plt.figure(figsize=(figSizeX, figSizeY))
gs1 = gridspec.GridSpec(gridX, gridY)
gs1.update(wspace=0.03, hspace=0.002)  # set the spacing between axes.

ax0 = plt.subplot(gs1[0, 0])
ax1 = plt.subplot(gs1[0, 1])
ax2 = plt.subplot(gs1[1, 0])
ax3 = plt.subplot(gs1[1, 1])

ax0.set_xticks([]); ax0.set_yticks([]);
ax1.set_xticks([]); ax1.set_yticks([]);
ax2.set_xticks([]); ax2.set_yticks([]);
ax3.set_xticks([]); ax3.set_yticks([]);

img = mpimg.imread('3OPL_loop_oriented.png')
ax0.imshow(img)

img = mpimg.imread('3OPL_loop_nonoriented.png')
ax1.imshow(img)

img = mpimg.imread('3OPL_Image12_IF1_loop3d.png')
ax2.imshow(img)

img = mpimg.imread('3OPL_Image12_oneSidedChaosWithZoom.jpg')
ax3.imshow(img)

#img = mpimg.imread('3OPL_Image12_oneSidedChaosMap.jpg')

ax0.autoscale(False)
ax0.set_adjustable('box-forced')
ax1.autoscale(False)
ax1.set_adjustable('box-forced')
ax2.autoscale(False)
ax2.set_adjustable('box-forced')
ax3.autoscale(False)
ax3.set_adjustable('box-forced')


################# Entire image ########################
ax0.text(1., 1., chr(0 + ord('a')), ha='right', va='top', bbox=bbox_props_index, transform=ax0.transAxes, fontsize=defaultFontSize)
ax1.text(1., 1., chr(1 + ord('a')), ha='right', va='top', bbox=bbox_props_index, transform=ax1.transAxes, fontsize=defaultFontSize)
ax2.text(1., 1., chr(2 + ord('a')), ha='right', va='top', bbox=bbox_props_index, transform=ax2.transAxes, fontsize=defaultFontSize)
ax3.text(1., 1., chr(3 + ord('a')), ha='right', va='top', bbox=bbox_props_index, transform=ax3.transAxes, fontsize=defaultFontSize)


fig.text(0.25, 0.52, r'$\mathrm{\Gamma_1}$', ha='center', bbox=bbox_props, color='darkslategray', fontsize=largeFontSize,
         weight='roman')
fig.text(0.175, 0.57, r'$\mathrm{W^s}$', ha='center', bbox=bbox_props,
         color='darkslategray', fontsize=largeFontSize, weight='roman')
fig.text(0.16, 0.65, r'$\mathrm{\pi_2}$', ha='center', bbox=bbox_props,
         color='darkslategray', fontsize=largeFontSize, weight='roman')
fig.text(0.255, 0.645, r'$\mathrm{\pi_1}$', ha='center', bbox=bbox_props,
         color='darkslategray', fontsize=largeFontSize, weight='roman')

fig.text(0.63, 0.52, r'$\mathrm{\Gamma_1}$', ha='center', bbox=bbox_props, color='darkslategray', fontsize=largeFontSize,
         weight='roman')
fig.text(.56, 0.57, r'$ \mathrm{W^s}$' , ha='center', bbox=bbox_props, color='darkslategray', fontsize=largeFontSize,
         weight='roman')
fig.text(0.55, 0.645, r'$\mathrm{\pi_2}$', ha='center', bbox=bbox_props, color='darkslategray', fontsize=largeFontSize,
         weight='roman')
fig.text(0.64, 0.64, r'$\mathrm{\pi_1}$', ha='center', bbox=bbox_props,
         color='darkslategray', fontsize=largeFontSize, weight='roman')

fig.text(.27, 0.12, r'$ \mathrm{\Gamma_1}$' , ha='center', #bbox=bbox_props_fig,
         color='darkslategray', fontsize=largeFontSize, weight='roman')
fig.text(.15, 0.21, r'$ \mathrm{W^s}$' , ha='center', #bbox=bbox_props_fig,
         color='darkslategray', fontsize=largeFontSize, weight='roman')
fig.text(.26, 0.255, r'$ \mathrm{d_1}$' , ha='center', #bbox=bbox_props_fig,
          color='darkslategray', fontsize=largeFontSize, weight='roman')
fig.text(.3, 0.23, r'$ \mathrm{d_2}$' , ha='center', #bbox=bbox_props_fig,
         color='darkslategray', fontsize=largeFontSize, weight='roman')
fig.text(.16, 0.255, r'$ \mathrm{\pi}$' , ha='center', #bbox=bbox_props_fig,
         color='darkslategray', fontsize=largeFontSize, weight='roman')
fig.text(.36, 0.31, r'$ \mathrm{\pi_1}$' , ha='center', #bbox=bbox_props_fig,
         color='darkslategray', fontsize=largeFontSize, weight='roman')
fig.text(.29, 0.315, r'$ \mathrm{\pi_2}$' , ha='center', #bbox=bbox_props_fig,
         color='darkslategray', fontsize=largeFontSize, weight='roman')
fig.text(.36, 0.36, r'$ \mathrm{W^s}$' , ha='center', #bbox=bbox_props_fig,
         color='darkslategray', fontsize=largeFontSize, weight='roman')


fig.text(.7, 0.13, r'$ \mathrm{\beta}$' , ha='center', bbox=bbox_props_fig, color='darkslategray', fontsize=largeFontSize,
         weight='roman')
fig.text(0.55, 0.45, r'$\mathrm{-D_{23}}$', ha='center', bbox=bbox_props_fig, color='darkslategray', fontsize=largeFontSize,
         weight='roman', rotation = 90)
fig.text(0.84, 0.16, r'$\mathrm{-p_{23}}$', ha='center', bbox=bbox_props_fig, color='darkslategray', fontsize=largeFontSize,
         weight='roman', rotation = 23)

#plt.subplots_adjust(left=0.1, right=0.99, top=0.99, bottom=0.1)
fig.savefig('3OPL_Image12.jpg', bbox_inches='tight')
"""

"""
print 'Generating Images 13'

figSizeX = 0.5*figsize;
figSizeY = 0.5*figsize;
gridX = 10;
gridY = 1;
fig = plt.figure(figsize=(figSizeX, figSizeY))
fig.dpi = 1200
gs1 = gridspec.GridSpec(gridX, gridY)
gs1.update(wspace=0.02, hspace=0.2)  # set the spacing between axes.


ax1Left = plt.subplot(gs1[0:7, 0])
ax1Right = plt.subplot(gs1[7:10, 0])

ax1Left.set_xlim([-2.4,2.4]); ax1Left.set_ylim([-.52, -.15])
ax1Right.set_xlim([0,55]); ax1Right.set_ylim([-2.4, 2.4])

ax1Left.set_xticks([]); ax1Left.set_yticks([]);
ax1Right.set_xticks([]); ax1Right.set_yticks([])

plotTrajectory(50, 1.5, 3.8, .43858, 1500, axLeft=ax1Left, color1='lightgray', color2='lightgray', plotSymbols=False)
plotTrajectory(50, 1.5, 3.8, .43858, 55, axLeft=ax1Left, color1='firebrick', color2='firebrick', plotSymbols=False, plotTime=True, axRight=ax1Right)
plotTrajectory(50, 1.5, 3.8, .43855, 55, axLeft=ax1Left, color1='mediumblue', color2='mediumblue', plotSymbols=False, plotTime=True, axRight=ax1Right)

################# Entire image ########################
ax1Left.text(1., 1., chr(0 + ord('a')), ha='right', va='top', bbox=bbox_props_index, transform=ax1Left.transAxes, fontsize=defaultFontSize)
ax1Right.text(1., 1., chr(1 + ord('a')), ha='right', va='top', bbox=bbox_props_index, transform=ax1Right.transAxes, fontsize=defaultFontSize)

ax1Left.plot(0, -0.497, 'ko', ms=8, mec='k')
ax1Left.plot(0.8809, -0.33265, 'ko', ms=8, mec='k')
ax1Left.plot(-0.8809, -0.33265, 'ko', ms=8, mec='k')

fig.text(0.82, 0.11, r'$ \mathrm{time}$', ha='center', bbox=bbox_props, color='darkslategray', fontsize=largeFontSize, weight='roman')
fig.text(0.13, 0.3, r'$ \mathrm{\beta}$', ha='center', bbox=bbox_props, color='darkslategray', fontsize=largeFontSize, weight='roman', rotation='vertical')
fig.text(0.85, 0.4, r'$ \mathrm{\beta}$', ha='center', bbox=bbox_props, color='darkslategray', fontsize=largeFontSize, weight='roman')
fig.text(0.13, 0.6, r'$ \mathrm{-D_{23}}$', ha='center', bbox=bbox_props, color='darkslategray', fontsize=largeFontSize, weight='roman', rotation='vertical')
rainbow_text(fig, ax1Left, 0.03, 0.03, ['{','$10010110$','...}'], ['k','firebrick','k'])
rainbow_text(fig, ax1Left, 0.03, 0.09, ['{','$10011001$','...}'], ['k','mediumblue','k'])


plt.subplots_adjust(left=0.1, right=0.99, top=0.99, bottom=0.1)

fig.savefig('3OPL_Image13.jpg', bbox_inches='tight')
"""

show()