#!/bin/bash

nvcc --ptxas-options=-v -arch=sm_30 --compiler-options '-fPIC' -o laserCuda1.so --shared laserCuda1.cu
nvcc --ptxas-options=-v -arch=sm_30 --compiler-options '-fPIC' -o laserCuda11.so --shared laserCuda11.cu
nvcc --ptxas-options=-v -arch=sm_30 --compiler-options '-fPIC' -o laserSweepPCLZ1.so --shared laserSweepPCLZ1.cu
nvcc --ptxas-options=-v -arch=sm_30 --compiler-options '-fPIC' -o laserSweepPCLZ11.so --shared laserSweepPCLZ11.cu


