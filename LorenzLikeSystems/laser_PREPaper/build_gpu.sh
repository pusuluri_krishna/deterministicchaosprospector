#!/bin/bash

nvcc --ptxas-options=-v -arch=sm_30 --compiler-options '-fPIC' -o laserCuda1.so --shared laserCuda1.cu
nvcc --ptxas-options=-v -arch=sm_30 --compiler-options '-fPIC' -o laserCuda11.so --shared laserCuda11.cu
nvcc --ptxas-options=-v -arch=sm_30 --compiler-options '-fPIC' -o laserSweepPeriodicKneadings1.so --shared laserSweepPeriodicKneadings1.cu
nvcc --ptxas-options=-v -arch=sm_30 --compiler-options '-fPIC' -o laserSweepPeriodicKneadings11.so --shared laserSweepPeriodicKneadings11.cu
nvcc --ptxas-options=-v -arch=sm_30 --compiler-options '-fPIC' -o laserSweepLZ1.so --shared laserSweepLZ1.cu
nvcc --ptxas-options=-v -arch=sm_30 --compiler-options '-fPIC' -o laserSweepLZ11.so --shared laserSweepLZ11.cu

