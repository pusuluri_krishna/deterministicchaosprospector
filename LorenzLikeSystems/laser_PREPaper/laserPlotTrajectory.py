from scipy.integrate import ode
import numpy as n
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import colors
from pylab import *

def f(t, y, g, sigma, a, b):
    return [g * y[2] - sigma * y[0], - y[1] - y[0] * y[3] + a * y[4], - y[2] + y[0] * y[5] - a * y[3], - y[3] + y[0] * y[1] + a * y[2], - b * (y[4] + 1) - 2 * y[0] * y[2] - 4 * a * y[1], - b * y[5] - 4 * y[0] * y[2] - 2 * a * y[1]]

r = ode(f).set_integrator('rk4')

dt = 0.001

matplotlib.rcParams['savefig.dpi'] = 1200 #2*sweepSize/figSize #300

figSizeX=6; figSizeY=2; gridX = 1 ; gridY = 3;

fig = plt.figure(figsize=(figSizeX, figSizeY))

gs1 = gridspec.GridSpec(gridX, gridY)
gs1.update(wspace=0.002, hspace=0.002)  # set the spacing between axes.

ax1Left = plt.subplot(gs1[0,0])
ax1Middle = plt.subplot(gs1[0,1])
ax1Right = plt.subplot(gs1[0,2])

ax1Left.set_xlim([-1,3]); ax1Left.set_ylim([-.52,-0.05])
ax1Middle.set_xlim([-1,3]); ax1Middle.set_ylim([-.52,-0.05])
ax1Right.set_xlim([-2,2]); ax1Right.set_ylim([-0.52,-0.2])

ax1Left.set_xticks([]); ax1Left.set_yticks([])
ax1Middle.set_xticks([]); ax1Middle.set_yticks([])
ax1Right.set_xticks([]); ax1Right.set_yticks([])


def plotTrajectory(g, sigma, a, b, tk, color1='r', color2='b', axLeft = ax1Left):
    t0=0
    y0 = [0 + 0.00000001, -a * b / (b + 4 * a * a), 0, 0, -b / (b + 4 * a * a), 2 * a * a / (b + 4 * a * a)]
    #r.set_initial_value(y0, t0).set_f_params(g, sigma, a, b)
    r.set_initial_value(y0, 0).set_f_params(g, sigma, a, b)
    output = np.zeros(((tk - t0) / dt + 3, 7))
    #output[0, :] = n.concatenate([[t0], y0], 0)
    i = 0;
    while r.successful() and r.t < tk:
        x = np.concatenate([[r.t + dt], r.integrate(r.t + dt)], 0)
        if(r.t > t0) :
            output[i, :] = x
            i = i + 1

    yPositive = output[:, 1] > 0
    #axLeft.plot(xScale*output[yPositive, 1], yScale*output[yPositive, 6], marker='o', mec=color1, ms = .5, mfc = color1, ls='None')
    axLeft.plot(output[yPositive, 1], -output[yPositive, 6], marker='o', mec=color1, ms=.5, mfc=color1,
                ls='None')
    axLeft.plot(output[~yPositive, 1], -output[~yPositive, 6], marker='o', mec = color2, ms=.5, mfc= color2, ls='None')
    axLeft.plot(y0[0], y0[5], 'ko', ms=2, mec='k')


#[g, sigma, a, b, tk] =[50, 1.5, 3.5, 0.32, 1500]    #Chaotic
#plotTrajectory(50, 1.5, 3.6, 0.35, 1500, 'lightgray', 'lightgray', plotTime=False, axLeft=ax1Left)
#plotTrajectory(50, 1.5, 3.6, 0.35, 1500, 'lightgray', 'lightgray', plotTime=False, axLeft=ax2Left)

#example trajectory
#plotTrajectory(50, 1.5, 3.765, .44, 50, axLeft=ax1Left, axRight=ax1Right)

#[g, sigma, a, b, tk] =[50, 1.5, 3.68179, .351831, 40]    #T-point above
#plotTrajectory(50, 1.5, 3.68201, .351708, 50, axLeft=ax2Left, axRight=ax2Right)

#[g, sigma, a, b, tk] =[50, 1.5, 3.6516, .341926, 50]    #T-point below
#plotTrajectory(50, 1.5, 3.65203, .34193, 50, 'firebrick', 'darksalmon', axRightIndex=1)

#periodic
#plotTrajectory(50, 1.5, 3.4, .37, 50, 'navy', 'royalblue', t0=30)
#plotTrajectory(50, 1.5, 4.4, .6, 50, 'navy', 'royalblue', t0=0, axRightIndex=2)

#example trajectory
#plotTrajectory(50, 1.5, 3.55, .32, 40, 'red', 'blue', t0=0, axRightIndex=2)
#plotTrajectory(50, 1.5, 3.7, .365, 50, 'red', 'blue', t0=0, axRightIndex=2)

#Sigma 10 main T-point
#plotTrajectory(50, 10., 1.10054, 0.192854, 100, 'darkolivegreen', 'y', axRightIndex=0)


PHC = [
        4.372760, 0.647872, 25,
        4.265360, 0.626795, 25,
        4.124910, 0.595354, 23,
        3.961320, 0.555088, 17,
#        3.827, 0.51903, 17,  # IF1
]
colors = [ cm.cool(x) for x in linspace(0, 1., int(len(PHC)/3)) ]
for i in xrange(0,int(len(PHC)/3)).__reversed__():
    plotTrajectory(50, 1.5, PHC[3*i], PHC[3*i+1], PHC[3*i+2], colors[i], colors[i], axLeft=ax1Left)
plotTrajectory(50, 1.5, 3.827, .51903, 17., 'k', 'k', axLeft=ax1Left)

PHC = [
        3.765600, 0.501360, 17,
        3.547360, 0.424725, 17,
        3.546020, 0.353647, 17,
        3.714940, 0.350212, 17,
        #3.98, .40037, 23.,  # IF2
]
colors = [ cm.cool(x) for x in linspace(0, 1., int(len(PHC)/3)) ]
for i in xrange(0,int(len(PHC)/3)).__reversed__():
    plotTrajectory(50, 1.5, PHC[3*i], PHC[3*i+1], PHC[3*i+2], colors[i], colors[i], axLeft=ax1Middle)
plotTrajectory(50, 1.5, 3.98, .40037, 23., 'r', 'r', axLeft=ax1Middle)
plotTrajectory(50, 1.5, 3.827, .51903, 17., 'k', 'k', axLeft=ax1Middle)

PHC = [
        4.060800, 0.435417, 21,
        4.125120, 0.479283, 21,
        4.157850, 0.521501, 21,
        4.175540, 0.554952, 21,
        4.206750, 0.580039, 21,
        4.241910, 0.598092, 25,
        4.273680, 0.611580, 25,
]
colors = [ cm.cool(x) for x in linspace(0, 1., int(len(PHC)/3)) ]
for i in xrange(0,int(len(PHC)/3)).__reversed__():
    plotTrajectory(50, 1.5, PHC[3*i], PHC[3*i+1], PHC[3*i+2], colors[i], colors[i], axLeft=ax1Right)
plotTrajectory(50, 1.5, 3.98, .40037, 23., 'r', 'r', axLeft=ax1Right)





fig.savefig('Image.jpg', bbox_inches='tight')
plt.show()
