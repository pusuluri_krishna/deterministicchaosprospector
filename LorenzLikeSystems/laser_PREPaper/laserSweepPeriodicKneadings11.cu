#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include <sys/time.h>
#include<string.h>
#include<stdbool.h>

#define N_EQ1	6
#define MAX_KNEADING_LENGTH 2001
#define NUM_THREADS_PER_BLOCK 512

__device__ void stepper(const double* y, double* dydt, const double* params)
{
	double a=params[0];
	double b=params[1];
	double g=params[2];
	double sigma=params[3];

	dydt[0] = g * y[2] - sigma * y[0];
	dydt[1] = - y[1] - y[0] * y[3] + a * y[4];
	dydt[2] = - y[2] + y[0] * y[5] - a * y[3];
	dydt[3] = - y[3] + y[0] * y[1] + a * y[2];
	dydt[4] = - b * (y[4] + 1) - 2 * y[0] * y[2] - 4 * a *y[1];
	dydt[5] = - b * y[5] - 4 * y[0] * y[2] - 2 * a * y[1];
}

__device__ void computeFirstDerivative(const double *y, double *dydt, const double* params){
	stepper(y, dydt, params);
	return;
}

__device__ double  computePeriodNormalizedKneadingSum(bool *kneadings, unsigned kneadingsLength, unsigned periodLength ){
    double kneadingSum=0, minPeriodSum=0, currPeriodSum=0;
    unsigned i=0, normalizedPeriodIndex=0;

    if(periodLength<kneadingsLength){
        for(i=0; i<periodLength; i++) {
            currPeriodSum=0;
            for(unsigned j=0; j<periodLength; j++) {
                currPeriodSum+= 2*currPeriodSum + kneadings[i+j]; 
            }
            if(minPeriodSum==0 || currPeriodSum < minPeriodSum) {
                minPeriodSum = currPeriodSum;
                normalizedPeriodIndex=i;
            }
        }
    }

    //filling kneading sequence with normalized period
    for(i=0; i<kneadingsLength; i++) {
        kneadingSum = kneadingSum + kneadings[normalizedPeriodIndex+periodLength-1-i%periodLength]/pow(2.,double(-i+kneadingsLength));
    }
    return kneadingSum;

    /* not filling kneading sequence with normalized period, just using a single period
    for(i=0; i<periodLength; i++) {
        kneadingSum = kneadingSum + kneadings[normalizedPeriodIndex+periodLength-1-i%periodLength]/pow(2.,double(-i+periodLength));
    }
    */

    return kneadingSum;
}

__device__ double getNormalizedPeriodAndKneadingSum(bool* kneadings, unsigned kneadingsLength){
    //After a long transient when the periodic orbits if any have already been reached, this method computes a kneading sum that is invariant between different cyclic permutations of the period

    bool periodFound=true;
    unsigned periodLength = kneadingsLength;
    for(unsigned currPeriod=1; currPeriod < kneadingsLength/2; currPeriod++) {
        periodFound=true;
        //Check if the kneading sequence has a period with periodicity of currPeriod
        for(unsigned i=currPeriod; i < kneadingsLength-currPeriod; i+=currPeriod) {
            for ( unsigned j=0; j<currPeriod; j++) {
                if(kneadings[j] != kneadings[i+j]) {
                    periodFound=false;
                    break;
                }
            }
            if(!periodFound) {
                break;
            }
        }
        //compute kneadingSum based on period found, if any
        if(periodFound) {
            //currPeriod is the period of the kneading sequence. So this will be normalized to a sequence with the sorted period(0's followed by 1's)
            periodLength = currPeriod;
            break;
        }
    }

    return computePeriodNormalizedKneadingSum( kneadings, kneadingsLength, periodLength);
    //return periodLength==kneadingsLength?1:0;
    //return periodLength==kneadingsLength? -0.05 : computePeriodNormalizedKneadingSum( kneadings, kneadingsLength, periodLength);

}

__device__ double integrator_rk4(double* y_current, const double* params, const double dt, const unsigned N, const unsigned stride,
                                            const unsigned kneadingsStart, const unsigned kneadingsEnd)
{
	unsigned i, j, k, kneadingIndex=0, kneadingArrayIndex=0;
	double dt2, dt6;
	double y1[N_EQ1], y2[N_EQ1], k1[N_EQ1], k2[N_EQ1], k3[N_EQ1], k4[N_EQ1];
	double firstDerivativeCurrent[N_EQ1],firstDerivativePrevious;
	double kneadingsWeightedSum=0;
    bool kneadings[MAX_KNEADING_LENGTH];

	bool isPreviousEventOne=false, isDerviative2FirstTimeOnThisSidePositive=0, isPreviousDerivative2Positive=0, isCurrentDerivate2Positive=0;

	dt2 = dt/2.; dt6 = dt/6.;

	for(i=1; i<N; i++)
	{
		for(j=0; j<stride; j++)
		{
			stepper(y_current, k1, params);
			for(k=0; k<N_EQ1; k++) y1[k] = y_current[k]+k1[k]*dt2;
			stepper(y1, k2, params);
			for(k=0; k<N_EQ1; k++) y2[k] = y_current[k]+k2[k]*dt2;
			stepper(y2, k3, params);
			for(k=0; k<N_EQ1; k++) y2[k] = y_current[k]+k3[k]*dt;
			stepper(y2, k4, params);

			//Copy latest value into y_current
			for(k=0; k<N_EQ1; k++) y_current[k] += dt6*(k1[k]+2.*(k2[k]+k3[k])+k4[k]);
		}

		computeFirstDerivative(y_current, firstDerivativeCurrent, params);


        // event check with first variable x - check y, and also eliminate all false loop turns on one side.. no check z..
        if(firstDerivativePrevious * firstDerivativeCurrent[0] < 0) {
            //look at graphs of x[0]Vs.x[2] and x[0]Vs.x[5] in Dynamic Solver - their orientations are different; dxdt[5] should be negative for both 1 and 0 sides of the loop at dxdt[0]=0 to avoid transients; dxdt[2] at dxdt[0]=0 different for 1 and 0 loops.

            if(y_current[0]>0){
                isCurrentDerivate2Positive = (firstDerivativeCurrent[2]>0);

                if(!isPreviousEventOne){

                    if(kneadingIndex>=kneadingsStart){
                        //printf("1");
                        kneadings[kneadingArrayIndex++]=1;
                    }
                    kneadingIndex++;
                    isDerviative2FirstTimeOnThisSidePositive = isCurrentDerivate2Positive;

                }else{

                    if(isCurrentDerivate2Positive==isDerviative2FirstTimeOnThisSidePositive && isCurrentDerivate2Positive!=isPreviousDerivative2Positive){
                        if(kneadingIndex>=kneadingsStart){
                            //printf("1");
                            kneadings[kneadingArrayIndex++]=1;
                        }
                        kneadingIndex++;
                    }

                }

                isPreviousEventOne=true;
                isPreviousDerivative2Positive = isCurrentDerivate2Positive;

            }else{
                isCurrentDerivate2Positive = (firstDerivativeCurrent[2]>0);

                if(isPreviousEventOne){

                    if(kneadingIndex>=kneadingsStart){
                        kneadings[kneadingArrayIndex++]=0;
                    }
                    kneadingIndex++;
                    isDerviative2FirstTimeOnThisSidePositive = isCurrentDerivate2Positive;

                }else{

                    if(isCurrentDerivate2Positive==isDerviative2FirstTimeOnThisSidePositive && isCurrentDerivate2Positive!=isPreviousDerivative2Positive){
	                    if(kneadingIndex>=kneadingsStart){
        	                kneadings[kneadingArrayIndex++]=0;
                	    }
                        kneadingIndex++;
                    }

                }

                isPreviousEventOne=false;
                isPreviousDerivative2Positive = isCurrentDerivate2Positive;
            }

        }
		firstDerivativePrevious = firstDerivativeCurrent[0];

		if(kneadingIndex>kneadingsEnd)
			return getNormalizedPeriodAndKneadingSum(kneadings, kneadingArrayIndex);
	}

        //use only to clean up long range images for AH bifurcation
        //if(y_current[0]>0.01) {for(i=0;i<kneadingsEnd-kneadingsStart+1;i++)kneadings[i]=1; return getNormalizedPeriodAndKneadingSum(kneadings,kneadingsEnd-kneadingsStart+1);}
        //else if (y_current[0]<-0.01) {for(i=0;i<kneadingsEnd-kneadingsStart+1;i++)kneadings[i]=0; return getNormalizedPeriodAndKneadingSum(kneadings,kneadingsEnd-kneadingsStart+1);}

	return -0.1;
}

__global__ void sweepLaserThreads(double* kneadingsWeightedSumSet,
                                        double alphaStart, double alphaEnd, unsigned alphaCount,
										double lStart, double lEnd, unsigned lCount,
										double g, double sigma,
										double dt, unsigned N, unsigned stride,
										unsigned kneadingsStart, unsigned kneadingsEnd){

    int tx=blockIdx.x * blockDim.x + threadIdx.x;

	double params[4],a,b;
	double alphaStep = (alphaEnd - alphaStart)/(alphaCount-1);
	double lStep = (lEnd-lStart)/(lCount -1);
	int i,j,k;


    if(tx<alphaCount*lCount){

        i=tx/alphaCount;
        j=tx%alphaCount;

        params[0]=alphaStart+i*alphaStep;
        params[1]=lStart+j*lStep;
        params[2]=g;
        params[3]=sigma;

        a=params[0];b=params[1];
      	double y_initial[N_EQ1]={0+0.00000001L,-a*b/(b+4*a*a),0,0,-b/(b+4*a*a),2*a*a/(b+4*a*a)};
        kneadingsWeightedSumSet[i*lCount+j] = integrator_rk4(y_initial, params, dt, N, stride,
                                                                kneadingsStart, kneadingsEnd);
    }
}

/**
 * error checking routine
 */
void checkAndDisplayErrors(char *label)
{
  // we need to synchronise first to catch errors due to
  // asynchronous operations that would otherwise
  // potentially go unnoticed

  cudaError_t err;

  err = cudaThreadSynchronize();
  if (err != cudaSuccess)
  {
    char *e = (char*) cudaGetErrorString(err);
    fprintf(stderr, "CUDA Error: %s (at %s)", e, label);
  }

  err = cudaGetLastError();
  if (err != cudaSuccess)
  {
    char *e = (char*) cudaGetErrorString(err);
    fprintf(stderr, "CUDA Error: %s (at %s)", e, label);
  }
}


extern "C" {

void sweepLaser(double* kneadingsWeightedSumSet,
                                        double alphaStart, double alphaEnd, unsigned alphaCount,
										double lStart, double lEnd, unsigned lCount,
										double g, double sigma,
										double dt, unsigned N, unsigned stride,
										unsigned kneadingsStart, unsigned kneadingsEnd){

        int totalParameterSpaceSize = alphaCount*lCount;
        double *kneadingsWeightedSumSetGpu;

        /*allocate device memory. */
        (cudaMalloc( (void**) &kneadingsWeightedSumSetGpu, totalParameterSpaceSize*sizeof(double)));
        checkAndDisplayErrors("Memory allocation");

        /*timing*/
        cudaEvent_t start_event, stop_event;
        cudaEventCreate(&start_event) ;
        cudaEventCreate(&stop_event) ;
        cudaEventRecord(start_event, 0);

        /*Dimensions. */

        int gridXDimension = totalParameterSpaceSize/NUM_THREADS_PER_BLOCK;
        if(totalParameterSpaceSize%NUM_THREADS_PER_BLOCK!=0) {
                gridXDimension += 1;
        }
        dim3 dimGrid(gridXDimension,1);
        dim3 dimBlock(NUM_THREADS_PER_BLOCK,1);

        printf(" Num of blocks per grid:       %d\n", gridXDimension);
        printf(" Num of threads per block:     %d\n", NUM_THREADS_PER_BLOCK);
        printf(" Total Num of threads running: %d\n", gridXDimension*NUM_THREADS_PER_BLOCK);
        printf(" Parameters alphaCount=%d, lCount=%d\n",alphaCount,lCount);

        /*Call kernel(global function)*/
        sweepLaserThreads<<<dimGrid, dimBlock>>>(kneadingsWeightedSumSetGpu, alphaStart, alphaEnd, alphaCount, lStart, lEnd, lCount, g, sigma, dt, N, stride, kneadingsStart, kneadingsEnd);

        cudaThreadSynchronize();
        cudaEventRecord(stop_event, 0);
        cudaEventSynchronize(stop_event);

        float time_kernel;
        cudaEventElapsedTime(&time_kernel, start_event, stop_event);
        printf("Total time(sec) %f\n", time_kernel/1000);

        /*copy data from device memory to memory. */
        (cudaMemcpy( kneadingsWeightedSumSet, kneadingsWeightedSumSetGpu, totalParameterSpaceSize*sizeof(double), cudaMemcpyDeviceToHost));
        checkAndDisplayErrors("Error while copying kneading sum values from device to host.");

        /*Free all allocated memory. */
        cudaFree(kneadingsWeightedSumSetGpu);

}

}


int main(){
	double dt=0.01;
	unsigned N=30000;
	unsigned stride = 1;
	unsigned maxKneadings = 20;
	unsigned sweepSize = 5;
	double g = 50, sigma = 1.5;
	double *kneadingsWeightedSumSet;
	int i,j;
	printf("Enter sweep size :");
	scanf("%d",&sweepSize);
	kneadingsWeightedSumSet = (double *)malloc(sweepSize*sweepSize*sizeof(double));
	sweepLaser(kneadingsWeightedSumSet, 0.8, 1.1, sweepSize, 0, 15, sweepSize, g, sigma, dt, N, stride, 0, 19);

	for(i=0;i<sweepSize;i++){
	    for(j=0;j<sweepSize;j++){
	        printf("%f \n",kneadingsWeightedSumSet[i*sweepSize+j]);
	    }
	}



	/*
	//Testing eigenValue and eigenVectors
    double a=3.6582,b=0.4736;

    int indx = getPositiveEigenValueIndex(a,b);
    double positiveEigenVector[N_EQ1];
    getPostiveEigenVector(a,b,indx,positiveEigenVector);
    for(i=0;i<N_EQ1;i++){
        printf("%lf\n",positiveEigenVector[i]);
    }
    */
}
