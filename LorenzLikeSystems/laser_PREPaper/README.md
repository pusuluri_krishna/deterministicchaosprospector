
The code is presented for all the computational methods described and to generate the images in the following publications:

	Pusuluri K, Shilnikov A. Homoclinic chaos and its organization in a nonlinear optics model. Physical Review E. 2018 Oct 30;98(4):040202.

If you make use of this repository in your research, please consider citing the above article.
