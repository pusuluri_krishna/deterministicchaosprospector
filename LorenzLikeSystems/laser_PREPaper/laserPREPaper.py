#All labels adjusted for Smaug workstation at lab

from __future__ import division
import matplotlib
matplotlib.use('TkAgg')
import ctypes as ct
from pylab import *
import numpy as np
from matplotlib import colors
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from mpl_toolkits.axes_grid.inset_locator import inset_axes
from mpl_toolkits.axes_grid.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid.inset_locator import mark_inset
import matplotlib.patches as patches
import pickle
import gzip
from scipy.integrate import ode
from matplotlib import transforms
import matplotlib.image as mpimg

colorMapLevels = 2**8; N = 30000 ; dataSaved = True; sweepSize = 2000

lib1 = ct.cdll.LoadLibrary('./laserCuda1.so')
lib1.sweepLaser.argtypes = [ct.POINTER(ct.c_double),
                            ct.c_double, ct.c_double, ct.c_uint,
                            ct.c_double, ct.c_double, ct.c_uint,
                            ct.c_double, ct.c_double,
                            ct.c_double, ct.c_uint, ct.c_uint,
                            ct.c_uint, ct.c_uint]
def sweepLaser1(dt=0.01,
                N=N,
                stride=1,
                xmin=3, xmax=5,
                ymin=0.3, ymax=0.7,
                g=50., sigma=1.5,
                kneadingsStart=0,
                kneadingsEnd=16,
                sweepSize=100
                ):
    kneadingsWeightedSumSet = np.zeros(sweepSize * sweepSize, float)
    lib1.sweepLaser(kneadingsWeightedSumSet.ctypes.data_as(ct.POINTER(ct.c_double)),
                    ct.c_double(xmin), ct.c_double(xmax), ct.c_uint(sweepSize),
                    ct.c_double(ymin), ct.c_double(ymax), ct.c_uint(sweepSize),
                    ct.c_double(g), ct.c_double(sigma),
                    ct.c_double(dt), ct.c_uint(N), ct.c_uint(stride),
                    ct.c_uint(kneadingsStart), ct.c_uint(kneadingsEnd))
    # imshow takes fortran style array input
    return np.reshape(kneadingsWeightedSumSet, (sweepSize, sweepSize), 'F')

lib11 = ct.cdll.LoadLibrary('./laserCuda11.so')
lib11.sweepLaser.argtypes = [ct.POINTER(ct.c_double),
                             ct.c_double, ct.c_double, ct.c_uint,
                             ct.c_double, ct.c_double, ct.c_uint,
                             ct.c_double, ct.c_double,
                             ct.c_double, ct.c_uint, ct.c_uint,
                             ct.c_uint, ct.c_uint]
def sweepLaser11(dt=0.01,
                 N=N,
                 stride=1,
                 xmin=3, xmax=5,
                 ymin=0.3, ymax=0.7,
                 g=50., sigma=1.5,
                 kneadingsStart=0,
                 kneadingsEnd=16,
                 sweepSize=100
                 ):
    kneadingsWeightedSumSet = np.zeros(sweepSize * sweepSize, float)
    lib11.sweepLaser(kneadingsWeightedSumSet.ctypes.data_as(ct.POINTER(ct.c_double)),
                     ct.c_double(xmin), ct.c_double(xmax), ct.c_uint(sweepSize),
                     ct.c_double(ymin), ct.c_double(ymax), ct.c_uint(sweepSize),
                     ct.c_double(g), ct.c_double(sigma),
                     ct.c_double(dt), ct.c_uint(N), ct.c_uint(stride),
                     ct.c_uint(kneadingsStart), ct.c_uint(kneadingsEnd))
    # imshow takes fortran style array input
    return np.reshape(kneadingsWeightedSumSet, (sweepSize, sweepSize), 'F')

libPeriodic1 = ct.cdll.LoadLibrary('./laserSweepPeriodicKneadings1.so')
libPeriodic1.sweepLaser.argtypes = [ct.POINTER(ct.c_double),
                          ct.c_double,ct.c_double,ct.c_uint,
                          ct.c_double,ct.c_double,ct.c_uint,
                          ct.c_double,ct.c_double,
                          ct.c_double, ct.c_uint, ct.c_uint,
                          ct.c_uint, ct.c_uint]
def sweepLaserPeriodic1 (    dt=0.01,
                    N=N,
                    stride = 1,
                    xmin = 3, xmax = 5,
                    ymin = 0.3, ymax = 0.7,
                    g = 50., sigma = 1.5,
                    kneadingsStart = 0,
                    kneadingsEnd = 16,
                    sweepSize = 100
                ):
    kneadingsWeightedSumSet = np.zeros(sweepSize*sweepSize, float)
    libPeriodic1.sweepLaser(kneadingsWeightedSumSet.ctypes.data_as(ct.POINTER(ct.c_double)),
                  ct.c_double(xmin), ct.c_double(xmax), ct.c_uint(sweepSize),
                  ct.c_double(ymin), ct.c_double(ymax), ct.c_uint(sweepSize),
                  ct.c_double(g), ct.c_double(sigma),
                  ct.c_double(dt), ct.c_uint(N), ct.c_uint(stride),
                  ct.c_uint(kneadingsStart), ct.c_uint(kneadingsEnd))
    #imshow takes fortran style array input
    return np.reshape(kneadingsWeightedSumSet, (sweepSize,sweepSize), 'F')

libPeriodic11 = ct.cdll.LoadLibrary('./laserSweepPeriodicKneadings11.so')
libPeriodic11.sweepLaser.argtypes = [ct.POINTER(ct.c_double),
                          ct.c_double,ct.c_double,ct.c_uint,
                          ct.c_double,ct.c_double,ct.c_uint,
                          ct.c_double,ct.c_double,
                          ct.c_double, ct.c_uint, ct.c_uint,
                          ct.c_uint, ct.c_uint]
def sweepLaserPeriodic11 (    dt=0.01,
                    N=N,
                    stride = 1,
                    xmin = 3, xmax = 5,
                    ymin = 0.3, ymax = 0.7,
                    g = 50., sigma = 1.5,
                    kneadingsStart = 0,
                    kneadingsEnd = 16,
                    sweepSize = 100
                ):
    kneadingsWeightedSumSet = np.zeros(sweepSize*sweepSize, float)
    libPeriodic11.sweepLaser(kneadingsWeightedSumSet.ctypes.data_as(ct.POINTER(ct.c_double)),
                  ct.c_double(xmin), ct.c_double(xmax), ct.c_uint(sweepSize),
                  ct.c_double(ymin), ct.c_double(ymax), ct.c_uint(sweepSize),
                  ct.c_double(g), ct.c_double(sigma),
                  ct.c_double(dt), ct.c_uint(N), ct.c_uint(stride),
                  ct.c_uint(kneadingsStart), ct.c_uint(kneadingsEnd))
    #imshow takes fortran style array input
    return np.reshape(kneadingsWeightedSumSet, (sweepSize,sweepSize), 'F')

libLZ1 = ct.cdll.LoadLibrary('./laserSweepLZ1.so')
libLZ1.sweepLaser.argtypes = [ct.POINTER(ct.c_double),
                          ct.c_double,ct.c_double,ct.c_uint,
                          ct.c_double,ct.c_double,ct.c_uint,
                          ct.c_double,ct.c_double,
                          ct.c_double, ct.c_uint, ct.c_uint,
                          ct.c_uint, ct.c_uint]
def sweepLaserLZ1 (dt=0.01,
                    N=N,
                    stride = 1,
                    xmin = 3, xmax = 5,
                    ymin = 0.3, ymax = 0.7,
                    g = 50., sigma = 1.5,
                    kneadingsStart = 0,
                    kneadingsEnd = 16,
                    sweepSize = 100
                ):
    kneadingsWeightedSumSet = np.zeros(sweepSize*sweepSize, float)
    libLZ1.sweepLaser(kneadingsWeightedSumSet.ctypes.data_as(ct.POINTER(ct.c_double)),
                  ct.c_double(xmin), ct.c_double(xmax), ct.c_uint(sweepSize),
                  ct.c_double(ymin), ct.c_double(ymax), ct.c_uint(sweepSize),
                  ct.c_double(g), ct.c_double(sigma),
                  ct.c_double(dt), ct.c_uint(N), ct.c_uint(stride),
                  ct.c_uint(kneadingsStart), ct.c_uint(kneadingsEnd))
    #imshow takes fortran style array input
    return np.reshape(kneadingsWeightedSumSet, (sweepSize,sweepSize), 'F')

libLZ11 = ct.cdll.LoadLibrary('./laserSweepLZ11.so')
libLZ11.sweepLaser.argtypes = [ct.POINTER(ct.c_double),
                          ct.c_double,ct.c_double,ct.c_uint,
                          ct.c_double,ct.c_double,ct.c_uint,
                          ct.c_double,ct.c_double,
                          ct.c_double, ct.c_uint, ct.c_uint,
                          ct.c_uint, ct.c_uint]
def sweepLaserLZ11 (dt=0.01,
                    N=N,
                    stride = 1,
                    xmin = 3, xmax = 5,
                    ymin = 0.3, ymax = 0.7,
                    g = 50., sigma = 1.5,
                    kneadingsStart = 0,
                    kneadingsEnd = 16,
                    sweepSize = 100
                ):
    kneadingsWeightedSumSet = np.zeros(sweepSize*sweepSize, float)
    libLZ11.sweepLaser(kneadingsWeightedSumSet.ctypes.data_as(ct.POINTER(ct.c_double)),
                  ct.c_double(xmin), ct.c_double(xmax), ct.c_uint(sweepSize),
                  ct.c_double(ymin), ct.c_double(ymax), ct.c_uint(sweepSize),
                  ct.c_double(g), ct.c_double(sigma),
                  ct.c_double(dt), ct.c_uint(N), ct.c_uint(stride),
                  ct.c_uint(kneadingsStart), ct.c_uint(kneadingsEnd))
    #imshow takes fortran style array input
    return np.reshape(kneadingsWeightedSumSet, (sweepSize,sweepSize), 'F')


blue = np.linspace(0.01, 1, colorMapLevels)
red = 1 - blue
#green = np.random.random(colorMapLevels)
#For green, instead of using random values between 0 to 1, the images seem to be better with values between 0 and 0.8 so very strong green images won't be seen -- pictures look better and also look better with white text overlayed on top of these colors
min = 0.; max = .8
green = min + np.random.random(colorMapLevels) * (max - min)
RGB = np.column_stack((red, green, blue))
customColorMap = colors.ListedColormap(RGB)

#to force common color scheme without scaling across images, use the vmin and vmax arguments of imshow(), and make sure they are the same for all your images
#http://stackoverflow.com/questions/17989917/imshow-subplots-with-the-same-colorbar

# xmin = 1.; xmax = 5.; ymin = 0.1; ymax = 0.7;
figSize = 5

matplotlib.rcParams['savefig.dpi'] = 1200 #2*sweepSize/figSize #300
defaultFontSize = 11; largeFontSize = 14; indexFontSize = 16;
matplotlib.rcParams.update({'font.size': largeFontSize,
              'axes.labelsize': largeFontSize,
              'axes.titlesize': largeFontSize,
              'font.size': defaultFontSize,
              'legend.fontsize': largeFontSize,
              'xtick.labelsize': defaultFontSize,
              'ytick.labelsize': defaultFontSize})

#comment these two for rainbow text function to run properly, along with setting fig.dpi = 1200 described near rainbow text
matplotlib.rc('text', usetex=True)
matplotlib.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]

sweepData = [[0.1,0.2],[0.3,0.4]]                           #### For testing purposes

bbox_props = dict(boxstyle="square,pad=0.2", fc="white", ec="k", lw=.5, alpha=0.)
arrowprops = dict(arrowstyle="-", connectionstyle="arc3", facecolor='white', lw=1.,
                  ec="white")  # ,shrink=0.05, width=defaultFontSize/10.)
bbox_props_fig = dict(boxstyle="square,pad=0.001", fc="white", ec="k", lw=0)
bbox_props_index = dict(boxstyle="square,pad=0.", fc="white", ec="k", lw=0.)
bbox_props_label = dict(boxstyle="square,pad=0.01", fc="white", ec="k", lw=0	)

"""
###################################### Image 1 #######################################################################
print 'Generating Image1'

if not dataSaved :
    pickleDataFile1 = gzip.open('Image1.gz','wb')
else :
    pickleDataFile1 = gzip.open('Image1.gz','rb')

figSizeX=6; figSizeY=6; gridX = 3 ; gridY = 3;

fig = plt.figure(figsize=(figSizeX, figSizeY))
gs1 = gridspec.GridSpec(gridX, gridY)
gs1.update(wspace=0.03, hspace=0.03)  # set the spacing between axes.


################# Main sweep ########################
kneadingRangesAndParameters = [5, 12, 50., 1.5, 3., 5., 0.2, .7]
xmin = kneadingRangesAndParameters[4]
xmax = kneadingRangesAndParameters[5]
ymin = kneadingRangesAndParameters[6]
ymax = kneadingRangesAndParameters[7]


#http://matplotlib.org/users/gridspec.html
ax = plt.subplot(gs1[:,:])

plt.axis('on')

numberOfTicks = 4
xStep = round((xmax - xmin) / numberOfTicks, 2)
yStep = round((ymax - ymin) / numberOfTicks, 2)

#ax.set_xticks(arange(xmin, xmax + xStep, xStep))
#ax.set_yticks(arange(ymin, ymax + yStep, yStep))
ax.set_xticks([])
ax.set_yticks([])
ax.text( 0., -0.02, xmin, ha='left', va='top', bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)
ax.text( 1., -0.02, xmax, ha='right', va='top', bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)
ax.text(-0.02, 1., ymax, ha='right', va='top', rotation=90, bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)
ax.text(-0.02, 0, ymin, ha='right', va='bottom', rotation=90, bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)

if not dataSaved :
    sweepData = sweepLaser11(kneadingsStart=kneadingRangesAndParameters[0], kneadingsEnd=kneadingRangesAndParameters[1],
                    sweepSize=sweepSize, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax,
                    g=kneadingRangesAndParameters[2], sigma=kneadingRangesAndParameters[3])
    pickle.dump(sweepData, pickleDataFile1)
else :
    sweepData = pickle.load(pickleDataFile1)

ax.imshow(sweepData, extent=[xmin, xmax, ymin, ymax], aspect=(xmax - xmin) / (ymax - ymin),
          #cmap=customColorMap,origin='lower')
          vmin=-0.1, vmax=1, cmap=customColorMap,origin='lower')
ax.autoscale(False)
ax.set_adjustable('box-forced')

pf1 = np.loadtxt('laser_pf_1.5_50.dat')
pf2 = np.loadtxt('laser_hopf1_1.5_50.dat')
pf3 = np.loadtxt('laser_hopf2_1.5_50.dat')
pf4 = np.loadtxt('laser_homoclinic_1.5_50.dat')
pf5 = np.loadtxt('laser_hom_1.5_50.dat')
ax.plot(pf1[:, 0], pf1[:, 1], 'w-', linewidth=1.0)
ax.plot(pf2[:, 0], pf2[:, 1], 'w-', linewidth=1.0)
ax.plot(pf3[:, 0], pf3[:, 1], 'w-', linewidth=1.0)
ax.plot(pf4[:, 0], pf4[:, 1], 'w-', linewidth=1.0)
ax.plot(pf5[:, 0], pf5[:, 1], 'w-', linewidth=1.0)

ax.annotate(r'$\boldsymbol{PF}$', xy=(4.47131 , 0.535383), xytext=(4.36 , 0.535383), bbox=bbox_props, color='white' )
ax.annotate(r'$\boldsymbol{AH_0}$', xy=(4.63525 , 0.535383), xytext=(4.64 , 0.535383),  bbox=bbox_props, color='white' )
ax.annotate(r'$\boldsymbol{AH_{1,2}}$', xy=(3.26685, 0.409016), xytext=(3.1, 0.41), bbox=bbox_props, color='white' )
ax.annotate(r'$\boldsymbol{H_0}$', xy=(4.15682, 0.518533), xytext=(4.18, 0.518533), bbox=bbox_props, color='white' )
ax.plot(3.95902, 0.438616, 'o', color='white', ms=4)
ax.annotate(r'$\boldsymbol{T_0}$', xy=(3.95902, 0.438616), xytext=(3.95902, 0.48), arrowprops=arrowprops, bbox=bbox_props, color='white' )
ax.plot(3.37386 , 0.269558, 'o', color='white', ms=4)
ax.annotate(r'$\boldsymbol{S}$', xy=(3.37386 , 0.269558), xytext=(3.29, 0.269558), bbox=bbox_props, color='white' )
ax.plot(4.56255, 0.637487, 'o', color='white', ms=4)
ax.annotate(r'$\boldsymbol{BT}$', xy=(4.56255, 0.637487), xytext=(4.585, 0.634), bbox=bbox_props, color='white' )

ax.plot(4.124910, 0.595354, 'o', color='white', ms=4)
ax.annotate(r'$\boldsymbol{P_1}$', xy=(4.124910, 0.595354), xytext=(4.02, 0.59), bbox=bbox_props, color='white')
ax.plot(3.827, 0.5189, 'o', color='white', ms=4)
ax.annotate(r'$\boldsymbol{IF_1}$', xy=(3.827, 0.5189), xytext=(3.77, 0.534), bbox=bbox_props, color='white')
ax.plot(3.546020, 0.353647, 'o', color='white', ms=4)
ax.annotate(r'$\boldsymbol{P_2}$', xy=(3.546020, 0.353647), xytext=(3.47, 0.345), bbox=bbox_props, color='white')
ax.plot(3.98, .4003, 'o', color='white', ms=4)
ax.annotate(r'$\boldsymbol{IF_2}$', xy=(3.98, .4003), xytext=(3.96, 0.375), bbox=bbox_props, color='white' )
ax.plot(4.125120, 0.479283, 'o', color='white', ms=4)
ax.annotate(r'$\boldsymbol{P_3}$', xy=(4.125120, 0.479283), xytext=(4.15, 0.47), bbox=bbox_props, color='white')
ax.plot(4.241910, 0.598092, 'o', color='white', ms=4)
ax.annotate(r'$\boldsymbol{P_4}$', xy=(4.241910, 0.598092), xytext=(4.26, 0.59), bbox=bbox_props, color='white')

################# A=0 inset ########################

kneadingRangesAndParameters = [5, 12, 50., 1.5, 3.70, 3.90, 0.47, 0.53]
xmin = kneadingRangesAndParameters[4]
xmax = kneadingRangesAndParameters[5]
ymin = kneadingRangesAndParameters[6]
ymax = kneadingRangesAndParameters[7]

inset_axes1 = inset_axes(ax, width="33%", height="33%", loc=2)
plt.setp(inset_axes1.spines.values(), color='white')
inset_axes1.set_xticks([])
inset_axes1.set_yticks([])
inset_axes1.xaxis.set_ticklabels([])
inset_axes1.yaxis.set_ticklabels([])

plt.axis('on')

if not dataSaved :
    sweepData = sweepLaser1(kneadingsStart=kneadingRangesAndParameters[0], kneadingsEnd=kneadingRangesAndParameters[1],
                    sweepSize=sweepSize, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax,
                    g=kneadingRangesAndParameters[2], sigma=kneadingRangesAndParameters[3])
    pickle.dump(sweepData, pickleDataFile1)
else :
    sweepData = pickle.load(pickleDataFile1)

inset_axes1.imshow(sweepData, extent=[xmin, xmax, ymin, ymax], aspect=(xmax - xmin) / (ymax - ymin),
                   #cmap=customColorMap, origin='lower')
                   vmin=-0.1, vmax=1, cmap=customColorMap, origin='lower')
inset_axes1.autoscale(False)
inset_axes1.set_adjustable('box-forced')
for i in inset_axes1.spines.itervalues() :
    i.set_linewidth(1.) ; i.set_color('white')
ax.add_patch(patches.Rectangle((xmin, ymin), xmax-xmin, ymax-ymin, fill=False, linewidth=1., color='white'))
#mark_inset(ax, inset_axes1, loc1=1, loc2=3, fc="none", ec="white", lw=1.)

inset_axes1.plot(3.827, 0.5189, 'o', color='white', ms=4)
inset_axes1.annotate(r'$\boldsymbol{IF_1}$', xy=(3.827, 0.5189), xytext=(3.79, 0.52), bbox=bbox_props, color='white' )

inset_axes1.text(1., 1., r'$\boldsymbol{(a)}$', ha='right', va='top', color='w', fontweight=1000, transform=inset_axes1.transAxes, fontsize=indexFontSize)

################# T-points inset ########################
kneadingRangesAndParameters = [5, 12, 50., 1.5, 3.46, 3.8, 0.33, 0.36]
xmin = kneadingRangesAndParameters[4]
xmax = kneadingRangesAndParameters[5]
ymin = kneadingRangesAndParameters[6]
ymax = kneadingRangesAndParameters[7]

inset_axes2 = inset_axes(ax, width="45%", height="45%", loc=4)
plt.setp(inset_axes2.spines.values(), color='white')
inset_axes2.set_xticks([])
inset_axes2.set_yticks([])
inset_axes2.xaxis.set_ticklabels([])
inset_axes2.yaxis.set_ticklabels([])

plt.axis('on')

if not dataSaved :
    sweepData = sweepLaser11(kneadingsStart=kneadingRangesAndParameters[0], kneadingsEnd=kneadingRangesAndParameters[1],
                    sweepSize=sweepSize, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax,
                    g=kneadingRangesAndParameters[2], sigma=kneadingRangesAndParameters[3])
    pickle.dump(sweepData, pickleDataFile1)
else :
    sweepData = pickle.load(pickleDataFile1)

inset_axes2.imshow(sweepData, extent=[xmin, xmax, ymin, ymax], aspect=(xmax - xmin) / (ymax - ymin),
                   #cmap=customColorMap, origin='lower')
                   vmin=-0.1, vmax=1, cmap=customColorMap, origin='lower')
inset_axes2.autoscale(False)
inset_axes2.set_adjustable('box-forced')
for i in inset_axes2.spines.itervalues() :
    i.set_linewidth(1.) ; i.set_color('white')
ax.add_patch(patches.Rectangle((xmin, ymin), xmax-xmin, ymax-ymin, fill=False, linewidth=1., color='white'))
#mark_inset(ax, inset_axes2, loc1=2, loc2=3, fc="none", ec="white", lw=1.)


inset_axes2.plot(3.68179, .351831, 'o', color='white', ms=4)
inset_axes2.annotate(r'$\boldsymbol{T_1}$', xy=(3.68179, .351831), xytext=(3.59, .356), arrowprops=arrowprops, bbox=bbox_props, color='white' )

inset_axes2.plot(3.6516, .341926, 'o', color='white', ms=4)
inset_axes2.annotate(r'$\boldsymbol{T_2}$', xy=(3.6516, .341926), xytext=(3.55, .343), arrowprops=arrowprops, bbox=bbox_props, color='white' )

inset_axes2.text(1., 1., r'$\boldsymbol{(b)}$', ha='right', va='top', color='w', fontweight=1000, transform=inset_axes2.transAxes, fontsize=indexFontSize)

################# Entire image ########################
xlabelX=0.5; xlabelY=0.067; ylabelX=0.057; ylabelY=0.5;
xlabel = r'$ \mathrm{a}$ - parameter ';
ylabel = r'$ \mathrm{b}$ - parameter '
fig.text(xlabelX, xlabelY, xlabel, ha='center', bbox=bbox_props_fig, color='darkslategray', fontsize=largeFontSize,
         weight='roman')
fig.text(ylabelX, ylabelY, ylabel, va='center', rotation='vertical', bbox=bbox_props_fig, color='darkslategray',
         fontsize=largeFontSize, weight='roman')

plt.subplots_adjust(left=0.1, right=0.999, top=0.999, bottom=0.1)
fig.savefig('Image1.jpg', bbox_inches='tight')

pickleDataFile1.close()
###################################### Image 1 #######################################################################
"""

"""
###################################### Image 2 #######################################################################
print 'Generating Image2'

if not dataSaved :
    pickleDataFile2 = gzip.open('Image2.gz','wb')
else :
    pickleDataFile2 = gzip.open('Image2.gz','rb')

figSizeX=6; figSizeY=6; gridX = 3 ; gridY = 3;

fig = plt.figure(figsize=(figSizeX, figSizeY))
gs1 = gridspec.GridSpec(gridX, gridY)
gs1.update(wspace=0.03, hspace=0.03)  # set the spacing between axes.


################# Main sweep ########################
kneadingRangesAndParameters = [2, 9, 50., 10, 0.94, 1.17, 0.08, 0.24]
xmin = kneadingRangesAndParameters[4]
xmax = kneadingRangesAndParameters[5]
ymin = kneadingRangesAndParameters[6]
ymax = kneadingRangesAndParameters[7]

#http://matplotlib.org/users/gridspec.html
ax = plt.subplot(gs1[:,:])

plt.axis('on')

numberOfTicks = 4
xStep = round((xmax - xmin) / numberOfTicks, 2)
yStep = round((ymax - ymin) / numberOfTicks, 2)

#ax.set_xticks(arange(xmin, xmax + xStep, xStep))
#ax.set_yticks(arange(ymin, ymax + yStep, yStep))
ax.set_xticks([])
ax.set_yticks([])
ax.text( 0., -0.02, xmin, ha='left', va='top', bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)
ax.text( 1., -0.02, xmax, ha='right', va='top', bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)
ax.text(-0.02, 1., ymax, ha='right', va='top', rotation=90, bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)
ax.text(-0.02, 0, ymin, ha='right', va='bottom', rotation=90, bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)

if not dataSaved :
    sweepData = sweepLaser11(kneadingsStart=kneadingRangesAndParameters[0], kneadingsEnd=kneadingRangesAndParameters[1],
                   sweepSize=sweepSize, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax,
                   g=kneadingRangesAndParameters[2], sigma=kneadingRangesAndParameters[3])
    pickle.dump(sweepData, pickleDataFile2)
else :
    sweepData = pickle.load(pickleDataFile2)

ax.imshow(sweepData, extent=[xmin, xmax, ymin, ymax], aspect=(xmax - xmin) / (ymax - ymin),
          cmap=customColorMap,origin='lower')
ax.autoscale(False)
ax.set_adjustable('box-forced')

#A=0 point
ax.plot(1.157, 0.1935, 'o', color='white', ms=4)
ax.annotate(r'$\boldsymbol{IF_2}$', xy=(1.157, 0.1935), xytext=(1.157, 0.194), bbox=bbox_props, color='white' )
#Main T-point
ax.plot(1.10054, 0.192854, 'o', color='white', ms=4)
ax.annotate(r'$\boldsymbol{T_0}$', xy=(1.10054, 0.192854), xytext=(1.11, 0.205), arrowprops=arrowprops, bbox=bbox_props, color='white' )
# lower pair T-point 1.05398, .0885933
ax.plot(1.05398, .0885933, 'o', color='white', ms=4)
ax.annotate(r'$\boldsymbol{T_2^2}$', xy=(1.05398, .0885933), xytext=(1.11, .0885933), arrowprops=arrowprops, bbox=bbox_props, color='white' )
# upper pair T-point 1.06744, .122637
ax.plot(1.06744, .122637, 'o', color='white', ms=4)
ax.annotate(r'$\boldsymbol{T_2^1}$', xy=(1.06744, .122637), xytext=(1.115, .135), arrowprops=arrowprops, bbox=bbox_props, color='white' )
# middle T-point 1.07446, .138992
ax.plot(1.07446, .138992, 'o', color='white', ms=4)
ax.annotate(r'$\boldsymbol{T_1}$', xy=(1.07446, .138992), xytext=(1.107, .145), arrowprops=arrowprops, bbox=bbox_props, color='white' )
# false T-point 1.13485, .109461
ax.plot(1.13485, .109461, 'o', color='white', ms=4)
ax.annotate(r'$\boldsymbol{T_3}$', xy=(1.13485, .109461), xytext=(1.14, .109461), bbox=bbox_props, color='white' )
# circle near false T-point 1.10738, .106295
ax.plot(1.10738, .106295, 'o', color='white', ms=4)
ax.annotate(r'$\boldsymbol{C}$', xy=(1.10738, .106295), xytext=(1.109, .103295), arrowprops=arrowprops, bbox=bbox_props, color='white' )
# saddle pair 1.03528, .0975449
ax.plot(1.03528, .0975449, 'o', color='white', ms=4)
ax.annotate(r'$\boldsymbol{S}$', xy=(1.03528, .0975449), xytext=(1.029, .096), bbox=bbox_props, color='white' )

################# Larger image inset ########################
kneadingRangesAndParameters = [0, 7, 50., 10, .8, 1.2, 0.07, 0.35]

xminMain = xmin; xmaxMain = xmax; yminMain = ymin; ymaxMain = ymax;

xmin = kneadingRangesAndParameters[4]
xmax = kneadingRangesAndParameters[5]
ymin = kneadingRangesAndParameters[6]
ymax = kneadingRangesAndParameters[7]

inset_axes1 = inset_axes(ax, width="40%", height="40%", loc=2)
#plt.setp(inset_axes1.spines.values(), color='white')
for i in inset_axes1.spines.itervalues() :
    i.set_linewidth(1.) ; i.set_color('white')

inset_axes1.tick_params(axis='x',colors='white')
inset_axes1.tick_params(axis='y',colors='white')
inset_axes1.set_xticks([])
inset_axes1.set_yticks([])
inset_axes1.text( 0., -0.02, r'$\boldsymbol{0.8}$', ha='left', va='top', transform=inset_axes1.transAxes, fontsize=defaultFontSize, color='white')
inset_axes1.text( 1., -0.02, r'$\boldsymbol{1.2}$', ha='right', va='top', transform=inset_axes1.transAxes, fontsize=defaultFontSize, color='white')
inset_axes1.text(1.03, 1., r'$\boldsymbol{0.35}$', ha='left', va='top', rotation=90, transform=inset_axes1.transAxes, fontsize=defaultFontSize, color='white')
inset_axes1.text(1.03, 0, r'$\boldsymbol{0.07}$', ha='left', va='bottom', rotation=90, transform=inset_axes1.transAxes, fontsize=defaultFontSize, color='white')


#inset_axes1.yaxis.tick_right()
inset_axes1.yaxis.set_tick_params(which='both', right=True, labelright=True,
                left=True, labelleft=False)
[i.set_linewidth(1.0) for i in inset_axes1.spines.itervalues()]

plt.axis('on')

if not dataSaved :
    sweepData = sweepLaser11(kneadingsStart=kneadingRangesAndParameters[0], kneadingsEnd=kneadingRangesAndParameters[1],
                    sweepSize=sweepSize, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax,
                    g=kneadingRangesAndParameters[2], sigma=kneadingRangesAndParameters[3])
    pickle.dump(sweepData, pickleDataFile2)
else :
    sweepData = pickle.load(pickleDataFile2)

inset_axes1.imshow(sweepData, extent=[xmin, xmax, ymin, ymax], aspect=(xmax - xmin) / (ymax - ymin),
                   cmap=customColorMap, origin='lower')
inset_axes1.autoscale(False)
inset_axes1.set_adjustable('box-forced')

inset_axes1.add_patch(patches.Rectangle((xminMain, yminMain), xmaxMain-xminMain, ymaxMain-yminMain, fill=False, linewidth=1., color='white'))

inset_axes1.text(1., 1., r'$\boldsymbol{(a)}$', ha='right', va='top', color='w', fontweight=1000, transform=inset_axes1.transAxes, fontsize=indexFontSize)

################# A=0 inset ########################

kneadingRangesAndParameters = [16, 23, 50., 10., 1.15, 1.16, 0.188, 0.194]
xmin = kneadingRangesAndParameters[4]
xmax = kneadingRangesAndParameters[5]
ymin = kneadingRangesAndParameters[6]
ymax = kneadingRangesAndParameters[7]

inset_axes1 = inset_axes(ax, width="15%", height="15%", loc=1)
plt.setp(inset_axes1.spines.values(), color='white')
inset_axes1.set_xticks([])
inset_axes1.set_yticks([])
inset_axes1.xaxis.set_ticklabels([])
inset_axes1.yaxis.set_ticklabels([])

plt.axis('on')

if not dataSaved :
    sweepData = sweepLaser11(kneadingsStart=kneadingRangesAndParameters[0], kneadingsEnd=kneadingRangesAndParameters[1],
                    sweepSize=sweepSize, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax,
                    g=kneadingRangesAndParameters[2], sigma=kneadingRangesAndParameters[3], N=2*N)
    pickle.dump(sweepData, pickleDataFile2)
else :
    sweepData = pickle.load(pickleDataFile2)

inset_axes1.imshow(sweepData, extent=[xmin, xmax, ymin, ymax], aspect=(xmax - xmin) / (ymax - ymin),
                   cmap=customColorMap, origin='lower')
inset_axes1.autoscale(False)
inset_axes1.set_adjustable('box-forced')
for i in inset_axes1.spines.itervalues() :
    i.set_linewidth(1.) ; i.set_color('white')
ax.add_patch(patches.Rectangle((xmin, ymin), xmax-xmin, ymax-ymin, fill=False, linewidth=1., color='white'))
#mark_inset(ax, inset_axes1, loc1=3, loc2=4, fc="none", ec="white", lw=1.)

inset_axes1.plot(1.157, 0.1935, 'o', color='white', ms=4)
inset_axes1.annotate(r'$\boldsymbol{IF_2}$', xy=(1.157, 0.1935), xytext=(1.156, 0.19), bbox=bbox_props, arrowprops=arrowprops, color='white' )

inset_axes1.text(0., 1., r'$\boldsymbol{(b)}$', ha='left', va='top', color='w', fontweight=1000, transform=inset_axes1.transAxes, fontsize=indexFontSize)

################# Entire image ########################
xlabelX=0.5; xlabelY=0.07; ylabelX=0.057; ylabelY=0.5;
xlabel = r'$ \mathrm{a}$ - parameter ';
ylabel = r'$ \mathrm{b}$ - parameter '
fig.text(xlabelX, xlabelY, xlabel, ha='center', bbox=bbox_props_fig, color='darkslategray', fontsize=largeFontSize,
         weight='roman')
fig.text(ylabelX, ylabelY, ylabel, va='center', rotation='vertical', bbox=bbox_props_fig, color='darkslategray',
         fontsize=largeFontSize, weight='roman')

plt.subplots_adjust(left=0.1, right=0.9, top=0.9, bottom=0.1)
fig.savefig('Image2.jpg', bbox_inches='tight')
pickleDataFile2.close()

###################################### Image 2 #######################################################################
"""

"""
###################################### Image 3 #######################################################################
print 'Generating Image3'

if not dataSaved :
    pickleDataFile3 = gzip.open('Image3.gz','wb')
else :
    pickleDataFile3 = gzip.open('Image3.gz','rb')

figSizeX=6; figSizeY=6; gridX = 2 ; gridY = 2;

fig = plt.figure(figsize=(figSizeX, figSizeY))
gs1 = gridspec.GridSpec(gridX, gridY)
gs1.update(wspace=0.02, hspace=0.02)  # set the spacing between axes.

################# Sigma 1.5 sweeps ########################
kneadingRangesAndParameters = [1000, 1999, 50., 1.5, 3.3, 4.2, 0.27, 0.57]
xmin = kneadingRangesAndParameters[4]
xmax = kneadingRangesAndParameters[5]
ymin = kneadingRangesAndParameters[6]
ymax = kneadingRangesAndParameters[7]

numberOfTicks = 3
xStep = round((xmax - xmin) / numberOfTicks, 2)
yStep = round((ymax - ymin) / numberOfTicks, 2)
xticks = arange(xmin, xmax , xStep)
yticks = arange(ymin, ymax+yStep , yStep)

#Long range with periodicity correction
ax = plt.subplot(gs1[0,0])
plt.axis('on')
ax.set_xticks([])
ax.set_yticks([])
ax.text( 0., 1.02, xmin, ha='left', va='bottom', bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)
ax.text( 1., 1.02, xmax, ha='right', va='bottom', bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)
ax.text(-0.02, 1., ymax, ha='right', va='top', rotation=90, bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)
ax.text(-0.02, 0, ymin, ha='right', va='bottom', rotation=90, bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)

if not dataSaved :
    sweepData = sweepLaserPeriodic11(kneadingsStart=kneadingRangesAndParameters[0], kneadingsEnd=kneadingRangesAndParameters[1],
                    N=200 * N, sweepSize=sweepSize, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax,
                    g=kneadingRangesAndParameters[2], sigma=kneadingRangesAndParameters[3])
    pickle.dump(sweepData, pickleDataFile3)
else :
    sweepData = pickle.load(pickleDataFile3)

ax.imshow(sweepData, extent=[xmin, xmax, ymin, ymax], aspect=(xmax - xmin) / (ymax - ymin),
          #cmap=customColorMap,origin='lower')
          vmin=-0.1, vmax=1, cmap=customColorMap, origin='lower')
ax.autoscale(False)
ax.set_adjustable('box-forced')
ax.text(1., 1., r'$\boldsymbol{(a)}$', ha='right', va='top', color='w', fontweight=1000, transform=ax.transAxes, fontsize=indexFontSize)

ax.plot(3.827, 0.5189, 'o', color='white', ms=4)
ax.annotate(r'$\boldsymbol{IF_1}$', xy=(3.827, 0.5189), xytext=(3.72, 0.52), bbox=bbox_props, color='white' )
ax.plot(3.98, .4003, 'o', color='white', ms=4)
ax.annotate(r'$\boldsymbol{IF_2}$', xy=(3.98, .4003), xytext=(3.98, 0.385), bbox=bbox_props, color='white' )
ax.plot(3.95902, 0.438616, 'o', color='white', ms=4)
ax.annotate(r'$\boldsymbol{T_0}$', xy=(3.95902, 0.438616), xytext=(3.98, 0.435), bbox=bbox_props, color='white' )

################# A1 inset ########################

kneadingRangesAndParameters = [1000, 1999, 50., 1.5, 3.70, 3.80, 0.44, 0.50]
xmin = kneadingRangesAndParameters[4]
xmax = kneadingRangesAndParameters[5]
ymin = kneadingRangesAndParameters[6]
ymax = kneadingRangesAndParameters[7]

inset_axes1 = inset_axes(ax, width="30%", height="30%", loc=2)
plt.setp(inset_axes1.spines.values(), color='white')
inset_axes1.set_xticks([])
inset_axes1.set_yticks([])
inset_axes1.xaxis.set_ticklabels([])
inset_axes1.yaxis.set_ticklabels([])

plt.axis('on')

if not dataSaved :
    sweepData = sweepLaserPeriodic1(kneadingsStart=kneadingRangesAndParameters[0], kneadingsEnd=kneadingRangesAndParameters[1],
                    N=200*N, sweepSize=int(sweepSize/4), xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax,
                    g=kneadingRangesAndParameters[2], sigma=kneadingRangesAndParameters[3])
    pickle.dump(sweepData, pickleDataFile3)
else :
    sweepData = pickle.load(pickleDataFile3)

inset_axes1.imshow(sweepData, extent=[xmin, xmax, ymin, ymax], aspect=(xmax - xmin) / (ymax - ymin),
                   #cmap=customColorMap, origin='lower')
                   vmin=-0.1, vmax=1, cmap=customColorMap, origin='lower')
inset_axes1.autoscale(False)
inset_axes1.set_adjustable('box-forced')

for i in inset_axes1.spines.itervalues() :
    i.set_linewidth(1.) ; i.set_color('white')
ax.add_patch(patches.Rectangle((xmin, ymin), xmax-xmin, ymax-ymin, fill=False, linewidth=1., color='white'))
#mark_inset(ax, inset_axes1, loc1=1, loc2=3, fc="none", ec="rosybrown", lw=1.)


################# A1 inset ########################

################# A2 inset ########################

kneadingRangesAndParameters = [1000, 1999, 50., 1.5, 3.63, 3.71, .2993, .3243]
xmin = kneadingRangesAndParameters[4]
xmax = kneadingRangesAndParameters[5]
ymin = kneadingRangesAndParameters[6]
ymax = kneadingRangesAndParameters[7]

inset_axes2 = inset_axes(ax, width="30%", height="30%", loc=4)
plt.setp(inset_axes2.spines.values(), color='white')
inset_axes2.set_xticks([])
inset_axes2.set_yticks([])
inset_axes2.xaxis.set_ticklabels([])
inset_axes2.yaxis.set_ticklabels([])

plt.axis('on')

if not dataSaved :
    sweepData = sweepLaserPeriodic11(kneadingsStart=kneadingRangesAndParameters[0], kneadingsEnd=kneadingRangesAndParameters[1],
                    N=200*N, sweepSize=int(sweepSize/4), xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax,
                    g=kneadingRangesAndParameters[2], sigma=kneadingRangesAndParameters[3])
    pickle.dump(sweepData, pickleDataFile3)
else :
    sweepData = pickle.load(pickleDataFile3)

inset_axes2.imshow(sweepData, extent=[xmin, xmax, ymin, ymax], aspect=(xmax - xmin) / (ymax - ymin),
                   #cmap=customColorMap, origin='lower')
                   vmin=-0.1, vmax=1, cmap=customColorMap, origin='lower')
inset_axes2.autoscale(False)
inset_axes2.set_adjustable('box-forced')

for i in inset_axes2.spines.itervalues() :
    i.set_linewidth(1.) ; i.set_color('white')
ax.add_patch(patches.Rectangle((xmin, ymin), xmax-xmin, ymax-ymin, fill=False, linewidth=1., color='white'))
#mark_inset(ax, inset_axes2, loc1=2, loc2=3, fc="none", ec="rosybrown", lw=1.)

inset_axes2.plot(3.98, .4003, 'o', color='white', ms=4)
inset_axes2.annotate(r'$IF_1$', xy=(3.98, .4003), xytext=(3.97, .3853), arrowprops=arrowprops, bbox=bbox_props )

################# A2 inset ########################

kneadingRangesAndParameters = [1000, 1999, 50., 1.5, 3.3, 4.2, 0.27, 0.57]
xmin = kneadingRangesAndParameters[4]
xmax = kneadingRangesAndParameters[5]
ymin = kneadingRangesAndParameters[6]
ymax = kneadingRangesAndParameters[7]

#Long range with LZ complexity
ax = plt.subplot(gs1[0,1])
plt.axis('on')
ax.set_xticks([])
ax.set_yticks([])
ax.text( 0., 1.02, xmin, ha='left', va='bottom', bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)
ax.text( 1., 1.02, xmax, ha='right', va='bottom', bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)

if not dataSaved :
    sweepData = sweepLaserLZ11(kneadingsStart=kneadingRangesAndParameters[0], kneadingsEnd=kneadingRangesAndParameters[1],
                    N=200 * N, sweepSize=sweepSize, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax,
                    g=kneadingRangesAndParameters[2], sigma=kneadingRangesAndParameters[3])
    pickle.dump(sweepData, pickleDataFile3)
else :
    sweepData = pickle.load(pickleDataFile3)

ax.imshow(sweepData, extent=[xmin, xmax, ymin, ymax], aspect=(xmax - xmin) / (ymax - ymin),
          #cmap=customColorMap, origin='lower')
          vmin=0., vmax=0.1, cmap=customColorMap, origin='lower')
ax.autoscale(False)
ax.set_adjustable('box-forced')
ax.text(1., 1., r'$\boldsymbol{(b)}$', ha='right', va='top', color='w', fontweight=1000, transform=ax.transAxes, fontsize=indexFontSize)

ax.plot(3.827, 0.5189, 'o', color='white', ms=4)
ax.annotate(r'$\boldsymbol{IF_1}$', xy=(3.827, 0.5189), xytext=(3.72, 0.52), bbox=bbox_props, color='white' )
ax.plot(3.98, .4003, 'o', color='white', ms=4)
ax.annotate(r'$\boldsymbol{IF_2}$', xy=(3.98, .4003), xytext=(3.98, 0.385), bbox=bbox_props, color='white' )
ax.plot(3.95902, 0.438616, 'o', color='white', ms=4)
ax.annotate(r'$\boldsymbol{T_0}$', xy=(3.95902, 0.438616), xytext=(3.98, 0.435), bbox=bbox_props, color='white' )

################# A1 inset ########################

kneadingRangesAndParameters = [1000, 1999, 50., 1.5, 3.70, 3.80, 0.44, 0.50]
xmin = kneadingRangesAndParameters[4]
xmax = kneadingRangesAndParameters[5]
ymin = kneadingRangesAndParameters[6]
ymax = kneadingRangesAndParameters[7]

inset_axes1 = inset_axes(ax, width="30%", height="30%", loc=2)
plt.setp(inset_axes1.spines.values(), color='white')
inset_axes1.set_xticks([])
inset_axes1.set_yticks([])
inset_axes1.xaxis.set_ticklabels([])
inset_axes1.yaxis.set_ticklabels([])

plt.axis('on')

if not dataSaved :
    sweepData = sweepLaserLZ1(kneadingsStart=kneadingRangesAndParameters[0], kneadingsEnd=kneadingRangesAndParameters[1],
                    N=200*N, sweepSize=int(sweepSize/4), xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax,
                    g=kneadingRangesAndParameters[2], sigma=kneadingRangesAndParameters[3])
    pickle.dump(sweepData, pickleDataFile3)
else :
    sweepData = pickle.load(pickleDataFile3)

inset_axes1.imshow(sweepData, extent=[xmin, xmax, ymin, ymax], aspect=(xmax - xmin) / (ymax - ymin),
                   #cmap=customColorMap, origin='lower')
                   vmin=0., vmax=0.1, cmap=customColorMap, origin='lower')
inset_axes1.autoscale(False)
inset_axes1.set_adjustable('box-forced')

for i in inset_axes1.spines.itervalues() :
    i.set_linewidth(1.) ; i.set_color('white')
ax.add_patch(patches.Rectangle((xmin, ymin), xmax-xmin, ymax-ymin, fill=False, linewidth=1., color='white'))
#mark_inset(ax, inset_axes1, loc1=1, loc2=3, fc="none", ec="rosybrown", lw=1.)

inset_axes1.plot(3.827, 0.5189, 'o', color='white', ms=4)
inset_axes1.annotate(r'$IF_1$', xy=(3.827, 0.5189), xytext=(3.73, 0.52), arrowprops=arrowprops, bbox=bbox_props )

################# A1 inset ########################

################# A2 inset ########################

kneadingRangesAndParameters = [1000, 1999, 50., 1.5, 3.63, 3.71, .2993, .3243]
xmin = kneadingRangesAndParameters[4]
xmax = kneadingRangesAndParameters[5]
ymin = kneadingRangesAndParameters[6]
ymax = kneadingRangesAndParameters[7]

inset_axes2 = inset_axes(ax, width="30%", height="30%", loc=4)
plt.setp(inset_axes2.spines.values(), color='white')
inset_axes2.set_xticks([])
inset_axes2.set_yticks([])
inset_axes2.xaxis.set_ticklabels([])
inset_axes2.yaxis.set_ticklabels([])

plt.axis('on')

if not dataSaved :
    sweepData = sweepLaserLZ11(kneadingsStart=kneadingRangesAndParameters[0], kneadingsEnd=kneadingRangesAndParameters[1],
                    N=200*N, sweepSize=int(sweepSize/4), xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax,
                    g=kneadingRangesAndParameters[2], sigma=kneadingRangesAndParameters[3])
    pickle.dump(sweepData, pickleDataFile3)
else :
    sweepData = pickle.load(pickleDataFile3)

inset_axes2.imshow(sweepData, extent=[xmin, xmax, ymin, ymax], aspect=(xmax - xmin) / (ymax - ymin),
                   #cmap=customColorMap, origin='lower')
                   vmin=0., vmax=0.1, cmap=customColorMap, origin='lower')
inset_axes2.autoscale(False)
inset_axes2.set_adjustable('box-forced')

for i in inset_axes2.spines.itervalues() :
    i.set_linewidth(1.) ; i.set_color('white')
ax.add_patch(patches.Rectangle((xmin, ymin), xmax-xmin, ymax-ymin, fill=False, linewidth=1., color='white'))
#mark_inset(ax, inset_axes2, loc1=2, loc2=3, fc="none", ec="rosybrown", lw=1.)

inset_axes2.plot(3.98, .4003, 'o', color='white', ms=4)
inset_axes2.annotate(r'$IF_1$', xy=(3.98, .4003), xytext=(3.97, .3853), arrowprops=arrowprops, bbox=bbox_props )

################# A2 inset ########################

################# Sigma 10 sweeps ########################
kneadingRangesAndParameters = [1000, 1999, 50., 10, .8, 1.2, 0.07, 0.35]
xmin = kneadingRangesAndParameters[4]
xmax = kneadingRangesAndParameters[5]
ymin = kneadingRangesAndParameters[6]
ymax = kneadingRangesAndParameters[7]

numberOfTicks = 4
xStep = round((xmax - xmin) / numberOfTicks, 2)
yStep = round((ymax - ymin) / numberOfTicks, 2)

xticks = arange(xmin, xmax+xStep , xStep)
yticks = arange(ymin, ymax+yStep , yStep)

#Long range with periodicity correction
ax = plt.subplot(gs1[1,0])
plt.axis('on')
ax.set_xticks([])
ax.set_yticks([])

ax.text( 0., -.02, xmin, ha='left', va='top', bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)
ax.text( 1., -.02, xmax, ha='right', va='top', bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)
ax.text(-0.02, 1., ymax, ha='right', va='top', rotation=90, bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)
ax.text(-0.02, 0, ymin, ha='right', va='bottom', rotation=90, bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)

if not dataSaved :
    sweepData = sweepLaserPeriodic11(kneadingsStart=kneadingRangesAndParameters[0], kneadingsEnd=kneadingRangesAndParameters[1],
                    N=200 * N, sweepSize=sweepSize, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax,
                    g=kneadingRangesAndParameters[2], sigma=kneadingRangesAndParameters[3])
    pickle.dump(sweepData, pickleDataFile3)
else :
    sweepData = pickle.load(pickleDataFile3)

ax.imshow(sweepData, extent=[xmin, xmax, ymin, ymax], aspect=(xmax - xmin) / (ymax - ymin),
          #cmap=customColorMap,origin='lower')
          vmin=-0.1, vmax=1, cmap=customColorMap, origin='lower')
ax.autoscale(False)
ax.set_adjustable('box-forced')
ax.text(1., 1., r'$\boldsymbol{(c)}$', ha='right', va='top', color='w', fontweight=1000, transform=ax.transAxes, fontsize=indexFontSize)


ax.plot(1.10054, 0.192854, 'o', color='white', ms=4)
ax.annotate(r'$\boldsymbol{T_0}$', xy=(1.10054, 0.192854), xytext=(1.09, 0.198), bbox=bbox_props, color='white' )
ax.plot(1.157, 0.1935, 'o', color='white', ms=4)
ax.annotate(r'$\boldsymbol{IF_2}$', xy=(1.157, 0.1935), xytext=(1.159, 0.2), bbox=bbox_props, color='white' )

################# A2 inset ########################

kneadingRangesAndParameters = [1000, 1999, 50., 10, 1.127, 1.160, 0.1655, 0.1956]
xmin = kneadingRangesAndParameters[4]
xmax = kneadingRangesAndParameters[5]
ymin = kneadingRangesAndParameters[6]
ymax = kneadingRangesAndParameters[7]

inset_axes1 = inset_axes(ax, width="33%", height="33%", loc=2)
plt.setp(inset_axes1.spines.values(), color='white')
inset_axes1.set_xticks([])
inset_axes1.set_yticks([])
inset_axes1.xaxis.set_ticklabels([])
inset_axes1.yaxis.set_ticklabels([])

plt.axis('on')

if not dataSaved :
    sweepData = sweepLaserPeriodic11(kneadingsStart=kneadingRangesAndParameters[0], kneadingsEnd=kneadingRangesAndParameters[1],
                    N=200*N, sweepSize=int(sweepSize/4), xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax,
                    g=kneadingRangesAndParameters[2], sigma=kneadingRangesAndParameters[3])
    pickle.dump(sweepData, pickleDataFile3)
else :
    sweepData = pickle.load(pickleDataFile3)

inset_axes1.imshow(sweepData, extent=[xmin, xmax, ymin, ymax], aspect=(xmax - xmin) / (ymax - ymin),
                   #cmap=customColorMap, origin='lower')
                   vmin=-0.1, vmax=1, cmap=customColorMap, origin='lower')
inset_axes1.autoscale(False)
inset_axes1.set_adjustable('box-forced')

for i in inset_axes1.spines.itervalues() :
    i.set_linewidth(1.) ; i.set_color('white')
ax.add_patch(patches.Rectangle((xmin, ymin), xmax-xmin, ymax-ymin, fill=False, linewidth=1., color='white'))
#mark_inset(ax, inset_axes1, loc1=1, loc2=3, fc="none", ec="white", lw=1.)

inset_axes1.plot(1.157, 0.1935, 'o', color='white', ms=4)
inset_axes1.annotate(r'$\boldsymbol{IF_2}$', xy=(1.157, 0.1935), xytext=(1.149, 0.175), arrowprops=arrowprops, bbox=bbox_props, color='white')

################# A2 inset ########################


#Long range with LZ complexity
kneadingRangesAndParameters = [1000, 1999, 50., 10, .8, 1.2, 0.07, 0.35]
xmin = kneadingRangesAndParameters[4]
xmax = kneadingRangesAndParameters[5]
ymin = kneadingRangesAndParameters[6]
ymax = kneadingRangesAndParameters[7]

ax = plt.subplot(gs1[1,1])
plt.axis('on')
ax.set_xticks([])
ax.set_yticks([])
ax.text( 0., -.02, xmin, ha='left', va='top', bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)
ax.text( 1., -.02, xmax, ha='right', va='top', bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)

if not dataSaved :
    sweepData = sweepLaserLZ11(kneadingsStart=kneadingRangesAndParameters[0], kneadingsEnd=kneadingRangesAndParameters[1],
                    N=200 * N, sweepSize=sweepSize, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax,
                    g=kneadingRangesAndParameters[2], sigma=kneadingRangesAndParameters[3])
    pickle.dump(sweepData, pickleDataFile3)
else :
    sweepData = pickle.load(pickleDataFile3)

ax.imshow(sweepData, extent=[xmin, xmax, ymin, ymax], aspect=(xmax - xmin) / (ymax - ymin),
          #cmap=customColorMap, origin='lower')
          vmin = 0., vmax=0.1, cmap = customColorMap, origin = 'lower')
ax.autoscale(False)
ax.set_adjustable('box-forced')
ax.text(1., 1., r'$\boldsymbol{(d)}$', ha='right', va='top', color='w', fontweight=1000, transform=ax.transAxes, fontsize=indexFontSize)


ax.plot(1.10054, 0.192854, 'o', color='white', ms=4)
ax.annotate(r'$\boldsymbol{T_0}$', xy=(1.10054, 0.192854), xytext=(1.09, 0.198), bbox=bbox_props, color='white' )
ax.plot(1.157, 0.1935, 'o', color='white', ms=4)
ax.annotate(r'$\boldsymbol{IF_2}$', xy=(1.157, 0.1935), xytext=(1.159, 0.2), bbox=bbox_props, color='white' )

################# A2 inset ########################

kneadingRangesAndParameters = [1000, 1999, 50., 10, 1.127, 1.160, 0.1655, 0.1956]
xmin = kneadingRangesAndParameters[4]
xmax = kneadingRangesAndParameters[5]
ymin = kneadingRangesAndParameters[6]
ymax = kneadingRangesAndParameters[7]

inset_axes1 = inset_axes(ax, width="33%", height="33%", loc=2)
plt.setp(inset_axes1.spines.values(), color='white')
inset_axes1.set_xticks([])
inset_axes1.set_yticks([])
inset_axes1.xaxis.set_ticklabels([])
inset_axes1.yaxis.set_ticklabels([])

plt.axis('on')

if not dataSaved :
    sweepData = sweepLaserLZ11(kneadingsStart=kneadingRangesAndParameters[0], kneadingsEnd=kneadingRangesAndParameters[1],
                    N=200*N, sweepSize=int(sweepSize/4), xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax,
                    g=kneadingRangesAndParameters[2], sigma=kneadingRangesAndParameters[3])
    pickle.dump(sweepData, pickleDataFile3)
else :
    sweepData = pickle.load(pickleDataFile3)

inset_axes1.imshow(sweepData, extent=[xmin, xmax, ymin, ymax], aspect=(xmax - xmin) / (ymax - ymin),
                   #cmap=customColorMap, origin='lower')
                   vmin = 0., vmax=0.1, cmap = customColorMap, origin = 'lower')
inset_axes1.autoscale(False)
inset_axes1.set_adjustable('box-forced')

for i in inset_axes1.spines.itervalues() :
    i.set_linewidth(1.) ; i.set_color('white')
ax.add_patch(patches.Rectangle((xmin, ymin), xmax-xmin, ymax-ymin, fill=False, linewidth=1., color='white'))
#mark_inset(ax, inset_axes1, loc1=1, loc2=3, fc="none", ec="rosybrown", lw=1.)

inset_axes1.plot(1.157, 0.1935, 'o', color='white', ms=4)
inset_axes1.annotate(r'$\boldsymbol{IF_2}$', xy=(1.157, 0.1935), xytext=(1.149, 0.175), bbox=bbox_props, arrowprops=arrowprops, color='white' )

################# A2 inset ########################

################# Entire image ########################
xlabelX=0.65; xlabelY=-0.02; ylabelX=-0.03; ylabelY=0.65;
xlabel = r'$ \mathrm{a}$ - parameter ';
ylabel = r'$ \mathrm{b}$ - parameter '
fig.text(xlabelX, xlabelY, xlabel, ha='center', bbox=bbox_props_fig, color='darkslategray', fontsize=largeFontSize,
         weight='roman')
fig.text(ylabelX, ylabelY, ylabel, va='center', rotation='vertical', bbox=bbox_props_fig, color='darkslategray',
         fontsize=largeFontSize, weight='roman')

plt.subplots_adjust(left=0.01, right=0.9, top=0.9, bottom=0.01)
fig.savefig('Image3.jpg', bbox_inches='tight')
pickleDataFile3.close()
###################################### Image 3 #######################################################################
"""

"""
###################################### Image 4 #######################################################################
print 'Generating Image4'

if not dataSaved:
    pickleDataFile4 = gzip.open('Image4.gz', 'wb')
else:
    pickleDataFile4 = gzip.open('Image4.gz', 'rb')

figSizeX=6; figSizeY=6; gridX = 2 ; gridY = 2;

fig = plt.figure(figsize=(figSizeX, figSizeY))
gs1 = gridspec.GridSpec(gridX, gridY)
gs1.update(wspace=0.03, hspace=0.03)  # set the spacing between axes.


#sigma 1.5
kneadingRangesAndParameters = [0, 0, 50., 1.5, 3.2, 3.6, 0.25, 0.29]
xmin = kneadingRangesAndParameters[4]
xmax = kneadingRangesAndParameters[5]
ymin = kneadingRangesAndParameters[6]
ymax = kneadingRangesAndParameters[7]

numberOfTicks = 2
xStep = round((xmax - xmin) / numberOfTicks, 2)
yStep = round((ymax - ymin) / numberOfTicks, 2)

xticks = arange(xmin, xmax+xStep , xStep)
yticks = arange(ymin, ymax+yStep , yStep)

# Lines to be draws for insets a, b, c
lines1 = [
    [3.4854, .250082,         3.59989, .280977],
#    [3.4545, .250082,         3.59989, .286156],
#    [3.44087, .250082,        3.59989, .288707],
    [3.20189, .26244,         3.30275, .289882],
    [3.20189, .265075,        3.28731, .289882]
]
# Lines to be draws for insets d, e, f
lines2 = [
    [3.54145, .265057,        3.55998, .27008],
    [3.51541, .265057,        3.55944, .275978],
    [3.50505, .265057,        3.54949, .275978]
]

#########################################
kneadingRangesAndParameters[0] = 8; kneadingRangesAndParameters[1] = 15;
#Long range
ax = plt.subplot(gs1[0,0])
plt.axis('on')
ax.set_xticks([])
ax.set_yticks([])
ax.text( 0., 1.02, xmin, ha='left', va='bottom', bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)
ax.text( 1., 1.02, xmax, ha='right', va='bottom', bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)
ax.text(-0.02, 1.04, ymax, ha='right', va='top', rotation=90, bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)
ax.text(-0.02, 0,  ymin, ha='right', va='bottom', rotation=90, bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)
#ax.text(-0.02, 1., ("%g" % ymax) [1:], ha='right', va='top', rotation=90, bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)
#ax.text(-0.02, 0, ("%g" % ymin) [1:], ha='right', va='bottom', rotation=90, bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)

if not dataSaved :
    sweepData = sweepLaser11(kneadingsStart=kneadingRangesAndParameters[0], kneadingsEnd=kneadingRangesAndParameters[1],
                    N=N, sweepSize=sweepSize, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax,
                    g=kneadingRangesAndParameters[2], sigma=kneadingRangesAndParameters[3])
    pickle.dump(sweepData, pickleDataFile4)
else :
    sweepData = pickle.load(pickleDataFile4)

ax.imshow(sweepData, extent=[xmin, xmax, ymin, ymax], aspect=(xmax - xmin) / (ymax - ymin),
          cmap=customColorMap,origin='lower')
ax.autoscale(False)
ax.set_adjustable('box-forced')
ax.text(1., 1., r'$\boldsymbol{(a)}$', ha='right', va='top', color='w', fontweight=1000, transform=ax.transAxes, fontsize=indexFontSize)

for line in lines1:
    ax.plot((line[0],line[2]),(line[1],line[3]),'.w-', linewidth=1.0, ms=1.)

# zoomed inset marked -- 3.5, 3.56, 0.265, 0.276
ax.add_patch(patches.Rectangle((3.5, .265), .06, .011, fill=False, linewidth=1.2, color='white'))
ax.plot(3.37593, 0.269557, 'o', color='white', ms=4)
ax.annotate(r'$\boldsymbol{S}$', xy=(3.37593, 0.269557), xytext=(3.298, 0.269557), bbox=bbox_props, arrowprops=arrowprops, color='white' )

#########################################
kneadingRangesAndParameters[0] = 100; kneadingRangesAndParameters[1] = 123;
#Long range
ax = plt.subplot(gs1[0,1])
plt.axis('on')
ax.set_xticks([])
ax.set_yticks([])
ax.text( 0., 1.02, xmin, ha='left', va='bottom', bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)
ax.text( 1., 1.02, xmax, ha='right', va='bottom', bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)

if not dataSaved :
    sweepData = sweepLaserPeriodic11(kneadingsStart=kneadingRangesAndParameters[0], kneadingsEnd=kneadingRangesAndParameters[1],
                    N=5*N, sweepSize=sweepSize, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax,
                    g=kneadingRangesAndParameters[2], sigma=kneadingRangesAndParameters[3])
    pickle.dump(sweepData, pickleDataFile4)
else :
    sweepData = pickle.load(pickleDataFile4)

ax.imshow(sweepData, extent=[xmin, xmax, ymin, ymax], aspect=(xmax - xmin) / (ymax - ymin),
          cmap=customColorMap,origin='lower')
ax.autoscale(False)
ax.set_adjustable('box-forced')
ax.text(1., 1., r'$\boldsymbol{(b)}$', ha='right', va='top', color='w', fontweight=1000, transform=ax.transAxes, fontsize=indexFontSize)

for line in lines1:
    ax.plot((line[0],line[2]),(line[1],line[3]),'.w-', linewidth=1.0, ms=1.)

#########################################
#zoomed

kneadingRangesAndParameters = [0, 0, 50., 1.5, 3.5, 3.56, 0.265, 0.276]
xmin = kneadingRangesAndParameters[4]
xmax = kneadingRangesAndParameters[5]
ymin = kneadingRangesAndParameters[6]
ymax = kneadingRangesAndParameters[7]

numberOfTicks = 4
xStep = round((xmax - xmin) / numberOfTicks, 2)
yStep = round((ymax - ymin) / numberOfTicks, 3)

xticks = arange(xmin, xmax , xStep)
yticks = arange(ymin, ymax+yStep , yStep)

#########################################
kneadingRangesAndParameters[0] = 15; kneadingRangesAndParameters[1] =22;
#Long range
ax = plt.subplot(gs1[1,0])
plt.axis('on')
ax.set_xticks([])
ax.set_yticks([])

ax.text( 0., -.02, xmin, ha='left', va='top', bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)
ax.text( 1., -.02, xmax, ha='right', va='top', bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)
ax.text(-0.02, 1., ymax, ha='right', va='top', rotation=90, bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)
ax.text(-0.02, 0,  ymin, ha='right', va='bottom', rotation=90, bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)
#ax.text(-0.02, 1., ("%g" % ymax) [1:], ha='right', va='top', rotation=90, bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)
#ax.text(-0.02, 0, ("%g" % ymin) [1:], ha='right', va='bottom', rotation=90, bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)

if not dataSaved :
    sweepData = sweepLaser11(kneadingsStart=kneadingRangesAndParameters[0], kneadingsEnd=kneadingRangesAndParameters[1],
                    N=N, sweepSize=sweepSize, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax,
                    g=kneadingRangesAndParameters[2], sigma=kneadingRangesAndParameters[3])
    pickle.dump(sweepData, pickleDataFile4)
else :
    sweepData = pickle.load(pickleDataFile4)

ax.imshow(sweepData, extent=[xmin, xmax, ymin, ymax], aspect=(xmax - xmin) / (ymax - ymin),
          cmap=customColorMap,origin='lower')
ax.autoscale(False)
ax.set_adjustable('box-forced')
ax.text(1., 1., r'$\boldsymbol{(c)}$', ha='right', va='top', color='w', fontweight=1000, transform=ax.transAxes, fontsize=indexFontSize)

for line in lines2:
    ax.plot((line[0],line[2]),(line[1],line[3]),'.w-', linewidth=1.0, ms=1.)

#########################################
kneadingRangesAndParameters[0] = 100; kneadingRangesAndParameters[1] = 123;
#Long range
ax = plt.subplot(gs1[1,1])
plt.axis('on')
ax.set_xticks([])
ax.set_yticks([])
ax.text( 0., -.02, xmin, ha='left', va='top', bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)
ax.text( 1., -.02, xmax, ha='right', va='top', bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)

if not dataSaved :
    sweepData = sweepLaserPeriodic11(kneadingsStart=kneadingRangesAndParameters[0], kneadingsEnd=kneadingRangesAndParameters[1],
                    N=5*N, sweepSize=sweepSize, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax,
                    g=kneadingRangesAndParameters[2], sigma=kneadingRangesAndParameters[3])
    pickle.dump(sweepData, pickleDataFile4)
else :
    sweepData = pickle.load(pickleDataFile4)

ax.imshow(sweepData, extent=[xmin, xmax, ymin, ymax], aspect=(xmax - xmin) / (ymax - ymin),
          cmap=customColorMap,origin='lower')
ax.autoscale(False)
ax.set_adjustable('box-forced')
ax.text(1., 1., r'$\boldsymbol{(d)}$', ha='right', va='top', color='w', fontweight=1000, transform=ax.transAxes, fontsize=indexFontSize)

for line in lines2:
    ax.plot((line[0],line[2]),(line[1],line[3]),'.w-', linewidth=1.0, ms=1.)

################# Entire image ########################
xlabelX=0.71; xlabelY=0.068; ylabelX=0.061; ylabelY=0.71;
xlabel = r'$ \mathrm{a}$ - parameter ';
ylabel = r'$ \mathrm{b}$ - parameter '
fig.text(xlabelX, xlabelY, xlabel, ha='center', bbox=bbox_props_fig, color='darkslategray', fontsize=largeFontSize,
         weight='roman')
fig.text(ylabelX, ylabelY, ylabel, va='center', rotation='vertical', bbox=bbox_props_fig, color='darkslategray',
         fontsize=largeFontSize, weight='roman')

plt.subplots_adjust(left=0.1, right=0.9, top=0.9, bottom=0.1)
fig.savefig('Image4.jpg', bbox_inches='tight')
pickleDataFile4.close()
###################################### Image 4 #######################################################################
"""

"""
###################################### Image 5 #######################################################################
print 'Generating Image5'

figSizeX=6; figSizeY=4; gridX = 2 ; gridY = 3;

fig = plt.figure(figsize=(figSizeX, figSizeY))
fig.dpi = 1200 ## need to set this to see the rainbow_text properly

gs1 = gridspec.GridSpec(gridX, gridY)
gs1.update(wspace=0.03, hspace=0.03)  # set the spacing between axes.

gsSub1 = gridspec.GridSpecFromSubplotSpec(1, 2, subplot_spec = gs1[0,0:2], wspace=.03, hspace=.03 )
gsSub2 = gridspec.GridSpecFromSubplotSpec(1, 1, subplot_spec = gs1[0,2], wspace=.03, hspace=.03)
#gsSub0 = gridspec.GridSpecFromSubplotSpec(1, 3, subplot_spec = gs1[1,0:3], wspace=.03, hspace=.03)
gsSub3 = gridspec.GridSpecFromSubplotSpec(1, 3, subplot_spec = gs1[1,0:3], wspace=.03, hspace=.03)

ax1Left = plt.subplot(gsSub1[0,0])
ax1Middle = plt.subplot(gsSub1[0,1])
ax1Right = plt.subplot(gsSub2[0,0])
ax2Left = plt.subplot(gsSub3[0,0])
ax2Middle = plt.subplot(gsSub3[0,1])
ax2Right = plt.subplot(gsSub3[0,2])

ax1Left.set_xlim([-3,3]); ax1Left.set_ylim([-.52, -.1])
ax1Middle.set_xlim([-3,3]); ax1Middle.set_ylim([-.52, -.1])
ax1Right.set_xlim([5,50]); ax1Right.set_ylim([-2.5,3.5])
ax2Left.set_xlim([-3,3]); ax2Left.set_ylim([-.52, -.1])
ax2Middle.set_xlim([-3,3]); ax2Middle.set_ylim([-.52, -.1])
ax2Right.set_xlim([-1.7,3]); ax2Right.set_ylim([-.52, -.1])

ax1Left.set_xticks([]); ax1Left.set_yticks([]);
ax1Middle.set_xticks([]); ax1Middle.set_yticks([]);
ax1Right.set_xticks([]); ax1Right.set_yticks([])
ax2Left.set_xticks([]); ax2Left.set_yticks([]);
ax2Middle.set_xticks([]); ax2Middle.set_yticks([]);
ax2Right.set_xticks([]); ax2Right.set_yticks([]);

ax2Left.text( 0., -.02, -3, ha='left', va='top', bbox=bbox_props_label, transform=ax2Left.transAxes, fontsize=defaultFontSize)
ax2Left.text( 1., -.02, 3, ha='right', va='top', bbox=bbox_props_label, transform=ax2Left.transAxes, fontsize=defaultFontSize)
ax2Middle.text( 0., -.02, -3, ha='left', va='top', bbox=bbox_props_label, transform=ax2Middle.transAxes, fontsize=defaultFontSize)
ax2Middle.text( 1., -.02, 3, ha='right', va='top', bbox=bbox_props_label, transform=ax2Middle.transAxes, fontsize=defaultFontSize)
ax2Right.text( 0., -.02, -1.7, ha='left', va='top', bbox=bbox_props_label, transform=ax2Right.transAxes, fontsize=defaultFontSize)
ax2Right.text( 1., -.02, 3, ha='right', va='top', bbox=bbox_props_label, transform=ax2Right.transAxes, fontsize=defaultFontSize)

ax1Left.text(-0.02, 1., -0.1, ha='right', va='top', rotation=90, bbox=bbox_props_label, transform=ax1Left.transAxes, fontsize=defaultFontSize)
ax1Left.text(-0.02, 0,  -0.52, ha='right', va='bottom', rotation=90, bbox=bbox_props_label, transform=ax1Left.transAxes, fontsize=defaultFontSize)
ax2Left.text(-0.02, 1., -0.1, ha='right', va='top', rotation=90, bbox=bbox_props_label, transform=ax2Left.transAxes, fontsize=defaultFontSize)
ax2Left.text(-0.02, 0,  -0.52, ha='right', va='bottom', rotation=90, bbox=bbox_props_label, transform=ax2Left.transAxes, fontsize=defaultFontSize)

#ax1Right.text( 0.02, .01, 5, ha='left', va='bottom', bbox=bbox_props_label, transform=ax1Right.transAxes, fontsize=defaultFontSize)
ax1Right.text( .98, .01, 50, ha='right', va='bottom', bbox=bbox_props_label, transform=ax1Right.transAxes, fontsize=defaultFontSize)
ax1Right.text(0.02, .98, 3.5, ha='left', va='top', rotation=90, bbox=bbox_props_label, transform=ax1Right.transAxes, fontsize=defaultFontSize)
ax1Right.text(0.02, 0.01,  -2.5, ha='left', va='bottom', rotation=90, bbox=bbox_props_label, transform=ax1Right.transAxes, fontsize=defaultFontSize)
ax1Right.text(0.005, .35, r'$ \mathrm{\beta}$', ha='left', va='top', rotation=90, bbox=bbox_props_label, transform=ax1Right.transAxes, color='darkslategray', fontsize=largeFontSize,
              weight='roman')
ax1Right.text( .5, .01, r'time', ha='right', va='bottom', bbox=bbox_props_label, transform=ax1Right.transAxes, color='darkslategray', fontsize=defaultFontSize,
         weight='roman')


def f(t, y, g, sigma, a, b):
    return [g * y[2] - sigma * y[0], - y[1] - y[0] * y[3] + a * y[4], - y[2] + y[0] * y[5] - a * y[3], - y[3] + y[0] * y[1] + a * y[2], - b * (y[4] + 1) - 2 * y[0] * y[2] - 4 * a * y[1], - b * y[5] - 4 * y[0] * y[2] - 2 * a * y[1]]

r = ode(f).set_integrator('dopri5')
dt = 0.001

def rainbow_text(fig, ax, x,y,textList,colorList, isVertical=False):
    t = ax.transAxes
    if(not isVertical):
        for text,color in zip(textList, colorList):
            text = ax.text(x,y,text,color=color, transform=t, va='bottom',ha='left', fontsize=defaultFontSize)
            ex = text.get_window_extent(fig.canvas.get_renderer())
            t = transforms.offset_copy(t, x=ex.width, units='dots', fig=fig)
    else:
        for text,color in zip(textList, colorList):
            text = ax.text(x,y,text,color=color, transform=t,rotation='vertical',va='bottom',ha='left', fontsize=defaultFontSize)
            ex = text.get_window_extent(fig.canvas.get_renderer())
            t = transforms.offset_copy(t, y=ex.height, units='dots')

def plotTrajectory(g, sigma, a, b, tk, color1='r', color2='b', plotTrajectory=True, plotTime=True, axLeft = ax1Left, axRight = ax1Middle, plotSymbols = True, kneadingsText=[''], kneadingsColors=['k'], t0=0, positiveSide=True):
    if(positiveSide):
        y0 = [0 + 0.00000001, -a * b / (b + 4 * a * a), 0, 0, -b / (b + 4 * a * a), 2 * a * a / (b + 4 * a * a)]
    else :
        y0 = [0 - 0.00000001, -a * b / (b + 4 * a * a), 0, 0, -b / (b + 4 * a * a), 2 * a * a / (b + 4 * a * a)]
    #r.set_initial_value(y0, t0).set_f_params(g, sigma, a, b)
    r.set_initial_value(y0, 0).set_f_params(g, sigma, a, b)
    output = np.zeros((int((tk - t0) / dt) + 3, 7))
    #output[0, :] = n.concatenate([[t0], y0], 0)
    i = 0;
    while r.successful() and r.t < tk:
        x = np.concatenate([[r.t + dt], r.integrate(r.t + dt)], 0)
        if(r.t > t0) :
            output[i, :] = x
            i = i + 1

    yPositive = output[:, 1] > 0

    if(plotTrajectory):
        axLeft.plot(output[yPositive, 1], -output[yPositive, 6], marker='o', mec=color1, ms = 1., mfc = color1, ls='None')
        axLeft.plot(output[~yPositive, 1], -output[~yPositive, 6], marker='o', mec = color2, ms=1., mfc= color2, ls='None')
        print y0
        axLeft.plot(-y0[0], y0[5], 'ko', ms=4, mec='k')
        if plotSymbols :
            rainbow_text(fig, axLeft, 0.03, 0.9, kneadingsText, kneadingsColors)

    if(plotTime):
        axRight.plot(output[yPositive, 0], output[yPositive, 1], marker='o', ms=1., mec=color1, mfc = color1, ls='None')
        axRight.plot(output[~yPositive, 0], output[~yPositive, 1], marker='o', ms=1., mec=color2, mfc = color2, ls='None')
        axRight.plot(0, y0[0], 'ko', ms=4, mec='k')
        #if plotSymbols:
        #    rainbow_text(fig, axRight, 0.01, 0.9, kneadingsText, kneadingsColors)


# example trajectory
plotTrajectory(50, 1.5, 3.765, .44, 1500, 'lightgray', 'lightgray', plotTime=False, axLeft=ax1Middle, plotSymbols=False)
plotTrajectory(50, 1.5, 3.765, .44, 55, axLeft=ax1Middle, color1='mediumblue', color2='mediumblue', axRight=ax1Right, kneadingsText = ['{','$10100101$','...}'], kneadingsColors = ['k','mediumblue','k'])
plotTrajectory(50, 1.5, 3.765, .47, 50, plotTrajectory=False, plotSymbols=False, axLeft=ax1Left, color1='firebrick', color2='firebrick', axRight=ax1Right, kneadingsText = ['{','$10100110$','...}'], kneadingsColors = ['k','firebrick','k'])
rainbow_text(fig, ax1Right, 0.15, 0.81, ['{','$10100110$','...}'], ['k','firebrick','k'])
rainbow_text(fig, ax1Right, 0.15, 0.9, ['{','$10100101$','...}'], ['k','mediumblue','k'])

# Homoclinic
plotTrajectory(50, 1.5, 3.765, .4, 1500, 'lightgray', 'lightgray', plotTime=False, axLeft=ax1Left, plotSymbols=False)
plotTrajectory(50, 1.5, 3.827, 0.54, 14.5, axLeft=ax1Left, color1='mediumblue', color2='mediumblue', plotTime=False, plotSymbols=False)
plotTrajectory(50, 1.5, 3.827, 0.50, 14.8, axLeft=ax1Left, color1='darkgreen', color2='darkgreen', plotTime=False, plotSymbols=False)
plotTrajectory(50, 1.5, 3.827, 0.51903, 17, axLeft=ax1Left, color1='firebrick', color2='firebrick', plotTime=False, plotSymbols=False)
ax1Left.plot(0.7, -0.473, marker=(3,0,65), ms=10, mfc='firebrick', mec='firebrick')
ax1Left.text(0.77, 0.13, r'$ \mathrm{\Gamma_1}$', ha='right', va='top', rotation=0, bbox=bbox_props, transform=ax1Left.transAxes, fontsize=largeFontSize)
rainbow_text(fig, ax1Left, 0.03, 0.03, ['{','$11$','...}'], ['k','mediumblue','k'])
rainbow_text(fig, ax1Left, 0.03, 0.12, ['{','$1$','}'], ['k','firebrick','k'])
rainbow_text(fig, ax1Left, 0.03, 0.21, ['{','$10$','...}'], ['k','darkgreen','k'])

#T0 3.95902, 0.438616
#plotTrajectory(50, 1.5, 3.9598, 0.438855, 39, axLeft=ax2Middle, color1='firebrick', color2='firebrick', plotTime=False, plotSymbols=False)
#rainbow_text(fig, ax2Middle, 0.03, 0.9, ['{','$1$',r'$\overline{0}$','}'], ['k','firebrick','firebrick','k'])
#plotTrajectory(50, 1.5, 3.9598, 0.438855, 39, axLeft=ax2Middle, color1='mediumblue', color2='mediumblue', plotTime=False, plotSymbols=False, positiveSide=False)

#T-point below : T2
#plotTrajectory(50, 1.5, 3.65206, .34195455, 48., axLeft=ax3Middle, color1='mediumblue', color2='mediumblue', plotTime=False, kneadingsText = ['{','$11$',r'$\overline{0}$','}'], kneadingsColors = ['k','mediumblue','mediumblue','k'])
#ax3Middle.plot([0.04,0.0],[-0.49,-0.43],color='mediumblue',mec='mediumblue',mfc='mediumblue',marker='o',ms=.5,ls='-')

#T-point above : T1
plotTrajectory(50, 1.5, 3.681989209, .351707034621031785, 1500, 'lightgray', 'lightgray', plotTime=False, axLeft=ax2Middle, plotSymbols=False)
plotTrajectory(50, 1.5, 3.681989209, .351707034621031785, 52.2, axLeft=ax2Middle, color1 = 'mediumblue', color2 = 'mediumblue', plotTime=False, plotSymbols=False, positiveSide=False)
ax2Middle.plot([0.0,0.0],[-0.49,-0.446],color='mediumblue',mec='mediumblue',mfc='mediumblue',marker='o',ms=.5,ls='-',lw=1.5)
plotTrajectory(50, 1.5, 3.681989209, .351707034621031785, 52.2, axLeft=ax2Middle, color1 = 'firebrick', color2 = 'firebrick', plotTime=False, plotSymbols=False)
ax2Middle.plot([0.0,0.0],[-0.49,-0.446],color='firebrick',mec='firebrick',mfc='firebrick',marker='o',ms=.5,ls='-',lw=1.5)
#plotTrajectory(50, 1.5, 3.6835, .35225, 52, axLeft=ax3Middle, color1 = 'mediumblue', color2 = 'mediumblue', plotTime=False, plotSymbols=False)
rainbow_text(fig, ax2Middle, 0.03, 0.03, ['{','$1$','$0$',r'$\overline{1}$','}'], ['k','firebrick','firebrick','firebrick','k'])

#Periodic big band near IF1
#plotTrajectory(50, 1.5, 3.71, 0.44, 150, axLeft=ax2Left, color1='firebrick', color2='firebrick', plotTime=False, plotSymbols=False, t0=100)
#plotTrajectory(50, 1.5, 3.15982, 0.252271, 221, axLeft=ax2Left, color1='firebrick', color2='firebrick', plotTime=False, plotSymbols=False, t0=200)
plotTrajectory(50, 1.5, 3.37326, 0.313333, 221, axLeft=ax2Left, color1='firebrick', color2='firebrick', plotTime=False, plotSymbols=False, t0=200)
#Periodic
#plotTrajectory(50, 1.5, 4.1, 0.53, 150, axLeft=ax2Left, color1='mediumblue', color2='mediumblue', plotTime=False, plotSymbols=False, t0=100)
plotTrajectory(50, 1.5, 4.2, 0.583, 150, axLeft=ax2Left, color1='mediumblue', color2='mediumblue', plotTime=False, plotSymbols=False, t0=100)
rainbow_text(fig, ax2Left, 0.03, 0.14, ['{',r'$\overline{01}$','}'], ['k','mediumblue','k'])
rainbow_text(fig, ax2Left, 0.03, 0.03, ['{',r'$\overline{0011}$','}'], ['k','firebrick','k'])

PHC = [
        4.124910, 0.595354, 23,
        3.827, 0.51903, 17,  # IF1
        3.546020, 0.353647, 17,
        3.98, .40037, 23.,  # IF2
        4.125120, 0.479283, 21,
        4.241910, 0.598092, 25,
]
#one sided chaos
#plotTrajectory(50, 1.5, 3.4, 0.4077, 1500, 'lightgray', 'lightgray', plotTime=False, axLeft=ax2Right, plotSymbols=False)
#3.51656, 0.4365 - one sided chaos
#plotTrajectory(50, 1.5, 3.51656, 0.4365, 2000, 'lightgray', 'lightgray', plotTime=False, axLeft=ax2Right, plotSymbols=False, t0=100)
plotTrajectory(50, 1.5, 3.72, 0.48954338, 2000, 'lightgray', 'lightgray', plotTime=False, axLeft=ax2Right, plotSymbols=False)
#3.7302766, 0.49228999671 -- Further closer to IF1 : right side converges to single loop on right ; left of it two sided chaotic ; in between difficult to find one sided chaos
#colors = [ cm.Greens(x) for x in linspace(0, 1., int(len(PHC)/3)) ]
colors = [ 'red','firebrick','olive','darkgreen','dodgerblue','mediumblue']
for i in xrange(0,int(len(PHC)/3)).__reversed__():
    plotTrajectory(50, 1.5, PHC[3*i], PHC[3*i+1], PHC[3*i+2], colors[i], colors[i], axLeft=ax2Right, plotSymbols=False, plotTime=False)

#img=mpimg.imread('surface_PRL.jpg')
#ax2Right.imshow(img)

################# Entire image ########################
ax1Left.text(1., 1., '(a)', ha='right', va='top', color='k', fontweight='normal', transform=ax1Left.transAxes, fontsize=indexFontSize)
ax1Middle.text(1., 1., '(b)', ha='right', va='top', color='k', fontweight='normal', transform=ax1Middle.transAxes, fontsize=indexFontSize)
ax1Right.text(1., 1., '(c)', ha='right', va='top', color='k', fontweight='normal', transform=ax1Right.transAxes, fontsize=indexFontSize)
ax2Left.text(1., 1., '(d)', ha='right', va='top', color='k', fontweight='normal', transform=ax2Left.transAxes, fontsize=indexFontSize)
ax2Middle.text(1., 1., '(e)', ha='right', va='top', color='k', fontweight='normal', transform=ax2Middle.transAxes, fontsize=indexFontSize)
ax2Right.text(1., 1., '(f)', ha='right', va='top', color='k', fontweight='normal', transform=ax2Right.transAxes, fontsize=indexFontSize)

ax1Left.plot(0, -0.497, 'ko', ms=4, mec='k')
ax1Left.plot(.9, -0.335, 'bo', ms=4, mec='b')
ax1Left.plot(-.9, -0.335, 'go', ms=4, mec='g')

ax1Middle.plot(0, -0.497, 'ko', ms=4, mec='k')
ax2Left.plot(0, -0.497, 'ko', ms=4, mec='k')
ax2Middle.plot(0, -0.497, 'ko', ms=4, mec='k')
ax2Right.plot(0, -0.497, 'ko', ms=4, mec='k')

xlabelX=0.85; xlabelY=0.06; ylabelX=0.065; ylabelY=0.8;
xlabel = r'$ \mathrm{\beta}$';
ylabel = r'$ \mathrm{-D_{23}}$'
fig.text(xlabelX, xlabelY, xlabel, ha='center', bbox=bbox_props_fig, color='darkslategray', fontsize=largeFontSize,
         weight='roman')
fig.text(.55, xlabelY, xlabel, ha='center', bbox=bbox_props_fig, color='darkslategray', fontsize=largeFontSize,
         weight='roman')
fig.text(.25, xlabelY, xlabel, ha='center', bbox=bbox_props_fig, color='darkslategray', fontsize=largeFontSize,
         weight='roman')

fig.text(ylabelX, ylabelY, ylabel, va='center', rotation='vertical', bbox=bbox_props_fig, color='darkslategray',
         fontsize=largeFontSize, weight='roman')
fig.text(ylabelX, .3, ylabel, va='center', rotation='vertical', bbox=bbox_props_fig, color='darkslategray',
         fontsize=largeFontSize, weight='roman')

plt.subplots_adjust(left=0.1, right=0.99, top=0.99, bottom=0.1)

fig.savefig('Image5.jpg', bbox_inches='tight')
###################################### Image 5 #######################################################################
"""

"""
###################################### Image 6 #######################################################################
# 3D Saddle
print 'Generating Image6'

figSizeX = 7.0*figSize/10.;
figSizeY = 7.0*figSize/10.;
gridX = 1;
gridY = 1;
fig = plt.figure(figsize=(figSizeX, figSizeY))
gs1 = gridspec.GridSpec(gridX, gridY)
gs1.update(wspace=0.03, hspace=0.03)  # set the spacing between axes.

ax = plt.subplot(gs1[0, 0])

ax.set_xticks([]); ax.set_yticks([]);

img = mpimg.imread('3dSaddle4.jpg')
ax.imshow(img)

ax.autoscale(False)
ax.set_adjustable('box-forced')

ax.text(1150, 300, r'$\boldsymbol{b}$', va='center', bbox=bbox_props, color='white')
ax.text(350, 1200, r'$\boldsymbol{a}$', va='center', bbox=bbox_props, color='white')
ax.text(1050, 2700, r'$\boldsymbol{\sigma}$', va='center', bbox=bbox_props, color='white')


################# Entire image ########################
#plt.subplots_adjust(left=0.1, right=0.99, top=0.99, bottom=0.1)
fig.savefig('Image6.jpg', bbox_inches='tight')
###################################### Image 6 #######################################################################
"""

"""
###################################### Suppl.Movie - 1 ; Saddle sigma 1.5 #######################################################################
kneadingRangesAndParametersToCompute=[]

numberOfSteps = 50
sigmaStart, aStart, bStart, cStart, dStart = 1.5, 3., 3.7, .23, .33
sigmaEnd, aEnd, bEnd, cEnd, dEnd = 1.3, 3.6, 4.2, .21, .26
sigmaStep, aStep, bStep, cStep, dStep = (sigmaEnd - sigmaStart) / numberOfSteps, (aEnd - aStart) / numberOfSteps, (bEnd - bStart) / numberOfSteps, (cEnd - cStart) / numberOfSteps, (dEnd - dStart) / numberOfSteps

for i in arange(0,numberOfSteps):
    kneadingRangesAndParametersToCompute.append([0, 13, 50, sigmaStart+i*sigmaStep, aStart+i*aStep, bStart + i * bStep, cStart + i * cStep, dStart + i * dStep])

numberOfSteps = numberOfSteps/2
sigmaStart, aStart, bStart, cStart, dStart = 1.3, 3.6, 4.2, .21, .26
sigmaEnd, aEnd, bEnd, cEnd, dEnd = 1.2, 4., 4.4, .16, .21
sigmaStep, aStep, bStep, cStep, dStep = (sigmaEnd - sigmaStart) / numberOfSteps, (aEnd - aStart) / numberOfSteps, (bEnd - bStart) / numberOfSteps, (cEnd - cStart) / numberOfSteps, (dEnd - dStart) / numberOfSteps

for i in arange(0,numberOfSteps):
    kneadingRangesAndParametersToCompute.append([0, 13, 50, sigmaStart+i*sigmaStep, aStart+i*aStep, bStart + i * bStep, cStart + i * cStep, dStart + i * dStep])

for kneadingRangesAndParameters in kneadingRangesAndParametersToCompute :
    fig.clear()
    xmin=kneadingRangesAndParameters[4]
    xmax=kneadingRangesAndParameters[5]
    ymin=kneadingRangesAndParameters[6]
    ymax=kneadingRangesAndParameters[7]
    imshow(sweepLaser11(kneadingsStart=kneadingRangesAndParameters[0], kneadingsEnd=kneadingRangesAndParameters[1], sweepSize=sweepSize, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax, g=kneadingRangesAndParameters[2], sigma=kneadingRangesAndParameters[3]),
           extent=[xmin, xmax, ymin, ymax], aspect=(xmax - xmin)/ (ymax-ymin),
           vmin=-0.1, vmax=1., cmap=customColorMap,
           origin='lower')
    #colorbar(ticks=np.arange(-0.2,1,0.1))
    parameterStr = "Sigma: %0.4f; " % (kneadingRangesAndParameters[3]);
    kneadingRangeStr =  "%0.2f-%0.2f" % (kneadingRangesAndParameters[0], kneadingRangesAndParameters[1]);
    #autoscale(False);
    #plot(3.98209,.401106,'k.',linewidth=5);
    ax = fig.gca()
    ax.set_xticks([])
    ax.set_yticks([])

    ax.text(0., -0.02, xmin, ha='left', va='top', transform=ax.transAxes)
    ax.text(1., -0.02, xmax, ha='right', va='top', transform=ax.transAxes)
    ax.text(-0.02, 1., ymax, ha='right', va='top', rotation=90, transform=ax.transAxes)
    ax.text(-0.02, 0, ymin, ha='right', va='bottom', rotation=90, transform=ax.transAxes)

    fig.suptitle(parameterStr + "Kneading Range : " + kneadingRangeStr, fontsize=2*figSize)

    parameterStr = "_sigma_%0.4f_a_%0.2f-%0.2f_b_%0.2f-%0.2f_g_%0.2f_sweepSize_%d" % (kneadingRangesAndParameters[3], xmin, xmax, ymin, ymax, kneadingRangesAndParameters[2], sweepSize);
    fig.savefig('Output/Saddle1.5-Movie-0-13-Improved/'+parameterStr+'_kneadingRange-'+kneadingRangeStr+".jpg", bbox_inches='tight')

###################################### Suppl.Movie - 1 ; Saddle sigma 1.5 #######################################################################
"""

"""
###################################### Suppl.Movie - 2 ; Semicircles sigma 10 #######################################################################
kneadingRangesAndParametersToCompute=[]

numberOfSteps = 100
sigmaStart, aStart, bStart, cStart, dStart = 11.58, 1.06, 1.085, .035, .056
sigmaEnd, aEnd, bEnd, cEnd, dEnd = 11.116, 1.06, 1.1018, .07, .09
sigmaStep, aStep, bStep, cStep, dStep = (sigmaEnd - sigmaStart) / numberOfSteps, (aEnd - aStart) / numberOfSteps, (bEnd - bStart) / numberOfSteps, (cEnd - cStart) / numberOfSteps, (dEnd - dStart) / numberOfSteps

for i in arange(0,numberOfSteps):
    kneadingRangesAndParametersToCompute.append([0, 9, 50, sigmaStart+i*sigmaStep, aStart+i*aStep, bStart + i * bStep, cStart + i * cStep, dStart + i * dStep])

for kneadingRangesAndParameters in kneadingRangesAndParametersToCompute :
    fig.clear()
    xmin=kneadingRangesAndParameters[4]
    xmax=kneadingRangesAndParameters[5]
    ymin=kneadingRangesAndParameters[6]
    ymax=kneadingRangesAndParameters[7]
    imshow(sweepLaser11(kneadingsStart=kneadingRangesAndParameters[0], kneadingsEnd=kneadingRangesAndParameters[1], N = 10*N, sweepSize=sweepSize, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax, g=kneadingRangesAndParameters[2], sigma=kneadingRangesAndParameters[3]),
           extent=[xmin, xmax, ymin, ymax], aspect=(xmax - xmin)/ (ymax-ymin),
           vmin=-0.1, vmax=1., cmap=customColorMap,
           origin='lower')
    #colorbar(ticks=np.arange(-0.2,1,0.1))
    parameterStr = "Sigma: %0.4f; " % (kneadingRangesAndParameters[3]);
    kneadingRangeStr =  "%0.2f-%0.2f" % (kneadingRangesAndParameters[0], kneadingRangesAndParameters[1]);
    #autoscale(False);
    #plot(3.98209,.401106,'k.',linewidth=5);
    ax = fig.gca()
    ax.set_xticks([])
    ax.set_yticks([])

    ax.text(0., -0.02, xmin, ha='left', va='top', transform=ax.transAxes)
    ax.text(1., -0.02, xmax, ha='right', va='top', transform=ax.transAxes)
    ax.text(-0.02, 1., ymax, ha='right', va='top', rotation=90, transform=ax.transAxes)
    ax.text(-0.02, 0, ymin, ha='right', va='bottom', rotation=90, transform=ax.transAxes)

    fig.suptitle(parameterStr + "Kneading Range : " + kneadingRangeStr, fontsize=2*figSize)

    parameterStr = "_sigma_%0.4f_a_%0.2f-%0.2f_b_%0.2f-%0.2f_g_%0.2f_sweepSize_%d" % (kneadingRangesAndParameters[3], xmin, xmax, ymin, ymax, kneadingRangesAndParameters[2], sweepSize);
    fig.savefig('Output/Semicircle-10-Movie-0-9-Improved/'+parameterStr+'_kneadingRange-'+kneadingRangeStr+".jpg", bbox_inches='tight')


###################################### Suppl.Movie - 2 ; Semicircle sigma 10 #######################################################################
"""


"""
###################################### Suppl. Image 1 #######################################################################
print 'Generating Suppl. Image1'

if not dataSaved :
    pickleDataFile1 = gzip.open('SupplImage1.gz','wb')
else :
    pickleDataFile1 = gzip.open('SupplImage1.gz','rb')

figSizeX=6; figSizeY=6; gridX = 3 ; gridY = 3;

fig = plt.figure(figsize=(figSizeX, figSizeY))
gs1 = gridspec.GridSpec(gridX, gridY)
gs1.update(wspace=0.03, hspace=0.03)  # set the spacing between axes.


################# Main sweep ########################
kneadingRangesAndParameters = [5, 12, 50., 1.5, 3., 5., 0.2, .7]
xmin = kneadingRangesAndParameters[4]
xmax = kneadingRangesAndParameters[5]
ymin = kneadingRangesAndParameters[6]
ymax = kneadingRangesAndParameters[7]


#http://matplotlib.org/users/gridspec.html
ax = plt.subplot(gs1[:,:])

plt.axis('on')

numberOfTicks = 4
xStep = round((xmax - xmin) / numberOfTicks, 2)
yStep = round((ymax - ymin) / numberOfTicks, 2)

#ax.set_xticks(arange(xmin, xmax + xStep, xStep))
#ax.set_yticks(arange(ymin, ymax + yStep, yStep))
ax.set_xticks([])
ax.set_yticks([])
ax.text( 0., -0.02, xmin, ha='left', va='top', bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)
ax.text( 1., -0.02, xmax, ha='right', va='top', bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)
ax.text(-0.02, 1., ymax, ha='right', va='top', rotation=90, bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)
ax.text(-0.02, 0, ymin, ha='right', va='bottom', rotation=90, bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)

if not dataSaved :
    sweepData = sweepLaser1(kneadingsStart=kneadingRangesAndParameters[0], kneadingsEnd=kneadingRangesAndParameters[1],
                    sweepSize=sweepSize, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax,
                    g=kneadingRangesAndParameters[2], sigma=kneadingRangesAndParameters[3])
    pickle.dump(sweepData, pickleDataFile1)
else :
    sweepData = pickle.load(pickleDataFile1)

ax.imshow(sweepData, extent=[xmin, xmax, ymin, ymax], aspect=(xmax - xmin) / (ymax - ymin),
          #cmap=customColorMap,origin='lower')
          vmin=-0.1, vmax=1, cmap=customColorMap,origin='lower')
ax.autoscale(False)
ax.set_adjustable('box-forced')

pf1 = np.loadtxt('laser_pf_1.5_50.dat')
pf2 = np.loadtxt('laser_hopf1_1.5_50.dat')
pf3 = np.loadtxt('laser_hopf2_1.5_50.dat')
pf4 = np.loadtxt('laser_homoclinic_1.5_50.dat')
pf5 = np.loadtxt('laser_hom_1.5_50.dat')
ax.plot(pf1[:, 0], pf1[:, 1], 'w-', linewidth=1.0)
ax.plot(pf2[:, 0], pf2[:, 1], 'w-', linewidth=1.0)
ax.plot(pf3[:, 0], pf3[:, 1], 'w-', linewidth=1.0)
ax.plot(pf4[:, 0], pf4[:, 1], 'w-', linewidth=1.0)
ax.plot(pf5[:, 0], pf5[:, 1], 'w-', linewidth=1.0)

ax.annotate(r'$\boldsymbol{PF}$', xy=(4.47131 , 0.535383), xytext=(4.36 , 0.535383), bbox=bbox_props, color='white' )
ax.annotate(r'$\boldsymbol{AH_0}$', xy=(4.63525 , 0.535383), xytext=(4.64 , 0.535383),  bbox=bbox_props, color='white' )
ax.annotate(r'$\boldsymbol{AH_{1,2}}$', xy=(3.26685, 0.409016), xytext=(3.1, 0.41), bbox=bbox_props, color='white' )
ax.annotate(r'$\boldsymbol{H_0}$', xy=(4.15682, 0.518533), xytext=(4.18, 0.518533), bbox=bbox_props, color='white' )
ax.plot(3.95902, 0.438616, 'o', color='white', ms=4)
ax.annotate(r'$\boldsymbol{T_0}$', xy=(3.95902, 0.438616), xytext=(3.95902, 0.48), arrowprops=arrowprops, bbox=bbox_props, color='white' )
ax.plot(3.37386 , 0.269558, 'o', color='white', ms=4)
ax.annotate(r'$\boldsymbol{S}$', xy=(3.37386 , 0.269558), xytext=(3.29, 0.269558), bbox=bbox_props, color='white' )
ax.plot(4.56255, 0.637487, 'o', color='white', ms=4)
ax.annotate(r'$\boldsymbol{BT}$', xy=(4.56255, 0.637487), xytext=(4.585, 0.634), bbox=bbox_props, color='white' )


ax.plot(4.124910, 0.595354, 'o', color='white', ms=4)
ax.annotate(r'$\boldsymbol{P_1}$', xy=(4.124910, 0.595354), xytext=(4.02, 0.59), bbox=bbox_props, color='white')
ax.plot(3.827, 0.5189, 'o', color='white', ms=4)
ax.annotate(r'$\boldsymbol{IF_1}$', xy=(3.827, 0.5189), xytext=(3.77, 0.534), bbox=bbox_props, color='white')
ax.plot(3.546020, 0.353647, 'o', color='white', ms=4)
ax.annotate(r'$\boldsymbol{P_2}$', xy=(3.546020, 0.353647), xytext=(3.47, 0.345), bbox=bbox_props, color='white')
ax.plot(3.98, .4003, 'o', color='white', ms=4)
ax.annotate(r'$\boldsymbol{IF_2}$', xy=(3.98, .4003), xytext=(3.96, 0.375), bbox=bbox_props, color='white' )
ax.plot(4.125120, 0.479283, 'o', color='white', ms=4)
ax.annotate(r'$\boldsymbol{P_3}$', xy=(4.125120, 0.479283), xytext=(4.15, 0.47), bbox=bbox_props, color='white')
ax.plot(4.241910, 0.598092, 'o', color='white', ms=4)
ax.annotate(r'$\boldsymbol{P_4}$', xy=(4.241910, 0.598092), xytext=(4.26, 0.59), bbox=bbox_props, color='white')

#ax.plot(4.00191, 0.361475, 'o', color='white', ms=4)
ax.annotate(r'$\boldsymbol{type\ I}$', xy=(3.97403, 0.336081), xytext=(4.1, 0.33), bbox=bbox_props, arrowprops=arrowprops, color='white')
ax.plot(4.05768, 0.432677, 'o', color='white', ms=4)
ax.annotate(r'$\boldsymbol{B}$', xy=(4.05768, 0.432677), xytext=(4.09, 0.43), bbox=bbox_props, color='white')

################# A=0 inset ########################

kneadingRangesAndParameters = [5, 12, 50., 1.5, 3.70, 3.90, 0.47, 0.53]
xmin = kneadingRangesAndParameters[4]
xmax = kneadingRangesAndParameters[5]
ymin = kneadingRangesAndParameters[6]
ymax = kneadingRangesAndParameters[7]

inset_axes1 = inset_axes(ax, width="33%", height="33%", loc=2)
plt.setp(inset_axes1.spines.values(), color='white')
inset_axes1.set_xticks([xmin,xmax])
inset_axes1.set_yticks([ymin,ymax])
inset_axes1.xaxis.set_ticklabels([])
inset_axes1.yaxis.set_ticklabels([])

plt.axis('on')

if not dataSaved :
    sweepData = sweepLaser11(kneadingsStart=kneadingRangesAndParameters[0], kneadingsEnd=kneadingRangesAndParameters[1],
                    sweepSize=sweepSize, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax,
                    g=kneadingRangesAndParameters[2], sigma=kneadingRangesAndParameters[3])
    pickle.dump(sweepData, pickleDataFile1)
else :
    sweepData = pickle.load(pickleDataFile1)

inset_axes1.imshow(sweepData, extent=[xmin, xmax, ymin, ymax], aspect=(xmax - xmin) / (ymax - ymin),
                   #cmap=customColorMap, origin='lower')
                   vmin=-0.1, vmax=1, cmap=customColorMap, origin='lower')
inset_axes1.autoscale(False)
inset_axes1.set_adjustable('box-forced')
for i in inset_axes1.spines.itervalues() :
    i.set_linewidth(1.) ; i.set_color('white')
ax.add_patch(patches.Rectangle((xmin, ymin), xmax-xmin, ymax-ymin, fill=False, linewidth=1., color='white'))
#mark_inset(ax, inset_axes1, loc1=1, loc2=3, fc="none", ec="white", lw=1.)

inset_axes1.plot(3.827, 0.5189, 'o', color='white', ms=4)
inset_axes1.annotate(r'$\boldsymbol{IF_1}$', xy=(3.827, 0.5189), xytext=(3.79, 0.52), bbox=bbox_props, color='white' )

inset_axes1.text(1., 1., chr(0 + ord('a')), ha='right', va='top', bbox=bbox_props_index, transform=inset_axes1.transAxes, fontsize=largeFontSize)

#ax.plot(3.7649, 0.494916, 'o', color='white', ms=4)
inset_axes1.annotate(r'$\boldsymbol{type\ II}$', xy=(3.7649, 0.49), xytext=(3.72, 0.51), bbox=bbox_props, arrowprops=arrowprops, color='white')

################# T-points inset ########################
kneadingRangesAndParameters = [5, 12, 50., 1.5, 3.46, 3.8, 0.33, 0.36]
xmin = kneadingRangesAndParameters[4]
xmax = kneadingRangesAndParameters[5]
ymin = kneadingRangesAndParameters[6]
ymax = kneadingRangesAndParameters[7]

inset_axes2 = inset_axes(ax, width="45%", height="45%", loc=4)
plt.setp(inset_axes2.spines.values(), color='white')
inset_axes2.set_xticks([xmin,xmax])
inset_axes2.set_yticks([ymin,ymax])
inset_axes2.xaxis.set_ticklabels([])
inset_axes2.yaxis.set_ticklabels([])

plt.axis('on')

if not dataSaved :
    sweepData = sweepLaser1(kneadingsStart=kneadingRangesAndParameters[0], kneadingsEnd=kneadingRangesAndParameters[1],
                    sweepSize=sweepSize, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax,
                    g=kneadingRangesAndParameters[2], sigma=kneadingRangesAndParameters[3])
    pickle.dump(sweepData, pickleDataFile1)
else :
    sweepData = pickle.load(pickleDataFile1)

inset_axes2.imshow(sweepData, extent=[xmin, xmax, ymin, ymax], aspect=(xmax - xmin) / (ymax - ymin),
                   #cmap=customColorMap, origin='lower')
                   vmin=-0.1, vmax=1, cmap=customColorMap, origin='lower')
inset_axes2.autoscale(False)
inset_axes2.set_adjustable('box-forced')
for i in inset_axes2.spines.itervalues() :
    i.set_linewidth(1.) ; i.set_color('white')
ax.add_patch(patches.Rectangle((xmin, ymin), xmax-xmin, ymax-ymin, fill=False, linewidth=1., color='white'))
#mark_inset(ax, inset_axes2, loc1=2, loc2=3, fc="none", ec="white", lw=1.)


inset_axes2.plot(3.68179, .351831, 'o', color='white', ms=4)
inset_axes2.annotate(r'$\boldsymbol{T_1}$', xy=(3.68179, .351831), xytext=(3.59, .356), arrowprops=arrowprops, bbox=bbox_props, color='white' )

inset_axes2.plot(3.6516, .341926, 'o', color='white', ms=4)
inset_axes2.annotate(r'$\boldsymbol{T_2}$', xy=(3.6516, .341926), xytext=(3.55, .343), arrowprops=arrowprops, bbox=bbox_props, color='white' )

inset_axes2.text(1., 1., chr(1 + ord('a')), ha='right', va='top', bbox=bbox_props_index, transform=inset_axes2.transAxes, fontsize=largeFontSize)

################# Entire image ########################
xlabelX=0.5; xlabelY=0.067; ylabelX=0.057; ylabelY=0.5;
xlabel = r'$ \mathrm{a}$ - parameter ';
ylabel = r'$ \mathrm{b}$ - parameter '
fig.text(xlabelX, xlabelY, xlabel, ha='center', bbox=bbox_props_fig, color='darkslategray', fontsize=largeFontSize,
         weight='roman')
fig.text(ylabelX, ylabelY, ylabel, va='center', rotation='vertical', bbox=bbox_props_fig, color='darkslategray',
         fontsize=largeFontSize, weight='roman')

plt.subplots_adjust(left=0.1, right=0.999, top=0.999, bottom=0.1)
fig.savefig('SupplImage1.jpg', bbox_inches='tight')

pickleDataFile1.close()
###################################### Suppl. Image 1 #######################################################################
"""

#plt.show()