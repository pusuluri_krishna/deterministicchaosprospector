import numpy as np

Tf = np.matrix('1.76, 0.55; 1.24, 0.81')

#t(a-0.24 b) = A * t(c d)

A = Tf.I
#[[ 1.08929532 -0.73964497]
# [-1.66756321  2.36686391]]

print A[0,]/np.linalg.norm(A[0,])
print A[1,]/np.linalg.norm(A[1,])

#[[ 0.82730625 -0.56175116]]
#[[-0.57595353  0.81748243]]

"""
import matplotlib.cm as cm
from matplotlib.colors import LinearSegmentedColormap
cdict = cm.get_cmap('Set1')._segmentdata

print cdict['red']
print cdict['green']
print cdict['blue']

#http://stackoverflow.com/questions/18926031/how-to-extract-a-subset-of-a-colormap-as-a-new-colormap-in-matplotlib
"""