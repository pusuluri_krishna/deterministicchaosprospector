from __future__ import division
import matplotlib
matplotlib.use('TkAgg')
import ctypes as ct
from pylab import *
import numpy as np
from matplotlib import colors
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from mpl_toolkits.axes_grid.inset_locator import inset_axes
from mpl_toolkits.axes_grid.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid.inset_locator import mark_inset
import matplotlib.patches as patches
import pickle
import gzip
from scipy.integrate import ode
from matplotlib import transforms
import matplotlib.image as mpimg
from numpy.ma import masked_array

colorMapLevels = 2**16; N = 30000 ; dataSaved = True; sweepSize = 2000


#lib1 = ct.cdll.LoadLibrary('./treseCuda.so')
libTransform = ct.cdll.LoadLibrary('./treseTransformCuda.so')
libTransformRightSided = ct.cdll.LoadLibrary('./treseTransformCuda_RightSidedLoops.so')
libTransformLeftSided = ct.cdll.LoadLibrary('./treseTransformCuda_LeftSidedLoops.so')
libLongKneadings = ct.cdll.LoadLibrary('./treseTransformLongKneadings.so')

libTransform.sweepTrese.argtypes = [ct.POINTER(ct.c_double),
                               ct.c_double, ct.c_double, ct.c_uint,
                               ct.c_double, ct.c_double, ct.c_uint,
                               ct.c_double, ct.c_uint, ct.c_uint,
                               ct.c_uint, ct.c_uint]
def sweepTransform(dt=0.01,
               N=30000,
               stride=1,
               xmin=3, xmax=5,
               ymin=0.3, ymax=0.7,
               kneadingsStart=0,
               kneadingsEnd=16,
               sweepSize=100
               ):
    kneadingsWeightedSumSet = np.zeros(sweepSize * sweepSize, float)
    libTransform.sweepTrese(kneadingsWeightedSumSet.ctypes.data_as(ct.POINTER(ct.c_double)),
                   ct.c_double(xmin), ct.c_double(xmax), ct.c_uint(sweepSize),
                   ct.c_double(ymin), ct.c_double(ymax), ct.c_uint(sweepSize),
                   ct.c_double(dt), ct.c_uint(N), ct.c_uint(stride),
                   ct.c_uint(kneadingsStart), ct.c_uint(kneadingsEnd))
    # imshow takes fortran style array input
    return np.reshape(kneadingsWeightedSumSet, (sweepSize, sweepSize), 'F')

    lib2.sweepTrese.argtypes = [ct.POINTER(ct.c_double),
                                ct.c_double, ct.c_double, ct.c_uint,
                                ct.c_double, ct.c_double, ct.c_uint,
                                ct.c_double, ct.c_uint, ct.c_uint,
                                ct.c_uint, ct.c_uint]


libTransformRightSided.sweepTrese.argtypes = [ct.POINTER(ct.c_double),
                            ct.c_double, ct.c_double, ct.c_uint,
                            ct.c_double, ct.c_double, ct.c_uint,
                            ct.c_double, ct.c_uint, ct.c_uint,
                            ct.c_uint, ct.c_uint]

def sweepTransformRightSided(dt=0.01,
                N=30000,
                stride=1,
                xmin=3, xmax=5,
                ymin=0.3, ymax=0.7,
                kneadingsStart=0,
                kneadingsEnd=16,
                sweepSize=100
                ):
    kneadingsWeightedSumSet = np.zeros(sweepSize * sweepSize, float)
    libTransformRightSided.sweepTrese(kneadingsWeightedSumSet.ctypes.data_as(ct.POINTER(ct.c_double)),
                    ct.c_double(xmin), ct.c_double(xmax), ct.c_uint(sweepSize),
                    ct.c_double(ymin), ct.c_double(ymax), ct.c_uint(sweepSize),
                    ct.c_double(dt), ct.c_uint(N), ct.c_uint(stride),
                    ct.c_uint(kneadingsStart), ct.c_uint(kneadingsEnd))
    # imshow takes fortran style array input
    return np.reshape(kneadingsWeightedSumSet, (sweepSize, sweepSize), 'F')

libTransformLeftSided.sweepTrese.argtypes = [ct.POINTER(ct.c_double),
                            ct.c_double, ct.c_double, ct.c_uint,
                            ct.c_double, ct.c_double, ct.c_uint,
                            ct.c_double, ct.c_uint, ct.c_uint,
                            ct.c_uint, ct.c_uint]

def sweepTransformLeftSided(dt=0.01,
                N=30000,
                stride=1,
                xmin=3, xmax=5,
                ymin=0.3, ymax=0.7,
                kneadingsStart=0,
                kneadingsEnd=16,
                sweepSize=100
                ):
    kneadingsWeightedSumSet = np.zeros(sweepSize * sweepSize, float)
    libTransformLeftSided.sweepTrese(kneadingsWeightedSumSet.ctypes.data_as(ct.POINTER(ct.c_double)),
                    ct.c_double(xmin), ct.c_double(xmax), ct.c_uint(sweepSize),
                    ct.c_double(ymin), ct.c_double(ymax), ct.c_uint(sweepSize),
                    ct.c_double(dt), ct.c_uint(N), ct.c_uint(stride),
                    ct.c_uint(kneadingsStart), ct.c_uint(kneadingsEnd))
    # imshow takes fortran style array input
    return np.reshape(kneadingsWeightedSumSet, (sweepSize, sweepSize), 'F')

libLongKneadings.sweepTrese.argtypes = [ct.POINTER(ct.c_double),
                                ct.c_double, ct.c_double, ct.c_uint,
                                ct.c_double, ct.c_double, ct.c_uint,
                                ct.c_double, ct.c_uint, ct.c_uint,
                                ct.c_uint, ct.c_uint]
def sweepLongKneadings(dt=0.01,
                N=30000,
                stride=1,
                xmin=3, xmax=5,
                ymin=0.3, ymax=0.7,
                kneadingsStart=0,
                kneadingsEnd=16,
                sweepSize=100
                ):
    kneadingsWeightedSumSet = np.zeros(sweepSize * sweepSize, float)
    libLongKneadings.sweepTrese(kneadingsWeightedSumSet.ctypes.data_as(ct.POINTER(ct.c_double)),
                    ct.c_double(xmin), ct.c_double(xmax), ct.c_uint(sweepSize),
                    ct.c_double(ymin), ct.c_double(ymax), ct.c_uint(sweepSize),
                    ct.c_double(dt), ct.c_uint(N), ct.c_uint(stride),
                    ct.c_uint(kneadingsStart), ct.c_uint(kneadingsEnd))
    # imshow takes fortran style array input
    return np.reshape(kneadingsWeightedSumSet, (sweepSize, sweepSize), 'F')


blue = np.linspace(0.01, 1, colorMapLevels)
red = 1 - blue
min = 0.; max = .8
green = min + np.random.random(colorMapLevels) * (max - min)
RGB = np.column_stack((red, green, blue))
customColorMap = colors.ListedColormap(RGB)


blue = np.linspace(0.8, 0, colorMapLevels)
red = blue
green = blue
RGB = np.column_stack((red, green, blue))
customGrayColorMap = colors.ListedColormap(RGB)

#to force common color scheme without scaling across images, use the vmin and vmax arguments of imshow(), and make sure they are the same for all your images
#http://stackoverflow.com/questions/17989917/imshow-subplots-with-the-same-colorbar

# xmin = 1.; xmax = 5.; ymin = 0.1; ymax = 0.7;
figSize = 5

matplotlib.rcParams['savefig.dpi'] = 1200 #2*sweepSize/figSize #300
defaultFontSize = 11; largeFontSize = 14
matplotlib.rcParams.update({'font.size': largeFontSize,
              'axes.labelsize': largeFontSize,
              'axes.titlesize': largeFontSize,
              'text.fontsize': defaultFontSize,
              'legend.fontsize': largeFontSize,
              'xtick.labelsize': defaultFontSize,
              'ytick.labelsize': defaultFontSize})

matplotlib.rc('text', usetex=True)
matplotlib.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]

sweepData = [[0.1,0.2],[0.3,0.4]]                           #### For testing purposes

bbox_props = dict(boxstyle="square,pad=0.2", fc="white", ec="k", lw=.5, alpha=0.)
arrowprops = dict(arrowstyle="-", connectionstyle="arc3", facecolor='white', lw=1.,
                  ec="white")  # ,shrink=0.05, width=defaultFontSize/10.)
bbox_props_fig = dict(boxstyle="square,pad=0.001", fc="white", ec="k", lw=0)
bbox_props_index = dict(boxstyle="square,pad=0.2", fc="white", ec="k", lw=.5)
bbox_props_label = dict(boxstyle="square,pad=0.01", fc="white", ec="k", lw=0	)


"""
###################################### Image 1 #######################################################################
print 'Generating Image1'

if not dataSaved :
    pickleDataFile1 = gzip.open('Output/Image1.gz','wb')
else :
    pickleDataFile1 = gzip.open('Output/Image1.gz','rb')

figSizeX=6; figSizeY=6; gridX = 3 ; gridY = 3;

fig = plt.figure(figsize=(figSizeX, figSizeY))
gs1 = gridspec.GridSpec(gridX, gridY)
gs1.update(wspace=0.03, hspace=0.03)  # set the spacing between axes.


################# Main sweep ########################
kneadingRangesAndParameters = [0, 1, 0., 1., 0, 1.]
xmin = kneadingRangesAndParameters[2]
xmax = kneadingRangesAndParameters[3]
ymin = kneadingRangesAndParameters[4]
ymax = kneadingRangesAndParameters[5]


#http://matplotlib.org/users/gridspec.html
ax = plt.subplot(gs1[:,:])

plt.axis('on')

ax.set_xticks([])
ax.set_yticks([])
ax.text( 0., -0.02, xmin, ha='left', va='top', bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)
ax.text( 1., -0.02, xmax, ha='right', va='top', bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)
ax.text(-0.02, 1., ymax, ha='right', va='top', rotation=90, bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)
ax.text(-0.02, 0, ymin, ha='right', va='bottom', rotation=90, bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)

if not dataSaved :
    sweepData = sweepTransform(kneadingsStart=kneadingRangesAndParameters[0], kneadingsEnd=kneadingRangesAndParameters[1],
                    sweepSize=sweepSize, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax)
    pickle.dump(sweepData, pickleDataFile1)
else :
    sweepData = pickle.load(pickleDataFile1)

ax.imshow(sweepData, extent=[xmin, xmax, ymin, ymax], aspect=(xmax - xmin) / (ymax - ymin),
          vmin=-0.1, vmax=1, cmap=customColorMap,origin='lower')
ax.autoscale(False)
ax.set_adjustable('box-forced')

xlabelX=0.6; xlabelY=0.067; ylabelX=0.057; ylabelY=0.5;
xlabel = r'$ \mathrm{c}$ - parameter ';
ylabel = r'$ \mathrm{d}$ - parameter '
fig.text(xlabelX, xlabelY, xlabel, ha='center', bbox=bbox_props_fig, color='darkslategray', fontsize=largeFontSize,
         weight='roman')
fig.text(ylabelX, ylabelY, ylabel, va='center', rotation='vertical', bbox=bbox_props_fig, color='darkslategray',
         fontsize=largeFontSize, weight='roman')

plt.subplots_adjust(left=0.1, right=0.999, top=0.999, bottom=0.1)
fig.savefig('Output/Image1.jpg', bbox_inches='tight')

pickleDataFile1.close()
###################################### Image 1 #######################################################################
"""


"""
###################################### Image 2 #######################################################################
print 'Generating Image2'

if not dataSaved :
    pickleDataFile1 = gzip.open('Output/Image2.gz','wb')
else :
    pickleDataFile1 = gzip.open('Output/Image2.gz','rb')

figSizeX=6; figSizeY=6; gridX = 3 ; gridY = 3;

fig = plt.figure(figsize=(figSizeX, figSizeY))
gs1 = gridspec.GridSpec(gridX, gridY)
gs1.update(wspace=0.03, hspace=0.03)  # set the spacing between axes.


################# Main sweep ########################
kneadingRangesAndParameters = [0, 2, 0., 1., 0, 1.]
xmin = kneadingRangesAndParameters[2]
xmax = kneadingRangesAndParameters[3]
ymin = kneadingRangesAndParameters[4]
ymax = kneadingRangesAndParameters[5]


#http://matplotlib.org/users/gridspec.html
ax = plt.subplot(gs1[:,:])

plt.axis('on')

ax.set_xticks([])
ax.set_yticks([])
ax.text( 0., -0.02, xmin, ha='left', va='top', bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)
ax.text( 1., -0.02, xmax, ha='right', va='top', bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)
ax.text(-0.02, 1., ymax, ha='right', va='top', rotation=90, bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)
ax.text(-0.02, 0, ymin, ha='right', va='bottom', rotation=90, bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)

if not dataSaved :
    sweepData = sweepTransform(kneadingsStart=kneadingRangesAndParameters[0], kneadingsEnd=kneadingRangesAndParameters[1],
                    sweepSize=sweepSize, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax)
    pickle.dump(sweepData, pickleDataFile1)
else :
    sweepData = pickle.load(pickleDataFile1)

ax.imshow(sweepData, extent=[xmin, xmax, ymin, ymax], aspect=(xmax - xmin) / (ymax - ymin),
          vmin=-0.1, vmax=1, cmap=customColorMap,origin='lower')
ax.autoscale(False)
ax.set_adjustable('box-forced')

xlabelX=0.6; xlabelY=0.067; ylabelX=0.057; ylabelY=0.5;
xlabel = r'$ \mathrm{c}$ - parameter ';
ylabel = r'$ \mathrm{d}$ - parameter '
fig.text(xlabelX, xlabelY, xlabel, ha='center', bbox=bbox_props_fig, color='darkslategray', fontsize=largeFontSize,
         weight='roman')
fig.text(ylabelX, ylabelY, ylabel, va='center', rotation='vertical', bbox=bbox_props_fig, color='darkslategray',
         fontsize=largeFontSize, weight='roman')

plt.subplots_adjust(left=0.1, right=0.999, top=0.999, bottom=0.1)
fig.savefig('Output/Image2.jpg', bbox_inches='tight')

pickleDataFile1.close()
###################################### Image 2 #######################################################################
"""

"""
###################################### Image 3 #######################################################################
print 'Generating Image3'

if not dataSaved :
    pickleDataFile1 = gzip.open('Output/Image3.gz','wb')
else :
    pickleDataFile1 = gzip.open('Output/Image3.gz','rb')

figSizeX=8; figSizeY=4; gridX = 2 ; gridY = 4;

fig = plt.figure(figsize=(figSizeX, figSizeY))
gs1 = gridspec.GridSpec(gridX, gridY)
gs1.update(wspace=0.03, hspace=0.03)  # set the spacing between axes.


for i in range(8):
    print i
    ################# sweeps ########################
    kneadingRangesAndParameters = [2, i+2, 0., .5, 0, 1.]
    xmin = kneadingRangesAndParameters[2]
    xmax = kneadingRangesAndParameters[3]
    ymin = kneadingRangesAndParameters[4]
    ymax = kneadingRangesAndParameters[5]

    #http://matplotlib.org/users/gridspec.html
    ax = plt.subplot(gs1[int(i/4),int(i%4)])

    plt.axis('on')

    ax.set_xticks([])
    ax.set_yticks([])

    if not dataSaved :
        sweepData = sweepTransformRightSided(kneadingsStart=kneadingRangesAndParameters[0], kneadingsEnd=kneadingRangesAndParameters[1],
                        sweepSize=sweepSize, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax)
        pickle.dump(sweepData, pickleDataFile1)
    else :
        sweepData = pickle.load(pickleDataFile1)

    ax.imshow(sweepData, extent=[xmin, xmax, ymin, ymax], aspect=(xmax - xmin) / (ymax - ymin),
              cmap=customColorMap, origin='lower')
    ax.autoscale(False)
    ax.set_adjustable('box-forced')

    if int(i/4) == 1 :
        ax.text( 0., -.02, xmin, ha='left', va='top', bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)
        ax.text( 1., -.02, xmax, ha='right', va='top', bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)
    if i%4 == 0 :
        ax.text(-0.02, 1.0, ymax, ha='right', va='top', rotation=90, bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)
        ax.text(-0.02, 0,  ymin, ha='right', va='bottom', rotation=90, bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)

##################full image#########################
xlabelX=0.89; xlabelY=0.065; ylabelX=0.075; ylabelY=0.77;
xlabel = r'$ \mathrm{c}$ - parameter ';
ylabel = r'$ \mathrm{d}$ - parameter '
fig.text(xlabelX, xlabelY, xlabel, ha='center', bbox=bbox_props_fig, color='darkslategray', fontsize=largeFontSize,
         weight='roman')
fig.text(ylabelX, ylabelY, ylabel, va='center', rotation='vertical', bbox=bbox_props_fig, color='darkslategray',
         fontsize=largeFontSize, weight='roman')

plt.subplots_adjust(left=0.1, right=0.999, top=0.999, bottom=0.1)
fig.savefig('Output/Image3.jpg', bbox_inches='tight')

pickleDataFile1.close()
###################################### Image 3 #######################################################################
"""


"""
###################################### Image 4 #######################################################################
print 'Generating Image4'

if not dataSaved :
    pickleDataFile1 = gzip.open('Output/Image4.gz','wb')
else :
    pickleDataFile1 = gzip.open('Output/Image4.gz','rb')

figSizeX=8; figSizeY=4; gridX = 2 ; gridY = 4;

fig = plt.figure(figsize=(figSizeX, figSizeY))
gs1 = gridspec.GridSpec(gridX, gridY)
gs1.update(wspace=0.03, hspace=0.03)  # set the spacing between axes.


for i in range(8):
    print i
    ################# sweeps ########################
    kneadingRangesAndParameters = [2, i+2, 0., .5, 0, 1.]
    xmin = kneadingRangesAndParameters[2]
    xmax = kneadingRangesAndParameters[3]
    ymin = kneadingRangesAndParameters[4]
    ymax = kneadingRangesAndParameters[5]

    #http://matplotlib.org/users/gridspec.html
    ax = plt.subplot(gs1[int(i/4),int(i%4)])

    plt.axis('on')

    ax.set_xticks([])
    ax.set_yticks([])

    if not dataSaved :
        sweepData = sweepTransformLeftSided(kneadingsStart=kneadingRangesAndParameters[0], kneadingsEnd=kneadingRangesAndParameters[1],
                        sweepSize=sweepSize, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax)
        pickle.dump(sweepData, pickleDataFile1)
    else :
        sweepData = pickle.load(pickleDataFile1)

    ax.imshow(sweepData, extent=[xmin, xmax, ymin, ymax], aspect=(xmax - xmin) / (ymax - ymin),
              cmap=customColorMap, origin='lower')
    ax.autoscale(False)
    ax.set_adjustable('box-forced')

    if int(i/4) == 1 :
        ax.text( 0., -.02, xmin, ha='left', va='top', bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)
        ax.text( 1., -.02, xmax, ha='right', va='top', bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)
    if i%4 == 0 :
        ax.text(-0.02, 1.0, ymax, ha='right', va='top', rotation=90, bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)
        ax.text(-0.02, 0,  ymin, ha='right', va='bottom', rotation=90, bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)

##################full image#########################
xlabelX=0.89; xlabelY=0.065; ylabelX=0.075; ylabelY=0.77;
xlabel = r'$ \mathrm{c}$ - parameter ';
ylabel = r'$ \mathrm{d}$ - parameter '
fig.text(xlabelX, xlabelY, xlabel, ha='center', bbox=bbox_props_fig, color='darkslategray', fontsize=largeFontSize,
         weight='roman')
fig.text(ylabelX, ylabelY, ylabel, va='center', rotation='vertical', bbox=bbox_props_fig, color='darkslategray',
         fontsize=largeFontSize, weight='roman')

plt.subplots_adjust(left=0.1, right=0.999, top=0.999, bottom=0.1)
fig.savefig('Output/Image4.jpg', bbox_inches='tight')

pickleDataFile1.close()
###################################### Image 4 #######################################################################
"""


"""
###################################### Image 5 #######################################################################
print 'Generating Image5'

if not dataSaved :
    pickleDataFile1 = gzip.open('Output/Image5.gz','wb')
else :
    pickleDataFile1 = gzip.open('Output/Image5.gz','rb')

figSizeX=8; figSizeY=4; gridX = 2 ; gridY = 4;

fig = plt.figure(figsize=(figSizeX, figSizeY))
gs1 = gridspec.GridSpec(gridX, gridY)
gs1.update(wspace=0.03, hspace=0.03)  # set the spacing between axes.


for i in range(8):
    print i
    ################# sweeps ########################
    kneadingRangesAndParameters = [2, i+2, 0., .5, 0, 1.]
    xmin = kneadingRangesAndParameters[2]
    xmax = kneadingRangesAndParameters[3]
    ymin = kneadingRangesAndParameters[4]
    ymax = kneadingRangesAndParameters[5]

    #http://matplotlib.org/users/gridspec.html
    ax = plt.subplot(gs1[int(i/4),int(i%4)])

    plt.axis('on')

    ax.set_xticks([])
    ax.set_yticks([])

    if not dataSaved :
        sweepData = sweepTransform(kneadingsStart=kneadingRangesAndParameters[0], kneadingsEnd=kneadingRangesAndParameters[1],
                        sweepSize=sweepSize, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax)
        pickle.dump(sweepData, pickleDataFile1)
    else :
        sweepData = pickle.load(pickleDataFile1)

    ax.imshow(sweepData, extent=[xmin, xmax, ymin, ymax], aspect=(xmax - xmin) / (ymax - ymin),
              cmap=customColorMap,origin='lower')
    ax.autoscale(False)
    ax.set_adjustable('box-forced')

    if int(i/4) == 1 :
        ax.text( 0., -.02, xmin, ha='left', va='top', bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)
        ax.text( 1., -.02, xmax, ha='right', va='top', bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)
    if i%4 == 0 :
        ax.text(-0.02, 1.0, ymax, ha='right', va='top', rotation=90, bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)
        ax.text(-0.02, 0,  ymin, ha='right', va='bottom', rotation=90, bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)

##################full image#########################
xlabelX=0.89; xlabelY=0.065; ylabelX=0.075; ylabelY=0.77;
xlabel = r'$ \mathrm{c}$ - parameter ';
ylabel = r'$ \mathrm{d}$ - parameter '
fig.text(xlabelX, xlabelY, xlabel, ha='center', bbox=bbox_props_fig, color='darkslategray', fontsize=largeFontSize,
         weight='roman')
fig.text(ylabelX, ylabelY, ylabel, va='center', rotation='vertical', bbox=bbox_props_fig, color='darkslategray',
         fontsize=largeFontSize, weight='roman')

plt.subplots_adjust(left=0.1, right=0.999, top=0.999, bottom=0.1)
fig.savefig('Output/Image5.jpg', bbox_inches='tight')

pickleDataFile1.close()
###################################### Image 5 #######################################################################
"""

"""
###################################### Image 6 #######################################################################
print 'Generating Image6'

if not dataSaved :
    pickleDataFile1 = gzip.open('Output/Image6.gz','wb')
else :
    pickleDataFile1 = gzip.open('Output/Image6.gz','rb')

figSizeX=6; figSizeY=6; gridX = 3 ; gridY = 3;

fig = plt.figure(figsize=(figSizeX, figSizeY))
gs1 = gridspec.GridSpec(gridX, gridY)
gs1.update(wspace=0.03, hspace=0.03)  # set the spacing between axes.


################# Main sweep ########################
kneadingRangesAndParameters = [600, 999, 0., 1., 0, 1.]
xmin = kneadingRangesAndParameters[2]
xmax = kneadingRangesAndParameters[3]
ymin = kneadingRangesAndParameters[4]
ymax = kneadingRangesAndParameters[5]


#http://matplotlib.org/users/gridspec.html
ax = plt.subplot(gs1[:,:])

plt.axis('on')

ax.set_xticks([])
ax.set_yticks([])
ax.text( 0., -0.02, xmin, ha='left', va='top', bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)
ax.text( 1., -0.02, xmax, ha='right', va='top', bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)
ax.text(-0.02, 1., ymax, ha='right', va='top', rotation=90, bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)
ax.text(-0.02, 0, ymin, ha='right', va='bottom', rotation=90, bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)

if not dataSaved :
    sweepData = sweepLongKneadings(kneadingsStart=kneadingRangesAndParameters[0], kneadingsEnd=kneadingRangesAndParameters[1],
                    sweepSize=sweepSize, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax, N=N*100)
    pickle.dump(sweepData, pickleDataFile1)
else :
    sweepData = pickle.load(pickleDataFile1)

sweepDataRunToInfinity = masked_array(sweepData, sweepData!=-1.2)
sweepDataInsufficientN = masked_array(sweepData, sweepData != -1.1)

sweepDataComputed = masked_array(sweepData, sweepData <= -1.1)
sweepDataChaotic = masked_array(sweepDataComputed, sweepData <= 0)
sweepDataPeriodic = masked_array(sweepDataComputed, sweepData > 0)

ax.autoscale(False)
ax.set_adjustable('box-forced')

ax.imshow(sweepDataPeriodic, extent=[xmin, xmax, ymin, ymax], aspect=(xmax - xmin)/ (ymax-ymin), cmap=customColorMap, origin='lower')
ax.imshow(sweepDataChaotic, extent=[xmin, xmax, ymin, ymax], aspect= (xmax - xmin) / (ymax - ymin), cmap=customGrayColorMap, origin='lower')
ax.imshow(sweepDataRunToInfinity, extent=[xmin, xmax, ymin, ymax], aspect= (xmax - xmin) / (ymax - ymin), cmap=colors.ListedColormap(['red']), origin='lower')
ax.imshow(sweepDataInsufficientN, extent=[xmin, xmax, ymin, ymax], aspect=(xmax - xmin) / (ymax - ymin), cmap=colors.ListedColormap(['red']), origin='lower')


xlabelX=0.6; xlabelY=0.067; ylabelX=0.057; ylabelY=0.5;
xlabel = r'$ \mathrm{c}$ - parameter ';
ylabel = r'$ \mathrm{d}$ - parameter '
fig.text(xlabelX, xlabelY, xlabel, ha='center', bbox=bbox_props_fig, color='darkslategray', fontsize=largeFontSize,
         weight='roman')
fig.text(ylabelX, ylabelY, ylabel, va='center', rotation='vertical', bbox=bbox_props_fig, color='darkslategray',
         fontsize=largeFontSize, weight='roman')

plt.subplots_adjust(left=0.1, right=0.999, top=0.999, bottom=0.1)
fig.savefig('Output/Image6.jpg', bbox_inches='tight')

pickleDataFile1.close()
###################################### Image 6 #######################################################################
"""




libChuaLong = ct.cdll.LoadLibrary('../chua/chuaLongKneadings.so')
libChuaLong.sweepChua.argtypes = [ct.POINTER(ct.c_double),
                          ct.c_double,ct.c_double,ct.c_uint,
                          ct.c_double,ct.c_double,ct.c_uint,
                          ct.c_double, ct.c_uint, ct.c_uint,
                          ct.c_uint, ct.c_uint]
def sweepChuaLongKneadings (     dt=0.01,
                    N=30000,
                    stride = 1,
                    xmin = 0.8, xmax = 1.1,
                    ymin = 0, ymax = 15,
                    kneadingsStart = 0,
                    kneadingsEnd = 19,
                    sweepSize = 100
                ):
    kneadingsWeightedSumSet = np.zeros(sweepSize*sweepSize, float)
    libChuaLong.sweepChua(kneadingsWeightedSumSet.ctypes.data_as(ct.POINTER(ct.c_double)),
                  ct.c_double(xmin), ct.c_double(xmax), ct.c_uint(sweepSize),
                  ct.c_double(ymin), ct.c_double(ymax), ct.c_uint(sweepSize),
                  ct.c_double(dt), ct.c_uint(N), ct.c_uint(stride),
                  ct.c_uint(kneadingsStart), ct.c_uint(kneadingsEnd))
    return np.reshape(kneadingsWeightedSumSet, (sweepSize,sweepSize), 'F')

###################################### Image 7 #######################################################################
print 'Generating Image7'

if not dataSaved :
    pickleDataFile1 = gzip.open('Output/Image7.gz','wb')
else :
    pickleDataFile1 = gzip.open('Output/Image7.gz','rb')

figSizeX=6; figSizeY=6; gridX = 3 ; gridY = 3;

fig = plt.figure(figsize=(figSizeX, figSizeY))
gs1 = gridspec.GridSpec(gridX, gridY)
gs1.update(wspace=0.03, hspace=0.03)  # set the spacing between axes.


################# Main sweep ########################
kneadingRangesAndParameters = [600, 999, 0.84, 1.05, 0, 15]
xmin = kneadingRangesAndParameters[2]
xmax = kneadingRangesAndParameters[3]
ymin = kneadingRangesAndParameters[4]
ymax = kneadingRangesAndParameters[5]


#http://matplotlib.org/users/gridspec.html
ax = plt.subplot(gs1[:,:])

plt.axis('on')

ax.set_xticks([])
ax.set_yticks([])
ax.text( 0., -0.02, xmin, ha='left', va='top', bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)
ax.text( 1., -0.02, xmax, ha='right', va='top', bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)
ax.text(-0.02, 1., ymax, ha='right', va='top', rotation=90, bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)
ax.text(-0.02, 0, ymin, ha='right', va='bottom', rotation=90, bbox=bbox_props_label, transform=ax.transAxes, fontsize=defaultFontSize)

if not dataSaved :
    sweepData = sweepChuaLongKneadings(kneadingsStart=kneadingRangesAndParameters[0], kneadingsEnd=kneadingRangesAndParameters[1],
                    sweepSize=sweepSize, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax, N=N*100)
    pickle.dump(sweepData, pickleDataFile1)
else :
    sweepData = pickle.load(pickleDataFile1)


sweepDataInsufficientN = masked_array(sweepData, sweepData != -1.1)
sweepDataComputed = masked_array(sweepData, sweepData == -1.1)
sweepDataChaotic = masked_array(sweepDataComputed, sweepData <= 0)
sweepDataPeriodic = masked_array(sweepDataComputed, sweepData > 0)


ax.imshow(sweepDataPeriodic, extent=[xmin, xmax, ymin, ymax], aspect=(xmax - xmin)/ (ymax-ymin), cmap=customColorMap, origin='lower')
ax.autoscale(False)
ax.set_adjustable('box-forced')

ax.imshow(sweepDataChaotic, extent=[xmin, xmax, ymin, ymax], aspect= (xmax - xmin) / (ymax - ymin), cmap=customGrayColorMap, origin='lower')
ax.imshow(sweepDataInsufficientN, extent=[xmin, xmax, ymin, ymax], aspect=(xmax - xmin) / (ymax - ymin), cmap=colors.ListedColormap(['red']), origin='lower')


xlabelX=0.6; xlabelY=0.067; ylabelX=0.057; ylabelY=0.5;
xlabel = r'$ \mathrm{\alpha}$ - parameter ';
ylabel = r'$ \mathrm{L}$ - parameter '
fig.text(xlabelX, xlabelY, xlabel, ha='center', bbox=bbox_props_fig, color='darkslategray', fontsize=largeFontSize,
         weight='roman')
fig.text(ylabelX, ylabelY, ylabel, va='center', rotation='vertical', bbox=bbox_props_fig, color='darkslategray',
         fontsize=largeFontSize, weight='roman')

plt.subplots_adjust(left=0.1, right=0.999, top=0.999, bottom=0.1)
fig.savefig('Output/Image7.jpg', bbox_inches='tight')

pickleDataFile1.close()
###################################### Image 7 #######################################################################



plt.show()

