#!/usr/bin/env bash
nvcc --ptxas-options=-v -arch=sm_30 --compiler-options '-fPIC' -o treseCuda.so --shared treseCuda.cu
nvcc --ptxas-options=-v -arch=sm_30 --compiler-options '-fPIC' -o treseTransformCuda.so --shared treseTransformCuda.cu
nvcc --ptxas-options=-v -arch=sm_30 --compiler-options '-fPIC' -o treseTransformCuda_RightSidedLoops.so --shared treseTransformCuda_RightSidedLoops.cu
nvcc --ptxas-options=-v -arch=sm_30 --compiler-options '-fPIC' -o treseTransformCuda_LeftSidedLoops.so --shared treseTransformCuda_LeftSidedLoops.cu

nvcc --ptxas-options=-v -arch=sm_30 --compiler-options '-fPIC' -o treseTransformLongKneadings.so --shared treseTransformLongKneadings.cu
