#include<stdio.h>

#define N_EQ1   3
#define NUM_THREADS_PER_BLOCK 512
#define INFINITY 100000

//Differential Equations for Laser model
__device__ void stepper(const double* y, double* dydt, const double* params)
{
        double a=params[0];
        double b=params[1];

        dydt[0] = y[1];
        dydt[1] = y[2];
        dydt[2] = -b*y[2]-y[1]+a*y[0]-a*y[0]*y[0]*y[0];
}

__device__ void computeFirstDerivative(const double *y, double *dydt, const double* params){
        stepper(y, dydt, params);
        return;
}


__device__ double integrator_rk4(double* y_current, const double* params, const double dt, const unsigned N, const unsigned stride,
                                            const unsigned kneadingsStart, const unsigned kneadingsEnd)
{
	unsigned i, j, k, kneadingIndex=0;
	double dt2, dt6;
	double y1[N_EQ1], y2[N_EQ1], k1[N_EQ1], k2[N_EQ1], k3[N_EQ1], k4[N_EQ1];
	double firstDerivativeCurrent[N_EQ1],firstDerivativePrevious;
	double kneadingsWeightedSum=0;

	dt2 = dt/2.; dt6 = dt/6.;

	for(i=1; i<N; i++)
	{
		for(j=0; j<stride; j++)
		{
			stepper(y_current, k1, params);
			for(k=0; k<N_EQ1; k++) y1[k] = y_current[k]+k1[k]*dt2;
			stepper(y1, k2, params);
			for(k=0; k<N_EQ1; k++) y2[k] = y_current[k]+k2[k]*dt2;
			stepper(y2, k3, params);
			for(k=0; k<N_EQ1; k++) y2[k] = y_current[k]+k3[k]*dt;
			stepper(y2, k4, params);

			//Copy latest value into y_current
			for(k=0; k<N_EQ1; k++) y_current[k] += dt6*(k1[k]+2.*(k2[k]+k3[k])+k4[k]);
		}

		for(k=0; k<N_EQ1; k++) {
		    if(y_current[k]>INFINITY || y_current[k]<-INFINITY){
		        return -0.2;
		    }
        }
		computeFirstDerivative(y_current, firstDerivativeCurrent, params);


        // event check with first variable x - check yz
        if(firstDerivativePrevious * firstDerivativeCurrent[0] < 0) {

        	if(firstDerivativeCurrent[1] < 0 && y_current[0] > 1) {
				if(kneadingIndex>=kneadingsStart){
					//printf("1");
					kneadingsWeightedSum+=1/pow(2.,double(-kneadingIndex+kneadingsEnd+1));
				}
				kneadingIndex++;

        	} else if(firstDerivativeCurrent[1]>0 && y_current[0]< -1) {
        			//printf("0");
        			kneadingIndex++;
        	}
        }
		firstDerivativePrevious = firstDerivativeCurrent[0];

		if(kneadingIndex>kneadingsEnd)
			return kneadingsWeightedSum;
	}

	return -0.1;
}


__global__ void sweepTreseThreads(double* kneadingsWeightedSumSet,
                                        double aStart, double aEnd, unsigned aCount,
										double bStart, double bEnd, unsigned bCount,
										double dt, unsigned N, unsigned stride,
										unsigned kneadingsStart, unsigned kneadingsEnd){

    int tx=blockIdx.x * blockDim.x + threadIdx.x;

	double params[2];
	double aStep = (aEnd - aStart)/(aCount-1);
	double bStep = (bEnd-bStart)/(bCount -1);
	int i,j,k;


    if(tx<aCount*bCount){

        i=tx/aCount;
        j=tx%aCount;

        params[0]=aStart+i*aStep;
        params[1]=bStart+j*bStep;

        double y_initial[N_EQ1]={0+0.00000001L,0,0};
        kneadingsWeightedSumSet[i*bCount+j] = integrator_rk4(y_initial, params, dt, N, stride,
                                                                kneadingsStart, kneadingsEnd);
    }
}

/**
 * error checking routine
 */
void checkAndDisplayErrors(char *label)
{
  // we need to synchronise first to catch errors due to
  // asynchronous operations that would otherwise
  // potentially go unnoticed

  cudaError_t err;

  err = cudaThreadSynchronize();
  if (err != cudaSuccess)
  {
    char *e = (char*) cudaGetErrorString(err);
    fprintf(stderr, "CUDA Error: %s (at %s)", e, label);
  }

  err = cudaGetLastError();
  if (err != cudaSuccess)
  {
    char *e = (char*) cudaGetErrorString(err);
    fprintf(stderr, "CUDA Error: %s (at %s)", e, label);
  }
}


extern "C" {

void sweepTrese(double* kneadingsWeightedSumSet,
                                        double aStart, double aEnd, unsigned aCount,
										double bStart, double bEnd, unsigned bCount,
										double dt, unsigned N, unsigned stride,
										unsigned kneadingsStart, unsigned kneadingsEnd){

        int totalParameterSpaceSize = aCount*bCount;
        double *kneadingsWeightedSumSetGpu;

        /*allocate device memory. */
        (cudaMalloc( (void**) &kneadingsWeightedSumSetGpu, totalParameterSpaceSize*sizeof(double)));
        checkAndDisplayErrors("Memory allocation");

        /*timing*/
        cudaEvent_t start_event, stop_event;
        cudaEventCreate(&start_event) ;
        cudaEventCreate(&stop_event) ;
        cudaEventRecord(start_event, 0);

        /*Dimensions. */

        int gridXDimension = totalParameterSpaceSize/NUM_THREADS_PER_BLOCK;
        if(totalParameterSpaceSize%NUM_THREADS_PER_BLOCK!=0) {
                gridXDimension += 1;
        }
        dim3 dimGrid(gridXDimension,1);
        dim3 dimBlock(NUM_THREADS_PER_BLOCK,1);

        printf(" Num of blocks per grid:       %d\n", gridXDimension);
        printf(" Num of threads per block:     %d\n", NUM_THREADS_PER_BLOCK);
        printf(" Total Num of threads running: %d\n", gridXDimension*NUM_THREADS_PER_BLOCK);
        printf(" Parameters aCount=%d, bCount=%d\n",aCount,bCount);

        /*Call kernel(global function)*/
        sweepTreseThreads<<<dimGrid, dimBlock>>>(kneadingsWeightedSumSetGpu, aStart, aEnd, aCount, bStart, bEnd, bCount, dt, N, stride, kneadingsStart, kneadingsEnd);

        cudaThreadSynchronize();
        cudaEventRecord(stop_event, 0);
        cudaEventSynchronize(stop_event);

        float time_kernel;
        cudaEventElapsedTime(&time_kernel, start_event, stop_event);
        printf("Total time(sec) %f\n", time_kernel/1000);

        /*copy data from device memory to memory. */
        (cudaMemcpy( kneadingsWeightedSumSet, kneadingsWeightedSumSetGpu, totalParameterSpaceSize*sizeof(double), cudaMemcpyDeviceToHost));
        checkAndDisplayErrors("Error while copying kneading sum values from device to host.");

        /*Free all allocated memory. */
        cudaFree(kneadingsWeightedSumSetGpu);

}

}


int main(){
	double dt=0.01;
	unsigned N=30000;
	unsigned stride = 1;
	unsigned maxKneadings = 20;
	unsigned sweepSize = 5;
	double *kneadingsWeightedSumSet;
	int i,j;
	printf("Enter sweep size :");
	scanf("%d",&sweepSize);
	kneadingsWeightedSumSet = (double *)malloc(sweepSize*sweepSize*sizeof(double));
	sweepTrese(kneadingsWeightedSumSet, 0., 2.2, sweepSize, 0, 1.5, sweepSize, dt, N, stride, 0, 10);

	for(i=0;i<sweepSize;i++){
	    for(j=0;j<sweepSize;j++){
	        printf("%f \n",kneadingsWeightedSumSet[i*sweepSize+j]);
	    }
	}
}
