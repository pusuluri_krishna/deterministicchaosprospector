import matplotlib
matplotlib.use('Agg')
import ctypes as ct
from pylab import *
import numpy as np
from matplotlib import colors
from numpy.ma import masked_array
import pickle
import gzip

lib1 = ct.cdll.LoadLibrary('./treseCuda.so')
lib2 = ct.cdll.LoadLibrary('./treseTransformCuda.so')
lib3 = ct.cdll.LoadLibrary('./treseTransformCuda_OneSidedLoops.so')
lib4 = ct.cdll.LoadLibrary('./treseTransformLongKneadings.so')

lib1.sweepTrese.argtypes = [ct.POINTER(ct.c_double),
                          ct.c_double,ct.c_double,ct.c_uint,
                          ct.c_double,ct.c_double,ct.c_uint,
                          ct.c_double, ct.c_uint, ct.c_uint,
                          ct.c_uint, ct.c_uint]
def sweepTrese1 (    dt=0.01,
                    N=30000,
                    stride = 1,
                    xmin = 3, xmax = 5,
                    ymin = 0.3, ymax = 0.7,
                    kneadingsStart = 0,
                    kneadingsEnd = 16,
                    sweepSize = 100
                ):
    kneadingsWeightedSumSet = np.zeros(sweepSize*sweepSize, float)
    lib1.sweepTrese(kneadingsWeightedSumSet.ctypes.data_as(ct.POINTER(ct.c_double)),
                  ct.c_double(xmin), ct.c_double(xmax), ct.c_uint(sweepSize),
                  ct.c_double(ymin), ct.c_double(ymax), ct.c_uint(sweepSize),
                  ct.c_double(dt), ct.c_uint(N), ct.c_uint(stride),
                  ct.c_uint(kneadingsStart), ct.c_uint(kneadingsEnd))
    #imshow takes fortran style array input
    return np.reshape(kneadingsWeightedSumSet, (sweepSize,sweepSize), 'F')


lib2.sweepTrese.argtypes = [ct.POINTER(ct.c_double),
                               ct.c_double, ct.c_double, ct.c_uint,
                               ct.c_double, ct.c_double, ct.c_uint,
                               ct.c_double, ct.c_uint, ct.c_uint,
                               ct.c_uint, ct.c_uint]
def sweepTrese2(dt=0.01,
               N=30000,
               stride=1,
               xmin=3, xmax=5,
               ymin=0.3, ymax=0.7,
               kneadingsStart=0,
               kneadingsEnd=16,
               sweepSize=100
               ):
    kneadingsWeightedSumSet = np.zeros(sweepSize * sweepSize, float)
    lib2.sweepTrese(kneadingsWeightedSumSet.ctypes.data_as(ct.POINTER(ct.c_double)),
                   ct.c_double(xmin), ct.c_double(xmax), ct.c_uint(sweepSize),
                   ct.c_double(ymin), ct.c_double(ymax), ct.c_uint(sweepSize),
                   ct.c_double(dt), ct.c_uint(N), ct.c_uint(stride),
                   ct.c_uint(kneadingsStart), ct.c_uint(kneadingsEnd))
    # imshow takes fortran style array input
    return np.reshape(kneadingsWeightedSumSet, (sweepSize, sweepSize), 'F')

    lib2.sweepTrese.argtypes = [ct.POINTER(ct.c_double),
                                ct.c_double, ct.c_double, ct.c_uint,
                                ct.c_double, ct.c_double, ct.c_uint,
                                ct.c_double, ct.c_uint, ct.c_uint,
                                ct.c_uint, ct.c_uint]


lib3.sweepTrese.argtypes = [ct.POINTER(ct.c_double),
                            ct.c_double, ct.c_double, ct.c_uint,
                            ct.c_double, ct.c_double, ct.c_uint,
                            ct.c_double, ct.c_uint, ct.c_uint,
                            ct.c_uint, ct.c_uint]

def sweepTrese3(dt=0.01,
                N=30000,
                stride=1,
                xmin=3, xmax=5,
                ymin=0.3, ymax=0.7,
                kneadingsStart=0,
                kneadingsEnd=16,
                sweepSize=100
                ):
    kneadingsWeightedSumSet = np.zeros(sweepSize * sweepSize, float)
    lib3.sweepTrese(kneadingsWeightedSumSet.ctypes.data_as(ct.POINTER(ct.c_double)),
                    ct.c_double(xmin), ct.c_double(xmax), ct.c_uint(sweepSize),
                    ct.c_double(ymin), ct.c_double(ymax), ct.c_uint(sweepSize),
                    ct.c_double(dt), ct.c_uint(N), ct.c_uint(stride),
                    ct.c_uint(kneadingsStart), ct.c_uint(kneadingsEnd))
    # imshow takes fortran style array input
    return np.reshape(kneadingsWeightedSumSet, (sweepSize, sweepSize), 'F')

lib4.sweepTrese.argtypes = [ct.POINTER(ct.c_double),
                                ct.c_double, ct.c_double, ct.c_uint,
                                ct.c_double, ct.c_double, ct.c_uint,
                                ct.c_double, ct.c_uint, ct.c_uint,
                                ct.c_uint, ct.c_uint]
def sweepTrese4(dt=0.01,
                N=30000,
                stride=1,
                xmin=3, xmax=5,
                ymin=0.3, ymax=0.7,
                kneadingsStart=0,
                kneadingsEnd=16,
                sweepSize=100
                ):
    kneadingsWeightedSumSet = np.zeros(sweepSize * sweepSize, float)
    lib4.sweepTrese(kneadingsWeightedSumSet.ctypes.data_as(ct.POINTER(ct.c_double)),
                    ct.c_double(xmin), ct.c_double(xmax), ct.c_uint(sweepSize),
                    ct.c_double(ymin), ct.c_double(ymax), ct.c_uint(sweepSize),
                    ct.c_double(dt), ct.c_uint(N), ct.c_uint(stride),
                    ct.c_uint(kneadingsStart), ct.c_uint(kneadingsEnd))
    # imshow takes fortran style array input
    return np.reshape(kneadingsWeightedSumSet, (sweepSize, sweepSize), 'F')


colorMapLevels=2**8
blue = np.linspace(0.01, 1, colorMapLevels)
red = 1 - blue
min = 0.; max = .8
green = min + np.random.random(colorMapLevels) * (max - min)
RGB = np.column_stack((red, green, blue))
customColorMap = colors.ListedColormap(RGB)


colorMapLevels=2**8
blue = np.linspace(0.8, 0, colorMapLevels)
red = blue
green = blue
RGB = np.column_stack((red, green, blue))
customGrayColorMap = colors.ListedColormap(RGB)

sweepSize = 1000
figSize = 10
fig = figure(figsize=(figSize,figSize))

outputdir1 = 'Output/treseSweep'
outputdir2 = 'Output/treseTransformSweep'
outputdir3 = 'Output/treseTransformSweep_OneSided'
outputdir4 = 'Output/treseTransformLongKneadings'

matplotlib.rcParams['savefig.dpi'] = 2*sweepSize/figSize

#TODO : create a data file and use that to compute different kneading ranges; for sigma=2 too noisy.. only smaller kneadings ranges 0-6,1-7 etc might be better


currentIteration=0
#maxIterations = len(kneadingRangesAndParametersToCompute1)+len(kneadingRangesAndParametersToCompute2)+len(kneadingRangesAndParametersToCompute3)

"""
#TreseSweep
kneadingRangesAndParametersToCompute1=[[0, 0, 0., 2.2, 0., 2.],[0, 1, 0., 2.2, 0., 2.],[0, 2, 0., 2.2, 0., 2.],[0, 3, 0., 2.2, 0., 2.],[0, 4, 0., 2.2, 0., 2.],[0, 5, 0., 2.2, 0., 2.],[0, 6, 0., 2.2, 0., 2.],[0, 7, 0., 2.2, 0., 2.],[3, 10, 0., 2.2, 0., 2.],[13, 20, 0., 2.2, 0., 2.]]
#kneadingRangesAndParametersToCompute1=[[0, 0, 1.2, 1.5, 0.6, 1.],[0, 1, 1.2, 1.5, 0.6, 1.],[0, 2, 1.2, 1.5, 0.6, 1.],[0, 3, 1.2, 1.5, 0.6, 1.],[0, 4, 1.2, 1.5, 0.6, 1.],[0, 5, 1.2, 1.5, 0.6, 1.],[0, 6, 1.2, 1.5, 0.6, 1.],[0, 7, 1.2, 1.5, 0.6, 1.],[3, 10, 1.2, 1.5, 0.6, 1.]]

for kneadingRangesAndParameters in kneadingRangesAndParametersToCompute1 :
    currentIteration=currentIteration+1
    print "Iteration :",currentIteration," out of a maximum of :", maxIterations
    fig.clear()
    xmin=kneadingRangesAndParameters[2]
    xmax=kneadingRangesAndParameters[3]
    ymin=kneadingRangesAndParameters[4]
    ymax=kneadingRangesAndParameters[5]
    imshow(sweepTrese1(kneadingsStart=kneadingRangesAndParameters[0], kneadingsEnd=kneadingRangesAndParameters[1], sweepSize=sweepSize, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax),
           extent=[xmin, xmax, ymin, ymax], aspect=(xmax - xmin)/ (ymax-ymin),
           cmap=customColorMap,
           origin='lower')
    colorbar(ticks=np.arange(-0.2,1,0.1))
    parameterStr = "a: %0.2f-%0.2f ; b: %0.2f-%0.2f " % (xmin, xmax, ymin, ymax);
    kneadingRangeStr =  "%0.2f-%0.2f" % (kneadingRangesAndParameters[0], kneadingRangesAndParameters[1]);
    fig.suptitle(parameterStr + "\nKneading Range : " + kneadingRangeStr, fontsize=2*figSize)

    parameterStr = "_a_%0.2f-%0.2f_b_%0.2f-%0.2f_sweepSize_%d" % (xmin, xmax, ymin, ymax,sweepSize);
    fig.savefig(outputdir1+parameterStr+'_kneadingRange-'+kneadingRangeStr+".jpg")
    print "\n"
"""
"""
#TreseTransformSweep
#kneadingRangesAndParametersToCompute2=[[0, 0, -0.5, 1.5, -1.5, 1.5],[0, 1, -0.5, 1.5, -1.5, 1.5],[0, 2, -0.5, 1.5, -1.5, 1.5],[0, 3, -0.5, 1.5, -1.5, 1.5],[0, 4, -0.5, 1.5, -1.5, 1.5],[0, 5, -0.5, 1.5, -1.5, 1.5],[0, 6, -0.5, 1.5, -1.5, 1.5],[0, 7, -0.5, 1.5, -1.5, 1.5],[3, 10, -0.5, 1.5, -1.5, 1.5],[13, 20, -0.5, 1.5, -1.5, 1.5]]
kneadingRangesAndParametersToCompute2=[[0, 0, 0., 1., 0., 1.],[0, 1, 0., 1., 0., 1.],[0, 2, 0., 1., 0., 1.],[0, 3,  0., 1., 0., 1.],[0, 4, 0., 1., 0., 1.],[0, 5, 0., 1., 0., 1.],[0, 6, 0., 1., 0., 1.],[0, 7, 0., 1., 0., 1.],[3, 10, 0., 1., 0., 1.],[13, 20, 0., 1., 0., 1.]]

for kneadingRangesAndParameters in kneadingRangesAndParametersToCompute2 :
    currentIteration=currentIteration+1
    print "Iteration :",currentIteration," out of a maximum of :",
    fig.clear()
    xmin=kneadingRangesAndParameters[2]
    xmax=kneadingRangesAndParameters[3]
    ymin=kneadingRangesAndParameters[4]
    ymax=kneadingRangesAndParameters[5]
    imshow(sweepTrese2(kneadingsStart=kneadingRangesAndParameters[0], kneadingsEnd=kneadingRangesAndParameters[1], sweepSize=sweepSize, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax),
           extent=[xmin, xmax, ymin, ymax], aspect=(xmax - xmin)/ (ymax-ymin),
           cmap=customColorMap,
           origin='lower')
    colorbar(ticks=np.arange(-0.2,1,0.1))
    parameterStr = "a: %0.2f-%0.2f ; b: %0.2f-%0.2f " % (xmin, xmax, ymin, ymax);
    kneadingRangeStr =  "%0.2f-%0.2f" % (kneadingRangesAndParameters[0], kneadingRangesAndParameters[1]);
    fig.suptitle(parameterStr + "\nKneading Range : " + kneadingRangeStr, fontsize=2*figSize)

    parameterStr = "_a_%0.2f-%0.2f_b_%0.2f-%0.2f_sweepSize_%d" % (xmin, xmax, ymin, ymax,sweepSize);
    fig.savefig(outputdir2+parameterStr+'_kneadingRange-'+kneadingRangeStr+".jpg")
    print "\n"
"""


"""
#TreseTransformSweep_OneSidedLoops
kneadingRangesAndParametersToCompute3=[[0, 0, -0.5, 1.5, -1.5, 1.5],[0, 1, -0.5, 1.5, -1.5, 1.5],[0, 2, -0.5, 1.5, -1.5, 1.5],[0, 3, -0.5, 1.5, -1.5, 1.5],[0, 4, -0.5, 1.5, -1.5, 1.5],[0, 5, -0.5, 1.5, -1.5, 1.5],[0, 6, -0.5, 1.5, -1.5, 1.5],[0, 7, -0.5, 1.5, -1.5, 1.5],[3, 10, -0.5, 1.5, -1.5, 1.5],[13, 20, -0.5, 1.5, -1.5, 1.5]]

for kneadingRangesAndParameters in kneadingRangesAndParametersToCompute3 :
    currentIteration=currentIteration+1
    print "Iteration :",currentIteration," out of a maximum of :", maxIterations
    fig.clear()
    xmin=kneadingRangesAndParameters[2]
    xmax=kneadingRangesAndParameters[3]
    ymin=kneadingRangesAndParameters[4]
    ymax=kneadingRangesAndParameters[5]
    imshow(sweepTrese3(kneadingsStart=kneadingRangesAndParameters[0], kneadingsEnd=kneadingRangesAndParameters[1], sweepSize=sweepSize, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax),
           extent=[xmin, xmax, ymin, ymax], aspect=(xmax - xmin)/ (ymax-ymin),
           cmap=customColorMap,
           origin='lower')
    colorbar(ticks=np.arange(-0.2,1,0.1))
    parameterStr = "a: %0.2f-%0.2f ; b: %0.2f-%0.2f " % (xmin, xmax, ymin, ymax);
    kneadingRangeStr =  "%0.2f-%0.2f" % (kneadingRangesAndParameters[0], kneadingRangesAndParameters[1]);
    fig.suptitle(parameterStr + "\nKneading Range : " + kneadingRangeStr, fontsize=2*figSize)

    parameterStr = "_a_%0.2f-%0.2f_b_%0.2f-%0.2f_sweepSize_%d" % (xmin, xmax, ymin, ymax,sweepSize);
    fig.savefig(outputdir3+parameterStr+'_kneadingRange-'+kneadingRangeStr+".jpg")
    print "\n"
"""


dataSaved = True
kneadingRangesAndParametersToComputeLong=[[400, 499, 0., 1., 0., 1.]]

for kneadingRangesAndParameters in kneadingRangesAndParametersToComputeLong :
    fig.clear()
    xmin=kneadingRangesAndParameters[2]
    xmax=kneadingRangesAndParameters[3]
    ymin=kneadingRangesAndParameters[4]
    ymax=kneadingRangesAndParameters[5]

    # colorbar(ticks=np.arange(-0.2,1,0.1))
    parameterStr = "_a_%0.2f-%0.2f_b_%0.2f-%0.2f_sweepSize_%d" % (xmin, xmax, ymin, ymax, sweepSize);
    kneadingRangeStr = "%0.2f-%0.2f" % (kneadingRangesAndParameters[0], kneadingRangesAndParameters[1]);
    # autoscale(False);
    # plot(3.98209,.401106,'k.',linewidth=5);
    fig.suptitle(parameterStr + "\nKneading Range : " + kneadingRangeStr, fontsize=2 * figSize)

    if not dataSaved:
        pickleDataFile1 = gzip.open(outputdir4+parameterStr+'_kneadingRange-'+kneadingRangeStr+'.pickle.gz', 'wb')
        sweepData = sweepTrese4(kneadingsStart=kneadingRangesAndParameters[0], kneadingsEnd=kneadingRangesAndParameters[1], sweepSize=sweepSize, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax, N=30000*50);
        pickle.dump(sweepData, pickleDataFile1)
    else:
        pickleDataFile1 = gzip.open(outputdir4 + parameterStr + '_kneadingRange-' + kneadingRangeStr + '.pickle.gz','rb')
        sweepData = pickle.load(pickleDataFile1)

    #sweepDataUncomputed = masked_array(sweepData, sweepData >-1.1) # -1.1 N insufficient; -1.2 Infinity
    sweepDataRunToInfinity = masked_array(sweepData, sweepData!=-1.2)
    sweepDataInsufficientN = masked_array(sweepData, sweepData != -1.1)

    sweepDataComputed = masked_array(sweepData, sweepData <= -1.1)
    sweepDataChaotic = masked_array(sweepDataComputed, sweepData <= 0)
    sweepDataPeriodic = masked_array(sweepDataComputed, sweepData > 0)

    imshow(sweepDataPeriodic, extent=[xmin, xmax, ymin, ymax], aspect=(xmax - xmin)/ (ymax-ymin), cmap=customColorMap, origin='lower')
    imshow(sweepDataChaotic, extent=[xmin, xmax, ymin, ymax], aspect= (xmax - xmin) / (ymax - ymin), cmap=customGrayColorMap, origin='lower')

    imshow(sweepDataRunToInfinity, extent=[xmin, xmax, ymin, ymax], aspect= (xmax - xmin) / (ymax - ymin), cmap=colors.ListedColormap(['white']), origin='lower')
    imshow(sweepDataInsufficientN, extent=[xmin, xmax, ymin, ymax], aspect=(xmax - xmin) / (ymax - ymin), cmap=colors.ListedColormap(['white']), origin='lower')

    fig.savefig(outputdir4+parameterStr+'_kneadingRange-'+kneadingRangeStr+".jpg")
    #show()

