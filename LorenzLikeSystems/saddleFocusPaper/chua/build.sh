#!/usr/bin/env bash
nvcc --ptxas-options=-v -arch=sm_30 --compiler-options '-fPIC' -o chuaSweep.so --shared chuaSweep.cu
nvcc --ptxas-options=-v -arch=sm_30 --compiler-options '-fPIC' -o chuaLongKneadings.so --shared chuaLongKneadings.cu
