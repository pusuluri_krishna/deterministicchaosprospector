#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include <sys/time.h>
#include<string.h>

#define N_EQ1	3
#define MAX_KNEADING_LENGTH 2001
#define NUM_THREADS_PER_BLOCK 512
#define INFINITY 100000

__device__ void stepper(const double* y, double* dydt, const double* params)
{
	double a=params[1]*cos(params[0])+1.8623;
	double b=params[1]*sin(params[0])+1.8743;

	dydt[0] = a*(y[1]+y[0]/6-y[0]*y[0]*y[0]/6);
	dydt[1] = y[0]-y[1]+y[2];
	dydt[2] = -b*y[1];
}

__device__ void computeFirstDerivative(const double *y, double *dydt, const double* params){
        stepper(y, dydt, params);
        return;
}

__device__ double LZ76(bool * s, int n) {
  int c=1,l=1,i=0,k=1,kmax = 1,stop=0;
  while(stop ==0) {
    if (s[i+k-1] != s[l+k-1]) {
      if (k > kmax) {
        kmax=k;
      }
      i++;

      if (i==l) {
        c++;
        l += kmax;
        if (l+1>n)
          stop = 1;
        else {
          i=0;
          k=1;
          kmax=1;
        }
      } else {
        k=1;
      }
    } else {
      k++;
      if (l+k > n) {
        c++;
        stop =1;
      }
    }
  }
  return double(c)/n;
}

__device__ double  computePeriodNormalizedKneadingSum(bool *kneadings, unsigned kneadingsLength, unsigned periodLength ){
    double kneadingSum=0, minPeriodSum=0, currPeriodSum=0;
    unsigned i=0, normalizedPeriodIndex=0;

    char s[200];unsigned si=0;
    //Also normalizing symmetric orbits -- so 00000000.. and 1111111111.. are treated identically
    double minPeriodSumSymmetric=0, currPeriodSumSymmetric=0;
    unsigned normalizedPeriodIndexSymmetric=0;

    if(periodLength<kneadingsLength){ 
        for(i=0; i<periodLength; i++) {
            currPeriodSum=0;
            for(unsigned j=0; j<periodLength; j++) {
                currPeriodSum+= 2*currPeriodSum + kneadings[i+j];
            }
            if(minPeriodSum==0 || currPeriodSum < minPeriodSum) {
                minPeriodSum = currPeriodSum;
                normalizedPeriodIndex=i;
            }
        }
        for(i=0; i<periodLength; i++) {
            currPeriodSumSymmetric=0;
            for(unsigned j=0; j<periodLength; j++) {
                currPeriodSumSymmetric+= 2*currPeriodSumSymmetric + 1-kneadings[i+j];
            }
            if(minPeriodSumSymmetric==0 || currPeriodSumSymmetric < minPeriodSumSymmetric) {
                minPeriodSumSymmetric = currPeriodSumSymmetric;
                normalizedPeriodIndexSymmetric=i;
            }
        }
    }

    //filling kneading sequence with normalized period
    for(i=0; i<kneadingsLength; i++) {
        if(minPeriodSum < minPeriodSumSymmetric){
            kneadingSum = kneadingSum + kneadings[normalizedPeriodIndex+periodLength-1-i%periodLength]/pow(2.,double(-i+kneadingsLength));
    	    s[si++]='0'+int(kneadings[normalizedPeriodIndex+periodLength-1-i%periodLength]);
        } else {
            kneadingSum = kneadingSum + (1-kneadings[normalizedPeriodIndexSymmetric+periodLength-1-i%periodLength])/pow(2.,double(-i+kneadingsLength));
    	    s[si++]='0'+int(kneadings[normalizedPeriodIndexSymmetric+periodLength-1-i%periodLength]);
        }
    }
    if(periodLength<kneadingsLength){
        //printf("\nPeriod length: %d, symbolic sequence: %s ",periodLength,s);
    } 
    /*//not filling kneading sequence with normalized period, just using a single period
    for(i=0; i<periodLength; i++) {
        if(minPeriodSum < minPeriodSumSymmetric){
            kneadingSum = kneadingSum + kneadings[normalizedPeriodIndex+periodLength-1-i%periodLength]/pow(2.,double(-i+periodLength));
        } else {
            kneadingSum = kneadingSum + (1-kneadings[normalizedPeriodIndexSymmetric+periodLength-1-i%periodLength])/pow(2.,double(-i+periodLength));
        }
    }*/

    return kneadingSum;

}

__device__ double getNormalizedPeriodAndKneadingSum(bool* kneadings, unsigned kneadingsLength){
    //After a long transient when the periodic orbits if any have already been reached, this method computes a kneading sum that is invariant between different cyclic permutations of the period

    bool periodFound=true;
    unsigned periodLength = kneadingsLength;
    for(unsigned currPeriod=1; currPeriod < kneadingsLength/2; currPeriod++) {
        periodFound=true;
        //Check if the kneading sequence has a period with periodicity of currPeriod
        for(unsigned i=currPeriod; i < kneadingsLength-currPeriod; i+=currPeriod) {
            for ( unsigned j=0; j<currPeriod; j++) {
                if(kneadings[j] != kneadings[i+j]) {
                    periodFound=false;
                    break;
                }
            }
            if(!periodFound) {
                break;
            }
        }
        //compute kneadingSum based on period found, if any
        if(periodFound) {
            //currPeriod is the period of the kneading sequence. So this will be normalized to a sequence with the sorted period(0's followed by 1's)
            periodLength = currPeriod;
            break;
        }
    }

    //return periodLength==kneadingsLength? LZ76(kneadings, kneadingsLength) : 0;
    return periodLength==kneadingsLength? LZ76(kneadings, kneadingsLength) : -computePeriodNormalizedKneadingSum( kneadings, kneadingsLength, periodLength);

    //return computePeriodNormalizedKneadingSum( kneadings, kneadingsLength, periodLength);
    //return periodLength==kneadingsLength?1:0;
    //return periodLength==kneadingsLength? -0.05 : computePeriodNormalizedKneadingSum( kneadings, kneadingsLength, periodLength);

}

__device__ double integrator_rk4(double* y_current, const double* params, const double dt, const unsigned N, const unsigned stride,
                                            const unsigned kneadingsStart, const unsigned kneadingsEnd)
{
	unsigned i, j, k, kneadingIndex=0, kneadingArrayIndex=0;
	double dt2, dt6;
	double y1[N_EQ1], y2[N_EQ1], k1[N_EQ1], k2[N_EQ1], k3[N_EQ1], k4[N_EQ1];
	double firstDerivativeCurrent[N_EQ1],firstDerivativePrevious;
	double kneadingsWeightedSum=0;
	bool kneadings[MAX_KNEADING_LENGTH];

	dt2 = dt/2.; dt6 = dt/6.;

	for(i=1; i<N; i++)
	{
		for(j=0; j<stride; j++)
		{
			stepper(y_current, k1, params);
			for(k=0; k<N_EQ1; k++) y1[k] = y_current[k]+k1[k]*dt2;
			stepper(y1, k2, params);
			for(k=0; k<N_EQ1; k++) y2[k] = y_current[k]+k2[k]*dt2;
			stepper(y2, k3, params);
			for(k=0; k<N_EQ1; k++) y2[k] = y_current[k]+k3[k]*dt;
			stepper(y2, k4, params);

			//Copy latest value into y_current
			for(k=0; k<N_EQ1; k++) y_current[k] += dt6*(k1[k]+2.*(k2[k]+k3[k])+k4[k]);
		}

		computeFirstDerivative(y_current, firstDerivativeCurrent, params);

		if(firstDerivativePrevious>0 && firstDerivativeCurrent[0]<0 &&y_current[0]>1 ){
			if(kneadingIndex>=kneadingsStart){
                            //printf("1");
			    //kneadingsWeightedSum+=1/pow(2.,double(-kneadingIndex+kneadingsEnd+1));
			    kneadings[kneadingArrayIndex++]=1;
			}
			kneadingIndex++;
		}
		if(firstDerivativePrevious<0 && firstDerivativeCurrent[0]>0 && y_current[0]<-1){
            if(kneadingIndex>=kneadingsStart){
                //printf("0");
                kneadings[kneadingArrayIndex++]=0;
            }
    		kneadingIndex++;
		}
		firstDerivativePrevious = firstDerivativeCurrent[0];

		if(kneadingIndex>kneadingsEnd)
			return getNormalizedPeriodAndKneadingSum(kneadings, kneadingArrayIndex);
			//return LZ76(kneadings, kneadingArrayIndex);
	}

        //use only to clean up long range images for AH bifurcation
        //if(y_current[0]>0.01) {for(i=0;i<kneadingsEnd-kneadingsStart+1;i++)kneadings[i]=1; return getNormalizedPeriodAndKneadingSum(kneadings,kneadingsEnd-kneadingsStart+1);}
        //else if (y_current[0]<-0.01) {for(i=0;i<kneadingsEnd-kneadingsStart+1;i++)kneadings[i]=0; return getNormalizedPeriodAndKneadingSum(kneadings,kneadingsEnd-kneadingsStart+1);}

	return -1.1;

}

__global__ void sweepChuaThreads(double* kneadingsWeightedSumSet,
                                        double aStart, double aEnd, unsigned aCount,
										double bStart, double bEnd, unsigned bCount,
										double dt, unsigned N, unsigned stride,
										unsigned kneadingsStart, unsigned kneadingsEnd){

    int tx=blockIdx.x * blockDim.x + threadIdx.x;

	double params[2];
	double aStep = aCount==1?0:(aEnd - aStart)/(aCount-1);
	double bStep = bCount==1?0:(bEnd-bStart)/(bCount -1);
	int i,j,k;


    if(tx<aCount*bCount){

        i=tx/aCount;
        j=tx%aCount;

        params[0]=aStart+i*aStep;
        params[1]=bStart+j*bStep;

        double y_initial[N_EQ1]={0+0.00000001L,0,0};
        kneadingsWeightedSumSet[i*bCount+j] = integrator_rk4(y_initial, params, dt, N, stride,
                                                                kneadingsStart, kneadingsEnd);
    }
}


/**
 * error checking routine
 */
void checkAndDisplayErrors(char *label)
{
  // we need to synchronise first to catch errors due to
  // asynchronous operations that would otherwise
  // potentially go unnoticed

  cudaError_t err;

  err = cudaThreadSynchronize();
  if (err != cudaSuccess)
  {
    char *e = (char*) cudaGetErrorString(err);
    fprintf(stderr, "CUDA Error: %s (at %s)", e, label);
  }

  err = cudaGetLastError();
  if (err != cudaSuccess)
  {
    char *e = (char*) cudaGetErrorString(err);
    fprintf(stderr, "CUDA Error: %s (at %s)", e, label);
  }
}


extern "C" {

void sweepChua(double* kneadingsWeightedSumSet,
                                        double aStart, double aEnd, unsigned aCount,
										double bStart, double bEnd, unsigned bCount,
										double dt, unsigned N, unsigned stride,
										unsigned kneadingsStart, unsigned kneadingsEnd){

        int totalParameterSpaceSize = aCount*bCount;
        double *kneadingsWeightedSumSetGpu;

        /*allocate device memory. */
        (cudaMalloc( (void**) &kneadingsWeightedSumSetGpu, totalParameterSpaceSize*sizeof(double)));
        checkAndDisplayErrors("Memory allocation");

        /*timing*/
        cudaEvent_t start_event, stop_event;
        cudaEventCreate(&start_event) ;
        cudaEventCreate(&stop_event) ;
        cudaEventRecord(start_event, 0);

        /*Dimensions. */

        int gridXDimension = totalParameterSpaceSize/NUM_THREADS_PER_BLOCK;
        if(totalParameterSpaceSize%NUM_THREADS_PER_BLOCK!=0) {
                gridXDimension += 1;
        }
        dim3 dimGrid(gridXDimension,1);
        dim3 dimBlock(NUM_THREADS_PER_BLOCK,1);

        printf(" Num of blocks per grid:       %d\n", gridXDimension);
        printf(" Num of threads per block:     %d\n", NUM_THREADS_PER_BLOCK);
        printf(" Total Num of threads running: %d\n", gridXDimension*NUM_THREADS_PER_BLOCK);
        printf(" Parameters aCount=%d, bCount=%d\n",aCount,bCount);

        /*Call kernel(global function)*/
        sweepChuaThreads<<<dimGrid, dimBlock>>>(kneadingsWeightedSumSetGpu, aStart, aEnd, aCount, bStart, bEnd, bCount, dt, N, stride, kneadingsStart, kneadingsEnd);

        cudaThreadSynchronize();
        cudaEventRecord(stop_event, 0);
        cudaEventSynchronize(stop_event);

        float time_kernel;
        cudaEventElapsedTime(&time_kernel, start_event, stop_event);
        printf("Total time(sec) %f\n", time_kernel/1000);

        /*copy data from device memory to memory. */
        (cudaMemcpy( kneadingsWeightedSumSet, kneadingsWeightedSumSetGpu, totalParameterSpaceSize*sizeof(double), cudaMemcpyDeviceToHost));
        checkAndDisplayErrors("Error while copying kneading sum values from device to host.");

        /*Free all allocated memory. */
        cudaFree(kneadingsWeightedSumSetGpu);

/*
        for(int i=0;i<aCount;i++){
            for(int j=0;j<bCount;j++){
                printf("%f ",kneadingsWeightedSumSet[i*aCount+j]);
            }
            printf("\n");
        }
        printf("\n");
*/
}

}


int main(){
	double dt=0.1;
	unsigned N=30000*10;
	unsigned stride = 1;
	unsigned maxKneadings = 200;
	unsigned sweepSize = 5;
	double *kneadingsWeightedSumSet;
	int i,j;
	printf("Enter sweep size :");
	scanf("%d",&sweepSize);
	kneadingsWeightedSumSet = (double *)malloc(sweepSize*sweepSize*sizeof(double));
	sweepChua(kneadingsWeightedSumSet, 0.8, 1.1, sweepSize, 0, 15, sweepSize, dt, N, stride, 800, 999);

	for(i=0;i<sweepSize;i++){
	    for(j=0;j<sweepSize;j++){
	        printf("%f \n",kneadingsWeightedSumSet[i*sweepSize+j]);
	    }
	}
}

