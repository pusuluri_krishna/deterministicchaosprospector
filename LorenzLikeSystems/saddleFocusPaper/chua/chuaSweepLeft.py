import ctypes as ct
from pylab import *
import numpy as np
from matplotlib import colors

lib = ct.cdll.LoadLibrary('./chuaSweepLeft.so')

lib.sweepChua.argtypes = [ct.POINTER(ct.c_double), ct.POINTER(ct.c_double),
                          ct.c_double,ct.c_double,ct.c_uint,
                          ct.c_double,ct.c_double,ct.c_uint,
                          ct.c_double, ct.c_uint, ct.c_uint,
                          ct.c_uint]
def sweepChua   (  y_initial = np.asarray([0.001,0.,0.]),
                    dt=0.1,
                    N=30000,
                    stride = 1,
                    xmin = 0.8, xmax = 1.1,
                    ymin = 0, ymax = 15,
                    kneadingsEnd = 19,
                    sweepSize = 100
                ):
    kneadingsWeightedSumSet = np.zeros(sweepSize*sweepSize, float)
    lib.sweepChua(y_initial.ctypes.data_as(ct.POINTER(ct.c_double)), kneadingsWeightedSumSet.ctypes.data_as(ct.POINTER(ct.c_double)),
                  ct.c_double(xmin), ct.c_double(xmax), ct.c_uint(sweepSize),
                  ct.c_double(ymin), ct.c_double(ymax), ct.c_uint(sweepSize),
                  ct.c_double(dt), ct.c_uint(N), ct.c_uint(stride),
                  ct.c_uint(kneadingsEnd))
    return np.reshape(kneadingsWeightedSumSet, (sweepSize,sweepSize), 'F')

colorMapLevels = 256
blue=np.linspace(0.01,1,colorMapLevels)
red = 1 - blue
green = np.random.random(colorMapLevels)
RGB = np.column_stack((red, green, blue))
customColorMap = colors.ListedColormap(RGB)

# resolution not good with PdfPages writing to a single file

sweepSize = 5000
xmin = 0.8; xmax = 1.0; ymin = 0; ymax = 300;
figSize = 5
fig = figure(figsize=(figSize,figSize))
outputdir = 'Output/chuaSweepLeft_'

matplotlib.rcParams['savefig.dpi'] = sweepSize

fig.clear()
imgplot = imshow(sweepChua(kneadingsEnd=20, sweepSize=sweepSize, xmin=xmin, xmax = xmax, ymin=ymin, ymax = ymax,
                            dt=0.01),
                 extent=[xmin, xmax, ymin, ymax], aspect=(xmax - xmin) / (ymax-ymin),
                 cmap=customColorMap)
colorbar()
fig.suptitle('Kneading Range : 0-20', fontsize=2*figSize)
fig.savefig(outputdir+'0-20.pdf')
#show()
