#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include <sys/time.h>

#define N_EQ1	3

#pragma acc routine seq
void stepper(const double* y, double* dydt, const double* params)
{
	double a=params[1]*cos(params[0])+1.8623;
	double b=params[1]*sin(params[0])+1.8743;
        
	dydt[0] = a*(y[1]+y[0]/6-y[0]*y[0]*y[0]/6);
	dydt[1] = y[0]-y[1]+y[2];
	dydt[2] = -b*y[1];	
}

#pragma acc routine seq
double computeFirstDerivative(const double *y){
	//bool firstDerivative = fabs(y[1]+y[0]/6-y[0]*y[0]*y[0]/6)<=1.0e-5;
	return y[1]+y[0]/6-y[0]*y[0]*y[0]/6;
}

#pragma acc routine seq
double integrator_rk4(double* y_current, const double* params, const double dt, const unsigned N, const unsigned stride,
                                            const unsigned kneadingsEnd)
{
	unsigned i, j, k, kneadingIndex=0;
	double dt2, dt6;
	double y1[N_EQ1], y2[N_EQ1], k1[N_EQ1], k2[N_EQ1], k3[N_EQ1], k4[N_EQ1];
	double firstDerivativeCurrent=0,firstDerivativePrevious=0;
	double kneadingsWeightedSum=0;

	dt2 = dt/2.; dt6 = dt/6.;

	for(i=1; i<N; i++)
	{
		for(j=0; j<stride; j++)
		{
			stepper(y_current, k1, params);
			for(k=0; k<N_EQ1; k++) y1[k] = y_current[k]+k1[k]*dt2;
			stepper(y1, k2, params);
			for(k=0; k<N_EQ1; k++) y2[k] = y_current[k]+k2[k]*dt2;
			stepper(y2, k3, params);
			for(k=0; k<N_EQ1; k++) y2[k] = y_current[k]+k3[k]*dt;
			stepper(y2, k4, params);
			for(k=0; k<N_EQ1; k++) y_current[k] += dt6*(k1[k]+2.*(k2[k]+k3[k])+k4[k]);
		}

		firstDerivativeCurrent=computeFirstDerivative(y_current);
		if(y_current[0]>1 && firstDerivativePrevious>0 && firstDerivativeCurrent<0 ){
            //printf("1 ");
            if(kneadingIndex>0)
				return kneadingsWeightedSum;
            kneadingIndex++;
		}
		if(y_current[0]<-1 && firstDerivativePrevious<0 && firstDerivativeCurrent>0){
		    //printf("0 ");
            kneadingsWeightedSum += 1./(kneadingsEnd-1);
            if(kneadingIndex==kneadingsEnd)
                return kneadingsWeightedSum;
			kneadingIndex++;
		}
		firstDerivativePrevious = firstDerivativeCurrent;
	}
}

void sweepChua(double* y, double* kneadingsWeightedSumSet,
                                        double alphaStart, double alphaEnd, unsigned alphaCount,
										double LStart, double LEnd, unsigned LCount,
										double dt, unsigned N, unsigned stride,
										unsigned kneadingsEnd){
	double params[2];
	double alphaStep = (alphaEnd - alphaStart)/(alphaCount-1);
	double LStep = (LEnd-LStart)/(LCount -1);
	int i,j,k;
	double y_initial[N_EQ1];
	struct timeval start_time, stop_time, elapsed_time;

	printf("Computing Chua Left sweep.\n");
    gettimeofday(&start_time,NULL);

	#pragma acc parallel loop independent private(y_initial[0:N_EQ1],params[0:2]) ,\
					copyout(kneadingsWeightedSumSet[0:alphaCount*LCount]),\
					pcopyin(y[0:N_EQ1])
	for(i=0;i<alphaCount; i++){
		for(j=0;j<LCount; j++){
		    //Copy the given initial condition
            for(k=0;k<N_EQ1;k++) {
                y_initial[k]=y[k];
            }
			params[0]=alphaStart+i*alphaStep;
			params[1]=LStart+j*LStep;
			kneadingsWeightedSumSet[i*LCount+j] = integrator_rk4(y_initial, params, dt, N, stride,
			                                                            kneadingsEnd);
		}	
	}

	gettimeofday(&stop_time,NULL);
    timersub(&stop_time, &start_time, &elapsed_time); // Unix time subtract routine
    printf("Total time was %f seconds.\n", elapsed_time.tv_sec+elapsed_time.tv_usec/1000000.0);
}

int main(){
	double y_initial[]={0.001,0.,0.};
	double dt=0.1; 
	unsigned N=5000;
	unsigned stride = 1;
	unsigned maxKneadings = 20;
	unsigned sweepSize = 5;
	double *kneadingsWeightedSumSet = (double *)malloc(sweepSize*sweepSize*sizeof(double));
    int i,j;
	//printf("Enter sweep size :");
	//scanf("%d",&sweepSize);
	sweepChua(y_initial, kneadingsWeightedSumSet, 0.8, 1., sweepSize, 0, 300, sweepSize, dt, N, stride, 20);

	for(i=0;i<sweepSize;i++){
	    for(j=0;j<sweepSize;j++){
	        printf("%f \n",kneadingsWeightedSumSet[i*sweepSize+j]);
	    }
	}
}
