import ctypes as ct
from pylab import *
import numpy as np
from matplotlib import colors
import matplotlib.pyplot as plt
from scipy import ndimage as ndi
from skimage.filters import roberts, sobel, scharr
from skimage import feature
from skimage import measure
from matplotlib.backends.backend_pdf import PdfPages

lib = ct.cdll.LoadLibrary('./chuaSweep.so')

lib.sweepChua.argtypes = [ct.POINTER(ct.c_double), ct.POINTER(ct.c_double),
                          ct.c_double,ct.c_double,ct.c_uint,
                          ct.c_double,ct.c_double,ct.c_uint,
                          ct.c_double, ct.c_uint, ct.c_uint,
                          ct.c_uint, ct.c_uint]
def sweepChua   (  y_initial = np.asarray([0.001,0.,0.]),
                    dt=0.1,
                    N=30000,
                    stride = 1,
                    xmin = 0.8, xmax = 1.1,
                    ymin = 0, ymax = 15,
                    kneadingsStart = 0,
                    kneadingsEnd = 19,
                    sweepSize = 100
                ):
    kneadingsWeightedSumSet = np.zeros(sweepSize*sweepSize, float)
    lib.sweepChua(y_initial.ctypes.data_as(ct.POINTER(ct.c_double)), kneadingsWeightedSumSet.ctypes.data_as(ct.POINTER(ct.c_double)),
                  ct.c_double(xmin), ct.c_double(xmax), ct.c_uint(sweepSize),
                  ct.c_double(ymin), ct.c_double(ymax), ct.c_uint(sweepSize),
                  ct.c_double(dt), ct.c_uint(N), ct.c_uint(stride),
                  ct.c_uint(kneadingsStart), ct.c_uint(kneadingsEnd))
    return np.reshape(kneadingsWeightedSumSet, (sweepSize,sweepSize), 'F')

colorMapLevels = 32
blue=np.linspace(0.01,1,colorMapLevels)
red = 1 - blue
green = np.random.random(colorMapLevels)
RGB = np.column_stack((red, green, blue))
customColorMap = colors.ListedColormap(RGB)

# resolution not good with PdfPages writing to a single file

sweepSize = 5000
xmin = 0.8; xmax = 1.1; ymin = 0.; ymax = 15.;
figSize = 5
fig = figure(figsize=(figSize,figSize))
outputdir = 'Output/chuaSweep_'

matplotlib.rcParams['savefig.dpi'] = sweepSize


#http://matplotlib.org/examples/pylab_examples/contour_demo.html
#http://matplotlib.org/examples/pylab_examples/contourf_demo.html
#http://scikit-image.org/docs/dev/user_guide.html

fig.clear()
sweepResult = sweepChua(kneadingsStart=2, kneadingsEnd=21, sweepSize=sweepSize)
print 'finished computing sweep'
dx = (xmax - xmin)/sweepSize
X = np.arange(xmin, xmax, dx)
dy = (ymax - ymin)/sweepSize
Y = np.arange(ymin, ymax, dy)

pp = PdfPages(outputdir+'print_justContours.pdf')

colorMapLevelsArray = [2,4,8,16,32,64]
for colorMapLevels in colorMapLevelsArray :
    print colorMapLevels
    levels = np.arange(0, 1, 1./colorMapLevels)
    CS = plt.contour(X, Y, sweepResult  , colors='k', origin='lower', extent=[xmin, xmax, ymin, ymax],
                     aspect=(xmax - xmin) / (ymax-ymin),
                     levels=levels, linewidths=0.1 )
    fig.suptitle('Kneading Range : 2-21, colorLevels : '+str(colorMapLevels), fontsize=2*figSize)
    pp.savefig(fig)
pp.close()

#plt.clabel(CS, inline=1, fontsize=0.)

#imgplot = plt.imshow(sweepResult, origin='lower',
#                extent=[xmin, xmax, ymin, ymax], aspect=(xmax - xmin) / (ymax-ymin),
#                 cmap='prism')
#colorbar()
#show()


#fig.savefig(outputdir+'2-21_contoursPrint2_colorkLevels-'+str(colorMapLevels)+'_sweepSize-'+str(sweepSize)+'.pdf')
#fig.savefig(outputdir+'print0.05-prism.pdf')