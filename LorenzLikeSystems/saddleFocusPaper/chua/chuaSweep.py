import matplotlib
matplotlib.use('Agg')
import ctypes as ct
from pylab import *
import numpy as np
from matplotlib import colors
from numpy.ma import masked_array
import pickle
import gzip

lib1 = ct.cdll.LoadLibrary('./chuaSweep.so')
lib2 = ct.cdll.LoadLibrary('./chuaLongKneadings.so')

lib1.sweepChua.argtypes = [ct.POINTER(ct.c_double),
                          ct.c_double,ct.c_double,ct.c_uint,
                          ct.c_double,ct.c_double,ct.c_uint,
                          ct.c_double, ct.c_uint, ct.c_uint,
                          ct.c_uint, ct.c_uint]
def sweepChua (     dt=0.1,
                    N=30000,
                    stride = 1,
                    xmin = 0.8, xmax = 1.1,
                    ymin = 0, ymax = 15,
                    kneadingsStart = 0,
                    kneadingsEnd = 19,
                    sweepSize = 100
                ):
    kneadingsWeightedSumSet = np.zeros(sweepSize*sweepSize, float)
    lib1.sweepChua(kneadingsWeightedSumSet.ctypes.data_as(ct.POINTER(ct.c_double)),
                  ct.c_double(xmin), ct.c_double(xmax), ct.c_uint(sweepSize),
                  ct.c_double(ymin), ct.c_double(ymax), ct.c_uint(sweepSize),
                  ct.c_double(dt), ct.c_uint(N), ct.c_uint(stride),
                  ct.c_uint(kneadingsStart), ct.c_uint(kneadingsEnd))
    return np.reshape(kneadingsWeightedSumSet, (sweepSize,sweepSize), 'F')

lib2.sweepChua.argtypes = [ct.POINTER(ct.c_double),
                          ct.c_double,ct.c_double,ct.c_uint,
                          ct.c_double,ct.c_double,ct.c_uint,
                          ct.c_double, ct.c_uint, ct.c_uint,
                          ct.c_uint, ct.c_uint]
def sweepChuaLongKneadings (     dt=0.1,
                    N=30000,
                    stride = 1,
                    xmin = 0.8, xmax = 1.1,
                    ymin = 0, ymax = 15,
                    kneadingsStart = 0,
                    kneadingsEnd = 19,
                    sweepSize = 100
                ):
    kneadingsWeightedSumSet = np.zeros(sweepSize*sweepSize, float)
    lib2.sweepChua(kneadingsWeightedSumSet.ctypes.data_as(ct.POINTER(ct.c_double)),
                  ct.c_double(xmin), ct.c_double(xmax), ct.c_uint(sweepSize),
                  ct.c_double(ymin), ct.c_double(ymax), ct.c_uint(sweepSize),
                  ct.c_double(dt), ct.c_uint(N), ct.c_uint(stride),
                  ct.c_uint(kneadingsStart), ct.c_uint(kneadingsEnd))
    return np.reshape(kneadingsWeightedSumSet, (sweepSize,sweepSize), 'F')

colorMapLevels=2**8
blue = np.linspace(0.01, 1, colorMapLevels)
red = 1 - blue
min = 0.; max = .8
green = min + np.random.random(colorMapLevels) * (max - min)
RGB = np.column_stack((red, green, blue))
customColorMap = colors.ListedColormap(RGB)

colorMapLevels=2**8
blue = np.linspace(0.8, 0, colorMapLevels)
red = blue
green = blue
RGB = np.column_stack((red, green, blue))
customGrayColorMap = colors.ListedColormap(RGB)

sweepSize = 1000
figSize = 10
fig = figure(figsize=(figSize,figSize))

outputdir1 = 'Output/chuaSweep_'

matplotlib.rcParams['savefig.dpi'] = 2*sweepSize/figSize


dataSaved = False

"""
#TransientSweeps
kneadingRangesToCompute=[[2,3], [2,4], [2,5], [2,6], [2,21]];

for kneadingRange in kneadingRangesToCompute :
    xmin = 0.8;
    xmax = 1.1;
    ymin = 0;
    ymax = 15;
    fig.clear()
    kneadingRangeStr =  "%d-%d" % (kneadingRange[0], kneadingRange[1]);
    if not dataSaved:
        pickleDataFile1 = gzip.open(outputdir1 + kneadingRangeStr + '.pickle.gz', 'wb')
        sweepData = sweepChua(kneadingsStart=kneadingRange[0], kneadingsEnd=kneadingRange[1], sweepSize=sweepSize, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax)
        pickle.dump(sweepData, pickleDataFile1)
    else:
        pickleDataFile1 = gzip.open(outputdir1 + kneadingRangeStr + '.pickle.gz', 'rb')
        sweepData = pickle.load(pickleDataFile1)

    imshow(sweepData,
           extent=[xmin, xmax, ymin, ymax], aspect=(xmax - xmin)/ (ymax-ymin),
           cmap=customColorMap,
           origin='lower')
    fig.suptitle("Kneading Range : " + kneadingRangeStr, fontsize=2*figSize)
    fig.savefig(outputdir1+kneadingRangeStr+".jpg")
"""

#LongRangeSweeps
kneadingRangesToCompute=[[800,999]];

for kneadingRange in kneadingRangesToCompute :
    xmin = 0.84;
    xmax = 1.05;
    ymin = 0;
    ymax = 15;
    fig.clear()
    kneadingRangeStr = "%d-%d" % (kneadingRange[0], kneadingRange[1]);
    if not dataSaved:
        pickleDataFile1 = gzip.open(outputdir1 + kneadingRangeStr +'_sweepSize-'+str(sweepSize)+'.pickle.gz', 'wb')
        sweepData = sweepChuaLongKneadings(kneadingsStart=kneadingRange[0], kneadingsEnd=kneadingRange[1], sweepSize=sweepSize, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax, N=30000*10)
        pickle.dump(sweepData, pickleDataFile1)
    else:
        pickleDataFile1 = gzip.open(outputdir1 + kneadingRangeStr +'_sweepSize-'+str(sweepSize)+ '.pickle.gz', 'rb')
        sweepData = pickle.load(pickleDataFile1)

    #sweepDataUncomputed = masked_array(sweepData, sweepData >-1.1) # -1.1 N insufficient; -1.2 Infinity
    sweepDataInsufficientN = masked_array(sweepData, sweepData != -1.1)

    sweepDataComputed = masked_array(sweepData, sweepData == -1.1)
    sweepDataChaotic = masked_array(sweepDataComputed, sweepData <= 0)
    sweepDataPeriodic = masked_array(sweepDataComputed, sweepData > 0)

    imshow(sweepDataPeriodic, extent=[xmin, xmax, ymin, ymax], aspect=(xmax - xmin)/ (ymax-ymin), cmap=customColorMap, origin='lower')
    imshow(sweepDataChaotic, extent=[xmin, xmax, ymin, ymax], aspect= (xmax - xmin) / (ymax - ymin), cmap=customGrayColorMap, origin='lower')

    imshow(sweepDataInsufficientN, extent=[xmin, xmax, ymin, ymax], aspect=(xmax - xmin) / (ymax - ymin), cmap=colors.ListedColormap(['white']), origin='lower')
    fig.suptitle("Kneading Range : " + kneadingRangeStr, fontsize=2 * figSize)
    fig.savefig(outputdir1+kneadingRangeStr+'_sweepSize-'+str(sweepSize)+".jpg")
    #show()

